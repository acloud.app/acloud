<?php
if (empty($_SESSION['admin']['adminId'])) {
    $this->redirect(APPLICATION_URL . 'admin/profile/login');
    die();
}
$this->addJS('library.js', false, 'library');
?>
<script>
    $(document).ready(function () {
        $("#sortable").sortable({
            update: function (event, ui) {
                //create the array that hold the positions...
                var order = [];
                //loop trought each li...
                $("#sortable li").each(function (e) {

                    //add each li position to the array...
                    // the +1 is for make it start from 1 instead of 0
                    order.push($(this).attr("id"));
                });
                // join the array as single variable...
                var positions = order.join('-');
                //use the variable as you need!
                $("#track_display_option").val(positions);
                // $.cookie( LI_POSITION , positions , { expires: 10 });
            }
        });
        $("#sortable").disableSelection();
    });
</script>
<div id="main-content">
    <div id="title-div">
        <div id="title-div-left">
            <h1><?php echo $this->lang['__SETTINGS_ADMIN_TITLE__'] ?></h1>
        </div>
        <div id="title-div-right">
            <span class="header2"><?php echo date("M"); ?></span>
            <span class="header1"><?php echo date("d"); ?></span>
        </div>
    </div>
<?php
    $settings = new settings();

    echo '<div class="settingOuterWrapper" style="margin-bottom: 100px;">
    <form action="' . ADMIN_URL . 'settings/web-settings/update" method="post">';
    echo '<div class="settingWrapper">';
    echo '<div class="settingDiv right-border-dotted">';
    $websiteSettings = $settings->getSettingDetails("website");
    echo "\n\n<table>\n";
    if (is_array($websiteSettings)) {
        foreach ($websiteSettings as $key => $value) {
            if ($key == "HOME_PAGE") {
                //set library link as an option
                $options = array('library');
                // get list of pages from database
                $db = new db();
                $db->fetchRecord(TBL_CMS . " as p", 'p.cmsPage', array('status' => ACTIVE_VALUE, 'type' => 'page'), " AND ");
                if (is_array($db->result)) {
                    foreach ($db->result as $rec) {
                        $options[] = 'information/content/page/' . $rec['cmsPage'];
                    }
                }
                //create select list with home page options
                $select = '<select name="' . $key . '">';
                foreach ($options as $opt) {
                    $selected = ($opt == trim($value['value'])) ? 'selected = "selected"' : '';
                    $select .= '<option value="' . $opt . '" ' . $selected . '>' . $opt . '</option>';
                }
                $select .= '</select>';
                echo "<tr><td class='text-property' width='160' valign='top'>" . str_replace("_", " ", $key) . ": </td><td>" . $select . "<br><span class='help'>[" . $value['help'] . "]</span></td></tr>\n";
            } elseif (trim($value['value']) != "true" && trim($value['value']) != "false") {
                if ($key == "DEFAULT_TRACK_NAME") {
                    $display_option_array = explode("-", trim($value['value']));

                    $show_title_checked = (in_array("title", $display_option_array)) ? 'checked="checked"' : '';
                    $show_album_checked = (in_array("album", $display_option_array)) ? 'checked="checked"' : '';
                    $show_artist_checked = (in_array("artist", $display_option_array)) ? 'checked="checked"' : '';

                    echo '<tr><td class="text-property" width="160" valign="top">' . str_replace("_", " ", $key) . ': </td><td><input type="text" class="inputFld" size="35" name ="' . $key . '" id="track_display_option" value="' . $value['value'] . '"> <br>
							<input type="checkbox" name="show_title" id="show_title" value="1" ' . $show_title_checked . ' onClick="add_to_sort(this.id,\'title\');"/> <label for="show_title">Show Title</label>
							<input type="checkbox" name="show_album" id="show_album" value="1" ' . $show_album_checked . ' onClick="add_to_sort(this.id,\'album\');" /> <label for="show_album">Show Album</label>
							<input type="checkbox" name="show_artist" id="show_artist" value="1" ' . $show_artist_checked . ' onClick="add_to_sort(this.id,\'artist\');"/> <label for="show_artist">Show Artist</label>
							';

                    echo '<ul id="sortable">';
                    if (is_array($display_option_array)) {
                        foreach ($display_option_array as $option) {
                            echo '<li id="' . $option . '">' . $option . '</li>';
                        }
                    }
                    echo '</ul>';

                    echo "</td></tr>\n";
                } elseif ($value['type'] == "checkbox") {
                    $checked_yes = '';
                    $checked_no = '';
                    if ($value['value'] == 'Yes')
                        $checked_yes = ' checked';
                    else
                        $checked_yes = '';

                    echo "<tr><td class='text-property' width='160' valign='top'>" . str_replace("_", " ", $key) . ": </td><td>
							<input type='hidden' class='inputFld' size='35' id='" . $key . "' name ='" . $key . "' value='" . $value['value'] . "'  >
							<input type='checkbox' name='checkbox_" . $key . "' id='checkbox_" . $key . "' value='" . $value['value'] . "' " . $checked_yes . " onClick='add_to_commint(this.id);'/>

						</td></tr>\n";

                } else {
                    echo "<tr><td class='text-property' width='160' valign='top'>" . str_replace("_", " ", $key) . ": </td><td><input type='text' class='inputFld' size='35' name ='" . $key . "' value='" . $value['value'] . "'> <br><span class='help'>[" . $value['help'] . "]</span></td></tr>\n";
                }
            }

        }
    }
?>

                    </table>
                    <br>
                    <button class="frmButtonGreen" type="submit" >Update Settings</button>
                    <hr class="hrlin">
                    <br><br>
                    <h2>Global library operations:</h2>
                    <button type="button" onClick="allToPublic()" class="frmButtonGreen">Make all public</button>
                    <a href='<?php echo MODULE_URL . 'library/ajax/scanFolders.php'; ?>' target="_blank" style="text-decoration: none;" class="frmButtonGreen">Scan Folders</a>
                    <!-- <input type=button onClick="location.href='../'" -->
                </div>
            </div>
        </form>
    </div>
</div>