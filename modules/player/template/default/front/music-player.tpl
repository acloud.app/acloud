<?php
session_start();
$this->addJS ( "jquery.jplayer.js", false, "player" );
$this->addJS ( "add-on/jplayer.playlist.js", false, "player" );
$this->addCSS ( "player.css", false, "player" );
$this->addCSS ( "skin.css", false, "player" );
?>

<script>

    var myPlaylist, Player = $("#jquery_jplayer_1"),
            shareDialog, friendlyNameInput, shareLink, shareButton, shareEmail, shareResponse, playlist;

    $(document).ready(function(){

        myPlaylist = new jPlayerPlaylist(
            { }, // Default elements
            [ ], // Playlist
            {
                playlistOptions: {
                    loopOnPrevious: true,
                    shuffleOnLoop: false,
                    enableRemoveControls: true,
                    displayTime: 0,
                    addTime: 0,
                    removeTime: 0
                },
                swfPath: "<?php echo MODULE_URL.'player/swf'?>",
                supplied: "mp3",
                loop: true,
                smoothPlayBar: true,
                keyEnabled: true,
                wmode: "window",
                timeFormat: {
                    showHour: true
                }
            }
        );

        shareDialog = $('.playlist-share-dialog');
        friendlyNameInput = $('.share-name');
        shareLink = $('.playlist-link');
        shareButton = $('.playlist-share-button');
        shareEmail = $('.share-email');
        shareResponse = $('.share-response');

        $('.jp-playlist').on('click', '.jp-playlist-item-remove', function () {
            var id = $(this).data('id') + ';', // Get id of current track (plus ';' at the end of id)
                    cookie = $.cookie('folder'); // It meant that cookie != null
            cookie = cookie.replace(new RegExp(id, "g"), ""); //TODO: issue #80
            $.cookie('folder', cookie, {expires: 365, path: '/'}); // Remove song from cookie
            $.removeCookie('playlist', {path: '/'}); // Completely remove playlist as it's changed
            if (myPlaylist.playlist.length < 1) // If this was the last song...
                shareButton.hide(); // Hide share playlist button
            cancelShare(); // Close share dialog
        });

        shareLink.on('click focus', function () {
            this.setSelectionRange(0, this.value.length);
        });

        $('#playlist-share').on('submit', function (e) {
            e.preventDefault();
            var link = shareLink.val(),
                    friendlyName = friendlyNameInput.val(),
                    email = shareEmail.val();
            if (link == '')
                shareLink.focus();
            else if (friendlyName == '')
                friendlyNameInput.focus();
            else if (email == '')
                shareEmail.focus();
            else {
                var request = "link=" + encodeURI(link)
                        + "&friendlyName=" + encodeURI(friendlyName)
                        + "&email=" + encodeURI(email);
                $.ajax({
                    url: APPLICATION_URL + "modules/library/ajax/emailPlaylist.php?" + request,
                    dataType: 'json',
                    success: function (data) {
                        shareResponse.text(data.answer);
                    }
                });
            }
        });

    });

    function playTrack(id) {
        clearPlaylist();
        addToPlaylist(id, true, true);
    }

    function addTrackToPlaylist(track) {
        if (shareButton.css('display') == 'none')
            shareButton.show();
        cancelShare();
        myPlaylist.add({
            title:      track.displayName,
            trackId:    track.trackId,
            mp3:        track.file,
            poster:     track.image
        });
    }

    function addSavedPlaylist(ids){
        var url = APPLICATION_URL+"modules/library/ajax/playlistTrackDetailsJSON.php?id="+ids;
        $.ajax({
            url: url,
            dataType: "json",
            success: function( data ) {
                jQuery.each(data.matches, function(i, item) {
                    addTrackToPlaylist(item);
                });
            }
        });
    }

    function addToPlaylist(id, save, startPlaying) {
        showPlaylist();
        if ($.cookie('playlist') != null)
            $.removeCookie('playlist', {path: '/'});
        var url = APPLICATION_URL + "modules/library/ajax/trackDetailsJSON.php?id=" + id;
        $.ajax({
            url: url,
            dataType: "json",
            success: function( data ) {
                var current = myPlaylist.current,
                    playlist = myPlaylist.playlist,
                    currentTrackId = 0,
                    item = data.matches[0];
                jQuery.each(playlist, function (index, obj) {
                    if(index == current) currentTrackId = obj.trackId;
                });
                addTrackToPlaylist(item);
                if(startPlaying)
                    myPlaylist.play();
                if(save) {
                    var oldCookieValue = $.cookie('folder') ? $.cookie('folder') : '';
                    $.cookie("folder", oldCookieValue + id + ';', {expires: 365, path: '/'});
                }
                $(".jp-time-holder").css("display","inline-block");
                $.jPlayer.timeFormat.padMin = false; // if true, zero pads the minute if less than 10
                $(".jp-time-holder").show();
            }
        });
    }

    function addFolderToPlaylist(id, startPlaying) {
        showPlaylist();
        if ($.cookie('playlist') != null)
            $.removeCookie('playlist', {path: '/'});
        var url = APPLICATION_URL + "modules/library/ajax/folderTrackDetailsJSON.php?id=" + id;
        $.ajax({
            url: url,
            dataType: "json",
            success: function( data ) {
                jQuery.each(data.matches, function(i, item) {
                    var oldCookieValue = $.cookie('folder') ? $.cookie('folder') : '';
                    $.cookie("folder", oldCookieValue + item.trackId + ';', {expires: 365, path: '/'});
                    addTrackToPlaylist(item);
                });
                if(startPlaying)
                    myPlaylist.play(0);
            }
        });
    }

    function playFolderPlaylist(id) {
        clearPlaylist();
        addFolderToPlaylist(id, true);
    }

    function showPlaylist() {
        if($('#playList').css('display') == 'none'){
            $('#playList').toggle();
            playListShown = true;
        }
    }

    function clearPlaylist(){
        Player.jPlayer("stop");
        myPlaylist.remove();
        $("#playlist-ul").html("");
        $("#playlist-current-track-title").html('');
        $("#playlist-current-track-artist").html('');
        cancelShare();
        shareButton.hide();
        $.removeCookie("folder", {path: '/'});
        $.removeCookie('playlist', {path: '/'});
    }

    function openSharePlaylistDialog() {
        if (shareDialog.css('display') == 'none') {
            shareResponse.text("");
            if ($.cookie('playlist') == null) {
                playlist = $.cookie("folder");
                var pathArray = myPlaylist.playlist[0].mp3.split('/'),
                        folderName = pathArray[pathArray.length - 2],
                        friendlyName = transliterate(folderName),
                        request = "trackId=" + playlist + "&friendlyName=" + encodeURI(friendlyName);
                friendlyNameInput.val(friendlyName);
                $.ajax({
                    url: APPLICATION_URL + "modules/library/ajax/generatePlaylist.php?" + request,
                    dataType: 'json',
                    success: function (data) {
                        setupShareConfigurations(data);
                    }
                });
            }
            else {
                var playlistData = JSON.parse($.cookie('playlist'));
                shareLink.val(
                        window.location.protocol + "//" + window.location.hostname + "/playlist/"
                        + playlistData.playlist_id + '/'
                        + playlistData.friendly_name
                );
                friendlyNameInput.val(playlistData.friendly_name);
            }
            shareLink.focus();
        }
        shareDialog.toggle();
    }

    function updateShare() {
        var friendlyName = friendlyNameInput.val(),
                playlistData = JSON.parse($.cookie('playlist'));
        friendlyName = transliterate(friendlyName);
        friendlyNameInput.val(friendlyName);
        if(friendlyName != playlistData.friendly_name){
            var request = "playlist_id=" + playlistData.playlist_id + "&friendlyName=" + encodeURI(friendlyName);
            $.ajax({
                url: APPLICATION_URL + "modules/library/ajax/updatePlaylist.php?" + request,
                dataType: 'json',
                success: function (data) {
                    setupShareConfigurations(data);
                    shareResponse.text("Название обновлено.");
                }
            });
        }
    }

    function setupShareConfigurations(data) {
        shareLink.val(
                window.location.protocol + "//" + window.location.hostname + "/playlist/"
                + data.playlist_id + '/'
                + data.friendly_name
        );
        shareLink.focus();
        $.cookie(
                'playlist',
                JSON.stringify({
                    playlist_id: data.playlist_id,
                    friendly_name: data.friendly_name
                }),
                {
                    expires: 365, // Expires in one year
                    path: '/'
                }
        );
    }

    function loadPlaylistDetails(data) {
        setupShareConfigurations(data);
        $.removeCookie('folder', {path:'/'});
        var request = "playlist_id=" + data.playlist_id + "&friendly_name=" + data.friendly_name;
        $.ajax({
            url: APPLICATION_URL + "modules/library/ajax/getPlaylistDetails.php?" + request,
            dataType: 'json',
            success: function (data) {
                if (data.ids != null) {
                    addSavedPlaylist(data.ids);
                    $.cookie(
                            'folder',
                            data.ids.replace(/_/g, ';'),
                            {
                                expires: 365,
                                path: '/'
                            }
                    );
                    window.location = APPLICATION_URL + 'library'; // Change location only after playlist saved to cookies
                }
                else
                    $.removeCookie('playlist', {path:'/'});
            }
        });
    }

    function cancelShare() {
        shareLink.val('Загрузка...');
        shareDialog.hide();
    }

    function checkCookieForTracks() {
        if ($.cookie('folder')) {
            var savedTracks = $.cookie('folder'),
                    trackIdsArray = savedTracks.split(';'),
                    ids = "";
            for (var i = 0; i < trackIdsArray.length; ++i)
                if (trackIdsArray[i] > 0) // Make sure current id is possible id
                    ids += trackIdsArray[i] + '_';
            addSavedPlaylist(ids);
        }
    }

    function transliterate(word) {
        word = decodeURIComponent(word);
        var characters = {
            'а':'a', 'б':'b', 'в':'v',
            'г':'g', 'д':'d', 'е':'e',
            'ё':'jo', 'ж':'zh', 'з':'z',
            'и':'i', 'й':'j', 'к':'k',
            'л':'l', 'м':'m', 'н':'n',
            'о':'o', 'п':'p', 'р':'r',
            'с':'s', 'т':'t', 'у':'u',
            'ф':'f', 'х':'h', 'ч':'ch',
            'ц':'c', 'ш':'sh', 'щ':'csh',
            'э':'e', 'ю':'ju', 'я':'ja',
            'ы':'y', 'ъ':'', 'ь':'',
            'А':'A', 'Б':'B', 'В':'V',
            'Г':'G', 'Д':'D', 'Е':'E',
            'Ё':'Jo', 'Ж':'Zh', 'З':'Z',
            'И':'I', 'Й':'J', 'К':'K',
            'Л':'L', 'М':'M', 'Н':'N',
            'О':'O', 'П':'P', 'Р':'R',
            'С':'S', 'Т':'T', 'У':'U',
            'Ф':'F', 'Х':'H', 'Ч':'CH',
            'Ц':'C', 'Ш':'Sh', 'Щ':'Csh',
            'Э':'E', 'Ю':'Ju', 'Я':'Ja',
            'Ы': 'Y', 'Ъ': '', 'Ь': '',
            'a': 'a', 'b': 'b', 'c': 'c',
            'd': 'd', 'e': 'e', 'f': 'f',
            'g': 'g', 'h': 'h', 'i': 'i',
            'j': 'j', 'k': 'k', 'l': 'l',
            'm': 'm', 'n': 'n', 'o': 'o',
            'p': 'p', 'q': 'q', 'r': 'r',
            's': 's', 't': 't', 'u': 'u',
            'v': 'v', 'w': 'w', 'x': 'x',
            'y': 'y', 'z': 'z',
            'A': 'A', 'B': 'B', 'C': 'C',
            'D': 'D', 'E': 'E', 'F': 'F',
            'G': 'G', 'H': 'H', 'I': 'I',
            'J': 'J', 'K': 'K', 'L': 'L',
            'M': 'M', 'N': 'N', 'O': 'O',
            'P': 'P', 'Q': 'Q', 'R': 'R',
            'S': 'S', 'T': 'T', 'U': 'U',
            'V': 'V', 'W': 'W', 'X': 'X',
            'Y': 'Y', 'Z': 'Z',
            '1': '1', '2': '2', '3': '3',
            '4': '4', '5': '5', '6': '6',
            '7': '7', '8': '8', '9': '9',
            '0': '0', ' ': '-', '-': '-',
            '_': '-'
        };
        return word
                .split('')
                .map(
                    function(char) {
                        return characters[char] || ''; // Char from array OR null
                    }
                )
                .join('');
    }
</script>

<div id="jquery_jplayer_1" class="jp-jplayer"></div>
<div id="jp_container_1" class="jp-audio">
    <div class="jp-type-playlist">
        <div class="jp-gui jp-interface">
            <div class="controls mute not-mobile">
                <button class="light-grey-style animated jp-mute"></button>
                <button class="light-grey-style animated jp-unmute"></button>
            </div>
            <div class="jp-volume-bar-container not-mobile">
                <div class="jp-volume-bar seeker animated">
                    <div class="jp-volume-bar-value light-grey-style animated"></div>
                </div>
            </div>
            <div class="jp-controls controls play">
                <button class="jp-previous light-grey-style animated"></button>
                <button class="jp-play light-grey-style animated"></button>
                <button class="jp-pause light-grey-style animated"></button>
                <button class="jp-next light-grey-style animated"></button>
            </div>
            <div class="jp-progress animated" id="progressSlider">
                <div class="jp-seek-bar animated">
                    <div class="jp-play-bar"></div>
                    <div class="jp-title track-title"></div>
                </div>
                <div class="jp-time-holder">
                    <span class="jp-current-time"></span>
                    <span class="jp-duration not-mobile"></span>
                </div>
            </div>
            <div class="controls repeat jp-toggles">
                <button class="jp-repeat light-grey-style animated"></button>
                <button class="jp-repeat-off light-grey-style animated"></button>
            </div>
        </div>

        <div class="jp-playlist" id="playList">
            <div id="titles">
                <button class="playlist-clear-list light-grey-style animated" onclick="clearPlaylist();">
                    Reset
                </button>
                <button class="playlist-share-button light-grey-style animated" onclick="openSharePlaylistDialog();">
                    Share
                </button>
                <div class="playlist-share-dialog">
                    <form id="playlist-share">
                        <div>Copy link:</div>
                        <input class="playlist-link" type="text" title="Link" readonly value="Loading...">
                        <div>
                            New playlist name:
                            <input class="share-name" type="text" title="Playlist name" maxlength="255">
                            <button class="update light-grey-style animated" onclick="updateShare(); return false;"
                                    title="Update playlist name"></button>
                        </div>
                        <div>Submit link to email:</div>
                        <input class="share-email" type="text" name="email" title="E-mail" placeholder="E-mail">
                        <button class="light-grey-style animated" type="submit">Send</button>
                        <button class="light-grey-style animated" type="reset" onclick="cancelShare()">Cancel</button>
                        <div class="share-response"></div>
                    </form>
                </div>
            </div>
            <ul id="playlist-ul" class="no-list-style"></ul>
        </div>
        <div class="jp-no-solution">
            <span>Update Required</span>
            To play the media you will need to either update your browser to a recent version
            or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
        </div>
    </div>
</div>
