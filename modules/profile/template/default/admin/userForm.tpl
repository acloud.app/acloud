<?php
/*
 * This page will render form to add/update website content
 */

//check for page editing
//query[3] will hold action
if(!empty($this->query[3])) {
	$frmTitle="__UPDATE_USER__";
	$action="update";
	$btnKey="__UPDATE__";
	$url=ADMIN_URL.$this->module.'/users/updateUser/'.$this->query[3];
} else {
	$frmTitle="__ADD_USER__";
	$action="add";
	$btnKey="__ADD__";
	$url=ADMIN_URL.$this->module.'/users/newUser';
}
?>
<div class="frmDiv" id="pane">
	<form method="post" action="<?php echo $url; ?>" enctype="multipart/form-data">
		<table border="0">
		<tr><td colspan="2"><h2><?php echo $this->lang[$frmTitle];	?></h2></td></tr>
			<tr>
				<td class="text-property"><?php echo $this->lang['__FIRST_NAME__'];?>:</td>
				<td><input type="text" name="f_name" size="40" class="inputFld" value="<?php echo substr($this->result['data']['name'],0, strpos($this->result['data']['name'], " "));?>" /></td>
			</tr>
			
			<tr>
				<td class="text-property"><?php echo $this->lang['__LAST_NAME__'];?>:</td>
				<td><input type="text" name="l_name" size="40" class="inputFld" value="<?php echo substr($this->result['data']['name'],strpos($this->result['data']['name'], " ")+1);?>" /></td>
			</tr>
			
			<tr>
				<td class="text-property"><?php echo $this->lang['__USERNAME__'];?>:</td>
				<td><input type="text" name="u_name" size="40" class="inputFld" value="<?php echo $this->result['data']['username'];?>" /></td>
			</tr>
			
			<tr>
				<td class="text-property"><?php echo $this->lang['__EMAIL__'];?>:</td>
				<td><input type="text" name="email" size="40" class="inputFld" value="<?php echo $this->result['data']['email'];?>" /></td>
			</tr>

			<tr>
				<td class="text-property"><?php echo $this->lang['__GROUP__'];?>:</td>
				<td>
                    <select name="groupId">
						<option value="admin" <?php if ( $this->result['data']['groupId'] == 'admin') echo 'selected';?> >Admin</option>
						<option value="author" <?php if ( $this->result['data']['groupId'] == 'author') echo 'selected';?> >Author</option>
					</select>
                </td>
			</tr>

			<tr>
				<td class="text-property"><?php echo $this->lang['__BASE_FOLDER__'];?>:</td>
				<td><input type="text" name="baseFolder" size="40" class="inputFld" value="<?php echo $this->result['data']['baseFolder'];?>" /></td>
			</tr>
			
			<tr>
				<td class="text-property"><?php echo $this->lang['__PASSWORD__'];?>:</td>
				<td><input type="password" name="password" size="40" class="inputFld" value="" /></td>
			</tr>
			
			<tr>
				<td class="text-property"><?php echo $this->lang['__CONFIRM_PASSWORD__'];?>:</td>
				<td><input type="password" name="confirm_password" size="40" class="inputFld" value="" /></td>
			</tr>
			
		
			<tr>
				<td></td>
				<td><button class="frmButtonGreen" type="submit"><?php echo $this->lang[$btnKey];?></button>
					<button class="frmButtonGreen" type="reset">Reset</button>
					<a href="<?php echo ADMIN_URL.$this->module.'/'.$this->page;?>">Cancel</a>
					<input type="hidden" name="action" id="action" value="<?php echo $action;?>" />
					</td>
			</tr>
		</table>
	</form>
	<script>
	<?php 
if(!empty($this->query[3]) && strpos($this->query[2],"edit")!==false){
echo '$("#pane").slideToggle(700);';
}
?>
	</script>
</div>