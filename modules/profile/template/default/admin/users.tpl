<?php
/*
 * This page will be used to manage users in the admin section
 */
if(empty($_SESSION['admin']['adminId'])) {
	$this->redirect(APPLICATION_URL.'admin/profile/login');
}
$profile = new profile();
$monthCon= "addDate >='".date("Y-m-d H;i:s",strtotime('-30 days'))."'";
$newUsers= $profile->totalUser('',$monthCon);
$totalUsers= $profile->totalUser();
$composerCon['composer']=ACTIVE_VALUE;
$newComposer = $profile->totalUser($composerCon,$monthCon);
$totalComposer = $profile->totalUser($composerCon);
?>
<div id="main-content">
<div id="title-div">
<div id="title-div-left">
<h1 ><?php echo $this->lang['__ADMIN_PAGE_TITLE__']?></h1></div> <!-- close title-div-left-->
<div id="title-div-right">
<span class="header2"><?php //echo date("M");?></span><span class="header1"><?php //echo date("d");?></span></div> <!-- close title-div-right-->
</div> <!-- close title-div-->

<div class="education-stats"><div class="education-stats-div border-right"><div  class="dashboard-right-number">
	<span class="whiteText font-size38 bold"><?php echo $newUsers;?></span></div><div class="dashboard-right-text">
	<span class="whiteText font-size16"><?php echo $this->lang['__USER_STAT_TITLE_1__']?></span></div></div><div class="education-stats-div border-right"><div  class="dashboard-right-number">
	<span class="whiteText font-size38 bold"><?php echo $totalUsers;?></span></div><div class="dashboard-right-text">
	<span class="whiteText font-size16"><?php echo $this->lang['__USER_STAT_TITLE_2__']?></span></div></div><div class="education-stats-div border-right"><div  class="dashboard-right-number">
	<span class="whiteText font-size38 bold"><?php echo $newComposer;?></span></div><div class="dashboard-right-text">
	<span class="whiteText font-size16"><?php echo $this->lang['__USER_STAT_TITLE_3__']?></span></div></div><div class="education-stats-div" ><div  class="dashboard-right-number">
	<span class="whiteText font-size38 bold"><?php echo $totalComposer;?></span></div><div class="dashboard-right-text">
	<span class="whiteText font-size16"><?php echo $this->lang['__USER_STAT_TITLE_4__']?></span></div></div>
</div>

<?php $this->addPage("userForm");?>
<div id="paneSelector">
<?php echo $this->lang['__ADD_ADMIN_USER__'];?></div>
<div class="related-buttons-div" style="margin-left:32px;">
</div>


	<div class="messageDiv">
<?php 
if(!empty($this->result['message'])){
	$this->renderMessage($this->result['message'], false);
} else if ($this->result['errorMessage']){
	$this->renderErrorMessage($this->result['errorMessage']);
}
?>
</div>

<?php 
//add database table
$this->addLoadDatabaseTableScript($this->module."_AdminUser","dbTableDiv",false,'','','','',true);
?>
	
</div> <!-- Main content closed -->