<?php
if (! empty ( $_SESSION ['admin'] ['userId'] )) {
	$this->redirect ( APPLICATION_URL . 'admin' );
}
?>
<div id="root-div">
	<div id="forgetPasswordDialog" title="Retrive Password">
		<form method="post"
			action="<?php echo APPLICATION_URL.'admin/profile/login/retrieveAdminLogin';?>"
			enctype="multipart/form-data">
			<table width="400" border="0">
				<tr>
					<td colspan="2">Please enter your email (one you used to register)
						to retrieve your password.</td>
				</tr>
				<tr>
					<td class="text-property">Your Email ID:</td>
					<td><input type="text" name="forgetPasswordEmail" size="25"
						class="inputFld" value="" />
						<button class="frmButtonGreen" type="submit">Proceed</button></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- close Retrive Password-->
	<div id="ribbon"></div>
	<!-- close ribbon-->
	<div id="login-container-bg">
		<div id="login-container">
			<div id="login-container-top">
				<div id="login-container-logo"></div>
				<!-- close login-container-logo-->
			</div>
			<!-- close login-container-top-->
			<div id="login-container-form">
				<form method="post"
					action="<?php echo APPLICATION_URL.'admin/profile/login/adminLogin';?>">
					<div class="table-row">
						<div class="formElement padding-left31">
							<input class="tb-size login-image1" type="text" name="username">
						</div>
					</div>
					<br>
					<div class="table-row">
						<div class="formElement padding-left31">
							<input class="tb-size login-image2" type="password"
								name="password">
						</div>
					</div>
					<br>
					<div class="float-right padding-right31 ">
						<input class="login-button" type="submit"
							value="<?php echo $this->lang['__LOGIN__']?>"><input
							class="login-button" type="button"
							value="<?php echo $this->lang['__FORGOT_PASSWORD__'];?>"
							id="dialogBtn">
					</div>
					
				</form>
			</div>
			<?php 
					if ($this->result['errorMessage']){
	$this->renderErrorMessage($this->result['errorMessage']);
}
?>
			<!-- close login-container-form-->
			<div id="loginMSG">
				<script>setTimeout(function(){
  $("#msgDiv").fadeOut("slow", function () {
  $("#msgDiv").attr("display","None");
      });
 
}, 2000);</script>
			</div>
			<!-- close loginMSG-->
		</div>
		<!-- close login-container-->
	</div>
	<!-- close login-container-bg-->
	<div id="bottom-shadow"></div>
	<!-- close bottom-shadow-->
</div>
<!-- close root-div-->