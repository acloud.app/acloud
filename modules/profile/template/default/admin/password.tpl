<?php
/*
 * This page will be used to manage users in the admin section
 */
if(empty($_SESSION['admin']['adminId'])) {
	$this->redirect(APPLICATION_URL.'admin/profile/login');
}
?>
<div id="main-content">
<div id="title-div">
<div id="title-div-left">
<h1 ><?php echo $this->lang['__CHANGE_PASSWORD__']?></h1></div> <!-- close title-div-left-->
<div id="title-div-right">
<span class="header2"><?php echo date("M");?></span><span class="header1"><?php echo date("d");?></span></div> <!-- close title-div-right-->
</div> <!-- close title-div-->


<?php $this->addPage("composerForm");?>



	<div class="messageDiv">
<?php 
if(!empty($this->result['message'])){
	$this->renderMessage($this->result['message'], false);
} else if ($this->result['errorMessage']){
	$this->renderErrorMessage($this->result['errorMessage']);
}
?>
</div>

<form method="post" action="<?php echo APPLICATION_URL."admin/profile/password/changeAdminPassword"; ?>" enctype="multipart/form-data">
		<table border="0">
			<tr>
				<td class="text-property"><?php echo $this->lang['__CURRENT_PASSWORD__'];?>:</td>
				<td><input type="password" name="current_password" size="40" class="inputFld" value="" /></td>
			</tr>
			
			<tr>
				<td class="text-property"><?php echo $this->lang['__NEW_PASSWORD__'];?>:</td>
				<td><input type="password" name="new_password" size="40" class="inputFld" value="" /></td>
			</tr>
			
			<tr>
				<td class="text-property"><?php echo $this->lang['__CONFIRM_PASSWORD__'];?>:</td>
				<td><input type="password" name="confirm_password" size="40" class="inputFld" value="" /></td>
			</tr>
			
			
		
			<tr>
				<td></td>
				<td><button class="frmButtonGreen" type="submit">Change Password</button>
					<a href="<?php echo ADMIN_URL?>">Cancel</a>
					<input type="hidden" name="action" id="action" value="<?php echo $action;?>" />
					</td>
			</tr>
		</table>
	</form>	
</div> <!-- Main content closed -->