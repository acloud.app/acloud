<?php
/*
 * $this will be object of html class
 * This script will redirect user to login page.
*/
if(empty($_SESSION['admin']['adminId'])) {
	$this->redirect(APPLICATION_URL.'admin/profile/login');
}
?>