<?php
class profile {
	public $result;
	private $general, $obj, $dbObj, $library;
	public function __construct() {
		$this->general = new general ();
		$this->library = new library();
	}
	private function setDBObj() {
		$this->dbObj = new db ();
	}
	private function setGeneral() {
		$this->general = new general ();
	}
	
	/**
	 * *************** ADMIN METHODS *******************
	 */
	public function adminLogin() {
		$data ['username'] = $_POST ['username'];
		$data ['password'] = $_POST ['password'];
		$obj = new auth ( TBL_ADMIN );
		$result = $obj->login ( $data, 1 );
		if ($result ['result'] == "success") {
			session_start();
			session_regenerate_id ();
			$_SESSION ['admin'] ['sessionId'] = session_id ();
			$_SESSION ['admin'] ['adminId'] = $result ['data'] ['adminId'];
			$_SESSION ['admin'] ['name'] = $result ['data'] ['name'];
			$_SESSION ['admin'] ['groupId'] = $result ['data'] ['groupId'];
			$_SESSION ['admin'] ['lastLogin'] = $this->general->convertDate ( $result ['data'] ['lastLogin'], 'date' ) . " at " . $this->general->convertDate ( $result ['data'] ['lastLogin'], 'time' );
			$_SESSION ['admin'] ['lastLoginRaw'] = $result ['data'] ['lastLogin'];
			$_SESSION ['admin'] ['logins'] = $result ['data'] ['logins'];
			$_SESSION ['admin'] ['baseFolder'] = $result ['data'] ['baseFolder'];
			$this->general->logToDB ('SESSION', 'NEW', 'REMOTE_ADDR='.$_SERVER['REMOTE_ADDR'].' HTTP_X_FORWARDED_FOR='.$_SERVER['HTTP_X_FORWARDED_FOR']);
			$this->general->redirect ( APPLICATION_URL . 'admin' );
		} else {
			$array['errorMessage']= $result['data']['error'];
			return $array;
		}
	}
	
	public function adminLogout() {
		session_start ();
		unset ( $_SESSION ['admin'] );
		return;
	}
	
	public function renderAdminUserTable($searchCon = "", $page = "1", $records = RECORDS_PER_PAGE, $order = "addDate desc,  userId asc", $condition = "") {
		$this->dbObj = new db ();
	
		// $this->totalRecords=$this->dbObj->getRows($this->table,$condition,"
		// status !=".TRASHED);
		if (! empty ( $page ) && ! empty ( $records )) {
			$start = (($page - 1) * $records);
		} else {
			$start = "";
		}
		$this->page = $page;
	
		if (! empty ( $records )) {
			$limit = $records;
			// $this->totalPages=ceil($this->totalRecords/$records);
		} else {
			$limit = "";
			$this->totalPages = 0;
		}
		if (! empty ( $searchCon )) {
			$searchArray = explode ( ",", $searchCon );
			$specCon = " (";
			foreach ( $searchArray as $searchKey ) {
				$specCon .= "username like '%" . $searchKey . "%' OR email like '%" . $searchKey . "%'  OR displayName like '%" . $searchKey . "%' OR ";
			}
			$specCon = rtrim ( $specCon, " OR " );
			$specCon .= ")";
				
			$specCon .= " AND status !=" . TRASHED;
		} else {
			$specCon = " status !=" . TRASHED;
		}
		$fields = "*";
		if ($this->dbObj->fetchRecord ( TBL_ADMIN . " as p",$fields, "", " AND ", $specCon, $order, $start, $limit )) {
			// echo $this->dbObj->query;
			$rawResult = $this->dbObj->result;
			if (is_array ( $rawResult )) {
				foreach ( $rawResult as $key => $val ) {
					$this->result [$key] ['id:adminId'] = $val ['adminId'];
					$this->result [$key] ['Username:username'] =  $val ['username'] .'<br><span class="">'.$this->general->convertDate ( $val ['lastLogin']).' ('.$val['logins'].' logins)</span>' ;
					$this->result [$key] ['Details:name'] = $val[ 'name' ] . " (" . $val[ 'email' ] . ")";
					$this->result [$key] ['composer'] = $val ['composer'];
					$this->result [$key] ['Base Folder:baseFolder'] = ($val['baseFolder']==0) ? '/' : $this->library->fetchParent($val[ 'baseFolder' ]);
					$this->result [$key] ['Role:groupId'] = $val[ 'groupId' ];
				}
			}
			// for total pages and total records
			$this->dbObj->fetchRecord ( TBL_ADMIN. " as p", $fields, "", " AND ", $specCon );
			$this->totalRecords = count ( $this->dbObj->result );
			$this->totalPages = ceil ( $this->totalRecords / $limit );
			return $this->result;
		} else {
			$this->message = "Unable to fetch Faq Page List";
			$this->debugMessage = "[" . $this->dbObj->errorCode . "]- " . $this->dbObj->error . ". Query: " . $this->dbObj->query;
			return false;
		}
	}	
	
	public function newUser() {
		$this->dbObj = new db ();
		$this->general = new general();
		if (! empty ( $_POST )) {
			$array ['data'] = $_POST;
			//check for existing user
			$con = '(username="'.$_POST ['u_name'].'" OR email="'.$_POST ['email'].'")';
			$count=$this->dbObj->getRows(TBL_ADMIN,'','AND',$con);
			if($count>0){
				$array ['errorMessage'] = "User with provided email and username already exists!";
				return $array;
			}
			if (! empty ( $_POST ['f_name'] ) && ! empty ( $_POST ['l_name'] ) && ! empty ( $_POST ['u_name'] ) && ! empty ( $_POST ['email']) && ! empty ( $_POST ['password']) && ! empty ( $_POST ['confirm_password'])) {
				// check for existing user

				if($_POST['password']!==$_POST['confirm_password']) {
					$array ['errorMessage'] = "Password not matching";
					return $array;
				}

				$data ['name'] = $_POST ['f_name'] . ' '. $_POST['l_name'];
				$data ['username'] = $_POST ['u_name'];
				$data ['email'] = $_POST ['email'];
				$data ['password'] = md5($_POST['password']);
				$data ['addDate'] = date("Y-m-d H:i:s");
				$data ['groupId'] = $_POST['groupId'];
				$data ['baseFolder'] = $_POST ['baseFolder'];
				$data ['emailVerification'] = 1;
				$data ['token'] = $this->general->randomCode(10);
				$data ['emailStatus'] = 1;
				$data ['tokenExpiryDate'] =  date("Y-m-d H:i:s", strtotime("30 days"));

				if ($this->dbObj->addRecord ( TBL_ADMIN, $data )) {
					$array ['message'] = $this->message = "You have successfully added new user !!! ";
				} else {
					$array ['errorMessage'] = $result ['data'] ['error'];
				}
			} else {
				$array ['errorMessage'] = "Please fill in all fields!";
			}
		}
		return $array;
	}

	public function updateUser($id) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		if (! empty ( $_POST )) {
			
			$data ['name'] = $_POST ['f_name'].' '. $_POST ['l_name'];
			$data ['username'] = $_POST ['u_name'];
			$data ['email'] = $_POST ['email'];
			$data ['groupId'] = $_POST ['groupId'];
			$data ['baseFolder'] = $_POST ['baseFolder'];
			$data ['updateDate'] = date ( "Y-m-d H:i:s" );
			if(!empty($_POST['password']) && $_POST['password']===$_POST['confirm_password']) {
				$data ['password'] = md5($_POST['password']);
			}
			$con ['adminId'] = $id;
			if ($this->dbObj->updateRecord ( TBL_ADMIN, $data, $con )) {
				$array ['data'] = $this->dbObj->result [0];
				$array ['message'] = "User record Updated Successfully!!!";
			} else {
				$array ['errorMessage'] = $this->message = $this->general->prepareMessage ( "There was some error in updating composer record. Please contact webmaster!", $this->dbObj->query );
			}
			return $array;
		}
	}
	
	public function editAdminUser($id) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$con ['adminId'] = $id;
		if ($this->dbObj->fetchRecord ( TBL_ADMIN, "*", $con )) {
			$array ['data'] = $this->dbObj->result [0];
		} else {
			$array ['errorMessage'] = $this->message = $this->general->prepareMessage ( "There was some error in managing column data. Please contact webmaster!", $this->dbObj->query );
		}
		return $array;
	}
	
	public function verifyAccount($token) {
		$this->dbObj = new db ();
		$condition ['token'] = $token;
		$newData ['emailVerification'] = ACTIVE_VALUE;
		if ($this->dbObj->updateRecord ( TBL_USER, $newData, $condition )) {
			$array ['message'] = $this->message = "Your email address has been successfully verified. You can now Log In to your account by using the details you used to Sign Up.";
		} else {
			$array ['message'] = "There was some error in your email verification. Please contact administrator at " . ADMIN_MAIL;
			$array ['errorMessage'] = $this->dbObj->query;
		}
		return $array;
	}
	
	/**
	 * ******************** COMMON *******************
	 */
	public function deactivateAdminUserRecord($recordId) {
		$this->dbObj = new db ();
		$data ['status'] = INACTIVE_VALUE;
		$data ['updateDate'] = date ( 'Y-m-d H:i:s' );
		if ($this->dbObj->updateRecord ( TBL_ADMIN, $data, '', ' AND ', 'adminId in (' . $recordId . ')' )) {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Successfully Deactivated.";
			return true;
		} else {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Technical Error in Deleting user record.";
		}
	}
	
	public function activateAdminUserRecord($recordId) {
		$this->dbObj = new db ();
		$data ['status'] = ACTIVE_VALUE;
		$data ['updateDate'] = date ( 'Y-m-d H:i:s' );
		if ($this->dbObj->updateRecord ( TBL_ADMIN, $data, '', ' AND ', 'adminId in (' . $recordId . ')' )) {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Successfully Deactivated.";
			return true;
		} else {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Technical Error in Deleting user record.";
		}
	}
	
	public function deleteAdminUserRecord($recordId) {
		$this->dbObj = new db ();
		$data ['status'] = TRASHED;
		$data ['updateDate'] = date ( 'Y-m-d H:i:s' );
		if ($this->dbObj->deleteRecord ( TBL_ADMIN,  '', ' AND ', 'adminId in (' . $recordId . ')' )) {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Successfully Deactivated.";
			return true;
		} else {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Technical Error in Deleting user record.";
		}
	}
	
	public function adminUserList() {
		$this->dbObj = new db ();
		if ($this->dbObj->fetchRecord ( TBL_USER, "userId, concat(firstName,' ',lastName) as name", "", " AND ", "status != " . TRASHED )) {
			$array = $this->dbObj->result;
		} else {
			$array ['errorMessage'] = $this->message = $this->general->prepareMessage ( "There was some error in adding Track. Please contact webmaster!", $this->dbObj->query );
		}
		return $array;
	}
	public function logout() {
		if (! empty ( $_SESSION ['user'] )) {
			unset ( $_SESSION ['user'] ); // session unset
			                              // session_destroy();
			$this->general->redirect ( APPLICATION_URL );
			return;
		}
	}
	
	/**
	 * ********************** FRONT METHODS *************************
	 */
    public function totalUser($con="",$specCon="") {
        $this->dbObj = new db();
        $specCon=!empty($specCon)?$specCon." AND status!=".TRASHED:"status!=".TRASHED;
        $count= $this->dbObj->getRows(TBL_ADMIN,$con, " AND ",$specCon);
        return $count;
    }

    public function sendForgotPasswordEmail($email){
		$this->dbObj = new db();
		$this->general = new general();
		$this->dbObj->fetchRecord(TBL_USER,"*",array('email'=>$email));
		$data = $this->dbObj->result[0];
		if(!empty($data['userId'])) {
		$obj = new message();
		$code=$this->general->randomCode(10);
		$message=$this->general->readFile(MODULE_PATH."profile/txt/forgotPassword.txt");
		$search = array('[USER_NAME]','[LOGO_PATH]', '[RESET_LINK]','[COMPANY_NAME]' );
		$replace = array($data ['firstName'].' '.$data['lastName'], COMMON_IMAGES_URL.'logo.png',APPLICATION_URL.'profile/reset-password/'.$code , COMPANY);
		$message = str_replace($search, $replace, $message);
		$condition["userId"]=$data['userId'];
		$newData['passToken']=$code;
		$this->dbObj->updateRecord(TBL_USER, $newData, $condition);
		if($obj->sendMail($email, "Password Reset Link on ".COMPANY, $message, ADMIN_MAIL)){
			$this->result= "Link has been sent to <b>".$email."</b>. Please check your email and click Link to reset password";
		} else {
			$this->result= "There was some technical error. Please try again later.";
		}
		} else {
			$this->result='Provided Email is not associated to any account. Please provide email id which you used during registration';
		}
		return $this->result;
	}	
	
	public function userLastLoginDate($userId){
		$this->dbObj = new db();
		$this->dbObj->fetchRecord(TBL_USER,'lastLogin',array('userId'=>$userId));
		return $this->dbObj->result[0]['lastLogin'];
	}
	
	public function removeUser($userId) {
		$this->dbObj = new db();
		$data['composer']=0;
		$this->dbObj->updateRecord(TBL_USER, $data, array('userId'=>$userId));
	}
	
	public function changeAdminPassword(){
		$currentPassword = $_POST['current_password'];
		$newPassword = $_POST['new_password'];
		$confirmPassword = $_POST['confirm_password'];
		if($newPassword!==$confirmPassword){
			$this->result['errorMessage']="Password Mismatch.";
			return $this->result;
		}
		
		$this->dbObj = new db();
		$this->dbObj->fetchRecord(TBL_ADMIN,"password",array('adminId'=>1));
		$passwordMD5=$this->dbObj->result[0]['password'];
		
		if(md5($currentPassword)===$passwordMD5){
			$data['password']=md5($newPassword);
			$this->dbObj->updateRecord(TBL_ADMIN,$data,array('adminId'=>1));
			$this->result['message']="Password Changed!";
			return $this->result;
		}
	}
	
}
