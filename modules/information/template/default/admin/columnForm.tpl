<?php
/*
 * This page will render form to add/update website content
 */
$this->addJS("editor/ckeditor.js",true);

//check for page editing
//query[3] will hold action
if(!empty($this->query[3])) {
	$frmTitle="__UPDATE_COLUMN_CONTENT__";
	$action="update";
	$btnKey="__UPDATE__";
	$url=ADMIN_URL.$this->module.'/column/updateCOLUMN/'.$this->query[3];
	if($this->result['data']['type']=="page") {
		$page="checked='checked'";
	} else {
		$content_block="checked='checked'";
	}
} 
?>
<div class="frmDiv" id="pane">
	<form method="post" action="<?php echo $url; ?>" enctype="multipart/form-data">
		<table border="0">
		<tr><td colspan="2"><h2><?php echo $this->lang[$frmTitle]?></h2></td></tr>
		
			
			
			
			<tr>
				<td class="text-property"><?php echo $this->lang['__COLUMN_TITLE__'];?>:</td>
				<td><input type="text" name="column_title" size="61"
					class="inputFld" value="<?php echo $this->result['data']['columnName'];?>" /></td>
			</tr>
			
			<tr>
				<td></td>
				<td><button class="frmButtonGreen" type="submit"><?php echo $this->lang[$btnKey];?></button>
					<button class="frmButtonGreen" type="reset">Reset</button>
					<a href="<?php echo ADMIN_URL.$this->module.'/'.$this->page;?>">Cancel</a>
					<input type="hidden" name="action" id="action" value="<?php echo $action;?>" />
					</td>
			</tr>
		</table>
	</form>
	<script>
	<?php 
if(!empty($this->query[3]) && strpos($this->query[2],"edit")!==false){
echo '$("#pane").slideToggle(700);';
}
?>
	</script>
</div>