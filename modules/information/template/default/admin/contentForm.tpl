<?php
/*
 * This page will render form to add/update website content
 */
$this->addJS("editor/ckeditor.js",true);

//check for page editing
//query[3] will hold action
if(!empty($this->query[3])) {
	$frmTitle="__CONTENT_PAGE_UPDATE_TEXT__";
	$action="update";
	$btnKey="__UPDATE__";
	$url=ADMIN_URL.$this->module.'/content/updateCMS/'.$this->query[3];
	if($this->result['data']['type']=="page") {
		$page="checked='checked'";
	} else {
		$content_block="checked='checked'";
	}
	
	if($this->result['data']['main_menu']=="1") {
		$main_menu_yes="checked='checked'";
	} else {
		$main_menu_no="checked='checked'";
	}
} else {
	$frmTitle="__CONTENT_PAGE_ADD_TEXT__";
	$action="add";
	$btnKey="__ADD__";
	$url=ADMIN_URL.$this->module.'/content/addCMS';
}
?>
<div class="frmDiv" id="pane">
	<form method="post" action="<?php echo $url; ?>" enctype="multipart/form-data">
		<table border="0">
		<tr><td colspan="2"><h2><?php echo $this->lang[$frmTitle]?></h2></td></tr>
				<input type="hidden" name="pageType" id="page" value="page" />
			<tr>
				<td class="text-property"><?php echo $this->lang['__MAIN_MENU_ITEM__'];?>:</td>
				<td><input type="radio" name="main_menu" id="main_menu_yes" value="1" <?php echo $main_menu_yes;?>/> <label for="main_menu_yes"><?php echo $this->lang['__YES__']?></label><br>
				<input type="radio" name="main_menu" id="main_menu_no" value="0" <?php echo $main_menu_no;?>/> <label for="main_menu_no"><?php echo $this->lang['__NO__']?></label>
				
				</td>
			</tr>
			
			
			<tr>
				<td class="text-property"><?php echo $this->lang['__CMS_PAGE__'];?>:</td>
				<td>
				<input type="text" name="cmsPage" id="cmsPage" size="61" class="inputFld " value="<?php echo $this->result['data']['cmsPage'];?>"/></td>
			</tr>
			<tr>
				<td class="text-property"><?php echo $this->lang['__CMS_PAGE_TITLE__'];?>:</td>
				<td><input type="text" name="cmsPageTitle" size="61" class="inputFld" value="<?php echo $this->result['data']['cmsPageTitle'];?>" /></td>
			</tr>
			<tr>
				<td valign="top" class="text-property"><?php echo $this->lang['__CMS_PAGE_CONTENT__'];?>:</td>
				<td><textarea name="content" class="textEditor" rows="20" id="textEditor"><?php echo $this->result['data']['content'];?></textarea></td>
			</tr>
			<tr>
				<td class="text-property"><?php echo $this->lang['__IMAGE__'];?>:</td>
				<td><input type="file" name="image"  value="" /><input name="removeImage" type="checkbox" value="1" id="removeImage" /> <label for ="removeImage">Remove Image</label>
				<?php if(!empty($this->result['data']['image'])){
					echo '<br><img src="'.MEDIA_URL.'cms/'.$this->result['data']['image'].'" width="80" />';
					
				}?>
				
				</td>
			</tr>
			<tr>
				<td class="text-property"><?php echo $this->lang['__SORT_ORDER__'];?>:</td>
				<td><input type="text" name="sort_order" size="61"
					class="inputFld" value="<?php echo $this->result['data']['sort_order'];?>" /></td>
			</tr>
			<tr>
				<td class="text-property"><?php echo $this->lang['__META_TITLE__'];?>:</td>
				<td><input type="text" name="meta_title" size="61" class="inputFld"
					value="<?php echo $this->result['data']['meta_title'];?>" /></td>
			</tr>
			<tr>
				<td class="text-property"><?php echo $this->lang['__META_KEYWORDS__'];?>:</td>
				<td><input type="text" name="meta_keywords" size="61"
					class="inputFld" value="<?php echo $this->result['data']['meta_keywords'];?>" /></td>
			</tr>
			<tr>
				<td class="text-property"><?php echo $this->lang['__META_DESCRIPTION__'];?>:</td>
				<td><input type="text" name="meta_description" size="61"
					class="inputFld" value="<?php echo $this->result['data']['meta_description'];?>" /></td>
			</tr>
			<tr>
				<td></td>
				<td><button class="frmButtonGreen" type="submit"><?php echo $this->lang[$btnKey];?></button>
					<button class="frmButtonGreen" type="button" onClick="window.location='<?php echo ADMIN_URL.$this->module.'/'.$this->page;?>'">Cancel</button>
					<input type="hidden" name="action" id="action" value="<?php echo $action;?>" />
					</td>
			</tr>
		</table>
	</form>
	<script>
	CKEDITOR.replace( 'textEditor');
	<?php 
if(!empty($this->query[3]) && strpos($this->query[2],"edit")!==false){
echo '$("#pane").slideToggle(700);';
}
?>
	</script>
</div>