<?php
/*
 * This page will be used to manage information pages in the admin section
 */
if(empty($_SESSION['admin']['adminId'])) {
	$this->redirect(APPLICATION_URL.'admin/profile/login');
}
$this->addCSS("style.css");
$info = new information();
$totalPages = $info->totalContent(array('type'=>'page'));
$totalBlocks = $info->totalContent(array('type'=>'content block'));
?>
<div id="main-content">
<div id="title-div">
<div id="title-div-left">
<h1 ><?php echo $this->lang['__CONTENT_ADMIN_PAGE_TITLE__']?></h1></div> <!-- close title-div-left-->
<div id="title-div-right">
<span class="header2"><?php echo date("M");?></span><span class="header1"><?php echo date("d");?></span></div> <!-- close title-div-right-->
</div> <!-- close title-div-->

<div class="education-stats"><div class="education-stats-div border-right"><div  class="dashboard-right-number">
	<span class="whiteText font-size38 bold"><?php echo $totalPages;?></span></div><div class="dashboard-right-text">
	<span class="whiteText font-size16"><?php echo $this->lang['__CMS_STAT_TITLE_1__']?></span></div></div><div class="education-stats-div border-right"><div  class="dashboard-right-number">
	<span class="whiteText font-size38 bold"><?php echo $totalBlocks;?></span></div><div class="dashboard-right-text">
	<span class="whiteText font-size16"><?php echo $this->lang['__CMS_STAT_TITLE_2__']?></span></div></div><div class="education-stats-div border-right"><div  class="dashboard-right-number">
	</div>
	</div>
</div>

<?php $this->addPage("contentForm");?>
	<div id="paneSelector">
<?php echo $this->lang['__CONTENT_PAGE_ADD_TEXT__'];?></div>

<div class="related-buttons-div">
<!-- <button type="button" onClick="location='<?php echo ADMIN_URL.'information/column';?>';" class="related-button"><?php echo $this->lang['__MANAGE_COLUMN__'];?></button>-->
</div>
<!--  message div -->
<div class="messageDiv">
<?php 
if(!empty($this->result['message'])){
	$this->renderMessage($this->result['message'], false);
} else if ($this->result['errorMessage']){
	$this->renderErrorMessage($this->result['errorMessage']);
}
?>
</div>

<?php 
//add database table
$this->addLoadDatabaseTableScript($this->module."_CMS");
?>
</div> <!-- Main content closed -->
