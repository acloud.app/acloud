<?php 
$this->addCSS("style.css",false,'information');

?>
<div id="pageContent">
<div id="cms-content">
<div class="heading"><?php echo ucwords($this->result['data']['cmsPageTitle']);?></div>

<div class="information-content">
<?php 
if(!empty($this->result['data']['image'])) {
	echo '<div class="information-image"><img src="'.MEDIA_URL.'cms/'.$this->result['data']['image'].'" title="'.ucwords($this->result['data']['cmsPageTitle']).'" alt="'.ucwords($this->result['data']['cmsPageTitle']).'" width="300" /></div>';
}
echo $this->result['data']['content'];
?>
</div>
</div>
</div>

<script>
$(document).ready(function () {
    if (myPlaylist.playlist.length < 1)
        checkCookieForTracks();
    document.title = "<?php echo $this->result['data']['meta_title']; ?>";
    $("#mobile-navigation-content").text("<?php echo $this->result['data']['cmsPageTitle']; ?>");
});
</script>
