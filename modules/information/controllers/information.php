<?php
class information {
	public $result, $message,$debugMessage;
	private $dbObj, $general;
	private function setDBObj() {
		$this->dbObj = new db ();
	}
	private function setGeneral() {
		$this->general = new general ();
	}
	
	/**
	 * ******************** ADMIN FUNCTIONS***********************************
	 */
	public function addCMS() {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		if (! empty ( $_POST )) {
			if (! empty ( $_FILES ['image'] ['name'] )) {
				$imageObj = new image ( "image" );
				$imageName = time () . str_replace ( " ", "_", substr ( $_FILES ['image'] ['name'], 0, strpos ( $_FILES ['image'] ['name'], "." ) ) );
				$path = MEDIA_PATH . "cms/";
				if (! file_exists ( $path )) {
					mkdir ( $path, 0777, true );
				}
				$imageObj->imageToFile ( $imageName, $path );
				$image = $imageObj->imageName;
			} else {
				$image = "";
			}
			
			if($_POST['removeImage']==1){
				$image = "";
			}
			
			$data ['cmsPage'] = $_POST ['cmsPage'];
			$data ['cmsPageTitle'] = $_POST ['cmsPageTitle'];
			$data ['content'] = $_POST ['content'];
			//$data ['column_title'] = $_POST ['columnId'];
			$data ['image'] = $image;
			$data ['type'] = $_POST ['pageType'];
			$data ['main_menu'] = !empty($_POST ['main_menu'])?$_POST ['main_menu']:0;
			$data ['sort_order'] = $_POST ['sort_order'];
			$data ['meta_title'] = $_POST ['meta_title'];
			$data ['meta_keywords'] = $_POST ['meta_keywords'];
			$data ['meta_description'] = $_POST ['meta_description'];
			$data ['addDate'] = date ( "Y-m-d" );
			
			if ($this->dbObj->addRecord ( TBL_CMS, $data )) {
				$array['message']=$this->message="Website content added!!!";
			} else {
				$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in adding Website Content. Please contact webmaster!",$this->dbObj->error);
			}
			return $array;
		}
	}
	
	public function editCMS($id){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$con['cmsId']=$id;
		if ($this->dbObj->fetchRecord ( TBL_CMS, "*", $con )) {
			$array['data']=$this->dbObj->result[0];
		} else {
			$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in adding Website Content. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
	}
	
	public function updateCMS($id){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		if (! empty ( $_POST )) {
			if (! empty ( $_FILES ['image'] ['name'] )) {
				$imageObj = new image ( "image" );
				$imageName = time () . str_replace ( " ", "_", substr ( $_FILES ['image'] ['name'], 0, strpos ( $_FILES ['image'] ['name'], "." ) ) );
				$path = MEDIA_PATH . "cms/";
				if (! file_exists ( $path )) {
					mkdir ( $path, 0777, true );
				}
				$imageObj->imageToFile ( $imageName, $path );
				$image = $imageObj->imageName;
				$data ['image'] = $image;
			} else if($_POST['removeImage']==1){
				$data ['image'] = "";
			}
				
			$data ['cmsPage'] = $_POST ['cmsPage'];
			$data ['cmsPageTitle'] = $_POST ['cmsPageTitle'];
			$data ['content'] = $_POST ['content'];
			//$data ['column_title'] = $_POST ['columnId'];
			$data ['type'] = $_POST ['pageType'];
			$data ['main_menu'] = !empty($_POST ['main_menu'])?$_POST ['main_menu']:0;
			$data ['sort_order'] = $_POST ['sort_order'];
			$data ['meta_title'] = $_POST ['meta_title'];
			$data ['meta_keywords'] = $_POST ['meta_keywords'];
			$data ['meta_description'] = $_POST ['meta_description'];
			$data ['updateDate'] = date ( "Y-m-d" );
		$con['cmsId']=$id;
		if ($this->dbObj->updateRecord ( TBL_CMS, $data, $con )) {
			$array['data']=$this->dbObj->result[0];
			$array['message']="Website Content Updated Successfully!!!";
		} else {
			$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in adding Website Content. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
		}
	}
	
	
	
public function updateCOLUMN($id){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		if (! empty ( $_POST )) {
		
				
			$data ['columnName'] = $_POST ['column_title'];
			$data ['updateDate'] = date ( "Y-m-d" );
		$con['columnId']=$id;
		if ($this->dbObj->updateRecord ( TBL_COLUMN, $data, $con )) {
			$array['data']=$this->dbObj->result[0];
			$array['message']="Column Content Updated Successfully!!!";
		} else {
			$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error while updating column Content. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
		}
	}
	
	
	public function deactivateCMSRecord($recordId) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$data ['status'] = INACTIVE_VALUE;
		$data ['updateDate'] = date ( 'Y-m-d H:i:s' );
		if ($this->dbObj->updateRecord ( TBL_CMS, $data, '', ' AND ', 'cmsId in (' . $recordId . ')' )) {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Successfully Deactivated.";
			return true;
		} else {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Technical Error in Deleting user record.";
		}
	}
	public function activateCMSRecord($recordId) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$data ['status'] = ACTIVE_VALUE;
		$data ['updateDate'] = date ( 'Y-m-d H:i:s' );
		if ($this->dbObj->updateRecord ( TBL_CMS, $data, '', ' AND ', 'cmsId in (' . $recordId . ')' )) {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Successfully Deactivated.";
			return true;
		} else {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Technical Error in Deleting user record.";
		}
	}
	public function deleteCMSRecord($recordId) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$data ['status'] = TRASHED;
		$data ['updateDate'] = date ( 'Y-m-d H:i:s' );
		if ($this->dbObj->updateRecord ( TBL_CMS, $data, '', ' AND ', 'cmsId in (' . $recordId . ')' )) {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Successfully Deactivated.";
			return true;
		} else {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Technical Error in Deleting user record.";
		}
	}
	public function renderCMSTable($searchCon = "", $page = "1", $records = RECORDS_PER_PAGE, $order = "updateDate desc,  cmsId asc, addDate desc", $condition = "") {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		// $this->totalRecords=$this->dbObj->getRows($this->table,$condition,"
		// status !=".TRASHED);
		if (! empty ( $page ) && ! empty ( $records )) {
			$start = (($page - 1) * $records);
		} else {
			$start = "";
		}
		$this->page = $page;
		
		if (! empty ( $records )) {
			$limit = $records;
			// $this->totalPages=ceil($this->totalRecords/$records);
		} else {
			$limit = "";
			$this->totalPages = 0;
		}
		if (! empty ( $searchCon )) {
			$searchArray = explode ( ",", $searchCon );
			$specCon = " (";
			foreach ( $searchArray as $searchKey ) {
				$specCon .= "cmsPageTitle like '%" . $searchKey . "%' OR cmsPage like '%" . $searchKey . "%'  OR content like '%" . $searchKey . "%' OR ";
			}
			$specCon = rtrim ( $specCon, " OR " );
			$specCon .= ")";
			
			$specCon .= " AND status !=" . TRASHED;
		} else {
			$specCon = " status !=" . TRASHED;
		}
		$fields = "p.cmsId, p.cmsPage, p.cmsPageTitle, p.content, p.image, p.type, p.sort_order, p.addDate,p.updateDate, p.status ";
		if ($this->dbObj->fetchRecord ( TBL_CMS . " as p", $fields, "", " AND ", $specCon, $order, $start, $limit )) {
			// echo $this->dbObj->query;
			$rawResult = $this->dbObj->result;
			if (is_array ( $rawResult )) {
				foreach ( $rawResult as $key => $val ) {
					$this->result [$key] ['id:cmsId'] = $val ['cmsId'];
					$this->result [$key] ['page:cmsPage'] = ucfirst ( $val ['cmsPage'] ) . '<br><span class="help">[' . $val ['type'] . ']</span>';
					$this->result [$key] ['title:cmsPageTitle'] = $val ['cmsPageTitle'];
					$this->result [$key] ['date:addDate'] = $this->general->convertDate ( $val ['addDate'] );
					$this->result [$key] ['status:status'] = ($val ['status'] == 1) ? 'Active' : 'Inactive';
				}
			}
			// for total pages and total records
			$this->dbObj->fetchRecord ( TBL_CMS . " as p", $fields, "", " AND ", $specCon );
			$this->totalRecords = count ( $this->dbObj->result );
			$this->totalPages = ceil ( $this->totalRecords / $limit );
			return $this->result;
		} else {
			$this->message = "Unable to fetch Faq Page List";
			$this->debugMessage = "[" . $this->dbObj->errorCode . "]- " . $this->dbObj->error . ". Query: " . $this->dbObj->query;
			return false;
		}
	}
	
	
	
	
	
	
	
public function renderCOLUMNTable($searchCon = "", $page = "1", $records = RECORDS_PER_PAGE, $order = "updateDate desc,  columnId asc", $condition = "") {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		// $this->totalRecords=$this->dbObj->getRows($this->table,$condition,"
		// status !=".TRASHED);
		if (! empty ( $page ) && ! empty ( $records )) {
			$start = (($page - 1) * $records);
		} else {
			$start = "";
		}
		$this->page = $page;
		
		if (! empty ( $records )) {
			$limit = $records;
			// $this->totalPages=ceil($this->totalRecords/$records);
		} else {
			$limit = "";
			$this->totalPages = 0;
		}
		if (! empty ( $searchCon )) {
			$searchArray = explode ( ",", $searchCon );
			$specCon = " (";
			foreach ( $searchArray as $searchKey ) {
				$specCon .= "columnName like '%" . $searchKey . "%' OR ";
			}
			$specCon = rtrim ( $specCon, " OR " );
			$specCon .= ")";
			
			$specCon .= " AND status !=" . TRASHED;
		} else {
			$specCon = " status !=" . TRASHED;
		}
		$fields = "p.* ";
		if ($this->dbObj->fetchRecord ( TBL_COLUMN . " as p", $fields, "", " AND ", $specCon, $order, $start, $limit )) {
			// echo $this->dbObj->query;
			$rawResult = $this->dbObj->result;
			if (is_array ( $rawResult )) {
				foreach ( $rawResult as $key => $val ) {
					$this->result [$key] ['id:columnId'] = $val ['columnId'];
					$this->result [$key] ['Column Name:columnName'] = ucfirst ( $val ['columnName'] );
					$this->result [$key] ['Last Update:updateDate'] = $this->general->convertDate ( $val ['updateDate'] );
					$this->result [$key] ['status:status'] = ($val ['status'] == 1) ? 'Active' : 'Inactive';
				}
			}
			// for total pages and total records
			$this->dbObj->fetchRecord ( TBL_COLUMN . " as p", $fields, "", " AND ", $specCon, $order, $start, $limit );
			$this->totalRecords = count ( $this->dbObj->result );
			$this->totalPages = ceil ( $this->totalRecords / $limit );
			return $this->result;
		} else {
			$this->message = "Unable to fetch Faq Page List";
			$this->debugMessage = "[" . $this->dbObj->errorCode . "]- " . $this->dbObj->error . ". Query: " . $this->dbObj->query;
			return false;
		}
	}
	
public function deactivateCOLUMNRecord($recordId) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$data ['status'] = INACTIVE_VALUE;
		$data ['updateDate'] = date ( 'Y-m-d H:i:s' );
		if ($this->dbObj->updateRecord ( TBL_COLUMN, $data, '', ' AND ', 'columnId in (' . $recordId . ')' )) {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Successfully Deactivated.";
			return true;
		} else {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Technical Error in Deleting user record.";
		}
	}
	public function activateCOLUMNRecord($recordId) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$data ['status'] = ACTIVE_VALUE;
		$data ['updateDate'] = date ( 'Y-m-d H:i:s' );
		if ($this->dbObj->updateRecord ( TBL_COLUMN, $data, '', ' AND ', 'columnId in (' . $recordId . ')' )) {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Successfully Deactivated.";
			return true;
		} else {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Technical Error in Deleting user record.";
		}
	}
	public function deleteCOLUMNRecord($recordId) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$data ['status'] = TRASHED;
		$data ['updateDate'] = date ( 'Y-m-d H:i:s' );
		if ($this->dbObj->updateRecord ( TBL_COLUMN, $data, '', ' AND ', 'columnId in (' . $recordId . ')' )) {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Successfully Deactivated.";
			return true;
		} else {
			$this->debugMessage = "Error [" . $this->dbObj->errorCode . "]:" . $this->dbObj->error . " Query:" . $this->dbObj->query;
			$this->message = "Technical Error in Deleting user record.";
		}
	}
	
	
	public function editCOLUMN($id){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$con['columnId']=$id;
		if ($this->dbObj->fetchRecord ( TBL_COLUMN, "*", $con )) {
			$array['data']=$this->dbObj->result[0];
		} else {
			$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in managing column data. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
	}
	
		public function columnList() {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		if ($this->dbObj->fetchRecord ( TBL_COLUMN, "*", ""," AND ", "status != ".TRASHED )) {
			$array=$this->dbObj->result;
		} else {
			$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in adding Track. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
	
	}
	
	
	/********************* FRONT END METHODS *************************************/
	public function infoPageList($con){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$con['type']='page';
		if ($this->dbObj->fetchRecord ( TBL_CMS, "cmsPage, cmsPageTitle", $con," AND ","","sort_order asc")) {
			//echo $this->dbObj->query;
			$array=$this->dbObj->result;
		} else {
			$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in adding Website Content. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
	}
	
	public function page($page) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$con['cmsPage']=$page;
		if ($this->dbObj->fetchRecord ( TBL_CMS, "*", $con )) {
			$array['data']=$this->dbObj->result[0];
		} else {
			echo $array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in adding Website Content. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
	}
	
	public function infoBlockList($con){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$con['type']='page';
		if ($this->dbObj->fetchRecord ( TBL_CMS, "cmsPage, cmsPageTitle", $con," AND ","","sort_order asc")) {
			$array=$this->dbObj->result;
		} else {
			$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in adding Website Content. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
	}
	
	public function infoBlocks($con){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$con['type']='content block';
		if ($this->dbObj->fetchRecord ( TBL_CMS, "*", $con," AND ","","sort_order asc")) {
			$array=$this->dbObj->result;
		} else {
			$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in adding Website Content. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
	}
	
	public function infoColumnList ($con) {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		if ($this->dbObj->fetchRecord ( TBL_COLUMN, "columnId, columnName", $con," AND ","","updateDate asc")) {
			$array=$this->dbObj->result;
		} else {
			$array['errorMessage']=$this->message=$this->general->prepareMessage("There was some error in adding Website Content. Please contact webmaster!",$this->dbObj->query);
		}
		return $array;
	}
	
	public function totalContent($con="",$specCon="") {
		$this->dbObj = new db();
		$specCon=!empty($specCon)?$specCon." AND status!=".TRASHED:"status!=".TRASHED;
		$count= $this->dbObj->getRows(TBL_CMS,$con, " AND ",$specCon);
		return $count;
	}
}

?>