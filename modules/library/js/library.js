var tag_edit = false;
var editId = null;
var initial = [];
var file_edit=false;
var dir_edit=false;
var fileEditId = null;
var dirEditId = null;
var initialFile = null;
var initialFile_extn = null;
var prohibitedCharactersCodes = [34, 39, 42, 47, 60, 62, 63, 92, 96, 124];

$(document).click(function (event) {
    if (!$(event.target).is(".tag_input, .tags, .editBtn") && tag_edit == true)
        cancelEdit();

    if (!$(event.target).is(".file_input, .filename, .trackEdit, .editBtn") && file_edit == true)
        editFilename(); // If some element (except listed above) is clicked, edit file name

    $(".file_input, .tag_input").bind('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) { //Enter keycode
            if (file_edit)
                editFilename();
            if (tag_edit)
                editTag();
        }
        return (prohibitedCharactersCodes.indexOf(code) == -1); // If keyCode is not found in array (it's good char), return true
    });

    $(".dir_input").bind('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        return (prohibitedCharactersCodes.indexOf(code) == -1); // If keyCode is not found in array (it's good char), return true
    });
});

$(document).ready(function() {
	$(".tags").live('hover', function(e){
		//console.log($(this).data("id"));
		var id = $(this).data("id");
		if (e.type == 'mouseenter') {
			$( ".tags_"+id ).each(function() {
			$(this).css('border','1px solid #ccc');
			$(this).parent().append("<span class='tag_edit' onClick='editDisplayTag("+$(this).data("id")+")'></span>");
			});
		  } else {
			  $( ".tags_"+id ).each(function() {
			  $(this).css('border','1px solid transparent');
			  $(this).parent().find('span').remove();
			  });
		  }
		
		});
		

});


function editDisplayTag(id) {
    if (tag_edit)
        cancelEdit();
    $(".tags_" + id).each(function () {
        var html = '<input type="text" id="tag_input_' + $(this).data('type') + '" name="'
            + $(this).data('type') + '" class="tag_input" value="' + $(this).html() + '" />';
        $(this).parent().html(html);
        initial[$(this).data('type')] = $(this).html();
    });
    var btns = '<div class="editBtns tag">' +
        '<button type="button" class="editBtn" onClick="editTag();"></button>' +
        '<button type="button" class="cancelBtn" onClick="cancelEdit();"></button>' +
        '</div>';
    $('#tags_' + id).append(btns);
    tag_edit = true;
    editId = id;
}

function editDisplayFile(id) {
    if (file_edit)
        cancelEditFile();
    var fileSpan = $('#file_' + id),
        html = '<input type="text" id="file_input_' + id + '" name="filename" class="file_input" ' +
            'value="' + fileSpan.data('value') + '" />' + fileSpan.data('extn');
    initialFile = $('#file_' + id + ' > a').html();
    initialFile_extn = fileSpan.data('extn');
    fileSpan.html(html);
    var btns = '<div class="editBtns"><button type="button" class="editBtn" onClick="editFilename();"></button>' +
        '<button type="button" class="cancelBtn" onClick="cancelEditFile();"></button></div>';
    fileSpan.append(btns);
    file_edit = true;
    fileEditId = id;
}

function appendDirectoryEditing(id) {
    if (dir_edit)
        cancelEditDir();
    var directory = $('#dir_' + id),
        html = '<input type="text" id="dir_input_' + id + '" name="filename" class="dir_input" ' +
            'value="' + directory.data('value') + '" onkeydown="if(event.keyCode==13) editDirectoryName();" />',
        buttons = '<div class="editBtns">' +
            '<button type="button" class="editBtn" onclick="editDirectoryName();"></button>' +
            '<button type="button" class="cancelBtn" onclick="cancelEditDir();"></button>' +
            '</div>';
    directory.html(html);
    directory.append(buttons);
    dir_edit = true;
    dirEditId = id;
}

function editTag() {
    var tagTitle = $("#tag_input_title").val(),
        tagArtist = $("#tag_input_artist").val(),
        tagAlbum = $("#tag_input_album").val(),
        tagYear = $("#tag_input_year").val();
    tag_edit = false;
    $.ajax({
        url: APPLICATION_URL + 'modules/library/ajax/editTrackTags.php',
        type: "POST",
        dataType: 'json',
        data: {id: editId, title: tagTitle, artist: tagArtist, album: tagAlbum, year: tagYear},
        success: function (data) {
            $(".tag_input").each(function () {
                var html = '<a href="javascript:;" data-id="' + editId + '" data-type="' + $(this).attr('name') + '" ' +
                    'class=" tags tags_' + editId + '" onClick="editDisplayTag(' + editId + ')">' + $(this).val() + '</a>';
                $(this).parent().html(html);
            });
            $('#tags_' + editId).find('div').remove();
            $('#message').html(
                '<div id="msgDiv" class="ui-widget">' +
                    '<div class="ui-state-' + (data.error ? 'error' : 'success') + // Decide: error or success
                            ' ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">' +
                        '<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>' +
                        data.answer +
                    '</div>' +
                '</div>'
            );
            editId = null;
            setTimeout(function () {
                $('#msgDiv').fadeOut('fast');
            }, 10000);
        }
    });
}

function cancelEdit() {
    $(".tag_input").each(function () {
        var html = '<a href="javascript:;" data-id="' + editId + '" data-type="' + $(this).attr('name')
            + '" class=" tags tags_' + editId + '" onClick="editDisplayTag(' + editId + ')">'
            + initial[$(this).attr('name')] + '</a>';
        $(this).parent().html(html);
    });
    $('#tags_' + editId).find('div').remove();
    tag_edit = false;
    editId = null;
}

function editFilename() {
    var filename = $("#file_input_" + fileEditId).val(),
        file = $('#file_' + fileEditId),
        path = file.data('path');
    file_edit = false; // Otherwise it'll trigger function twice
    $.ajax({
        url: APPLICATION_URL + 'modules/library/ajax/editTrackFilename.php',
        type: "POST",
        dataType: 'json',
        data: {
            id: fileEditId,
            filename: filename,
            extn: initialFile_extn
        },
        success: function (data) {
            if (!data.error) { // If rename is successful, restore elements to initial state
                var newName = data.name != undefined ? data.name : filename;
                file.html(
                    '<a href=" ' + path + newName + '" download>' + newName + '</a>' // Create link with new filename
                );
                file.data('value', newName.replace(new RegExp(initialFile_extn, "g"), "")); // Remove extension
                $('#file_' + editId).find('div').remove(); // Remove editing div
                fileEditId = null;
                file_edit = false;
            }
            else
                file_edit = true; // Because we set it to false at the beginning of function
            $('#message').html(
                '<div id="msgDiv" class="ui-widget">' +
                    '<div class="ui-state-' + (data.error ? 'error' : 'success') + // Decide: error or success
                            ' ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">' +
                        '<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>' +
                        data.answer +
                    '</div>' +
                '</div>'
            );
            setTimeout(
                function () {
                    $('#msgDiv').fadeOut('fast');
                },
                10000 // Response message will be displayed for 10 seconds
            );
        }
    });
}

function editDirectoryName() {
    var directory = $('#dir_' + dirEditId),
        directoryNewName = $("#dir_input_" + dirEditId).val();
    $.ajax({
        url: APPLICATION_URL + 'modules/library/ajax/editDirName.php',
        type: "POST",
        dataType: 'json',
        data: {
            id: dirEditId,
            directoryName: directoryNewName
        },
        success: function (data) {
            if(!data.error){
                directory.html(
                    '<a href="javascript:;"  onclick="listFolder(' + dirEditId + ');">' +
                    '<img src="/modules/common/images/folder_icon.png" valign="middle" hspace="30" />' +
                    directoryNewName + '</a>'
                );
                directory.data('value', directoryNewName);
                directory.find('div').remove();
                file_edit = false;
                fileEditId = null;
            }
            $('#message').html(
                '<div id="msgDiv" class="ui-widget">' +
                    '<div class="ui-state-' + (data.error ? 'error' : 'success') + // Decide: error or success
                            ' ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">' +
                        '<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>' +
                        data.answer +
                    '</div>' +
                '</div>'
            );
            setTimeout(
                function () {
                    $('#msgDiv').fadeOut('fast');
                },
                10000 // Response message will be displayed for 10 seconds
            );
        }
    });
}

function cancelEditFile() {
    var file = $('#file_' + fileEditId),
        path = file.data('path');
    file.html(
        '<a href=" ' + path + initialFile + '" download>' + initialFile + '</a>'
    );
    file.find('div').remove();
    file_edit = false;
    fileEditId = null;
}

function cancelEditDir() {
    var directory = $('#dir_' + dirEditId),
        directoryName = directory.data('value');
    directory.html(
        '<a href="javascript:;"  onclick="listFolder(' + dirEditId + ');">' +
        '<img src="/modules/common/images/folder_icon.png" valign="middle" hspace="30" />' +
        directoryName + '</a>'
    );
    directory.find('div').remove();
    dir_edit = false;
    dirEditId = null;
}

function allToPublic () {
    $("#message").html('<div id="msgDiv" class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> Executing DB query...</div></div>');
    $.ajax({
        url :  APPLICATION_URL
        + 'modules/library/ajax/allToPublic.php',
        success : function(data) {
            listFolder(0);
            $("#message").html('<div id="msgDiv" class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> DB query executed.</div></div>');
            setTimeout(function(){
                $("#msgDiv").fadeOut("slow", function () {
                    $("#msgDiv").attr("display","None");
                });
            }, 2000);
        }
    });
}

$(document).ready(function() {
	var folderId = baseFolder != undefined ? parseInt( baseFolder ) : 0
	listFolder(folderId);
});

function listFolder(folder, disableScroll, globalAlert) {
    var URL = APPLICATION_URL + 'modules/library/ajax/folderListAdmin.php',
        alert = (globalAlert != undefined) ? ('&alert=true&value=' + globalAlert.value + '&type=' + globalAlert.type) : '';
    $.ajax({
        url: URL + '?parentFolderId=' + folder + alert,
        success: function (data) {
            if (!disableScroll)
                window.scrollTo(0, 0);
            $("#media").html(data);
            if ($.browser.msie) {
                $("#side-bar").height($(document).height());
            }
        }
    });
}


function changeFolderProperty(property,folderId,value) {
	var URL = APPLICATION_URL+'modules/library/ajax/folderAction.php?action='+property+'&folderId='+folderId+'&value='+value;
	$.ajax({
		url :  URL,
		success : function(data) {
			var current_open_folder = $("#current_open_folder").val();
            listFolder(current_open_folder, true);
		}
	});
	
}

function deleteZip(folderId) {
	var URL = APPLICATION_URL+'modules/library/ajax/deleteZip.php?folderId='+folderId;
	$.ajax({
		url :  URL,
		success : function(data) {
			if(data.length>1) {
				alert(data);
			}
			var current_open_folder = $("#current_open_folder").val();
            listFolder(current_open_folder, true);

		}
	});

}

function changeFileProperty(property,fileId,value) {
	var URL = APPLICATION_URL+'modules/library/ajax/fileAction.php?action='+property+'&fileId='+fileId+'&value='+value;
	$.ajax({
		url :  URL,
		success : function(data) {
			var current_open_folder = $("#current_open_folder").val();
			listFolder(current_open_folder,true);
		}
	});
	
}


function deleteFolder (folderId) {
	$("#dialog").html(
	'<div><h4>All folders and files within this folder will be deleted! Do you Really want to delete Folder?</h4></div>').dialog(
	{
		modal : true,
		title : 'Delete Record',
		zIndex : 10000,
		autoOpen : true,
		width : 'auto',
		resizable : false,
		buttons : {
			Yes : function() {
				changeFolderProperty('delete',folderId,5);
				$(this).dialog("close");
			},
			No : function() {
				$(this).dialog("close");
			}
		},
		close : function(event, ui) {
			//$(this).dialog("close");
		}
	});
}
function deleteFile(fileId) {
	$("#dialog").html(
	'<div><h4>Do you Really want to delete this File?</h4></div>').dialog(
	{
		modal : true,
		title : 'Delete Record',
		zIndex : 10000,
		autoOpen : true,
		width : 'auto',
		resizable : false,
		buttons : {
			Yes : function() {
				changeFileProperty('delete',fileId,5);
				$(this).dialog("close");
			},
			No : function() {
				$(this).dialog("close");
			}
		},
		close : function(event, ui) {
			//$(this).dialog("close");
		}
	});
}

function editFile (fileId) {
	var URL = APPLICATION_URL+'modules/library/ajax/fileEdit.php?fileId='+fileId;
	var popup = $("#popup");
	// Get the screen height and width
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();
	

	// Set height and width to mask to fill up the whole screen
	$('#mask').css({
		'width' : maskWidth,
		'height' : maskHeight
	});

	// transition effect
	$('#mask').fadeTo("slow", 0.7);
	popup.load(URL);
	 popup.css("left", Math.max(0, (($(window).width() - popup.outerWidth()) / 2) + 
             $(window).scrollLeft()) + "px");
	 popup.css("top", Math.max(0, (($(window).height() - popup.outerHeight()) /4)) + "px");
	 //popup.css("top", ($(window).scrollTop()+50) + "px");
	popup.show();
}

function editFolder(folderId) {
	var URL = APPLICATION_URL+'modules/library/ajax/folderEdit.php?folderId='+folderId;
	var popup = $("#popup");
	// Get the screen height and width
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();
	

	// Set height and width to mask to fill up the whole screen
	$('#mask').css({
		'width' : maskWidth,
		'height' : maskHeight
	});

	// transition effect
	$('#mask').fadeTo("slow", 0.7);
	popup.load(URL);
	 popup.css("left", Math.max(0, (($(window).width() - popup.outerWidth()) / 2) + 
             $(window).scrollLeft()) + "px");
	 popup.css("top", Math.max(0, (($(window).height() - popup.outerHeight()) / 2) ) + "px");
	 //popup.css("top", ($(window).scrollTop()+50) + "px");
	popup.show();
}

function addZip(folderId) {
	var URL = APPLICATION_URL+'modules/library/ajax/addZip.php?folderId='+folderId;
	var popup = $("#popup");
	// Get the screen height and width
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();


	// Set height and width to mask to fill up the whole screen
	$('#mask').css({
		'width' : maskWidth,
		'height' : maskHeight
	});

	// transition effect
	$('#mask').fadeTo("fast", 0.7);
	popup.load(URL);
	popup.css("left", Math.max(0, (($(window).width() - popup.outerWidth()) / 2) +
			$(window).scrollLeft()) + "px");
	popup.css("top", Math.max(0, (($(window).height() - popup.outerHeight()) / 2) ) + "px");
	//popup.css("top", ($(window).scrollTop()+50) + "px");
	popup.show();
}

function setupNewFolderForm(parentFolderId){
	var newFolderForm = $("#newFolderForm"),
		defaultHTML = newFolderForm.html(),
		newFolderName = $('<input type="text" id="newFolderName" class="dir_input" placeholder="Enter new folder name">'),
		buttonsDiv = $('<div class="editBtns"></div>'),
		submit = $('<button type="button" class="editBtn"></button>'),
		cancel = $('<button type="button" class="cancelBtn"></button>'),
		error = $('<div class="error"></div>');

	newFolderName.keydown(function(e){
		if(e.keyCode == 13){
			if(newFolderName.val() != ''){
				createNewFolder(parentFolderId, newFolderName.val());
			}
			else{
				newFolderName.focus();
			}
		}
	});
	submit.click(function() {
		if(newFolderName.val() != ''){
			createNewFolder(parentFolderId, newFolderName.val());
		}
		else{
			newFolderName.focus();
		}
	});
	cancel.click(function() {
		newFolderForm.html(defaultHTML);
	});

	newFolderForm.html(newFolderName);
	newFolderName.focus();
	buttonsDiv.append(submit);
	buttonsDiv.append(cancel);
	newFolderForm.append(buttonsDiv);
	newFolderForm.append(error);
}

function createNewFolder(parentFolderId, newFolderName) {
	$.ajax({
		method: "GET",
		url: APPLICATION_URL + 'modules/library/ajax/createNewFolder.php',
		dataType: "json",
		data: {
			parent: parentFolderId,
			name: newFolderName
		}
	})
		.done(function(data) {
			if(!data.success){
				$("#newFolderForm .error").text(data.response);
			}
			else{
				listFolder(data.folderId);
			}

		});
}

function closeFilePopUp() {
	$("#mask").hide();
	$("#popup").hide();
	$("#popup").html('');
	var current_open_folder = $("#current_open_folder").val();
    listFolder(current_open_folder, true);
}

function add_to_sort(elementId,val){
   
		if($("#"+elementId).is(':checked')){
			var $li = $("<li id='"+val+"'/>").text(val);
			 
			$("#sortable").append($li);
		} else {
			$('#sortable li').each( function() {
					if($(this).attr('id')==val){
						$(this).remove();
					}
				});
		}
	    $("#sortable").sortable('refresh');
	    var order = []; 
	    $('#sortable li').each( function(e) {

            //add each li position to the array...     
            // the +1 is for make it start from 1 instead of 0
           order.push( $(this).attr('id') );
           });
           // join the array as single variable...
           var positions = order.join('-');
            //use the variable as you need!
           $("#track_display_option").val(positions);
}

function add_to_commint(elementId){
	m=elementId.split('heckbox_');
	var isChecked = $("#"+elementId).is(":checked");
	if(isChecked==true){$("#"+m[1]).val('Yes'); }
	if(isChecked==false){$("#"+m[1]).val('No'); }
}
