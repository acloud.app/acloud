<?php

echo '
    <div class="global-fixed-alert">
        <div class="alert" role="alert"
                ng-show="$root.globalAlert.isShown"
                ng-class="$root.globalAlert.type == \'error\' ? \'alert-danger\' : \'loading\' ? \'alert-info\' : \'alert-success\'">
            <b>{{ $root.globalAlert.value }}</b>
            <div class="progress" ng-show="$root.globalAlert.type == \'loading\'">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100"
                         aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    </div>
                </div>
        </div>
    </div>
    <div id="uploadForm" class="addNew_Forms">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> ' . $html->lang['__UPLOAD_FILES__'] . '</h4>
            </div>
            <div ng-controller="UploadController" nv-file-drop="" uploader="uploader" class="panel-body">
                <div class="well my-drop-zone" nv-file-over="" uploader="uploader">
                    Drop file(s) here...
                </div>
                <br clear="both">
                <table class="table" ng-show="isUploaderError">
                    <tr class="danger">
                        <td>Error: </td>
                        <td>{{error.text}}</td>
                    </tr>
                    <tr ng-show="error.response">
                        <td>Got response: </td>
                        <td>{{error.response}}</td>
                    </tr>
                    <tr ng-show="error.status">
                        <td>Got status: </td>
                        <td>{{error.status}}</td>
                    </tr>
                    <tr ng-show="error.headers">
                        <td>Got headers: </td>
                        <td>{{error.headers}}</td>
                    </tr>
                </table>
                <div class="uploader-queue" ng-show="uploader.queue.length > 0">
                    <h4>Upload Queue</h4>
                    <p>Queue length: {{ uploader.queue.length }}</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="50%">Name</th>
                            <th ng-show="uploader.isHTML5">Size</th>
                            <th ng-show="uploader.isHTML5">Progress</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="item in uploader.queue">
                            <td>
                                <strong>{{ item.file.name }}</strong><br>
                                <div class="text-danger" ng-show="item.error"><strong>Error: </strong> {{item.error.text}}</div>
                            </td>
                            <td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
                            <td ng-show="uploader.isHTML5">
                                <div class="progress" style="margin-bottom: 0;">
                                    <div class="progress-bar" role="progressbar" ng-style="{ \'width\': item.progress + \'%\' }"></div>
                                </div>
                            </td>
                            <td class="text-center">
                                <span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
                                <span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
                                <span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
                            </td>
                            <td nowrap>
                                <button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess || item.error.isExists">
                                    <span class="glyphicon glyphicon-upload"></span> Upload
                                </button>
                                <button type="button" class="btn btn-warning btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
                                    <span class="glyphicon glyphicon-ban-circle"></span> Cancel
                                </button>
                                <button type="button" class="btn btn-danger btn-xs" ng-click="removeItem(item)">
                                    <span class="glyphicon glyphicon-trash"></span> Remove
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <div ng-show="uploader.queue.length > 1">
                        <div>
                            Queue progress:
                            <div class="progress" style="">
                                <div class="progress-bar" role="progressbar" ng-style="{ \'width\': uploader.progress + \'%\' }"></div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
                            <span class="glyphicon glyphicon-upload"></span> Upload All
                        </button>
                        <button type="button" class="btn btn-warning btn-s" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
                            <span class="glyphicon glyphicon-ban-circle"></span> Cancel All
                        </button>
                        <button type="button" class="btn btn-danger btn-s" ng-click="clearQueue()" ng-disabled="!uploader.queue.length">
                            <span class="glyphicon glyphicon-trash"></span> Remove All
                        </button>
                    </div>
                    <br clear="both">
                    <div ng-show="isCompletedAll">
                        <button type="button" class="btn btn-default btn-s" onclick="listFolder(' . $parentFolderId . ');">
                            <span class="glyphicon glyphicon-refresh"></span> Refresh current directory
                        </button>
                    </div>
                </div>
                <br clear="both">
            </div>
        </div>
    </div>';

echo '</div>';// end list div
