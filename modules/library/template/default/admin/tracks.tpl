<?php
/*
 * This page will be used to manage tracks in the admin section
 */
if(empty($_SESSION['admin']['adminId'])) {
	$this->redirect(APPLICATION_URL.'admin/profile/login');
}

$this->addCSS ( "library.css");
$this->addCSSIE ( "library.css");

?>
<div id="main-content">
	<div id="title-div">
		<div id="title-div-left">
			<h1><?php echo $this->lang['__MANAGE_LIBRARY_LINK_TEXT__']?></h1>
		</div>
        <div id="title-div-right">
            <span class="header2"><?php echo date("M");?></span>
			<span class="header1"><?php echo date("d");?></span>
        </div>
	</div>
    
    <div id="message"></div>
    <div id="media"></div>
</div>
<?php 
$this->addJS ( "library.js");
?>