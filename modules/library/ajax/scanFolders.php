<?php
//http://test.propovednik.com/modules/library/ajax/scanFolders.php?start=0&skipScan=true&skipDelete=true&redoThumbs=true

// CLI example:
// cd modules/library/ajax; export QUERY_STRING="start=0&skipScan=false&redoThumbs=false&skipDelete=false" ; php -e -r 'parse_str($_SERVER["QUERY_STRING"], $_GET); include "scanFolders.php";'

session_start();
require_once '../../../config/config.php';

if (!(PHP_SAPI == 'cli') && empty($_SESSION['admin']['adminId'])) {  //it's ok for this script to run in CLI without authorization
    header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
    die("not authorized");
}

header('Content-Type: text/html; charset=utf-8');

if (!isset($_GET['start'])) { ?>
<head>
    <meta charset="UTF-8">
    <title>(Re)Scan the library</title>
    <script>
        function updateTheLink() {
            var start      =  document.getElementById('start').value;
            var skipScan   = !document.getElementById('skipScan').checked;
            var redoThumbs =  document.getElementById('redoThumbs').checked;
            var skipDelete = !document.getElementById('skipDelete').checked;
            var link = window.location.href +
                '?start='      + start +
                '&skipScan='   + skipScan +
                '&redoThumbs=' + redoThumbs +
                '&skipDelete=' + skipDelete;
            document.getElementById('link').href        = link;
            document.getElementById('link').textContent = link;
        }

        function startScan () {
            document.getElementById('settings').innerHTML = '<h2>Starting scanning process. Please wait...</h2>';
            location.href = document.getElementById('link').href = link;
        }
    </script>
</head>
<body>
<div id="settings">
    <h3>Select parameters for scanning:</h3>

    <input type="number" value="0" id="start" onkeyup="updateTheLink();" onchange="updateTheLink();">
    ID of the folder to start. Enter '0' to scan everything<br>
    
    <input type="checkbox" id="skipScan" checked="checked" onclick="updateTheLink();">
      (Re)-scan files<br>

    <input type="checkbox" id="redoThumbs" onclick="updateTheLink();">
      Regenerate thumbnails. Works, only if previous option enabled<br>
    
    <input type="checkbox" id="skipDelete" checked="checked" onclick="updateTheLink();">
      Remove non-existing folders and files from DB. REMOVE operation is global (it ignores 'start' parameter).<br>
    
    <br>Click on the link to start scanning:<br>
    <a href="toBeDefinedHere" id="link" onclick="startScan();">Start scanning...</a>
    <script>
        updateTheLink ();
    </script>
</div>
</body>
</html>
<?php die (); }

include ("scanFile.php");
include ("setMP3FlagFunction.php");

echo "<pre>\n";
$root_folder = MEDIA_PATH."mp3/";
$db = new db();
$lib = new library();
$foldersWithMp3 = array();

$general->log("SCAN_UPD","Scanning started from IP: " . $_SERVER['REMOTE_ADDR']);

if ($_GET ['skipScan']=="true")
    echo "skipping scanning\n";
else {
    if ($_GET ['redoThumbs']=="true")
        echo "Will recreate Thumbnails\n";
    else
        echo "Will not recreate Thumbnails\n";
    $startId = intval ($_GET ['start']);
    $startFolder = "";
    if ($startId != 0)
        $startFolder = $lib->fetchParent ($startId);
    $general->log("SCAN_UPD","Scanning, starting from path: /" . $startFolder . " ID: " . $startId, true);
    scanFolder($startId,$startFolder);
    setMp3Flag( $foldersWithMp3 );
    echo "folders scanned!\n";
}

function scanFolder ($folderId, $relPath) {
    global $general;
    global $db;
    global $root_folder;
    global $foldersWithMp3;
    $fullPath = $root_folder.$relPath;
    echo "Processing $fullPath ; ID: $folderId\n";
    ob_flush();
    $folder_array = $general->scanFolder( $fullPath ); // Returns array of folders and files in parent folder
    if ( is_array( $folder_array ) ) {
        foreach ( $folder_array as $child ) {
            if ( is_file( $fullPath . $child ) ) { //WIN: is_file(iconv('UTF-8','Windows-1251',$fullPath
                $response = scanFile( $fullPath, $child, $folderId, null, $db );
                if ( isset( $response[ 'error' ] ) )
                    $general->log( "SCAN_ERROR", $response[ 'error' ], true );
                else
                    if ( $response[ 'answer' ][ 'file' ][ 'isMP3' ] == 1 )
                        $foldersWithMp3[] = $folderId;
            }
            else {
                //check if FOLDER is present in DB
                $relChildPath = $relPath . $child . "/";
                if (strlen($child) > 512) { //Max path length in DB
                    $general->log("SCAN_ERROR", "skipping " . $child . " because name is too long", true);
                } else {
                    $ignoreNamesString = preg_replace ('/\s+/', '', SCAN_IGNORE); // RegEx removes white spaces
                    $ignoreNamesArray  = explode (',', $ignoreNamesString); // Split with comma
                    if (!in_array( $child, $ignoreNamesArray ) ) {
                        $db->fetchRecord(TBL_FOLDERS, "folderId", array('folderName' => htmlspecialchars($child), 'parentId' => $folderId));
                        if (empty($db->result[0]['folderId'])) {
                            $data['folderName'] = $child;
                            $data['parentId'] = $folderId;
                            $data['addDate'] = date("Y-m-d H:i:s");
                            if (!$db->addRecord(TBL_FOLDERS, $data))
                                $general->log("SCAN_ERROR", "DB error - folderName: " . $child ."; parentId: " . $folderId ."<br>", true);
                            unset($data);
                            $childFolderId = $db->result;
                            $general->log("SCAN_NEW", "New folder: $relChildPath ID: $childFolderId\n", true);
                        } else {
                            $childFolderId = $db->result[0]['folderId'];
                            if (SCAN_UPD)
                                $general->log("SCAN_UPD", "Reviewing existing folder: " . $relChildPath . " ID: " . $childFolderId);
                        }
                        scanFolder($childFolderId, $relChildPath);
                    }
                }
            }
        } // foreach
    } // if is_array
}

if ($_GET ['skipDelete']=="true")
    echo "Dry run. No records will be deleted.\n";
$general->log("SCAN_DEL", "scanDelete started from IP: " . $_SERVER['REMOTE_ADDR']);

echo "\nChecking FILES for deletions...\n";
$db->fetchRecord(TBL_TRACKS,"*");
$result = $db->result;
if(is_array($result))
    foreach ($result as $fileRecord) {
        $name = ($lib->fetchParentFolders($fileRecord['folderId'])) . htmlspecialchars_decode ($fileRecord['trackFile']);
        if (!file_exists($name)) {
            $general->log("SCAN_DEL", "Deleting file DB record: $name", true);
            ob_flush();
            if (!($_GET ['skipDelete']=="true"))
                $db->deleteRecord(TBL_TRACKS, array('trackId' => $fileRecord['trackId']));
        }
    }


echo "\nChecking FOLDERS for deletions...\n";
$db->fetchRecord(TBL_FOLDERS,"*");
$result = $db->result;
if(is_array($result))
    foreach ($result as $folder) {
        $name = $lib->fetchParentFolders($folder['folderId']);
        if (!file_exists($name)) {
            $general->log("SCAN_DEL", "Deleting folder DB record: id=" . $folder['folderId'] . ", $name", true);
            ob_flush();
            if (!($_GET ['skipDelete'] == "true"))
                $db->deleteRecord(TBL_FOLDERS, array('folderId' => $folder['folderId']));
        }
    }

$general->log("SCAN_DEL", "scanDelete completed", true);
$general->log("SCAN_UPD", "Scanning completed", true);
echo "</pre>\n";