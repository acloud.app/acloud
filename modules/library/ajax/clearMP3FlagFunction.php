<?php
function clearMP3Flag( $folderId, $recursion = false, $isMP3 = false ) {
    global $lib;
    global $db;
    $directoryDetails = $lib->getFolderDetails( $folderId );
    if ( ( $directoryDetails[ 'hasMP3' ] || $recursion ) && !$directoryDetails[ 'isCategory' ] ) {
        $parentDirectoryId = $isMP3 ? $folderId : $directoryDetails[ 'parentId' ];
        $parentDirectoryDetails = $lib->getFolderDetails( $parentDirectoryId );
        if ( !$parentDirectoryDetails[ 'isCategory'] ) {
            $hasMP3 = false;
            $parentDirectoryContent = $lib->fetchFolderContent( $parentDirectoryDetails[ 'folderId' ] );
            if ( is_array( $parentDirectoryContent ) )
                if ( count( $parentDirectoryContent[ 'folders' ] ) > 0 )
                    foreach ( $parentDirectoryContent[ 'folders' ] as $neighborDirectory ) {
                        if ( $neighborDirectory[ 'folderId' ] != $folderId || $recursion ) {
                            $neighborDirectoryDetails = $lib->getFolderDetails( $neighborDirectory[ 'folderId' ] );
                            if ( $neighborDirectoryDetails[ 'hasMP3' ] ) {
                                $hasMP3 = true;
                                break;
                            }
                        }
                    }
                if ( count( $parentDirectoryContent[ 'files' ] ) > 0 && !$hasMP3 )
                    foreach ( $parentDirectoryContent[ 'files' ] as $neighborFile ) {
                        $response = scanFile(
                            MEDIA_PATH . "mp3/" . ( $lib->fetchParent( $parentDirectoryDetails[ 'folderId' ] ) ),
                            $neighborFile[ 'trackFile' ],
                            $parentDirectoryDetails[ 'folderId' ],
                            null,
                            $db
                        );
                        if ( isset( $response[ 'answer' ] ) )
                            if ( $response[ 'answer' ][ 'file' ][ 'isMP3' ]) {
                                $hasMP3 = true;
                                break;
                            }
                    }
            if ( $db->updateRecord(
                TBL_FOLDERS,
                array( 'hasMP3' => $hasMP3 ),
                array( 'folderId' => $parentDirectoryId )
            ) )
                echo "Updated mp3 status for folder with id " . $parentDirectoryId . ", hasMP3 = $hasMP3\n";
            else
                echo "Cannot update mp3 status for folder with id " . $parentDirectoryId . "\n";
            if ($parentDirectoryId != 0)
                clearMP3Flag( $parentDirectoryId, true );
        }
    }
}
