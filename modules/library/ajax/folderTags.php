<?php

session_start();
require_once '../../../config/config.php';

if (empty($_SESSION['admin']['adminId'])) {
    http_response_code(403);
    exit('HTTP/1.1 403 Forbidden');
} else {
    $data = file_get_contents("php://input");
    if ($data) { // Have to post something
        $response = array();
        $decoded = json_decode($data);
        if (is_null($decoded)) { // Error: JSON not valid
            $response['status'] = array(
                'type' => 'error',
                'value' => 'Server error: Invalid JSON.'
            );
        } else {
            $request = $decoded->request;
            $data = $decoded->data;
            $response = getResponse($request, $data);
        }
        $encoded = json_encode($response);
        header('Content-type: application/json');
        exit($encoded);
    } else { // Otherwise, error: Forbidden
        http_response_code(403);
        exit('HTTP/1.1 403 Forbidden');
    }
}

function getResponse($request, $data) {
    switch ($request) {
        case 'get_folder_tags':
            return getFolderTags($data->parent_folder_id);
            break;
        case 'update_tag':
            return updateTag($data->file_id, $data->tag_name, $data->tag_value, $data->is_new_and_essential);
            break;
        case 'delete_tag':
            return deleteTag($data->file_id, $data->tag_name, $data->is_essential);
            break;
        case 'change_track_display_option':
            return changeTrackDisplayOption($data->file_id, $data->track_display_option);
            break;
        default:
            return array(
                'type' => 'error',
                'value' => 'Server error: Invalid request.',
                'request' => $request
            );
    }
}

/* getFolderTags:
 *  Gets folder id
 *  Finds all music files in this folder
 *  For each music file finds tags
 *  Return JSON object:
 *      fileArray: array of {
 *              id: fileId,
 *              essential_tags: array of { name: tagName, value: tagValue, is_shown: bool },
 *              other_tags: array of { name: tagName, value: tagValue }
 *          }
*/
function getFolderTags($parentFolderId) {
    $lib = new library();
    $folderContentArray = $lib->fetchFolderContent($parentFolderId);
    if (count($folderContentArray['files']) > 0) {
        $filePropertiesArray = array();
        foreach ($folderContentArray['files'] as $file) {
            if ($file['music_file']) {
                // Get track display option for this mp3
                $condition['condition']['trackId'] = $file['trackId'];
                $trackList = $lib->trackList($condition);
                $trackList = $trackList['data'];
                $trackDisplayOption = array();
                if (is_array($trackList)) {
                    $trackDisplayOptionString = $trackList[0]['track_display_option'] ?
                        $trackList[0]['track_display_option'] : DEFAULT_TRACK_NAME;
                    $tempArray = explode('-', $trackDisplayOptionString);
                    foreach ($tempArray as $item)
                        // Need to ignore empty element if string has `-` on the end
                        if (strlen($item) > 1)
                            $trackDisplayOption[] = $item;
                }
                // Get all tags for this mp3
                $essentialTags = array();
                $otherTags = array();
                $childFile = $lib->fetchParentFolders($parentFolderId) . $file['trackFile'];
                $getID3 = new getid3();
                $ThisFileInfo = $getID3->analyze($childFile);
                $v2TagComments = $ThisFileInfo['id3v2']['comments'];
                if ($v2TagComments != NULL) {
                    $shownEssentialTags = array();
                    $order = 0;
                    // First comes tags that in $trackDisplayOption
                    foreach ($trackDisplayOption as $tag)
                        foreach ($v2TagComments as $tagName => $tagValue)
                            if (!empty($v2TagComments[$tagName][0])) {
                                if ($tag == $tagName) {
                                    $essentialTags[] = array(
                                        'name' => $tagName,
                                        'value' => $tagValue[0],
                                        'is_shown' => true,
                                        'order' => ++$order
                                    );
                                    $shownEssentialTags[] = $tag;
                                }
                            }
                    // Now all other tags
                    foreach ($v2TagComments as $tagName => $tagValue)
                        if (!empty($v2TagComments[$tagName][0])) {
                            if ($tagName == 'title' ||
                                $tagName == 'album' ||
                                $tagName == 'artist' ||
                                $tagName == 'track_number'
                            ) {
                                if (!in_array($tagName, $shownEssentialTags)) // If we don't have it already
                                    $essentialTags[] = array(
                                        'name' => $tagName,
                                        'value' => $tagValue[0],
                                        'is_shown' => false,
                                        'order' => ++$order
                                    );
                            } else {
                                if ($tagName != 'picture' && json_encode($tagValue[0]))
                                    $otherTags[] = array(
                                        'name' => $tagName,
                                        'value' => $tagValue[0]
                                    );
                            }
                        }
                }
                // Combine everything into array
                $filePropertiesArray[] = array(
                    'id' => $file['trackId'],
                    'essential_tags' => $essentialTags,
                    'other_tags' => $otherTags
                );
            }
        }
        return array(
            'type' => 'success',
            'files' => $filePropertiesArray
        );
    } else return array(
        'type' => 'success',
        'value' => 'No files found.'
    );
}

/* updateTag:
 *  Gets file id, tag name, tag value
 *  Prepares tag data array
 *  Calls editTags function
 *  Changes track display option if necessary
 *  Returns JSON object:
 *      {
 *          type: success or error
 *          value: value
 *      }
*/
function updateTag($fileId, $tagName, $tagValue, $isNewAndEssential) {
    $tagData = array(
        $tagName => array($tagValue)
    );
    include 'editTrackTagsFunction.php';
    $response = editTrackTags($fileId, $tagData);
    if ($isNewAndEssential && $tagName == 'title')
        changeTrackDisplayOption($fileId, null, 'add', $tagName);
    return array(
        'type' => (isset($response['error']) && $response['error']) ? 'error' : 'success',
        'value' => $response['answer']
    );
}

/* deleteTag:
 *  Gets file id, tag name
 *  Prepares tag data array for deletion 
 *  Calls editTags function with resulted array
 *  Changes track display option if necessary
 *  Returns JSON object:
 *      {
 *          type: success or error
 *          value: value
 *      }
*/
function deleteTag($fileId, $tagName, $isEssential) {
    $tagData = array(
        $tagName => array(null)
    );
    include 'editTrackTagsFunction.php';
    $response = editTrackTags($fileId, $tagData);
    if ($isEssential)
        changeTrackDisplayOption($fileId, null, 'delete', $tagName);
    return array(
        'type' => (isset($response['error']) && $response['error']) ? 'error' : 'success',
        'value' => $response['answer']
    );
}

/* changeTrackDisplayOption:
 *  Gets file id, track display option
 *  Prepares data for database
 *  If function was called with special case (when deleted or added tag), handle that:
 *      Get track_display_option and explode it to array
 *      If `delete` action, remove that tag from track_display_option
 *      If `add` action, add that tag to track_display_option
 *  Executes database statement
 *  Returns JSON object:
 *      {
 *          type: success or error
 *          value: value
 *      }
*/
function changeTrackDisplayOption($fileId, $trackDisplayOption, $action = null, $tag = null) {
    $data = array(
        'updateDate' => date("Y-m-d H:i:s"),
        'track_display_option' => $trackDisplayOption
    );
    $db = new db();
    if ($trackDisplayOption == null) {
        $db->fetchRecord(TBL_TRACKS, 'track_display_option', array('trackId' => $fileId));
        $dbTrackDisplayOption = $db->result[0]['track_display_option'];
        $dbTrackDisplayOption = explode('-', $dbTrackDisplayOption);
        if ($action == 'delete') {
            if (count($db->result) > 0)
                foreach ($dbTrackDisplayOption as $key => $value)
                    if ($value == $tag)
                        unset($dbTrackDisplayOption[$key]);
        } elseif ($action == 'add' && !in_array ($tag, $dbTrackDisplayOption)) {
            $dbTrackDisplayOption[] = $tag;
        }
        $data['track_display_option'] = implode('-', $dbTrackDisplayOption);
    }
    if ($db->updateRecord(TBL_TRACKS, $data, array('trackId' => $fileId))) {
        return array(
            'type' => 'success',
            'value' => 'Successfully updated track display option.'
        );
    } else
        return array(
            'type' => 'error',
            'value' => 'Can\'t update record in DB.'
        );
}
