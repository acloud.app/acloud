<?php

session_start();
require_once '../../../config/config.php';
if (empty($_SESSION['admin']['adminId'])) {
    header('location:' . APPLICATION_URL . 'admin/profile/login');
    die();
}
if (empty($_POST))
    die ("Invalid request");

$fileId = intval($_POST['id']);

// populate data array
$tagData = array(
    'title' => array($_POST['title']),
    'artist' => array($_POST['artist']),
    'album' => array($_POST['album']),
    'year' => array($_POST['year'])
);

include 'editTrackTagsFunction.php';
$response = editTrackTags($fileId, $tagData);
echo json_encode($response);
