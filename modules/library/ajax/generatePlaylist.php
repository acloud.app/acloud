<?php
require_once '../../../config/config.php';
$db = new db();
if (empty($_GET['trackId'])) {
    $response = array('error' => 'Playlist is empty!');
    echo json_encode($response);
}
elseif (empty($_GET['friendlyName'])) {
    $response = array('error' => 'Friendly name is empty!');
    echo json_encode($response);
} else {
    $trackId = intval($_GET['trackId']);
    $friendlyName = $_GET['friendlyName'];
    $ids_array = explode(";", $trackId);    // This creates empty element at the end
                                            // because at the end of each id there is ';'
                                            // which php handles as another element.
                                            // This problem is solved below.
    $playlist_id = 0;

    if ($db->addRecord(
        TBL_PLAYLIST,
        array(
            "friendly_name" => $friendlyName,
            "last_accessed" => date("Y-m-d H:i:s")
        )
    )) {
        $playlist_id = $db->result;
        foreach ($ids_array as $id)
            if ($id > 0) // Detect that empty element at the end of array caused by explode(";", $trackId);
                if (!($db->addRecord(
                    TBL_PLAYLIST_DATA,
                    array(
                        "playlist_id" => $playlist_id,
                        "track_id" => $id
                    )
                ))) {
                    $response = array('error' => 'Cannot add track to playlist.');
                    break;
                }
    } else
        $response = array('error' => 'Cannot create new playlist.');

    if (!isset($response['error']))
        $response = array(
            'playlist_id' => $playlist_id,
            'friendly_name' => $friendlyName,
        );
    echo json_encode($response);
}
