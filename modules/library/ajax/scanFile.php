<?php

/** scanFile will scan file and populate DB with it's details
 *
 * Use  cases:
 * - Update MP3 tags: existing file         ($trackId known)
 * - Upload:          new file              ($trackId does not exists)
 * - Scan:            new or existing file  ($trackId may exists)
 *
 * @return array of status: 'STATUS' => 'details'
 * where STATUS is `error` / `answer`
 */
function scanFile($folderName, $fileName, $folderId, $trackId = null, $db = null, $saveTrackDisplayOption = false) {
    global $general;
    $fullPath = $folderName . $fileName;
    $fullPath_old=$fullPath;

    if (strpos($fileName, "&") || strpos($fileName, "#")) {
        $vowels = array("&","#");
        $fileName = str_replace($vowels, "_", $fileName);
        $fullPath =$folderName.$fileName;
        if (!rename($fullPath_old, $fullPath)) {
            $general->logToDB('FILE', 'ERROR', "Error renaming $fullPath_old");
        }
    }
    $extension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
    $system_format = explode(",", AVAILABLE_MUSIC_FORMAT);
    $prohibited_extensions_string = preg_replace('/\s+/', '', PROHIBITED_EXTENSIONS); // RegEx removes white spaces
    $prohibited_extensions_array = explode(',', $prohibited_extensions_string); // Split with comma

    if ( in_array( $extension, $prohibited_extensions_array ) ){
        $answer = array( 'error' => 'A file [ ' . $fileName . ' ] with prohibited extension: .' . $extension );
        $general->logToDB('FILE','ERROR', $answer[ 'error' ] );
        return $answer;
    }

        //check if FILE is present in DB
        if ($db == null)
            $db = new db();
        if ($trackId===null) {
            $db->fetchRecord(TBL_TRACKS, 'trackId, track_display_option',
                array('trackFile' => htmlspecialchars($fileName), 'folderId' => $folderId));
            $count = count($db->result);
            $currentId = $db->result[0]['trackId'];
        } else {
            $db->fetchRecord(TBL_TRACKS, 'trackId, track_display_option',
                array('trackId' => $trackId));
            $count = count($db->result);
            $currentId = $trackId;
        }

        $trackDisplayOption = $saveTrackDisplayOption ? $db->result[0]['track_display_option'] : DEFAULT_TRACK_NAME;

        $isPicture = false;
        $width = $height = 0;
        $type = '';

        if(!in_array($extension, $system_format)) { // if not music file
            $data['music_file'] = 0;
            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png') {
                $isPicture = true;
                list($width, $height, $type) = getimagesize($fullPath);
                $data['width']  = $width;
                $data['height'] = $height;
            }
        }
        else { // if music file
            $data['music_file'] = 1;

            $getID3 = new getid3();
            $ThisFileInfo = $getID3->analyze($fullPath);
            $duration = $ThisFileInfo['playtime_string'];
            $data['duration'] = $duration;
            $v2TagComments = $ThisFileInfo['id3v2']['comments'];

            if (SCAN_TAG) {
                $general->log("SCAN_TAG3","$fullPath");
                $general->log("SCAN_TAG3","-- playtime: ".print_r($ThisFileInfo['playtime_seconds'],true) . " == " . print_r($ThisFileInfo['playtime_string'],true));

                $tags=$ThisFileInfo['tags'];

                if ($tags['id3v2'] != NULL)
                    foreach ($tags['id3v2'] as $key => $val) {
                        if ($key == "picture")
                            $general->log("SCAN_TAG3", "  v2: $key ");
                        elseif ($key != "music_cd_identifier" && $key != "url_source" && $key != "url_user" && $key != "encoded_by")
                            $general->log("SCAN_TAG3", "  v2: $key == " . $val[0]);
                } elseif ($tags['id3v1'] != NULL)
                    foreach ($tags['id3v1'] as $key => $val) {
                        if ($key != "music_cd_identifier" && $key != "url_source" && $key != "url_user" && $key != "encoded_by")
                            $general->log("SCAN_TAG3", "  v1: $key == " . $val[0]);
                    }

            }

            if (!empty($v2TagComments['title'] [0]) ||
                !empty($v2TagComments['album'] [0]) ||
                !empty($v2TagComments['genre'] [0]) ||
                !empty($v2TagComments['year']  [0]) ||
                !empty($v2TagComments['artist']  [0]) ||
                !empty($v2TagComments['track_number'][0])
            ) {
                $artist         = $v2TagComments['artist'][0];
                $album          = $v2TagComments['album'][0];
                $year           = $v2TagComments['year'][0];
                $comment        = $v2TagComments['comment'][0];
                $genre          = $v2TagComments['genre'][0];
                $track_number   = empty($v2TagComments['track_number']) ? null : intval($v2TagComments['track_number'][0]);
                $trackTitle = (!empty($v2TagComments['title'][0]) && strlen($v2TagComments['title'][0]) > 0) ?
                    $v2TagComments['title'][0] : '';
            }

            $data['artist']         = $artist;
            $data['album']          = $album;
            $data['track_year']     = $year;
            $data['comments']       = $comment;
            $data['genre']          = $genre;
            $data['track_number']   = $track_number;
        }

        $data['file_size']  = filesize($fullPath);
        $data['folderId']   = $folderId;
        $data['trackFile']  = $fileName;
        $data['trackTitle'] = !empty($trackTitle) ? // trackTitle cannot be empty, either it's title of mp3 or filename
                                    $trackTitle : ($data['music_file']? // If music file, remove extension, else - use whole file name
                                                        preg_replace("/\\.[^.\\s]{3,4}$/", "", $fileName) : $fileName);  // RegEx removes file extension
        $data['track_display_option'] = $trackDisplayOption;

        if($isPicture && (($width < 1) || ($height < 1))){
            $general->logToDB('FILE', 'ERROR', "Skipping photo $fullPath because it's broken." );
            return array(
                'error' => "Skipping photo because it's broken.",
                'err' => "Skipping photo [ '. $fullPath . ' ] because it's broken.",
                'action' => 'delete'
            );
        } else {
            if ($count < 1) { // if does not exist in DB
                $data['addDate'] = date("Y-m-d H:i:s");
                if ($db->addRecord(TBL_TRACKS, $data)) {
                    $general->logToDB('FILE', 'NEW', "$fullPath");
                    $result['answer']['status'] = 'File added to DB.';
                    $result['answer']['file'] = array(
                        'full_path' => $fullPath,
                        'name' => $fileName,
                        'isMP3' => $data['music_file']
                    );
                    if ($isPicture) {
                        // Get id of newly created entry
                        $db->fetchRecord(TBL_TRACKS, 'trackId', array('trackFile' => htmlspecialchars($fileName), 'folderId' => $folderId));
                        $currentId = $db->result[0]['trackId'];
                        $thumbStatus = makeThumbnails($folderName, $fileName, $currentId, $width, $height, $type);
                        $result['answer']['thumb'] = $thumbStatus;
                    }
                    return $result;
                } else {
                    $general->logToDB('FILE', 'ERROR', 'Cannot add file [ '. $fullPath . ' ] to DB: ' . $db->error );
                    return array(
                        'error'    => 'Cannot add file to DB.',
                        'err'      => $db->error, // Error details
                        'duration' => $duration,  // Error details
                        'action'   => 'delete'    // Delete file from actual directory
                    );
                }
            } else { //updating existing DB record
                if (SCAN_UPD)
                    $general->log("SCAN_UPD", "--scanning " . $fileName);
                $data['updateDate'] = date("Y-m-d H:i:s");
                if ($db->updateRecord(TBL_TRACKS, $data, array('trackId' => $currentId))) {
                    $result['answer']['status'] = 'DB record updated.';
                    $result['answer']['file'] = array(
                        'full_path' => $fullPath,
                        'name' => $fileName,
                        'id' => $currentId,
                        'isMP3' => $data['music_file']
                    );
                    if ($isPicture)
                        if ($_GET ['redoThumbs']=="true") {
                            $thumbStatus = makeThumbnails($folderName, $fileName, $currentId, $width, $height, $type);
                            $result['answer']['thumb'] = $thumbStatus;
                    }
                    return $result;
                } else {
                    $general->logToDB ('FILE', 'ERROR', 'Cannot update DB record [ '. $fullPath . ' ]: ' . $db->error );
                    return array('error' => 'Cannot update DB record.', 'err' => $db->error);
                }
            }
        }
}

function makeThumbnails($dir, $img, $id, $width, $height, $type) {
    global $general;
    if ($type == IMG_JPEG || $type == IMG_JPG || $type == IMAGETYPE_JPEG) {
        $original_image = @imagecreatefromjpeg($dir . $img);
        $extension = '.jpg';
    } elseif ($type == IMG_PNG || $type == IMAGETYPE_PNG) {
        $original_image = @imagecreatefrompng($dir . $img);
        $extension = '.png';
    } else {
        $general->logToDB ('FILE', 'ERROR', 'Bad image type: ' . $type . '. File: ' . $dir . $img );
        return array ('error' => 'Bad image type.');
    }
    
    if (!$original_image) {
        $general->logToDB ('FILE', 'ERROR', 'Bad image file: ' . $dir . $img );
        return array ('error' => 'Bad image file.');
    } else {
        $thumb_name   = THUMBNAIL_SUFFIX . $id . $extension;
        if (!createSmallerImage ($width, $height, $original_image, $id, $extension, true ) ||
            !createSmallerImage ($width, $height, $original_image, $id, $extension, false )) {
            $general->logToDB ('FILE', 'ERROR', 'Error while creating thumbnail file: ' . $dir . $img . ' ; type: ' . $type);
            return array('error' => 'Error while creating thumbnail file', 'type' => $type);
        }
        return array('created' => true, 'name' => $thumb_name);
    }
    $general->logToDB ('FILE', 'ERROR', 'Unsupported photo file: ' . $dir . $img . ' ; type: '. $type);
    return array ('error' => 'Unsupported photo file.', 'type' => $type);
}

require_once "calculateImageProportion.php";
function createSmallerImage ($original_width, $original_height, $original_image, $id, $extension, $isThumb ) {
    if ($isThumb) {  // generating thumbnail
        $thumb_name = THUMBNAIL_SUFFIX . $id . $extension;
        $desired_width = THUMBNAIL_WIDTH;
        $desired_height = THUMBNAIL_HEIGHT;
    } else {         // generating display image
        $thumb_name = DISPLAY_SUFFIX . $id . $extension;
        $proportion = calculateImageProportion($original_width, $original_height);
        $desired_width = intval($original_width * $proportion);
        $desired_height = intval($original_height * $proportion);
    }

    $new_width = $desired_width;
    $new_height = $desired_height;

    $new_image = imagecreatetruecolor($desired_width, $desired_height);
    $dest_x = 0;
    $dest_y = 0;
    if ($isThumb) {
        $white = imagecolorallocate($new_image, 255, 255, 255);
        imagefill($new_image, 0, 0, $white);

        // calculate space for the fill area
        if ($original_width > $original_height)
            $new_height = intval($original_height * $desired_width / $original_width);
        else
            $new_width  = intval($original_width * $desired_height / $original_height);
        $dest_x = intval(($desired_width  - $new_width)  / 2);
        $dest_y = intval(($desired_height - $new_height) / 2);
    }
    imagecopyresampled ($new_image, $original_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);

    $thumb_fullname = MEDIA_PATH . THUMBNAIL_DIR . $thumb_name;
    if ($extension == '.jpg')
        $isSuccess = ImageJPEG($new_image, $thumb_fullname);
    else
        $isSuccess = ImagePNG ($new_image, $thumb_fullname);
    chmod($thumb_fullname, 0777);
    return $isSuccess;
}