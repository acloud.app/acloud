<?php
//include config file
session_start();
require_once '../../../config/config.php';
if(empty($_SESSION['admin']['adminId'])) {
	header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
	die();
}

$addZip = false;
$folderId = intval($_GET['folderId']);
$lib = new library ();

include 'deleteZipFunction.php';

deleteZip($lib, $folderId);

//Запись в базу
$data['zip_file'] = NULL;
$data['downloadable'] = 0;
$data['updateDate'] = date("Y-m-d H:i:s");
$lib->updateFolder($folderId, $data);
print $error;