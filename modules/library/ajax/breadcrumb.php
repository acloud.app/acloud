<?php
/**
 * Created by PhpStorm.
 * User: Fursov
 * Date: 12/31/2015
 * Time: 10:54 PM
 */
echo '<input type="hidden" id="current_open_folder" value="' . $parentFolderId . '" />';
// get and generate breadcrumb
$array = $lib->fetchParentFolderInfo ( $parentFolderId );
$array = array_reverse ( $array );
$isCategory = 1;
$hasMP3 = 0;
$isZIP = NULL;
$path='';

if (is_array ( $array )) {
    $breadcrumbEntry = 0;
    $breadcrumb = '';
    foreach ( $array as $item ) {
        $folderId = $item['folderId'];
        if (! empty ( $item ['folder'] )) {
            $path .= $item ['folder']."/";
            $breadcrumbEntry++;
            if ($breadcrumbEntry == count($array)) { //use info about last folder in breadcrumb
                $isCategory = $item['isCategory'];
                $hasMP3 = $item[ 'hasMP3' ];
                $isZIP = $item['zip_file'];
            }
            $breadcrumb .= '<a href="/library?album='.$item ['folderId'].'"';
            $breadcrumb .= ( !$isCategory || $hasMP3 ) ? ' class="withButtons"' : '';
            $breadcrumb .= ' onClick="listFolder(' . $item ['folderId'] . '); return false;" >' . ucwords ( $item ['folder'] ) . '</a> &raquo; ';
            $title=$item ['folder'];
        }
    }

    echo '<div class="breadcrumb-wrapper">';
    echo '<div class="manage-button-div breadcrumb-buttons">';

    if (isset($arTrack)) { // TRACK ot FILE
        // PLAY and ADD icons
        echo '<a href="javascript:;"  onClick="playTrack(\'' . $trackId . '\')"><img src ="' . COMMON_IMAGES_URL . 'play_button_small.png" alt="' . $html->lang ['__PLAY__'] . '" title="' . $html->lang ['__PLAY__'] . '" /></a>';
        echo '<a href="javascript:;"  onClick="addToPlaylist(\'' . $trackId . '\',true)"><img src ="' . COMMON_IMAGES_URL . 'add_play_button_small.png" alt="' . $html->lang ['__ADD_TO_PLAYLIST__'] . '" title="' . $html->lang ['__ADD_TO_PLAYLIST__'] . '" /></a>';
        // DOWNLOAD icon
        if ($arTrack['downloadable'] == "1") {
            $download_image_src = COMMON_IMAGES_URL . "download.png";
            $download_image_alt = "Download '".$arTrack['trackFile']."' (".round($arTrack['file_size']/1024/1024,2)." MB)";
            $file = $lib->fetchParentURL($parentFolderId) . $arTrack['trackFile'];
            echo '<a href="' . $file . '" title="' . $download_image_alt . '" download><img src ="' . $download_image_src . '" alt="' . $download_image_alt . '" valign="middle" /></a>';
        }
    } elseif ( !$isCategory ) { // FOLDER. we do not let users play huge 'category'. only albums.
        // PLAY and ADD icons
        if ( $hasMP3 ) {
            echo ' <a href="javascript:;"  onClick="playFolderPlaylist (\'' . $parentFolderId . '\')"><img src ="' . COMMON_IMAGES_URL . 'play_button_small.png" alt="'     . $html->lang ['__PLAY__']            . '" title="' . $html->lang ['__PLAY__'] . '" /></a>';
            echo ' <a href="javascript:;"  onClick="addFolderToPlaylist(\'' . $parentFolderId . '\')"><img src ="' . COMMON_IMAGES_URL . 'add_play_button_small.png" alt="' . $html->lang ['__ADD_TO_PLAYLIST__'] . '" title="' . $html->lang ['__ADD_TO_PLAYLIST__'] . '" /></a>';
        }
        // DOWNLOAD icon
        if ($isZIP != NULL) {
            echo ' <a href="/media/zip/'.$isZIP.'" title="Download \''.$isZIP.'\'" download>';
            echo '  <img src ="' . COMMON_IMAGES_URL . 'download.png' . '" valign="middle" /></a>';
        }
    }
    echo '</div>';

    echo '<div class="breadcrumb';
    echo ( !$isCategory || $hasMP3 )?' withButtons':'';
    echo '">';
    echo '<a href="/library" onClick="listFolder(0); return false;">'.AUDIO_LIBRARY.'</a> &raquo; ';
    echo $breadcrumb;

    if (isset($arTrack)) {  // TRACK
        if (strlen($arTrack['trackTitle']) < 3) {
            @$formatFile = end(explode(".", $arTrack['trackFile'])); // Узнаем расширение файла
            $length = strlen($formatFile) + 1; // Узнаем количество символов расширения файла
            $title = mb_substr($arTrack['trackFile'], 0, -$length);
        } else
            $title = $arTrack['trackTitle'];
        echo $title;
    }

    echo '</div>';
    echo "</div>\n";// closing <div class="breadcrumb-wrapper">
}