<?php
session_start();
require_once '../../../config/config.php';

if (empty($_SESSION['admin']['adminId'])) {
    header('location:' . APPLICATION_URL . 'admin/profile/login');
    die();
}

if (empty($_POST))
    die ("Invalid request");

$fileId = intval($_POST['id']);
$fileName = trim($_POST['filename']); // trim: remove whitespaces from the beginning and end of a string
$newName = $fileName . $_POST['extn'];
$lib = new library();

$trackRecord = $lib->getTrackDetails($fileId); // Get file name from DB
$parentFolder = MEDIA_PATH . 'mp3/' . $lib->fetchParent($trackRecord['folderId']);

if (file_exists($parentFolder . $newName))
    $response = array('answer' => 'File with such name already exists.', 'error' => true);
elseif (!(rename($parentFolder . $trackRecord['trackFile'], $parentFolder . $newName)))
    $response = array('answer' => 'Failed to change Filename!', 'error' => true);
else {
    $data['trackFile'] = $newName;
    $lib->updateTrack($fileId, $data);
    $response = array(
        'answer' => 'Successfully changed filename.',
        'name' => $newName
    );
    require_once "scanFile.php";
    $status = scanFile($parentFolder, $newName, $trackRecord['folderId'], $fileId, null, true);
    if (array_key_exists("error", $status)) {
        $response['answer'] .= $status["error"];
        $response['error'] = true;
        $general->log("ERROR", "Error while scanning after renaming " . $parentFolder . $trackRecord['trackFile'] . ": " . print_r($status,true));
    } else {
        $response['scanning'] = $status;
    }
}

echo json_encode($response);
