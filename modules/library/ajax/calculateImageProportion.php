<?php

function calculateImageProportion($width, $height)
{
    $proportion = DISPLAY_WIDTH / $width;
    $proportionY = DISPLAY_HEIGHT / $height;
    if ($proportionY < $proportion)
        $proportion = $proportionY;
    if ($proportion > 1) {// only downsize, never upsize
        $proportion = 1;
        //TODO do not resize, use originals
    }
    return $proportion;
}