<?php

session_start();
require_once '../../../config/config.php';

if(empty($_SESSION['admin']['adminId'])) {
    header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
    die();
}

function getResponse($request, $data) {
    switch ( $request ) {
        case 'get_track_name_order':
            return getTrackNameOrder($data->parent_folder_id);
            break;
        case 'save_tag_changes':
            return saveChangesWrapper($data, 'tag');
            break;
        case 'save_track_name_changes':
            return saveChangesWrapper($data, 'track_name');
            break;
        case 'reset_track_number':
            return saveChangesWrapper($data, 'reset_track_number');
            break;
        default:
            return array (
                'type' => 'error',
                'value' => 'Invalid request.',
                'request' => $request
            );
    }
}

/*
 * If folder has an mp3, get its track display option
 * Otherwise use default value
 * */
function getTrackNameOrder($folderId)
{
    $parentFolderId = intval($folderId);
    $trackDisplayOption = '';
    $trackNameOrderObject = array();
    $library = new library();
    $folderContent = $library->fetchFolderContent($parentFolderId);
    if (count($folderContent['files']) > 0)
        $trackDisplayOption = $folderContent['files'][0]['track_display_option'];
    if ($trackDisplayOption == '') {
        $settings = new settings();
        $websiteSettings = $settings->getSettingDetails("website");
        if (is_array($websiteSettings))
            foreach ($websiteSettings as $key => $value)
                if ($key == "DEFAULT_TRACK_NAME")
                    $trackDisplayOption = $value['value'];
    }
    $trackNameOrderArray = explode("-", trim($trackDisplayOption));
    foreach ($trackNameOrderArray as $item)
        if (strlen($item) > 1)
            $trackNameOrderObject[] = array('tag' => $item);
    return array(
        'type' => 'success',
        'trackNameOrderObject' => $trackNameOrderObject,
        'value' => 'Got title order.'
    );
}

function saveChangesWrapper($data, $type) {
    $parentFolderId = intval($data->parent_folder_id);
    $library = new library();
    $folderContent = $library->fetchFolderContent($parentFolderId);
    if (is_array($folderContent)){
        if ($data->recursion == 'recursive' || ($data->recursion == 'not_recursive' && count($folderContent['files']) > 0)
        ) {
            if ($type == 'tag' || $type == 'reset_track_number') {
                if ($data->tag == 'album')
                    $tagData['album'] = array($data->tag_value);
                else if ($data->tag == 'artist')
                    $tagData['artist'] = array($data->tag_value);
                else if ($data->tag == 'year')
                    $tagData['year'] = array($data->tag_value);
                else if ($type == 'reset_track_number')
                    $tagData['track_number'] = array(null);
                if (isset($tagData) && is_array($tagData)) {
                    require_once 'editTrackTagsFunction.php';
                    return saveChanges($data, $folderContent, $tagData, null, $library, null, $type);
                } else
                    return array(
                        'type' => 'error',
                        'value' => 'No tag data to write.'
                    );
            } elseif ($type == 'track_name') {
                $title = '';
                foreach ($data->track_name_order as $particle)
                    $title .= $particle->tag . '-';
                $title = rtrim($title, '-');
                $db = new db();
                return saveChanges($data, $folderContent, null, $title, $library, $db, $type);
            } else
                return array(
                    'type' => 'error',
                    'value' => 'No data of what to change.'
                );
        }
        else
            return array(
                'type' => 'error',
                'value' => 'No files or folders found (try recursive option).'
            );
    }
    else
        return array(
            'type' => 'error',
            'value' => 'Can\'t get files from library.'
        );
}

function saveChanges($data, $folderContent, $tagData, $title, $library, $db, $type) {
    if (count($folderContent['files']) > 0)
        foreach ($folderContent['files'] as $file)
            if ($file['music_file']) {
                if ($type == 'tag' || $type == 'reset_track_number')
                    $tag_writer[] = editTrackTags($file['trackId'], $tagData);
                elseif ($type == 'track_name') {
                    $db_data['updateDate'] = date("Y-m-d H:i:s");
                    $db_data['track_display_option'] = $title;
                    if ($db->updateRecord(TBL_TRACKS, $db_data, array('trackId' => $file['trackId']))) {
                        $track_name_response[] = array(
                            'trackId' => $file['trackId'],
                            'response' => 'Successfully updated track.'
                        );
                    } else
                        return array(
                            'type' => 'error',
                            'value' => 'Can\'t update record in DB.'
                        );
                } else
                    return array(
                        'type' => 'error',
                        'value' => 'Unknown change requested.'
                    );
            }
    if ($data->recursion == 'recursive' && count($folderContent['folders']) > 0) {
        foreach ($folderContent['folders'] as $folder) {
            $nextFolderContent = $library->fetchFolderContent($folder['folderId']);
            if (is_array($nextFolderContent))
                saveChanges($data, $nextFolderContent, $tagData, $title, $library, $db, $type);
        }
    }
    $response = array(
        'type' => 'success',
        'value' => ($type == 'tag' ? 'Saved tag data.' : ($type == 'track_name' ? 'Saved track name data.' : 'Reset track number.'))
    );
    if ($type == 'tag')
        $response['tag_writer'] = $tag_writer;
    elseif ($type == 'track_name' || $type == 'reset_track_number')
        $response['track_name'] = $track_name_response;
    return $response;
}

$data = file_get_contents("php://input");
if ($data) { // Have to post something
    $response = array();
    $decoded = json_decode($data);
    if (is_null($decoded)) { // Error: JSON not valid
        $response['status'] = array(
            'type' => 'error',
            'value' => 'Invalid JSON.'
        );
    } else {
        $request = $decoded->request;
        $data = $decoded->data;
        $response = getResponse($request, $data);
    }
    $encoded = json_encode($response);
    header('Content-type: application/json');
    exit($encoded);
} else { // Otherwise, error: Forbidden
    http_response_code(403);
    exit('HTTP/1.1 403 Forbidden');
}
