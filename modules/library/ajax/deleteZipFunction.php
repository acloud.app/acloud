<?php

$error = null;

function deleteZip($lib, $folderId){
    $folderDetail = $lib->getFolderDetails($folderId);
    $dir_zip = MEDIA_PATH . "zip/";//путь к папке
    $name = $folderDetail['zip_file'];
    //Имя архива
    $fileName = $dir_zip . $name;
    //Удаляем архив
    if(is_file($fileName)) {
        unlink($fileName);
        if(is_file($fileName)){
            $error = null;
        }
    }
    else{
        $error = "Файл не найден";
    }
}