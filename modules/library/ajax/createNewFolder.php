<?php

//include config file
session_start();
require_once '../../../config/config.php';

if(empty($_SESSION['admin']['adminId'])) {
    header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
    die();
}

$parentFolderId = intval($_GET['parent']);
$newFolderName = $_GET['name'];

$lib = new library();

$parentFolder = $lib->fetchParentFolders($parentFolderId);
$newFolder = $parentFolder.str_replace('"', "'", $newFolderName).'/';

if(!file_exists($newFolder) && !is_dir($newFolder)){
    if(!mkdir($newFolder)){
        $arr = array('success' => false, 'response' => 'Can\'t create folder '.$newFolder.'.');
    }
    else{
        chmod($newFolder, 0777);

        $data['folderName'] = $newFolderName;
        $data['parentId'] = $parentFolderId;
        $data['privacy'] = 'private';
        $data['isCategory'] = '0';
        $data['addDate'] = date("Y-m-d H:i:s");

        $db = new db();

        if (!$db->addRecord(TBL_FOLDERS, $data))
            $arr = array('success' => false, 'response' => 'DB error: Can\'t create folder record of '.$newFolder.' in database.');

        $id = $db->result;

        $arr = array('success' => true, 'response' => 'Created folder '.$newFolder.'.', 'folderId' => $id);
    }
}
else{
    $arr = array('success' => false, 'response' => 'Can\'t create folder '.$newFolder.', already exist.');
}

$jsn = json_encode($arr);
print_r($jsn);
