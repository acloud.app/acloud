<?php
//include config file
session_start();
require_once '../../../config/config.php';

if(empty($_SESSION['admin']['adminId'])) {
	header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
	die();
}


$fileId = intval($_GET['fileId']);
$lib = new library();

//check if data is posted
if(!empty($_POST)) {
	$data['trackTitle']=$_POST['trackTitle'];
	$data['artist']=$_POST['artist'];
	$data['album']=$_POST['album'];
	$data['track_year']=$_POST['track_year'];
	$data['track_display_option']=$_POST['track_display_option'];
	$data['updateDate']=date("Y-m-d H:i:s");
	$lib->updateTrack($fileId, $data);
}

// get file record details
$trackDetail=$lib->getTrackDetails($fileId);
$display_option_string = ($trackDetail['track_display_option'])?$trackDetail['track_display_option']:DEFAULT_TRACK_NAME;
$display_option_array = explode("-", $display_option_string);

$show_title_checked = (in_array("title", $display_option_array))?'checked="checked"':'';
$show_album_checked = (in_array("album", $display_option_array))?'checked="checked"':'';
$show_artist_checked = (in_array("artist", $display_option_array))?'checked="checked"':'';
$show_year_checked = (in_array("year", $display_option_array))?'checked="checked"':'';

$helper_array = array(
	'album' => 'album',
	'title' => 'trackTitle',
	'artist' => 'artist',
	'filename' => 'trackFile',
	'year' => 'track_year',
	'track_number' => 'track_number'
);


if(is_array($trackDetail)) { 
	$html = new html();
	?>
	<script src="<?php echo COMMON_JS_URL?>jQueryForm.js"></script>
	<div id="close-button" onClick="closeFilePopUp();"><img src="<?php echo COMMON_IMAGES_URL?>popup-close-button.png" /></div>
	<div id="popUpContentDiv">
	<div id="popUpBody">
	<div class="messageDiv">
<?php 
if(!empty($lib->message)){
	$html->renderMessage($lib->message,false);	
} else if ($lib->errorMessage){
	$html->renderErrorMessage($lib->errorMessage,false);
}
?>
</div>
<script>
  $(function() {
    $( "#sortable" ).sortable({
    	update: function(event, ui) {
            //create the array that hold the positions...
            var order = []; 
              //loop trought each li...
             $('#sortable li').each( function(e) {

             //add each li position to the array...     
             // the +1 is for make it start from 1 instead of 0
            order.push( $(this).attr('id') );
            });
            // join the array as single variable...
            var positions = order.join('-');
             //use the variable as you need!
            $("#track_display_option").val(positions);
           // $.cookie( LI_POSITION , positions , { expires: 10 });
          }
    });
    $( "#sortable" ).disableSelection();
  });
  </script>
	<form method="post" id="frm" action="<?php echo APPLICATION_URL.'modules/library/ajax/fileEdit.php?fileId='.$fileId; ?>" enctype="multipart/form-data">
	<table border="0">
				<tr>
					<td class="text-property"><?php echo $html->lang['__TRACK_TITLE__'];?>:</td>
					<td><input type="text" name="trackTitle" size="61" class="inputFld" value="<?php echo $trackDetail['trackTitle'];?>" /> <input type="checkbox" name="show_title" id="show_title" value="1" <?php echo $show_title_checked;?> onClick="add_to_sort(this.id,'title');"/> <label for="show_title">Show Title</label></td>
				</tr>
				<tr>
					<td class="text-property"><?php echo $html->lang['__ALBUM__'];?>:</td>
					<td><input type="text" name="album" size="61" class="inputFld" value="<?php echo $trackDetail['album'];?>" /> <input type="checkbox" name="show_album" id="show_album" value="1" <?php echo $show_album_checked;?> onClick="add_to_sort(this.id,'album');" /> <label for="show_album">Show Album</label></td>
				</tr>
				<tr>
					<td class="text-property"><?php echo $html->lang['__ARTIST__'];?>:</td>
					<td><input type="text" name="artist" size="61" class="inputFld" value="<?php echo $trackDetail['artist'];?>" /> <input type="checkbox" name="show_artist" id="show_artist" value="1" <?php echo $show_artist_checked;?> onClick="add_to_sort(this.id,'artist');"/> <label for="show_artist">Show Artist</label></td>
				</tr>
				<tr>
					<td class="text-property"><?php echo $html->lang['__TRACK_YEAR__'];?>:</td>
					<td><input type="text" name="track_year" size="61" class="inputFld" value="<?php echo $trackDetail['track_year'];?>" /> <input type="checkbox" name="show_year" id="show_year" value="1" <?php echo $show_year_checked;?> onClick="add_to_sort(this.id,'year');"/> <label for="show_year">Show Year</label></td>
					
				</tr>
				<tr>
					<td class="text-property"><?php echo $html->lang['__TRACK_DISPLAY_OPTION__'];?>:</td>
					<td><input type="hidden" name="track_display_option" id="track_display_option" size="61" class="inputFld" value="<?php echo $trackDetail['track_display_option'];?>" />
					<ul id="sortable">
					<?php 
					if(is_array($display_option_array)){
						foreach ($display_option_array as $option){
							echo '<li id="'.$option.'">'.$option.'</li>';
						}
					}
					?>
					
					</ul>
					
					</td>
					
				</tr>
				<tr>
					<td></td>
					<td><button class="frmButtonGreen" type="submit"><?php echo $html->lang['__UPDATE__'];?></button>
						<button class="frmButtonGreen" type="button" onClick="closeFilePopUp()"><?php echo $html->lang['__CANCEL__'];?></button>
						</td>
				</tr>
			</table>
		</form>
		
		
		</div>
		</div>
		<script>
		var options = { 
		        target:        "#popup" ,  
		        resetForm: true 
		    };
			$("#frm").ajaxForm(options);
		</script>
<?php 
} else {
	echo 'Incorrect Request!';
}