<?php
if (!defined("APPLICATION_PATH")) {
    require_once '../../../config/config.php';
    session_start();
    $parentFolderId = intval($_GET ['parentFolderId']);
    $ajax_flag = true;
}
$lib = new library ();
include('breadcrumb.php');

//$sort = ($_GET['sort']=='undefined' || empty($_GET['sort']))?'trackFile':$_GET['sort'];
$libContentArray = $lib->fetchFolderContent($parentFolderId);
$html = new html();
if (is_array($libContentArray)) {
    echo '<div class="list">';

    // render folders
    if (count($libContentArray ['folders']) > 0) {
        echo '<ul>';
        $background = "#ffffff";
        $foldersCategories = "";
        $foldersOthers = "";
        foreach ($libContentArray ['folders'] as $folder)
            if ($folder ['privacy'] == "public") {
                $entry = '<li style="background:' . $background . '">';
                $entry .= '<div class="folder-name';
                $entry .= $folder['isCategory'] ? ' category' : '';
                $entry .= '" title="' . $folder ['folderName'] . '"><a href="/library?album=' . $folder ['folderId'] . '" onClick="listFolder(' . $folder ['folderId'] . '); return false;">';
                $icon = $folder ['isCategory'] ? 'folder_icon.png' : ($folder['hasMP3'] ? 'album.png' : 'folder_doc.png');
                $entry .= '<img src="' . COMMON_IMAGES_URL . $icon . '" valign="middle" hspace="25" />';
                $entry .= $folder ['folderName'] . '</a></div>';
                $entry .= '<div class="button-div">';
                if ($folder ['downloadable'] == "1" && !empty ($folder ['zip_file'])) {
                    $download_image_src = COMMON_IMAGES_URL . "download.png";
                    $download_image_alt = "Download Folder";
                    $file = "/media/zip/" . $folder ['zip_file'];
                    $entry .= '<a href="' . $file . '" title="Download \'' . $folder ['zip_file'] . '\'">';
                    $entry .= '<img src ="' . COMMON_IMAGES_URL . 'download.png' . '" valign="top"  style="margin-top: 5px"/></a>';
                }
                if ($folder['isCategory'] || !$folder['hasMP3'])
                    $foldersCategories .= $entry . "</div></li>\n";
                else {
                    $entry .= '<a href="javascript:;" onClick="playFolderPlaylist (\'' . $folder ['folderId'] . '\')"><img src ="' . COMMON_IMAGES_URL . 'play_button_small.png" alt="' . $html->lang ['__PLAY__'] . '" title="' . $html->lang ['__PLAY__'] . '" valign="top"  style="margin-top: 2px"/></a>';
                    $entry .= '<a href="javascript:;" onClick="addFolderToPlaylist(\'' . $folder ['folderId'] . '\')"><img src ="' . COMMON_IMAGES_URL . 'add_play_button_small.png" alt="' . $html->lang ['__ADD_TO_PLAYLIST__'] . '" title="' . $html->lang ['__ADD_TO_PLAYLIST__'] . '" valign="top"/></a>';
                    $foldersOthers .= $entry . "</div></li>\n";
                }
                // $background = ($background=="#ffffff")?'#efefef':'#ffffff';
            }
        echo $foldersCategories . "\n";
        echo $foldersOthers . "\n";
        echo '</ul>';
    }

    // render files
    if (count($libContentArray ['files']) > 0) {
        $helper_array = array(
            'album' => 'album',
            'title' => 'trackTitle',
            'artist' => 'artist',
            'filename' => 'trackFile',
            'year' => 'track_year',
            'track_number' => 'track_number'
        );
        /*
         * echo '<select id="filter" onChange="listFolder ('.$parentFolderId.')">'; echo '<option value="">Defualt Sorting</option>'; echo '<option value="trackTitle asc">Track Title Ascending </option>'; echo '<option value="trackTitle desc">Track Title descending </option>'; echo '<option value="album asc">Track Album Ascending </option>'; echo '<option value="album desc">Track Album Descending </option>'; echo '<option value="artist asc">Track Artist Ascending </option>'; echo '<option value="artist desc">Track Artist Descending </option>'; echo '<option value="file_size asc">File Size Ascending </option>'; echo '<option value="file_size desc">File Size Descending </option>'; echo '</select>';
         */
        echo '<ul>';
        $background = "#ffffff";
        $photo_file_id = 0;
        $photo_file_present = false;
        $title = '';
        $thumbGallery = '<div>';
        $path = $lib->fetchParentURL($parentFolderId);

        $files = $libContentArray['files'];

        foreach ($files as $file_in_folder)
            if ($file_in_folder ['privacy'] == "public") {
                $artist = ($file_in_folder ['artist']) ? $file_in_folder ['artist'] : "N/A";
                $album = ($file_in_folder ['album']) ? $file_in_folder ['album'] : "N/A";
                $id = $file_in_folder['trackId'];
                $file = $file_in_folder['trackFile'];
                $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                $music_file = false;
                $isCurrentFilePhoto = false;
                $ext_new_window = preg_replace('/\s+/', '', NEW_WINDOW_EXT); //Removing all whitespaces
                $ext_new_window_array = explode(",", $ext_new_window);

                if ($ext == 'mp3') {
                    $file_img = 'music_icon.png';
                    $music_file = true;

                    $display_option_string = $file_in_folder['track_display_option'] ?
                        $file_in_folder['track_display_option'] : DEFAULT_TRACK_NAME;
                    $display_option_array = explode("-", $display_option_string);
                    foreach ($display_option_array as $option)
                        if (!empty($file_in_folder[$helper_array[$option]])) {
                            //check if filename is used as title.
                            if ($option == "filename")
                                $title .= preg_replace("/\\.[^.\\s]{3,4}$/", "", $file);  //remove file extension
                            else
                                $title .= $file_in_folder[$helper_array[$option]] . ' - ';
                        } elseif ($option == "title")
                            $title .= preg_replace("/\\.[^.\\s]{3,4}$/", "", $file) . ' - ';  //remove file extension

                    if (empty($title))
                        $title = preg_replace("/\\.[^.\\s]{3,4}$/", "", $file);

                    $title .= "[" . $file_in_folder['duration'] . "]";

                    $li = '<div class="folder-name"><a href="/library?track=' . $id . '" title="Track details" onClick="fileTrack(\'' . $id . '\'); return false;">';
                } else {
                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') { // If photo
                        $width = $file_in_folder['width'];
                        $height = $file_in_folder['height'];
                        require_once "calculateImageProportion.php";
                        $proportion = calculateImageProportion($width, $height);
                        $isCurrentFilePhoto = true;
                        $photo_file_present = true;
                        $file_img = 'file_icon.png';
                        //$title = $file;
                        $thumbFullPath = MEDIA_URL . THUMBNAIL_DIR . THUMBNAIL_SUFFIX . $id . '.' . $ext;
                        $thumbGallery .= '<div class="img_thumb">
                            <a href="javascript:;"
                                link-type="galleyItem"
                                img-width="' . intval($width * $proportion). '"
                                img-height="' . intval($height * $proportion). '"
                                img-url="' . MEDIA_URL . THUMBNAIL_DIR . DISPLAY_SUFFIX . $id . '.' . $ext . '"
                                img-url-original="' . $path . $file . '"
                                onclick="openPhotoSwipe(\'' . $photo_file_id . '\')" >
                                    <img src="' . $thumbFullPath . '" style="max-width: ' . THUMBNAIL_WIDTH . 'px">
                            </a>
                        </div>';
                        $photo_file_id++;
                    } elseif (in_array($ext, $ext_new_window_array)) {
                        $file_img = $ext == 'pdf' ? 'pdf_icon.png' : 'file_icon.png';
                        $title = $file;
                        $li = '<div class="folder-name" title="' . $title . '"><a href="' . $path . $file . '" target="_blank" >';
                    } else { // that's for non-mp3 / non-image / non-PDF
                        $file_img = 'file_icon.png';
                        $title = $file;
                        $li = '<div class="folder-name" title="' . $title . '"><a href="' . $path . $file . '" download >';
                    }
                    $title .= " - [" . number_format(($file_in_folder['file_size'] / (1024 * 1024)), 2) . ' MB] ';
                }

                if (!$isCurrentFilePhoto) { // If not photo, list it here.
                    echo '<li style="background:' . $background . '">' . $li;
                    echo '<img src="' . COMMON_IMAGES_URL . $file_img . '" valign="middle" hspace="25" />';
                    echo rtrim($title, ' - ') . '</a></div>';

                    echo '<div class="manage-button-div">';
                    // PLAY and ADD icons
                    if ($music_file == true) {
                        echo '<a href="javascript:;"  onClick="playTrack(\'' . $id . '\')"><img src ="' . COMMON_IMAGES_URL . 'play_button_small.png" alt="' . $html->lang ['__ADD_TO_PLAYLIST__'] . 's" valign="top" title="' . $html->lang ['__PLAY__'] . '" alt="' . $html->lang ['__PLAY__'] . '" /></a>';
                        echo '<a href="javascript:;"  onClick="addToPlaylist(\'' . $id . '\',true)"><img src ="' . COMMON_IMAGES_URL . 'add_play_button_small.png" alt="' . $html->lang ['__ADD_TO_PLAYLIST__'] . 's" valign="top" title="' . $html->lang ['__ADD_TO_PLAYLIST__'] . '" alt="' . $html->lang ['__ADD_TO_PLAYLIST__'] . '" /></a>';
                    }
                    // DOWNLOAD icon
                    if ($file_in_folder ['downloadable'] == "1") {
                        $download_image_src = COMMON_IMAGES_URL . "download.png";
                        $download_image_alt = "Download '" . $file . "' (" . round($file_in_folder['file_size'] / 1024 / 1024, 2) . " MB)";
                        echo '<a href="' . $path . $file . '" title="' . $download_image_alt . '"  download>
                            <img src ="' . $download_image_src . '" alt="' . $download_image_alt . '" valign="middle" /></a>';
                    }

                    echo '</div>';
                    echo '</li>';
                }
                $title = "";
                $file = "";
                $parentFolder = "";
                // $background = ($background=="#ffffff")?'#efefef':'#ffffff';
            }
        echo '</ul>';

        // Gallery init
        if ($photo_file_present) {
            echo $thumbGallery . '</div>';
            include('gallery.php');
            initGallery();
        }
    }

    if ($parentFolderId != 0 and COMMENTS_PUBLICATIONS == 'Yes')
        print '<div id="hypercomments_widget"></div>';

    echo '</div>'; // end list div
    if ($ajax_flag == true) {
        $lib2 = new library ();
        $folderDetail = $lib2->getFolderDetails($parentFolderId);
        $title = $folderDetail['folderName'];

        print "<=||=>" . $title . "<=||=>";
    }
}
