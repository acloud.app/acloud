<?php
session_start();
require_once '../../../config/config.php';
if(empty($_SESSION['admin']['adminId'])) {
	header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
	die();
}

$addZip=false;
$folderId = intval($_GET['folderId']);
$lib = new library ();
$error=false;
// check if data is posted
if (! empty ( $_POST )) {
	$folderDetail = $lib->getFolderDetails ( $folderId );
	$dir_zip= MEDIA_PATH . "zip/";//путь к папке
	$dir= $lib->fetchParentFolders ( $folderId );//путь к папке
	$general = new general ();
	$ar=array();
	$folder_array = $general->scanDir($dir, $ar);//массив файлов для добавления в архив

	//проверка на наличие дубликатов имен
	if(($folderDetail['zip_file']!==NULL)) {
		$name=$folderDetail['zip_file'];
	} else {
		//Проверка в есть ли архив с таким именем в папке zip
		$file_zip = MEDIA_PATH . "zip/" . $folderDetail['folderName'] . ".zip";
		if (is_file($file_zip)) {

			$parent = $lib->getFolderDetails($folderDetail['parentId']);
			$name = $parent['folderName'] . '_' . $folderDetail['folderName'] . ".zip";

		} else {
			$name = $folderDetail['folderName'] . ".zip";
		}
	}
	//Имя архива
	$fileName = $dir_zip.$name;
	//Удаляем старый архив
	if (is_file($fileName) )
		unlink($fileName);

	//Создаем архив
	$zip = new ZipArchive();
	if ($zip->open($fileName, ZIPARCHIVE::CREATE)!==true) {
		$lib->errorMessage="Error creating archive";
		$error=false;
	} else {
		$error=true;
		//добавляем файлы в архив
		if (is_array ( $folder_array ))
			foreach ( $folder_array as $folder ) {
				$file=str_replace($dir, "", $folder);
				$zip->addFile($folder, $file);
			}
		@$zip->close();
	}


//Запись в базу
	if(is_file($fileName) and $error===true) {
		$data ['zip_file'] = $name;
		$data ['downloadable'] = 1;
		$data ['updateDate'] = date("Y-m-d H:i:s");
		$lib->updateFolder($folderId, $data);
		$addZip = true;
		$lib->message = "Archive created: " . $name;
	} else
		$lib->errorMessage="Error creating archive";
}

// get file record details
$folderDetail = $lib->getFolderDetails ( $folderId );
if (is_array ( $folderDetail )) {
	$html = new html ();
	?>
<script src="<?php echo COMMON_JS_URL?>jQueryForm.js"></script>
<div id="close-button" onClick="closeFilePopUp();">
	<img src="<?php echo COMMON_IMAGES_URL?>popup-close-button.png" />
</div>
<div id="popUpContentDiv">
	<div id="popUpBody">
		<div class="messageDiv">
<?php
	if (! empty ( $lib->message )) {
		echo $lib->message;
	} else if ($lib->errorMessage) {
		echo $lib->errorMessage;
	}
	?>
</div>
		<?php
		if($addZip==false) {

			?>
			<form method="post" id="frm"
				  action="<?php echo APPLICATION_URL . 'modules/library/ajax/addZip.php?folderId=' . $folderId; ?>"
				  enctype="multipart/form-data">
				<img src="/modules/common/images/loading_blockUI.gif" style="position:relative; left:40%; top: -20px; display: none; " id="img1">
				<table border="0" id="table1">
					<tr>
						<td class="text-property"><?php echo $html->lang['__DISPLAY_NAME__']; ?>:</td>
						<td> <?php echo $folderDetail['folderName']; ?> </td>
					</tr>
					<tr>
						<td class="text-property">The archive name will be: </td>
						<td> <?php
							//Проверка имени в ДБ
							if (($folderDetail['zip_file']!=NULL)) {
								$file_name=$folderDetail['zip_file'];
							} else {
								//Проверка в есть ли архив с таким именем в папке zip

								$file_zip = MEDIA_PATH . "zip/" . $folderDetail['folderName'] . ".zip";
								if (is_file($file_zip)) {
									$parent = $lib->getFolderDetails($folderDetail['parentId']);
									$file_name = $parent['folderName'] . '_' . $folderDetail['folderName'] . ".zip";
								} else
									$file_name = $folderDetail['folderName'] . ".zip";
							}

							print $file_name;
							?> </td>
					</tr>
					<tr>
						<td class="text-property">Size of files to be archived: </td>
						<!--<td><input type="text" name="zip_file" size="61" class="inputFld" value="<?php echo $folderDetail['zip_file']; ?>" /></td>-->
						<td><b>

								<?php
								$general = new general ();
								$dir = $lib->fetchParentFolders($folderId);//путь к папке
								$ar = array();
								$folder_array = $general->scanDir($dir, $ar);
								if (is_array($folder_array)) {
									$size = 0;
									$sum = count($folder_array);
									foreach ($folder_array as $folder)
										$size += filesize($folder);
									print $general->formatSizeUnits($size);
									if ($size>=ZIP_LIMIT)
										print '. <span style="color:red;">Can`t create archive greater, than '.ZIP_LIMIT_TEXT.'</span>';
								}
								?>
								<tr>
									<td class="text-property">Number of files:</td>
									<td> <?php
										echo $sum;
										if($size<ZIP_LIMIT) {
											print "<input type = 'hidden' name = 'addzip' value = '1' >";
											}
									?>

									</td>
								</tr>
							</b></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<?php if($size<ZIP_LIMIT){ ?>

							<button class="frmButtonGreen" onClick="formPost()"
									type="submit">CREATE</button>

							<?php } ?>
							<button class="frmButtonGreen" type="button"
									onClick="closeFilePopUp()"><?php echo $html->lang['__CANCEL__']; ?></button>
						</td>
					</tr>
				</table>
			</form>
			<?php
		}
		?>

	</div>
</div>
<script>
	function formPost() {
		$("#table1").hide();
		$("#img1").show();
		var options = {
			target: "#popup",
			resetForm: true,
		};
		$("#frm").ajaxForm(options);
	}
</script>
<?php
} else
	echo 'Incorrect Request!';