<?php
require_once '../../../config/config.php';
if (empty($_GET['playlist_id'])) {
    $response = array('error' => 'Playlist is not recognized!');
} else {
    $db = new db();
    $playlist_id = intval($_GET['playlist_id']);
    if (!($db->fetchRecord( // Check if playlist with this friendly_name exists
        TBL_PLAYLIST,
        'playlist_id',
        array(
            'playlist_id' => $playlist_id,
            'friendly_name' => $_GET['friendly_name']
        )
    )))
        $response = array('error' => 'Cannot load playlist!');
    else {
        if (count($db->result) < 1)
            $response = array('error' => 'No playlist found for this id and name!');
        else { // If playlist with this friendly_name exists, proceed
            if (!($db->fetchRecord( // Check if there any tracks for this playlist
                TBL_PLAYLIST_DATA,
                'track_id',
                array('playlist_id' => $playlist_id)
            )))
                $response = array('error' => 'Cannot load playlist details!');
            else {
                if (count($db->result) < 1)
                    $response = array('error' => 'No tracks found for this playlist!');
                else {
                    foreach ($db->result as $track)
                        $ids .= $track['track_id'] . "_";
                    $response['ids'] = $ids;
                    $db->updateRecord( // Update `last_accessed` column
                        TBL_PLAYLIST,
                        array("last_accessed" => date("Y-m-d H:i:s")),
                        array('playlist_id' => $playlist_id)
                    );
                }
            }
        }
    }
}
if(isset($response['error']))
    $response['ids'] = null;
echo json_encode($response);
