<?php
function setMp3Flag( $folderIdArray ) {
    global $lib;
    global $db;
    global $general;
    $folderIdArray = array_unique( $folderIdArray );
    $parentIdArray = array();
    foreach ( $folderIdArray as $folderId )
        foreach ( $lib->fetchArrayOfFolders( $folderId ) as $parentId )
            $parentIdArray[] = $parentId[ 'folderId' ];
    $folderIdArray = array_unique( array_merge( $folderIdArray, $parentIdArray ) );
    foreach ( $folderIdArray as $folderId ) {
        if ( $db->updateRecord(
            TBL_FOLDERS,
            array( 'hasMP3' => 1 ),
            array( 'folderId' => $folderId )
        ) )
            $general->log("SCAN_UPD", "Updated mp3 folder status for folder with id $folderId.");
        else
            $general->log("ERROR", "Cannot update mp3 folder status for folder with id $folderId.", true );
    }
}
