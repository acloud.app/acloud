<?php
//include config file
session_start();
require_once '../../../config/config.php';

if(empty($_SESSION['admin']['adminId'])) {
    header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
    die();
}

header('Content-Type: text/html; charset=utf-8');
$db = new db();
$lib = new library();

echo 'Processing folders: ';
print ($db->runQuery("UPDATE `folder` SET `privacy`='public'")."<br>");
echo 'Processing tracks: ';
print ($db->runQuery("UPDATE `tracks` SET `privacy`='public'")."<br>");

echo 'folders scanned!<br>';
