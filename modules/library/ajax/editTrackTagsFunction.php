<?php
function editTrackTags($fileId, $tagData)
{
    $lib = new library();
    $general = new general ();
    $trackRecord = $lib->getTrackDetails(intval($fileId)); // get file name from DB

    $TextEncoding = 'UTF-8';
    $getID3 = new getid3();
    $getID3->setOption(array('encoding' => $TextEncoding));
    $tag_writer = new getid3_writetags();
    $folderName = MEDIA_PATH . 'mp3/' . $lib->fetchParent($trackRecord['folderId']);

    //$general->log("DEBUG", "processing " . $folderName . $trackRecord['trackFile']);
    //$general->log("DEBUG", "-- tagData: ". print_r($tagData,true));

    $ThisFileInfo = $getID3->analyze($folderName . $trackRecord['trackFile']);
    $v2TagComments = $ThisFileInfo['id3v2']['comments'];
    if ($v2TagComments != NULL) {
        //$general->log("DEBUG", "-- found v2TagComments: " . print_r($v2TagComments,true));
        // Get all tags found in this file, we don't want anything to be missed
        $newTagData = (object)$v2TagComments; // Each tag should be converted to object, otherwise getID3 will complain
        $newTagData = (array)$newTagData; // While tagData should be an array

        // Replace only specified tags
        foreach ($tagData as $tag_key => $value)
            $newTagData[$tag_key] = $value;

        //foreach ($newTagData as $tag_key => $value)
        //    $general->log("DEBUG", "-- newTagData->$tag_key of type: ". gettype($value));

        unset ($newTagData['text']);
    } else {
        // If there is no original tags, don't care about them, just rewrite with what we have 
        $newTagData = $tagData;
        //$general->log("DEBUG", "-- didn't find v2TagComments");
    }
    //$general->log("DEBUG", "-- newTagData: ". print_r($newTagData,true));

    //set options
    $response = array('answer' => $tag_writer->filename = $folderName . $trackRecord['trackFile']);
    $tag_writer->tagformats = array('id3v2.3');
    $tag_writer->overwrite_tags = true;
    $tag_writer->remove_other_tags = false;
    $tag_writer->tag_encoding = $TextEncoding;

    $tag_writer->tag_data = $newTagData;

    if ($tag_writer->WriteTags()) {
        $response['answer'] .= "\nSuccessfully changed tags.";

        if (!empty($tag_writer->warnings)) {
            $response['answer'] .= ' There were some warnings:<br>' . implode('<br><br>', $tag_writer->warnings);
            $response['error'] = true;
            $general->log("ERROR", "Warnings while editing tags " . $folderName . $trackRecord['trackFile'] . ": " . print_r($tag_writer->warnings,true));
        }
        require_once "scanFile.php";
        $status = scanFile($folderName, $trackRecord['trackFile'], $trackRecord['folderId'], $fileId, null, true);
        if (array_key_exists("error", $status)) {
            $response['answer'] .= $status["error"];
            $response['error'] = true;
            $general->log("ERROR", "Error while scanning after edit " . $folderName . $trackRecord['trackFile'] . ": " . print_r($status,true));
        }
    } else {
        $response['answer'] .= 'Failed to write tags. ' . implode('<br><br>', $tag_writer->errors);
        $response['error'] = true;
        $general->log("ERROR", "Error editing tags " . $folderName . $trackRecord['trackFile'] . ": " . print_r($tag_writer->errors,true));
    }

    return $response;

}
