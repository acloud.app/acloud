<?php
if(!defined("APPLICATION_PATH")) {
    require_once '../../../config/config.php';
    session_start();
    $folderId = 0;
    $trackId = intval($_GET ['trackId']);
    $ajax_flag=true;
}

$lib = new  library ();

$arTrack = $lib->getTrackDetails($trackId);
$parentFolderId = $arTrack['folderId'];
include ('breadcrumb.php');

$teg_arr['title'] = 'Название';
$teg_arr['artist'] = 'Исполнитель';
$teg_arr['album'] = 'Альбом';
$teg_arr['year'] = 'Год';
$teg_arr['genre'] = 'Жанр';
$teg_arr['track_number'] = 'Номер трека';
$teg_arr['comment'] = 'Комментарий';
$tr = '';
$childFile = $lib->fetchParentFolders($folderId).$arTrack['trackFile'];
$getID3 = new getid3();
$ThisFileInfo = $getID3->analyze($childFile);
$duration = $ThisFileInfo['playtime_string'];

if ($ThisFileInfo['id3v2'] != NULL)
    if ($ThisFileInfo['id3v2']['comments'] != NULL)
        foreach ($teg_arr as $mp3tagName => $tagDisplayName)
            if (!empty($ThisFileInfo['id3v2']['comments'][$mp3tagName][0])) {
                $tr .= "<tr><td><b>" . $tagDisplayName . "</b></td><td>" . $ThisFileInfo['id3v2']['comments'][$mp3tagName][0] . "</td></tr>";
            }
$tr .= "<tr><td><b>Продолжительность</b></td><td>" . $duration . "</td></tr>";
print "<table style='margin-left: 10px;' cellspacing='10'>" . $tr . "</table>";

if($parentFolderId!=0 and COMMENTS_PUBLICATIONS=='Yes'){
    print '<div id="hypercomments_widget"></div>';
}
echo '</div>';// end list div

if ($ajax_flag==true)
    print "<=||=>" . $arTrack['album'] . "<=||=>" . $arTrack['artist'] . "<=||=>" . $title;
