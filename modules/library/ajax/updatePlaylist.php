<?php
require_once '../../../config/config.php';
if (empty($_GET['playlist_id']) || empty($_GET['friendlyName'])) {
    $response = array('error' => 'Playlist is not recognized!');
    echo json_encode($response);
} else {
    $db = new db();
    $playlistId = intval($_GET['playlist_id']);
    $friendlyName = $_GET['friendlyName'];

    if (!($db->updateRecord(
        TBL_PLAYLIST,
        array(
            "friendly_name" => $friendlyName,
            "last_accessed" => date("Y-m-d H:i:s")
        ),
        array(
            'playlist_id' => $playlistId
        )
    )))
        $response = array('error' => 'Cannot update playlist.');

    if (!isset($response['error']))
        $response = array(
            'playlist_id' => $playlistId,
            "friendly_name" => $friendlyName
        );
    echo json_encode($response);
}
