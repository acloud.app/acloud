<?php
//include config file
session_start();
require_once '../../../config/config.php';

if (empty($_SESSION['admin']['adminId'])) {
    header('location:' . APPLICATION_URL . 'admin/profile/login');
    die();
}

$path = '';
$parentFolderId = intval($_GET['parentFolderId']);
$photo_file_present = false;

function convertToHoursMins($time, $format = '%d:%02d')
{
    settype($time, 'integer');
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

echo '<input type="hidden" id="current_open_folder" value="' . $parentFolderId . '" />';
$lib = new library();
//get and generate breadcrumb
$array = $lib->fetchParentFolderInfo($parentFolderId);
$array = array_reverse($array);
if (is_array($array)) {
    echo '<div class="breadcrumb">';
    if ($_SESSION['admin']['groupId'] == "admin")
        echo '<a href="javascript:;" onClick="listFolder(0);">' . AUDIO_LIBRARY . '</a> &raquo; ';
    foreach ($array as $item) {
        if (!empty($item['folder'])) {
            $path .= $item ['folder'] . "/";
            echo '<a href="javascript:;" onClick="listFolder(' . $item['folderId'] . ');" >' . ucwords($item['folder']) . '</a> &raquo; ';
            $fid = $item['folderId'];
        }
    }
    echo '<a href="' . APPLICATION_URL . 'library?album=' . $fid . '" target="_blank" title="URL page" ><img src ="' . COMMON_IMAGES_URL . 'link.png" alt="URL page"  /></a>';
    echo '</div>';
}

//$sort = ($_GET['sort']=='undefined' || empty($_GET['sort']))?'trackFile':$_GET['sort'];
$libContentArray = $lib->fetchFolderContent($parentFolderId);

echo '<div id="angularTools">';

if (is_array($libContentArray)) {
    echo '<span ng-init="$root.parentFolderId = ' . $parentFolderId . '"></span>';
    if (isset($_GET['alert']) && $_GET['alert'])
        echo '<span ng-init="$root.incomingAlert = { value: \'' . $_GET['value'] . '\', type: \'' . $_GET['type'] . '\' }"></span>';
    echo '<div class="list" ng-controller="FolderListController">';
    //render folders
    if (count($libContentArray['folders']) > 0) {
        echo '<ul>';
        $background = "#ffffff";
        foreach ($libContentArray['folders'] as $folder) {
            echo '<li class="admin-list-li" style="background:' . $background . '"><div class="folder-name">
                <span  data-id="' . $folder['folderId'] . '" data-value="' . $folder['folderName'] . '"  id="dir_' . $folder['folderId'] . '">
                <a href="javascript:;"  onClick="listFolder(' . $folder['folderId'] . ');">';
            echo '<img src="' . COMMON_IMAGES_URL . 'folder_icon.png" valign="middle" hspace="30" />';
            echo $folder['folderName'] . '</a></span></div>';
            echo '<div class="manage-button-div">';

            echo '<a href="' . APPLICATION_URL . 'library?album=' . $folder['folderId'] . '" target="_blank" title="URL page" ><img src ="' . COMMON_IMAGES_URL . 'link.png" alt="URL page"  /></a>';
            echo '<a href="javascript:;" title="Edit" onClick="appendDirectoryEditing(\'' . $folder['folderId'] . '\')"><img src ="' . COMMON_IMAGES_URL . 'edit-icon.png" alt="Edit" /></a>';
            if ($folder['zip_file'] == null) {
                echo '<a href="javascript:;" title="Zip" onClick="addZip(\'' . $folder['folderId'] . '\')"><img src ="' . COMMON_IMAGES_URL . 'zip.png" alt="Zip" /></a>';
            } else {
                echo '<a href="javascript:;" title="Zip Delete" onClick="deleteZip(\'' . $folder['folderId'] . '\')"><img src ="' . COMMON_IMAGES_URL . 'ZipDelete.png" alt="Zip Delete" /></a>';

            }

            // privacy Icon
            if ($folder['privacy'] == "private") {
                $privacy_image_src = COMMON_IMAGES_URL . "lock.png";
                $privacy_image_alt = "Make Public";
                $privacy_value = "public";
            } else {
                $privacy_image_src = COMMON_IMAGES_URL . "unlock.png";
                $privacy_image_alt = "Make Private";
                $privacy_value = "private";
            }
            echo '<a href="javascript:;" title="' . $privacy_image_alt . '" onClick="changeFolderProperty(\'privacy\',\'' . $folder['folderId'] . '\',\'' . $privacy_value . '\')"><img src ="' . $privacy_image_src . '" alt="' . $privacy_image_alt . '" id="folder_privacy_' . $folder['folderId'] . '" /></a>';

            // download Icon
            if ($folder['downloadable'] == "1") {
                $download_image_src = COMMON_IMAGES_URL . "download.png";
                $download_image_alt = "Make non-downloadable";
                $download_value = 0;
            } else {
                $download_image_src = COMMON_IMAGES_URL . "download-disable.png";
                $download_image_alt = "Make downloadable";
                $download_value = 1;
            }
            if ($folder['zip_file'] !== null) {
                echo '<a href="javascript:;" title="' . $download_image_alt . '" onClick="changeFolderProperty(\'downloadable\',\'' . $folder['folderId'] . '\',\'' . $download_value . '\')"><img src ="' . $download_image_src . '" alt="' . $download_image_alt . '" /></a>';
            }
            // isCategory Icon
            if ($folder['isCategory']) {
                $category_image_src = COMMON_IMAGES_URL . "folder_icon.png";
                $category_image_alt = "Make it album";
                $isCategory = 0;
            } else {
                $category_image_src = COMMON_IMAGES_URL . "album.png";
                $category_image_alt = "Make it folder";
                $isCategory = 1;
            }
            echo '<a href="javascript:;" title="' . $category_image_alt . '" onClick="changeFolderProperty(\'isCategory\',\'' . $folder['folderId'] . '\',\'' . $isCategory . '\')"><img src ="' . $category_image_src . '" alt="' . $category_image_alt . '" /></a>';

            // delete Icon
            echo '<a href="javascript:;" title="Delete"  onClick="deleteFolder(\'' . $folder['folderId'] . '\')"><img src ="' . COMMON_IMAGES_URL . 'trash_can.png" alt="Delete" /></a>';
            echo '</div>';
            echo '</li>';

            $background = ($background == "#ffffff") ? '#efefef' : '#ffffff';
        }

        echo '</ul>';
    }

    //render files
    if (count($libContentArray['files']) > 0) {
        $files = $libContentArray['files'];

        /*
        echo '<select id="filter" onChange="listFolder ('.$parentFolderId.')">';
        echo '<option value="">Defualt Sorting</option>';
        echo '<option value="trackTitle asc">Track Title Ascending </option>';
        echo '<option value="trackTitle desc">Track Title descending </option>';
        echo '<option value="album asc">Track Album Ascending </option>';
        echo '<option value="album desc">Track Album Descending </option>';
        echo '<option value="artist asc">Track Artist Ascending </option>';
        echo '<option value="artist desc">Track Artist Descending </option>';
        echo '<option value="file_size asc">File Size Ascending </option>';
        echo '<option value="file_size desc">File Size Descending </option>';
        echo '</select>';
        */

        echo '<ul>';
        $background = "#ffffff";
        $photo_file_id = 0;

        foreach ($files as $file) {

            $file_name = $file['trackFile'];
            $id = $file['trackId'];

            if ($file['music_file']) {
                $tags = '<tags file-id="' . $id . '"></tags>';
                $file_img = 'music_icon.png';
                $music_file = true;
                $title = $file['trackTitle']; //trackTitle is holding title value stored in mp3 file and NOT filename
                sscanf($file['duration'], "%d:%d", $minutes, $seconds);
                $duration = convertToHoursMins($minutes);
                $duration .= ":" . $seconds;
                $file_property = number_format(($file['file_size'] / (1024 * 1024)), 2) . ' MB / ' . $duration;
            }
            else {
                $tags = '';
                $file_img = 'file_icon.png';
                $music_file = false;
                $title = $file_name;
                $file_property = number_format(($file['file_size'] / (1024 * 1024)), 2) . ' MB ';
            }

            // URL page button
            $btns = '<a href="' . APPLICATION_URL . 'library?track=' . $id . '" target="_blank" title="URL page" >
                <img src ="' . COMMON_IMAGES_URL . 'link.png" alt="URL page"  /></a>';

            // Edit name button
            $btns .= '<a href="javascript:;" class="trackEdit" title="Edit" onClick="editDisplayFile(\'' . $id . '\')">
                <img src ="' . COMMON_IMAGES_URL . 'edit-icon.png" alt="Edit" class="trackEdit" /></a>';

            // privacy Icon
            if ($file['privacy'] == "private") {
                $privacy_image_src = COMMON_IMAGES_URL . "lock.png";
                $privacy_image_alt = "Make Public";
                $privacy_value = 'public';
            } else {
                $privacy_image_src = COMMON_IMAGES_URL . "unlock.png";
                $privacy_image_alt = "Make Private";
                $privacy_value = 'private';
            }
            $btns .= '<a href="javascript:;" title="' . $privacy_image_alt . '" onClick="changeFileProperty(\'privacy\',\'' . $id . '\',\'' . $privacy_value . '\')"><img src ="' . $privacy_image_src . '" alt="' . $privacy_image_alt . '" /></a>';

            // download Icon
            if ($file['downloadable'] == "1") {
                $download_image_src = COMMON_IMAGES_URL . "download.png";
                $download_image_alt = "Make non-downloadable";
                $download_value = 0;
            } else {
                $download_image_src = COMMON_IMAGES_URL . "download-disable.png";
                $download_image_alt = "Make downloadable";
                $download_value = 1;
            }

            //show only if file is music file
            if ($music_file == true) {
                $btns .= '<a href="javascript:;" title="' . $download_image_alt . '" onClick="changeFileProperty(\'downloadable\',\'' . $id . '\',\'' . $download_value . '\')"><img src ="' . $download_image_src . '" alt="' . $download_image_alt . '" /></a>';
            }

            // delete Icon
            $btns .= '<a href="javascript:;" title="Delete" onClick="deleteFile(\'' . $id . '\')"><img src ="' . COMMON_IMAGES_URL . 'trash_can.png" alt="Delete" /></a>';

            $ext = '.' . strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

            echo '<li class="admin-list-li" style="background:' . $background . '"><div class="file-name"><div class="track-title">';
            echo '<img src="' . COMMON_IMAGES_URL . $file_img . '" valign="middle" hspace="40" /> ';
            //echo rtrim($title,' - ').'<br><span data-id="'.$id.'" data-value="'.substr($file_name, 0, strrpos($file_name, ".")).'" id="file_'.$id.'">'.$file_name.'</span> <br><span>('.$file_property.')</span><br><br>'.$btns.'</div> <div id="tags_'.$id.'" class="track-tags">'.$trackTitle.$artist.$album.$year.' </div>';
            echo '<span class="filename"
                        data-id="' . $id . '"
                        data-value="' . substr($file_name, 0, strrpos($file_name, ".")) . '"
                        data-extn="' . $ext . '"
                        data-path="' . MEDIA_URL . 'mp3/' . $path . '"
                        id="file_' . $id . '">
                    <a href="' . MEDIA_URL . 'mp3/' . $path . $file_name . '" download>' . $file_name . '</a>
                </span><br>
                <span>(' . $file_property . ')</span><br><br>'
                . $btns . '</div>';

            // Thumbnails
            if ($ext == '.jpg' || $ext == '.jpeg' || $ext == '.png') {
                $photo_file_present = true;
                $thumbFullPath = MEDIA_URL . THUMBNAIL_DIR . THUMBNAIL_SUFFIX . $id . $ext;
                echo '<div class="img_thumb">
                        <a href="javascript:;"
                                link-type="galleyItem"
                                img-width="' . $file['width'] . '"
                                img-height="' . $file['height'] . '"
                                img-url="' . MEDIA_URL . 'mp3/' . $path . $file_name . '"
                                onclick="openPhotoSwipe(\'' . $photo_file_id . '\')" >
                            <img class="admin" src="' . $thumbFullPath . '">
                        </a>
                    </div>';
                $photo_file_id++;
            }

            echo '<div id="tags_' . $id . '" class="track-tags">' . $tags . ' </div>';

            //echo '<div class="manage-button-div">';

            echo '</div>';
            echo '</li>';
            $title = "";
            $background = ($background == "#ffffff") ? '#efefef' : '#ffffff';
        }
        echo '</ul>';

        // gallery init
        if ($photo_file_present) {
            include('gallery.php');
            initGallery();
        }
    }

    // Batch tag editor tool
    if ($parentFolderId != 0)
        echo '<batch-tag-editor></batch-tag-editor>';

    // New folder tool
    $html = new html();
    echo '<div id="newFolderForm" class="addNew_Forms">
        <button type="button" onclick="setupNewFolderForm(' . $parentFolderId . ');" class="frmButtonGreen">' .
        $html->lang['__CREATE_NEW_FOLDER__'] . '</button></div>';

    // Uploader tool
    include '../template/default/admin/uploaderInclude.php';

    // Close #angularTools div and init Angular tools
    echo '</div><script>angular.bootstrap($("#angularTools"), ["AudioCMSApp"]);</script>';

}
