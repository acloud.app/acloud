<?php
//include config file
require_once '../../../config/config.php';
session_start();
$session_id = session_id();
if (!empty($_GET['query'])) {
    $query = $_GET['query'];
}
$library = new library();
$general = new general();

$trackList = $library->folderTrackList($_GET['id']);
$folderList = $library->result;
$helper_array = array(
    'album' => 'album',
    'title' => 'trackTitle',
    'artist' => 'artist',
    'filename' => 'trackFile',
    'year' => 'track_year',
    'track_number' => 'track_number'
);
if (is_array($folderList)) {
    $parentFolder = "";
    foreach ($folderList as $trackList)
        foreach ($trackList as $trackRecord) {
            if ($trackRecord['privacy'] == 'public') {
                if ($trackRecord['music_file'] == 1) {
                    $image = COMMON_IMAGES_URL . "music.png";
                    $file = $library->fetchParentURL($trackRecord['folderId']) . $trackRecord['trackFile'];
                    $artist = (!empty($trackRecord['artist'])) ? $trackRecord['artist'] : 'N/A';
                    $album = (!empty($trackRecord['album'])) ? $trackRecord['album'] : 'N/A';

                    $display_option_string = ($trackRecord['track_display_option']) ? $trackRecord['track_display_option'] : DEFAULT_TRACK_NAME;
                    $display_option_array = explode("-", $display_option_string);
                    $title = "";
                    foreach ($display_option_array as $option) {
                        if (!empty($trackRecord[$helper_array[$option]]))
                            $title .= $trackRecord[$helper_array[$option]] . ' - ';
                        elseif ($option == "title")
                            $title .= preg_replace("/\\.[^.\\s]{3,4}$/", "", $trackRecord['trackFile']) . ' - ';  //remove extension
                    }
                    if (empty($title)) {
                        $title = preg_replace("/\\.[^.\\s]{3,4}$/", "", $trackRecord['trackFile']);
                        $title = str_replace('"', "&#039;", $title);
                    }
                    $_SESSION['playlist'][] = array('title' => $title,
                        'album' => $trackRecord['album'],
                        'trackId' => $trackRecord['trackId'],
                        'mp3' => $file,
                        'poster' => $image,
                        'artist' => $artist);
                    $List .= '{"trackId":"' . $trackRecord['trackId'] . '","displayName":"' . rtrim(str_replace('"', '\"', $title), " - ") . '","image":"' . $image . '","artist":"' . $artist . '","id":"' . $trackRecord['trackId'] . '","album":"' . $album . '","file":"' . $file . '"},';
                    $artist = "";
                    $parentFolder = "";
                }
            }
        }
    echo '{"matches":[';
    echo rtrim($List, ",");
    echo "]}";
}