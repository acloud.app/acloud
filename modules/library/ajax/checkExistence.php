<?php

session_start();
require_once '../../../config/config.php';

if(empty($_SESSION['admin']['adminId'])) {
    header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
    die();
}

$parentFolderId = intval($_GET['parent']);
$fileName = $_GET['name'];

$lib = new library();
$parentFolder = $lib->fetchParentFolders($parentFolderId);

if (file_exists( $parentFolder . $fileName )) {
    $answer = array( 'error' => 'File with such name already exists in this directory.' );
}
else {
    $answer = array( 'answer' => 'File ready to go.' );
}

array_push($answer, array('file' => array('name' => $fileName, 'full_path' => $parentFolder . $fileName)));

echo json_encode( $answer );