<?php
//include config file
session_start();
require_once '../../../config/config.php';
if(empty($_SESSION['admin']['adminId'])) {
	header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
	die();
}

$general = new general ();
$folderId = intval($_GET ['folderId']);
$lib = new library ();

// check if data is posted
if (! empty ( $_POST )) {
	$data ['folderName'] = $_POST ['folderName'];
	$data ['zip_file'] = $_POST ['zip_file'];
	$data ['updateDate'] = date ( "Y-m-d H:i:s" );
	$lib->updateFolder ( $folderId, $data );
}

// get file record details
$folderDetail = $lib->getFolderDetails ( $folderId );
if (is_array ( $folderDetail )) {
	$html = new html ();
	?>
<script src="<?php echo COMMON_JS_URL?>jQueryForm.js"></script>
<div id="close-button" onClick="closeFilePopUp();">
	<img src="<?php echo COMMON_IMAGES_URL?>popup-close-button.png" />
</div>
<div id="popUpContentDiv">
	<div id="popUpBody">
		<div class="messageDiv">
<?php
	if (! empty ( $lib->message )) {
		$html->renderMessage ( $lib->message, false );
	} else if ($lib->errorMessage) {
		$html->renderErrorMessage ( $lib->errorMessage );
	}
	?>
</div>
	<form method="post" id="frm"
			action="<?php echo APPLICATION_URL.'modules/library/ajax/folderEdit.php?folderId='.$folderId; ?>"
			enctype="multipart/form-data">
			<table border="0">
				<tr>
					<td class="text-property"><?php echo $html->lang['__DISPLAY_NAME__'];?>:</td>
					<td><input type="text" name="folderName" size="61"
						class="inputFld"
						value="<?php echo $folderDetail['folderName'];?>" /></td>
				</tr>
				<tr>
					<td class="text-property"><?php echo $html->lang['__ZIP_FILE__'];?>:</td>
					<!--<td><input type="text" name="zip_file" size="61" class="inputFld" value="<?php echo $folderDetail['zip_file'];?>" /></td>-->
					<td><select name="zip_file" class="inputFld">
							<option value="">-- None --</option>
					<?php
	$folder_array = $general->scanFolder ( $lib->fetchParentFolders ( $folderId ));
	if (is_array ( $folder_array )) {
		foreach ( $folder_array as $folder ) {
			$extn = pathinfo ( $folder, PATHINFO_EXTENSION );
			if ($extn == "zip" || $extn == "rar") {
				if ($folder == $folderDetail ['zip_file']) {
					echo '<option value="' . $folder . '" selected="selected">' . $folder . '</option>';
				} else {
					echo '<option value="' . $folder . '">' . $folder . '</option>';
				}
			}
		}
	}
	?>
					</select></td>
				</tr>
				<tr>
					<td></td>
					<td><button class="frmButtonGreen" type="submit"><?php echo $html->lang['__UPDATE__'];?></button>
						<button class="frmButtonGreen" type="button"
							onClick="closeFilePopUp()"><?php echo $html->lang['__CANCEL__'];?></button>
					</td>
				</tr>
			</table>
		</form>


	</div>
</div>
<script>
		var options = { 
		        target:        "#popup" ,  
		        resetForm: true 
		    };
			$("#frm").ajaxForm(options);
		</script>
<?php
} else {
	$general->errorTrace('fd9ff600: Incorrect request to folderEdit');
	echo 'Incorrect Request!';
}