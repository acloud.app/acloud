<?php
session_start();
require_once '../../../config/config.php';

if(empty($_SESSION['admin']['adminId'])) {
    header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
    die();
}

$parentFolderId = intval($_POST['parent']);

$lib = new library();
$db = new db();
$general = new general();
include ("scanFile.php");
include ("setMP3FlagFunction.php");
$parentFolder = $lib->fetchParentFolders($parentFolderId);

function codeToMessage($code) {
    switch ($code) {
        case UPLOAD_ERR_INI_SIZE:
            $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
            break;
        case UPLOAD_ERR_FORM_SIZE:
            $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
            break;
        case UPLOAD_ERR_PARTIAL:
            $message = "The uploaded file was only partially uploaded";
            break;
        case UPLOAD_ERR_NO_FILE:
            $message = "No file was uploaded";
            break;
        case UPLOAD_ERR_NO_TMP_DIR:
            $message = "Missing a temporary folder";
            break;
        case UPLOAD_ERR_CANT_WRITE:
            $message = "Failed to write file to disk";
            break;
        case UPLOAD_ERR_EXTENSION:
            $message = "File upload stopped by extension";
            break;

        default:
            $message = "Unknown upload error";
            break;
    }
    return $message;
}

if (!empty($_FILES)) {
    $myFile = $_FILES['file'];
    $path_parts = pathinfo( $myFile[ 'name' ] );
    $prohibited_extensions_string = preg_replace('/\s+/', '', PROHIBITED_EXTENSIONS); // RegEx removes white spaces
    $prohibited_extensions_array = explode(',', $prohibited_extensions_string); // Split with comma

    if ($myFile["error"] !== UPLOAD_ERR_OK)
        $answer = array( 'error' => 'An error occurred: ' . codeToMessage($myFile["error"]) );
    elseif ( in_array( $path_parts[ 'extension' ], $prohibited_extensions_array ) ){
        $answer = array(
            'error' => 'Can\'t upload a file [ ' . $myFile[ 'name' ] . ' ] with following extension: .'
                . $path_parts[ 'extension' ]
        );
        $general->logToDB ('FILE', 'ERROR', $answer[ 'error' ]);
    }
    else {
        if (file_exists( $parentFolder . $myFile['name'] )) {
            $answer = array( 'error' => 'File with such name already exists.' );
        }
        else {
            if (!(move_uploaded_file( $myFile['tmp_name'], $parentFolder . $myFile['name'] ))) {
                $answer = array( 'error' => 'Unable to save file.' );
            }
            else {
                $answer = scanFile ($parentFolder, $myFile['name'], $parentFolderId);
                if($answer['action'] == 'delete')
                    unlink($parentFolder . $myFile['name']);
                else {
                    chmod($parentFolder . $myFile['name'], 0777);
                    if ( $answer[ 'answer' ][ 'file' ][ 'isMP3' ] )
                        setMp3Flag( array( 0 => $parentFolderId ) );
                }
            }
        }
    }
}
else
    $answer = array( 'error' => 'No files sent.');

echo json_encode( $answer );