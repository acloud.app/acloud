<?php
//include config file

session_start();
require_once '../../../config/config.php';
include 'clearMP3FlagFunction.php';
include 'scanFile.php';

if(empty($_SESSION['admin']['adminId'])) {
	header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
	die();
}

$lib = new library();
if(!empty($_GET['action'])) {
	if($_GET['action']=="delete"){
		//delete file form file system
		$db = new db();
		$db->fetchRecord(TBL_TRACKS,"*",array('trackId'=>$_GET['fileId']));
		$trackRecord = $db->result[0];
		$file = $trackRecord['trackFile'];
		$fullPath = $lib->fetchParentFolders($trackRecord['folderId']) . htmlspecialchars_decode($file);
		$result = array(
			'file' => array(
				'name' => $file,
				'full_path' => $fullPath,
				'server_delete_status' => unlink($fullPath)
			));
		// Delete thumbnail also
		$extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
		if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png') {
			$ext = ($extension == 'jpg' || $extension == 'jpeg')?'.jpg':'.png';
			$thumbName = THUMBNAIL_SUFFIX . $_GET['fileId'] . $ext;
			$thumbFullPath = MEDIA_PATH . THUMBNAIL_DIR . $thumbName;
			$result['thumb'] = array(
				'name' => $thumbName,
				'full_path' => $thumbFullPath,
				'delete_status' => unlink($thumbFullPath)
			);
		}
		$result['file']['db_delete_status'] = $db->deleteRecord(TBL_TRACKS, array('trackId'=>$_GET['fileId']));
		if ( $extension == 'mp3' )
			clearMP3Flag( $trackRecord[ 'folderId' ], false, true );
	} else {
		$data[$_GET['action']] = $_GET['value'];
		$result = $lib->updateTrack(intval($_GET['fileId']), $data);
	}
}
else $result = array( 'status' => 'error', 'response' => 'No action.' );

echo json_encode( $result );