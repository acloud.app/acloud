<?php
session_start();
require_once '../../../config/config.php';

if (empty($_SESSION['admin']['adminId'])) {
    header('location:' . APPLICATION_URL . 'admin/profile/login');
    die();
}

if (!empty($_POST)) {
    $folderId = intval($_POST['id']);
    $folderName = trim($_POST['directoryName']); // trim: remove whitespaces from the beginning and end of a string
    $folderName = str_replace('"', "'", $folderName);
    $lib = new library();

    $folderDetails = $lib->getFolderDetails($folderId);
    $parentPath = $lib->fetchParentFolders($folderDetails['parentId']);

    $oldDirectoryName = $lib->fetchParentFolders($folderId);
    $newDirectoryName = $parentPath . $folderName . '/';

    if (file_exists($newDirectoryName)) // file_exists: checks whether a file or directory exists
        $response = array('answer' => 'Folder with such name already exists.', 'error' => true);
    elseif (!(rename($oldDirectoryName, $newDirectoryName))) {
        $response = array('answer' => 'Failed to rename folder.', 'error' => true);
    } else {
        $data['folderName'] = $folderName;
        $data['updateDate'] = date("Y-m-d H:i:s");
        $lib->updateFolder($folderId, $data);
        $response = array(
            'answer' => 'Successfully changed folder name.',
            'name' => $newDirectoryName
        );
    }
}

echo json_encode($response);
