<?php
//include config file

session_start();
require_once '../../../config/config.php';
include 'deleteZipFunction.php';
include 'clearMP3FlagFunction.php';
include 'scanFile.php';

if(empty($_SESSION['admin']['adminId'])) {
	header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
	die();
}

$lib = new library();
$folderId = intval($_GET['folderId']);

function deleteDirectory($lib, $db, $id){
	$directoryContent = $lib->fetchFolderContent($id);
	if(is_array($directoryContent)){
		if(count($directoryContent['files']) > 0){
			foreach($directoryContent['files'] as $file){
				$db->deleteRecord(TBL_TRACKS, array('trackId' => $file['trackId']));
			}
		}
		if(count($directoryContent['folders']) > 0){
			foreach($directoryContent['folders'] as $folder){
				deleteDirectory($lib, $db, $folder['folderId']);
			}
		}
	}
	clearMP3Flag( $id );
	deleteZip($lib, $id);
	$db->deleteRecord(TBL_FOLDERS, array('folderId' => $id));
}

if(!empty($_GET['action'])) {
	if($_GET['action']=="delete" && $folderId != 0){
		$db = new db();
		$general = new general();
		$db->getRecordOrDie(TBL_FOLDERS,"*",array('folderId' => $folderId),"1b794fb5: No data while looking for folder $folderId");
		$folderRecord = $db->result[0];
		$file = $lib->fetchParentFolders($folderRecord['folderId']);

		deleteDirectory($lib, $db, $folderId);
		$general->removedir($file);
	} else {
        $data[$_GET['action']]=$_GET['value'];
        $result=$lib->updateFolder($_GET['folderId'],$data);
        //change in files
        $result=$lib->updateFolderTracks($_GET['folderId'],$data);
	}
}
