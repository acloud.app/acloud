<?php
require_once '../../../config/config.php';
if (empty($_GET['link']) || empty($_GET['friendlyName'])) {
    $response = array('answer' => 'Playlist is not recognized!');
    echo json_encode($response);
} elseif (empty($_GET['email'])) {
    $response = array('answer' => 'Email cannot be empty!');
    echo json_encode($response);
} else {
    $link = $_GET['link'];
    $friendlyName = $_GET['friendlyName'];
    $email = $_GET['email'];
    $subject = 'Playlist ' . $friendlyName;
    $message = "С Вами поделились плейлистом {$friendlyName}!\n";
    $message .= "Можете открыть записи по этой ссылке: {$link}.\n\n";
    $message .= $_SERVER['SERVER_NAME'] . " - " . APPLICATION_TITLE;
    $headers = array(
        "MIME-Version: 1.0",
        "Content-type: text/plain; charset=UTF-8",
        "From: " . COMPANY_NAME . " <noreply@" . $_SERVER['SERVER_NAME'] . ">",
        "Subject: {$subject}",
        "X-Mailer: PHP/" . phpversion()
    );

    if (mail($email, $subject, $message, implode("\r\n", $headers)))
        $response = array('answer' => 'E-mail успешно отправлен.');
    else
        $response = array('answer' => 'Ошибка, e-mail не может быть отправлен.');

    echo json_encode($response);
}
