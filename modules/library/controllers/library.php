<?php
class library {
	public $result, $message,$debugMessage;
	private $dbObj, $general;
	private function setDBObj() {
		$this->dbObj = new db ();
	}
	private function setGeneral() {
		$this->general = new general ();
	}
	
	/**
	 * ******************** ADMIN FUNCTIONS***********************************
	 */
	
	public function getTrackDetails($id){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$con['trackId']=$id;
		if ($this->dbObj->getRecordOrDie ( TBL_TRACKS.' as t', "*", $con, "3930a6fa: No data - track $id can't be read from DB" )) {
			$array['data']=$this->dbObj->result[0];
		} else {
			$array['errorMessage']="3930a6fa: Error while reading trackId $id from DB";
		}
		return $array['data'];
	}

	public function getSettings($id){
		$array=array();
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		if ($this->dbObj->fetchRecord (TBL_SETTINGS.' as t', "*",'', '','','name')) {
			$array_ol=$this->dbObj->result;
			foreach($array_ol as $k=>$v){
				$array[$v['name']]=array('value'=>$v['value'],'help'=>$v['help'], 'type'=>$v['type']);
			}
		} else {
			$array['errorMessage']="Error while getting settings from DB";
		}
		return $array;
	}

	public function updateSettings($name,$data) {
		$this->setDBObj();
		$this->setGeneral();

		if($this->dbObj->updateRecord(TBL_SETTINGS, $data, array('name'=>$name))){
			return true;
		} else {
			$array['errorMessage']="1c4f6cb4: There is technical issue with updating File record. Please contact Administrator";
			return $array;
		}
	}
	
	public function getFolderDetails($id){
	    if ($id==0) return false;
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$con['folderId']=$id;
		if ($this->dbObj->getRecordOrDie ( TBL_FOLDERS.' as t', "*", $con, "8f4b9586: No data, while getting folder $id" )) {
			$array['data']=$this->dbObj->result[0];
		} else {
			$array['errorMessage']=$this->message="8f4b9586: Error getting folder $id. Please contact webmaster";
		}
		return $array['data'];
	}

	/********************* FRONT END METHODS *************************************/
	
	public function trackList($con="",$specCon="") {
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		
		//get total rows
		$condition=$con['condition'];
		$rowCount=$this->dbObj->getRows(TBL_TRACKS. ' as t',$condition);
		$countPages=ceil($rowCount/RECORDS_PER_PAGE);
		$array['totalPages']=$countPages;
		$currentPage=$con['page']?$con['page']:1;
		$array['currentPage']=$currentPage;
		
		$start=(($currentPage-1)*RECORDS_PER_PAGE);
		$limit=RECORDS_PER_PAGE;

		$fields="*";
		$order=!empty($con['order'])?$con['order']:'updateDate desc,addDate desc,trackTitle asc';
		$specCon= !empty($specCon)?$specCon:"";
	
		if($this->dbObj->fetchRecord(TBL_TRACKS." as t",$fields,$condition," AND ", $specCon,$order,$start,$limit)){
			$array['data']=$this->dbObj->result;
		} else {
			$array['errorMessage']="6ad89f9d: There is technical issue with fetching Track List. Please contact Administrator";
		}
		return $array;
	}
	
	public function folderTrackList ($folderId,$sort=""){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$result = $this->fetchFolderContent($folderId);
		if(count($result['files'])>0) {
			$this->result[]= $result['files'];
		} 
		
		if(count($result['folders'])>0) {
			foreach($result['folders'] as $folder) {
				$this->folderTrackList($folder['folderId']);
			}
		}
	}
	
	public function playListTrackList ($ids,$sort=""){
		if (is_object ( $this->dbObj ) === false) {
			$this->setDBObj ();
			$this->setGeneral ();
		}
		$id_array = explode(",", $ids);
		foreach ($id_array as $id) {
		    if($this->dbObj->getRecordOrError(TBL_TRACKS." as t","*",array('music_file'=>1)," AND ", "trackId in (".$id.")")){
			    $array[]=$this->dbObj->result[0];
		    } else {
    			$array['errorMessage']="92321644: Error while fetching playlist $ids. Please contact Administrator";
    		}
		}
		return $array;
	}

    public function totalFiles($con="",$specCon="") {
        $this->dbObj = new db();
        $specCon=!empty($specCon)?$specCon:"";
        $count= $this->dbObj->getRows(TBL_TRACKS,$con, " AND ",$specCon);
        return $count;
    }
    
	public function totalTracks($con="",$specCon="") {
		$this->dbObj = new db();
		$con['music_file']=1;
		$specCon=!empty($specCon)?$specCon:"";
		$count= $this->dbObj->getRows(TBL_TRACKS,$con, " AND ",$specCon);
		return $count;
	}
	
	public function totalFolders($con="",$specCon="") {
		$this->dbObj = new db();
		$specCon=!empty($specCon)?$specCon:"";
		$count= $this->dbObj->getRows(TBL_FOLDERS,$con, " AND ",$specCon);
		return $count;
	}
	
	public function updateTrack($fileId,$data) {
		$this->setDBObj();
		$this->setGeneral();
		$data['updateDate'] = date("Y-m-d H:i:s");
		if($this->dbObj->updateRecord(TBL_TRACKS, $data, array('trackId'=>$fileId))){
			$this->message ="Track Updated!";
			return true;
		} else {
			$array['errorMessage']="9a6db361: There is technical issue with updating File record. Please contact Administrator";
			return $array;
		}
	}
	
	public function updateFolder($folderId,$data) {
		$this->setDBObj();
		$this->setGeneral();
		$data['updateDate'] = date("Y-m-d H:i:s");
		if($this->dbObj->updateRecord(TBL_FOLDERS, $data, array('folderId'=>$folderId))){
			$this->message ="Folder Updated!";
			return true;
		} else {
			$array['errorMessage']="b9fb77d7: There is technical issue with updating folder record. Please contact Administrator";
			return $array;
		}
	}
	
	public function updateFolderTracks($folderId,$data) {
		$this->setDBObj();
		$this->setGeneral();
		$data['updateDate'] = date("Y-m-d H:i:s");
		if($this->dbObj->updateRecord(TBL_TRACKS, $data, array('folderId'=>$folderId))){
			return true;
		} else {
			$array['errorMessage']="c823086f: There is technical issue with updating Track record. Please contact Administrator";
			return $array;
		}
	}

	public function recentFolders() {
		$this->dbObj = new db();
		$this->setGeneral();
		//get total rows
	
		$fields="*";
		$order='addDate desc, folderName asc';
		$specCon= !empty($specCon)?$specCon:"";
	
		if($this->dbObj->fetchRecord(TBL_FOLDERS." as t",$fields,""," AND ", $specCon,$order,0,10)){
			$array=$this->dbObj->result;
		} else {
			$array['errorMessage']="390affe3: There is technical issue with fetching Folder List. Please contact Administrator";
		}
		return $array;
	}

	public function fetchFolderContent($folderId = 0, $con=array(),$sort="") {
		$this->dbObj = new db();
		$this->setGeneral();
		
		$folderCon = $con;
		$folderCon ['parentId'] = $folderId;
		
		if($this->dbObj->fetchRecord(TBL_FOLDERS." as f","* ",$folderCon," AND ", "","isCategory desc,folderName asc")) {
			$array['folders']=$this->dbObj->result;
		} else {
			echo $array['errorMessage']="aad56c1f: There is technical issue with fetching Folder List. Please contact Administrator";
		}
		
		$trackCon = $con;
		$trackCon ['folderId'] = $folderId;
		
		$sort=!empty($sort)?$sort:"-track_number desc, trackTitle asc, trackFile asc";
		
		if($this->dbObj->fetchRecord(TBL_TRACKS." as t","* ",$trackCon," AND ", "",$sort)){
			$array['files']=$this->dbObj->result;
		} else {
			$array['errorMessage']="4ce521e6: There is technical issue with fetching File List. Please contact Administrator";
		}
		return $array;
	}

	public function fetchArrayOfFolders ($folderId = 0,&$arrayOfFolders=array()) {
		if($folderId===0)
			return $arrayOfFolders;

		$folderCon['folderId'] = $folderId;
		$this->dbObj = new db();
		$count = count($arrayOfFolders);
		if($this->dbObj->getRecordOrDie(TBL_FOLDERS." as f","folderName, parentId",$folderCon,
          "299f767f: No data - folderId $folderId doesn't exist in DB")) {
			$arrayOfFolders[$count]['folder']   = $this->dbObj->result[0]['folderName'];
			$arrayOfFolders[$count]['folderId'] = $folderId;
			if ($this->dbObj->result[0]['parentId'] != 0)
				$this->fetchArrayOfFolders($this->dbObj->result[0]['parentId'],$arrayOfFolders);
		} else
			echo ("ec7028c1: There is technical issue with fetching Folder List. Please contact Administrator.");
		return $arrayOfFolders;
	}

    /**
     * @param $folderId
     * @return string - relative (to MEDIA_PATH) path
     */
    public function fetchParent ($folderId) {
		$parentsArray = $this->fetchArrayOfFolders ($folderId);
		$parentsArray = array_reverse ( $parentsArray );
		$parentFolder="";
		if (is_array ( $parentsArray ))
			foreach ( $parentsArray as $parent )
				$parentFolder .=  $parent ['folder'] . "/";
        return $parentFolder;
    }

    //Return full path for '$folderId'
	public function fetchParentFolders ($folderId) {
		if($folderId==0)
			return MEDIA_PATH."mp3/";
        else
            return MEDIA_PATH."mp3/" . htmlspecialchars_decode ($this->fetchParent($folderId));
	}

    //Return URL for '$folderId'
    public function fetchParentURL ($folderId) {
        if($folderId==0)
            return MEDIA_URL.'mp3/';
        else
            return MEDIA_URL.'mp3/' . $this->fetchParent($folderId);
    }

    public function fetchParentFolderInfo ($folderId = 0,&$array=array()) {
        if($folderId===0)
            return $array;

        $this->dbObj = new db();
        $count = count($array);
        if ( $this->dbObj->getRecordOrError(
                TBL_FOLDERS." as f",
                "folderName, parentId, zip_file, isCategory, hasMP3",
                array( 'folderId' => $folderId )
        ) ){
            $array[$count]['folder']    =$this->dbObj->result[0]['folderName'];
            $array[$count]['isCategory']=$this->dbObj->result[0]['isCategory'];
            $array[$count]['hasMP3']    =$this->dbObj->result[0]['hasMP3'];
            $array[$count]['zip_file']  =$this->dbObj->result[0]['zip_file'];
            $array[$count]['folderId']  =$folderId;
            if($this->dbObj->result[0]['parentId']!=0)
                $this->fetchParentFolderInfo($this->dbObj->result[0]['parentId'],$array);
        } else
            $array['errorMessage']= "f2ed62f1: There is technical issue with fetching Folder List $folderId";
        return $array;
    }

}
