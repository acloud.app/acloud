(function () {
    angular
        .module('AudioCMSApp')
        .directive(
            'tags',
            function ($rootScope,
                      $timeout,
                      $http) {
                return {
                    restrict: 'E', // Looks for this element name
                    templateUrl: APPLICATION_URL + 'modules/library/template/default/admin/folder-list-tags.html',
                    scope: {
                        fileId: '=fileId' // In scope of each file we declare its id so we can work with tags further
                    },
                    link: function (scope) {
                        scope.isTagRequestInProgress = false;
                        scope.isVisibilityChanged = false;
                        scope.blankEssentialTags = [
                            {
                                name: 'title',
                                is_shown: true
                            },
                            {
                                name: 'album',
                                is_shown: false
                            },
                            {
                                name: 'artist',
                                is_shown: false
                            },
                            {
                                name: 'track_number',
                                is_shown: false
                            }
                        ];
                        /*scope.newTagArray = ['title', 'artist', 'album', 'track_number'];
                         scope.newTagValue = null;*/
                        scope.saveChanges = function (id, tag, isNewAndEssential) {
                            scope.isTagRequestInProgress = true;
                            scope.tagInputValue = document.getElementsByClassName(id + "_" + tag)[0].value;
                            $http({
                                method: 'POST',
                                url: APPLICATION_URL + 'modules/library/ajax/folderTags.php',
                                data: {
                                    request: 'update_tag',
                                    data: {
                                        file_id: id,
                                        tag_name: tag,
                                        tag_value: scope.tagInputValue,
                                        is_new_and_essential: isNewAndEssential
                                    }
                                }
                            })
                                .then(
                                    function successCallback(response) {
                                        scope.isTagRequestInProgress = false;
                                        if (response.data.type == 'success') {
                                            $rootScope.showGlobalAlert(response.data.value, response.data.type);
                                            var file = $rootScope.tagsArray.filter(function (element) {
                                                    return element.id == id;
                                                }),
                                                essentialTags = file[0].essential_tags,
                                                otherTags = file[0].other_tags;
                                            if (isNewAndEssential)
                                                essentialTags.push({
                                                    name: tag,
                                                    value: scope.tagInputValue,
                                                    is_shown: (tag == 'title') // If title - true, otherwise - false
                                                });
                                            else {
                                                essentialTags.forEach(function (item) {
                                                    if (item.name == tag)
                                                        item.value = scope.tagInputValue;
                                                });
                                                otherTags.forEach(function (item) {
                                                    if (item.name == tag)
                                                        item.value = scope.tagInputValue;
                                                });
                                            }
                                        }
                                        else
                                            $rootScope.showGlobalAlert(response.data.value, response.data.type);
                                    },
                                    function errorCallback(response) {
                                        scope.isTagRequestInProgress = false;
                                        $rootScope.showGlobalAlert(response.statusText, 'error');
                                        console.error(response);
                                    });
                        };
                        scope.deleteTag = function (id, tag, isEssential) {
                            scope.isTagRequestInProgress = true;
                            $http({
                                method: 'POST',
                                url: APPLICATION_URL + 'modules/library/ajax/folderTags.php',
                                data: {
                                    request: 'delete_tag',
                                    data: {
                                        file_id: id,
                                        tag_name: tag,
                                        is_essential: isEssential
                                    }
                                }
                            })
                                .then(
                                    function successCallback(response) {
                                        scope.isTagRequestInProgress = false;
                                        if (response.data.type == 'success') {
                                            $rootScope.showGlobalAlert(response.data.value, response.data.type);
                                            var node = document.getElementById(id + '_' + tag);
                                            node.parentNode.removeChild(node);
                                            var file = $rootScope.tagsArray.filter(function (element) {
                                                    return element.id == id;
                                                }),
                                                essentialTags = file[0].essential_tags,
                                                otherTags = file[0].other_tags;
                                            if (isEssential)
                                                essentialTags.forEach(function (item, index, object) {
                                                    if (item.name == tag)
                                                        object.splice(index, 1);
                                                });
                                            else
                                                otherTags.forEach(function (item, index, object) {
                                                    if (item.name == tag)
                                                        object.splice(index, 1);
                                                });
                                        }
                                        else
                                            $rootScope.showGlobalAlert(response.data.value, response.data.type);
                                    },
                                    function errorCallback(response) {
                                        scope.isTagRequestInProgress = false;
                                        $rootScope.showGlobalAlert(response.statusText, 'error');
                                        console.error(response);
                                    });
                        };
                        scope.saveSorting = function (id) {
                            scope.isTagRequestInProgress = true;
                            var file = $rootScope.tagsArray.filter(function (element) {
                                    return element.id == id;
                                }),
                                essentialTags = file[0].essential_tags,
                                orderString = '',
                                orderCount = 0;
                            essentialTags.forEach(function (item) {
                                item.order = ++orderCount;
                                if (item.is_shown)
                                    orderString += item.name + '-';
                            });
                            orderString = orderString.substring(0, orderString.length - 1);
                            $http({
                                method: 'POST',
                                url: APPLICATION_URL + 'modules/library/ajax/folderTags.php',
                                data: {
                                    request: 'change_track_display_option',
                                    data: {
                                        file_id: id,
                                        track_display_option: orderString
                                    }
                                }
                            })
                                .then(
                                    function successCallback(response) {
                                        scope.isTagRequestInProgress = false;
                                        if (response.data.type == 'success') {
                                            $rootScope.showGlobalAlert(response.data.value, response.data.type);
                                            scope.isVisibilityChanged = false;
                                        }
                                        else
                                            $rootScope.showGlobalAlert(response.data.value, response.data.type);
                                    },
                                    function errorCallback(response) {
                                        scope.isTagRequestInProgress = false;
                                        $rootScope.showGlobalAlert(response.statusText, 'error');
                                        console.error(response);
                                    });
                        };
                        scope.resetSorting = function (id) {
                            var file = $rootScope.tagsArray.filter(function (element) {
                                return element.id == id;
                            });
                            return file[0].essential_tags.sort(function (a, b) {
                                var x = a.order,
                                    y = b.order;
                                return ( ( x < y ) ? -1 : ( ( x > y ) ? 1 : 0 ) );
                            });
                        };
                        scope.changeVisibility = function (id, tag) {
                            scope.isVisibilityChanged = true;
                            var file = $rootScope.tagsArray.filter(function (element) {
                                    return element.id == id;
                                }),
                                essentialTags = file[0].essential_tags;
                            essentialTags.forEach(function (item) {
                                if (item.name == tag)
                                    item.is_shown = !item.is_shown;
                            });
                        };
                        /*scope.createTag = function (id) {
                         var tagName = document.getElementById('newTagName_' + id).value,
                         tagValue = document.getElementById('newTagValue_' + id).value;
                         // tagVisibility = document.getElementById('new_' + TagVisibility_' + id).checked;
                         scope.isTagRequestInProgress = true;
                         console.log(tagVisibility);
                         $http({
                         method: 'POST',
                         url: APPLICATION_URL + 'modules/library/ajax/folderTags.php',
                         data: {
                         request: 'update_tag',
                         data: {
                         file_id: id,
                         tag_name: tagName,
                         tag_value: tagValue
                         }
                         }
                         })
                         .then(
                         function successCallback(response) {
                         scope.isTagRequestInProgress = false;
                         if (response.data.type == 'success') {
                         $rootScope.showGlobalAlert(response.data.value, response.data.type);
                         var file = $rootScope.tagsArray.filter(function (element) {
                         return element.id == id;
                         }),
                         essentialTags = file[0].essential_tags;
                         console.log(essentialTags);
                         /!*essentialTags.forEach(function (item) {
                         if (item.name == tag)
                         item.value = scope.tagInputValue;
                         });*!/
                         }
                         else
                         $rootScope.showGlobalAlert(response.data.value, response.data.type);
                         },
                         function errorCallback(response) {
                         scope.isTagRequestInProgress = false;
                         $rootScope.showGlobalAlert(response.statusText, 'error');
                         console.error(response);
                         });
                         };*/
                    }
                }
            }
        )
        .filter('findFile', function () {
            return function (filesArray, id) {
                return filesArray.filter(
                    function (element) {
                        return element.id == id;
                    }
                )
            }
        })
        .filter('removeUsed', function ($rootScope) {
            return function (filesArray, id) {
                var file = $rootScope.tagsArray.filter(function (element) {
                        return element.id == id;
                    }),
                    essentialTags = file[0].essential_tags;
                return filesArray.filter(function (element) {
                    for (var i = 0; i < essentialTags.length; i++)
                        if (element.name == essentialTags[i].name)
                            return false;
                    return true;
                })
            }
        })
        .controller(
            'FolderListController',
            function ($rootScope,
                      $scope,
                      $timeout,
                      $http) {
                $rootScope.tagsArray = [];
                $rootScope.showGlobalAlert = function (value, type) {
                    $rootScope.globalAlert = {
                        isShown: true,
                        value: value,
                        type: type
                    };
                    if (type != 'loading')
                        $timeout(function () {
                            $rootScope.globalAlert.isShown = false;
                        }, 10000); // Hiding alert after 10 seconds
                };
                $scope.getFolderTags = function () {
                    $http({
                        method: 'POST',
                        url: APPLICATION_URL + 'modules/library/ajax/folderTags.php',
                        data: {
                            request: 'get_folder_tags',
                            data: {
                                parent_folder_id: $rootScope.parentFolderId
                            }
                        }
                    })
                        .then(
                            function successCallback(response) {
                                if (response.data.type == 'success')
                                    $rootScope.tagsArray = response.data.files;
                                else
                                    console.error(response.data.value);
                            },
                            function errorCallback(response) {
                                $rootScope.showGlobalAlert(response.statusText, 'error');
                                console.error(response);
                            });
                };
                $scope.getFolderTags();
                if ($rootScope.incomingAlert != undefined)
                    $rootScope.showGlobalAlert($rootScope.incomingAlert.value, $rootScope.incomingAlert.type);
            }
        );
})();
