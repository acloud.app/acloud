(function() {
    'use strict';
    angular
        .module(
            'AudioCMSApp', // App name
            [ // Dependencies
                'dndLists', // Drag-n-drop; https://github.com/marceljuenemann/angular-drag-and-drop-lists
                'angularFileUpload' // Uploader; https://github.com/nervgh/angular-file-upload
            ]
        );
})();
