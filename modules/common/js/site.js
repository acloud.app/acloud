var activeLink = "",
	isPopState = false,
	oldURL = window.location.href,
	menuShown = false,
	playListShown = false;

// This function returns value of any parameter from URL. Will need that on popstate event.
function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split('&'),
		sParameterName;

	for(var i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
}

function toggleMobile (element, bool, action) {
	if (action == 'off') {
		$(element).hide();
	}
	else {
		$(element).toggle();
	}
	bool = ($(element).css('display') == 'block');
}

$(document).ready(
		function($) {

			$(window).on("popstate", function(){

				var URL = document.location.href,
					isMobile = false;

				menuShown = ($('#header-links').css('display') == 'block');
				playListShown = ($('#playList').css('display') == 'block');
				isMobile = ($('.mobile-navigation').css('display') == 'block');

				// LOGIC:
				// If menu or playlist is open, we close both and replace URL in state it should be;
				// Otherwise, we should compare oldURL and URL:
					// If we are in library, and popstate gives us library URL, we shall use listFolder or listTrack;
					// Otherwise, we should use loadContent.

				if ((menuShown || playListShown) && isMobile) {
					toggleMobile('#header-links', menuShown, 'off');
					toggleMobile('#playList', playListShown, 'off');
					window.history.pushState(null, document.title, oldURL);
				}
				else {
					// Back button used. This bool is needed for functions loadContent, listFolder and fileTrack.
					isPopState = true;
					if (URL.indexOf('library') > -1) { // If current URL refers to library, then...
						if (oldURL.indexOf('library') > -1) { // and if oldURL refers to library, then...
							// we check whether it's album or track:
							// if album -> listFolder(id);
							// if track -> fileTrack(id);
							// neither -> it's library root.
							if (typeof getUrlParameter('album') != 'undefined') {
								listFolder(getUrlParameter('album'));
							}
							else if (typeof getUrlParameter('track') != 'undefined') {
								fileTrack(getUrlParameter('track'));
							}
							else {
								listFolder(0);
							}
						}
						else { // Otherwise can't use library methods to load info;
							loadContent(URL + '&ajax=true', 'content_root');
						}
					}
					else { // Otherwise can't use library methods to load info.
						loadContent(URL + '&ajax=true', 'content_root');
					}
					// Update oldURL
					oldURL = URL;
				}
			});

			$('#mask').click(function() {
				$(this).hide();
				$('#popUpDiv').hide();
				$('#popUpDiv1').hide();
				$('.links').removeClass("active-link");
				$('.links>a').addClass("active-link-anchor");
			});

			$(".link > a,.link").click(function() {
				$(".link>a").removeClass("normal-link-anchor");
				$(".link>a").addClass("active-link-anchor");
				$(".link").addClass("active-link");
			});

			$(".link > a,.link").mouseenter(function() {
				if (activeLink != "library") {
					$(".link>a").removeClass("normal-link-anchor");
					$(".link>a").addClass("active-link-anchor");
					$(".link").addClass("active-link");
				}
			});

			$(".link > a,.link").mouseleave(function() {
				if (activeLink != "library") {
					$(".link").removeClass("active-link");
					$(".link>a").addClass("normal-link-anchor");
					$(".link>a").removeClass("active-link-anchor");
				}
			});

			if (window.location.href.indexOf("library") > -1) {
				activeLink = "library";
				$(".link>a").removeClass("normal-link-anchor");
				$(".link>a").addClass("active-link-anchor");
				$(".link").addClass("active-link");
			}

		});

function historyPush (URL) {
	if (!isPopState) { // If it's false, we push URL to history:
		// Remove any "ajax" parameters, otherwise it'll generate bad URL
		URL = URL.replace(/&ajax=true/g, "").replace(/\?ajax=true/g, "");
		// We don't really need first parameter here.
		// Second one is title, yet for now it's ignored by most of the browsers. So, it's optional.
		// Last parameter is ULR.
		window.history.pushState(null, document.title, URL);
		oldURL = URL;
	}
	else {
		// In case we need to travel more through history, we don't push anything to history.
		isPopState = false;
	}
}

function loadContent(link, container) {

	$('#header').show();
	var ajaxUrl = link + '?ajax=true&popup=true';
    
	if (link.indexOf("library") > -1) {
		activeLink = "library";
		$("#libraryLink").addClass("active-link-anchor");
		$("#mobile-navigation-content").text(globalLibraryTitle);
		document.title = globalApplicationTitle;
	} else {
		activeLink = "";
		$(".link").removeClass("active-link");
		$(".link>a").addClass("normal-link-anchor");
		$(".link>a").removeClass("active-link-anchor");
		var title = (link).split('/');
		$("#mobile-navigation-content").text(title[title.length-1]);
	}
   
	$.ajax({
		url : ajaxUrl,
		success : function(data) {
			$("#" + container).html(data);
			$(".main-container").css({'height':(($(".profile-center").height())+'px')});
			console.log($(".profile-center").height());
			$(".profile-center").resize(function() {
				$(".main-container").css({'height':($(".profile-center").height()+'px')});
			});
			window.scrollTo(0, 0);
			historyPush(link);
		}
	});
	return false;
}

function listFolder(folder) {
	var URL = APPLICATION_URL+'modules/library/ajax/folderList.php';

	$('#header').show();
	$("#mobile-navigation-content").text(globalLibraryTitle);
	$.ajax({
		url :  URL+'?parentFolderId='+folder,
		success : function(data) {
			window.scrollTo(0, 0);
			m=data.split('<=||=>');
			t='';
			if(typeof m[1]!=='undefined'){t=m[1];}

			document.title = COMPANY_NAME + ': ' + t;
			$("#track-list").html(m[0]);
			$("#playList").css('min-height',$("#track-list").height()+13);
			if($("#track-list").height()+13<$(window).height()-125){
				$("#playList").css('min-height',$(window).height()-125);
			}

			var albumURL = ( folder > 0 ) ? ( '?album=' + folder ) : '';
			historyPush(APPLICATION_URL + 'library' + albumURL);
		}
	});

	$.cookie("album", folder);

    my_hypercomments(folder);

}

function fileTrack(track){

	var URL = APPLICATION_URL+'modules/library/ajax/fileTrack.php';

	$('#header').show();
	$("#mobile-navigation-content").text(globalLibraryTitle);
	$.ajax({
		url: URL + '?trackId=' + track,
		success: function(data) {
			m=data.split('<=||=>');
			var t1 = '', t2 = '', t3 = '';
			if(m[1]!=='undefined'){t1=m[1];}
			if(m[2]!=='undefined'){t2=m[2];}
			if(m[3]!=='undefined'){t3=m[3];}
			document.title = COMPANY_NAME + ': ' + t1 + ' ' + t2 + ' ' + t3 + ' ';

			$("#track-list").html(m[0]);
			$("#playList").css('min-height',$("#track-list").height()+13);
			if($("#track-list").height()+13<$(window).height()-125){
				$("#playList").css('min-height',$(window).height()-125);
			}

			var trackURL = ( track > 0 ) ? ( '?track=' + track ) : '';
			historyPush(APPLICATION_URL + 'library' + trackURL);
		}
	});

	$.cookie("track", track);

    my_hypercomments(track);

}

function my_hypercomments(folder){
    if(folder!==0){
		jQuery(document).ready(function($){
		if(window.comment=='Yes') {
			comment_id=window.comment_id;
			_hcwp = window._hcwp || [];
			//66781
			_hcwp.push({widget: "Stream", widget_id: comment_id, xid: folder});
			(function () {
				HC_LOAD_INIT = true;
				var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
				var hcc = document.createElement("script");
				hcc.type = "text/javascript";
				hcc.async = true;
				hcc.src = ("https:" == document.location.protocol ? "https" : "http") + "://w.hypercomments.com/widget/hc/66726/" + lang + "/widget.js";
				var s = document.getElementsByTagName("script")[0];
				s.parentNode.insertBefore(hcc, s.nextSibling);
			})();
		}
    });
    }
}

$(window).on('load resize', (function() {
	var mobile = false,
		size = $('body').prop('clientWidth');
	if(($('.mobile-navigation').css('display') == 'block')) mobile = true;
	$('.jp-time-holder').css('width', (mobile?70:120));
	$('.jp-progress').css('width', size - (mobile?167:335));
	$('.jp-title').css('width', size - (mobile?237:475));
}));