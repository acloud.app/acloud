$.curCSS = function (element, attrib, val) {
    $(element).css(attrib, val);
};
$(document).ready(function() {

	$("#forgetPasswordDialog").dialog({
		autoOpen : false,
		height : 180,
		width : 500,
		modal : true
	});
	//for css 
	if ($.browser.msie && $.browser.version == 10) {
		  $("html").addClass("ie10");
		}
	
	if ($.browser.msie) {
			var width = $(window).width() - 105; 
			$("#side-bar").height($(document).height());
		} else {
			var width = $(window).width() - 105;
		}
	
	// alert(width);
	$("#main-content").width(width + "px");

	$("#dialogBtn").click(function() {
		$("#forgetPasswordDialog").dialog("open");
	});
	// for radio buttons
	$(".radioSet").buttonset();
	$(function() {
		$("#paneSelector").click(function() {
			$("#pane").slideToggle(700);
			$("#paneSM").slideToggle(700);
		});

		// create pane
		$(".sideMenuImage").mouseover(function() {
			var currentSrc = $(this).attr("src");
			var src = $(this).attr("src").replace(".png", "_over.png");
			if (currentSrc.indexOf("_over") >= 0) {

			} else {
				$(this).attr("src", src);
			}
		}).mouseout(function() {
			var name = $(this).attr("name");
			if (name == "") {
				var src = $(this).attr("src").replace("_over.png", ".png");
				$(this).attr("src", src);
			}
		});
	});

});
$(window).resize(function() {
	var width = $(window).width() - 100;
	// alert(width);
	$("#main-content").width(width + "px");
});
function selectAll() {
	var checkBoxes = $('input[class=check_id]').attr('checked', true);
	;
}

function deselectAll() {
	var checkBoxes = $('input[class=check_id]').attr('checked', false);
}

function selectDeselect() {
	if (document.getElementById("selectAll").checked == true) {
		selectAll();
	} else {
		deselectAll();
	}
}
function uncheck() {
	var flag = true;
	var checkBoxes = $('input[class=check_id]');
	checkBoxes.each(function() {
		if ($(this).attr('checked')) {

		} else {
			flag = false;
		}
	});
	$("#selectAll").attr('checked', flag);
}

/* function to load database table */
function loadDBTable(divId, url) {
	url = encodeURI(url);
	// alert(url);
	$("#" + divId).load(url, function() {
		// alert('Load was performed.');
	});
}

function multiActionDBTable(divId, url) {
	var checkBoxes = $('input[class=check_id]');
	url = url + "&recordId=";
	checkBoxes.each(function() {
		if ($(this).attr('checked')) {
			url = url + $(this).attr("value") + ",";
		}
	});
	url = encodeURI(url);
	// alert(url);
	loadDBTable(divId, url);
}

setTimeout(function() {
	$("#msgDiv").fadeOut("slow", function() {
		$("#msgDiv").attr("display", "None");
	});

}, 2000);

function toggleDisable(id) {
	var obj = $("#" + id);
	if (obj.attr('disabled') == true) {
		obj.attr('disabled', false);
	} else {
		obj.attr('disabled', true);
	}
}

function confirmDelete(div, param) {
	$("#dialog").html(
			'<div><h4>Do you Really want to delete record?</h4></div>').dialog(
			{
				modal : true,
				title : 'Delete Record',
				zIndex : 10000,
				autoOpen : true,
				width : 'auto',
				resizable : false,
				position:'center',
				buttons : {
					Yes : function() {
						loadDBTable(div, param);
						$(this).dialog("close");
					},
					No : function() {
						$(this).dialog("close");
					}
				},
				close : function(event, ui) {
					//$(this).dialog("close");
				}
			});
}

function multiActionConfirmDelete(div, param) {
	$("#dialog")
			.html(
					'<div><h4>Do you Really want to delete multiple record?</h4></div>')
			.dialog({
				modal : true,
				title : 'Delete Records',
				zIndex : 10000,
				autoOpen : true,
				width : 'auto',
				resizable : false,
				buttons : {
					Yes : function() {
						// $(obj).removeAttr('onclick');
						// $(obj).parents('.Parent').remove();
						multiActionDBTable(div, param);
						$(this).dialog("close");
					},
					No : function() {
						$(this).dialog("close");
					}
				},
				close : function(event, ui) {
					//$(this).dialog("close");
				}
			});
}

function generateLicenceLink(id) {
	var url = APPLICATION_URL
			+ 'modules/library/ajax/licensePaymentLink.php?id=' + id;
	$.ajax({
		url : url,
		success : function(data) {
			$("#paymentLink").val(data);
		}
	});
}