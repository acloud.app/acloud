(function () {
    'use strict';
    angular
        .module(
            'AudioCMSApp'
        )
        .directive(
            'batchTagEditor',
            function () {
                return {
                    restrict: 'E', // Looks for this element name
                    templateUrl: APPLICATION_URL + 'modules/library/template/default/admin/batch-tag-editor.html'
                }
            }
        )
        .controller(
            'batchTagEditorController',
            function ($rootScope,
                      $scope,
                      $http) {
                $scope.isSavingTagInProgress = false;
                $scope.isSaveTrackNameInProgress = false;
                $scope.isResetTrackNumberInProgress = false;
                $scope.isShowArtist = true;
                $scope.isShowAlbum = true;
                $scope.recursion = 'not_recursive';
                $scope.visibilityArray = [
                    {
                        name: 'album',
                        isVisible: false
                    },
                    {
                        name: 'artist',
                        isVisible: false
                    },
                    {
                        name: 'track_number',
                        isVisible: false
                    }
                ];
                $scope.trackNameOrder = [];
                $scope.saveTagsChanges = function (tag) {
                    $scope.isSavingTagInProgress = true;
                    $rootScope.showGlobalAlert('Saving changes, please wait...', 'loading');
                    var tag_value = (tag == 'album' ? $scope.albumTag : (tag == 'artist' ? $scope.artistTag : $scope.yearTag));
                    $http({
                        method: 'POST',
                        url: APPLICATION_URL + 'modules/library/ajax/batchTagEditor.php',
                        data: {
                            request: 'save_tag_changes',
                            data: {
                                tag: tag,
                                tag_value: tag_value,
                                recursion: $scope.recursion,
                                parent_folder_id: $rootScope.parentFolderId
                            }
                        }
                    })
                        .then(function successCallback(response) {
                            $scope.isSavingTagInProgress = false;
                            $rootScope.showGlobalAlert(response.data.value, response.data.type);
                            listFolder($rootScope.parentFolderId, true, response.data);
                        }, function errorCallback(response) {
                            $scope.isSavingTagInProgress = false;
                            $rootScope.showGlobalAlert(response.statusText, 'error');
                        });
                };
                $scope.getTrackNameOrderFromServer = function () {
                    $http({
                        method: 'POST',
                        url: APPLICATION_URL + 'modules/library/ajax/batchTagEditor.php',
                        data: {
                            request: 'get_track_name_order',
                            data: {
                                parent_folder_id: $rootScope.parentFolderId
                            }
                        }
                    })
                        .then(function successCallback(response) {
                            if (response.data.type == 'success') {
                                $scope.trackNameOrder = response.data.trackNameOrderObject;
                                for (var i = 0; i < $scope.visibilityArray.length; i++) // Immediately apply this to visibility array 
                                    for (var j = 0; j < $scope.trackNameOrder.length; j++)
                                        if ($scope.trackNameOrder[j].tag == $scope.visibilityArray[i].name) {
                                            $scope.visibilityArray[i].isVisible = true;
                                            break;
                                        }
                            }
                            else
                                $rootScope.showGlobalAlert(response.data.value, response.data.type);
                        }, function errorCallback(response) {
                            $rootScope.showGlobalAlert(response.statusText, 'error');
                        });
                };
                $scope.rearrangeTrackName = function (tag) {
                    if (!tag.isVisible)
                        $scope.trackNameOrder = $scope.trackNameOrder.filter(function (obj) {
                            return obj.tag != tag.name; // Returns all other elements that don't have with tag
                        });
                    else
                        $scope.trackNameOrder.push({'tag': tag.name});
                };
                $scope.saveTrackNameChanges = function () {
                    $scope.isSaveTrackNameInProgress = true;
                    $rootScope.showGlobalAlert('Saving changes, please wait...', 'loading');
                    $http({
                        method: 'POST',
                        url: APPLICATION_URL + 'modules/library/ajax/batchTagEditor.php',
                        data: {
                            request: 'save_track_name_changes',
                            data: {
                                track_name_order: $scope.trackNameOrder,
                                recursion: $scope.recursion,
                                parent_folder_id: $rootScope.parentFolderId
                            }
                        }
                    })
                        .then(function successCallback(response) {
                            $scope.isSaveTrackNameInProgress = false;
                            $rootScope.showGlobalAlert(response.data.value, response.data.type);
                            listFolder($rootScope.parentFolderId, true, response.data);
                        }, function errorCallback(response) {
                            $scope.isSaveTrackNameInProgress = false;
                            $rootScope.showGlobalAlert(response.statusText, 'error');
                            console.error(response);
                        });
                };
                $scope.resetTrackNumber = function () {
                    $scope.isResetTrackNumberInProgress = true;
                    $rootScope.showGlobalAlert('Saving changes, please wait...', 'loading');
                    $http({
                        method: 'POST',
                        url: APPLICATION_URL + 'modules/library/ajax/batchTagEditor.php',
                        data: {
                            request: 'reset_track_number',
                            data: {
                                recursion: $scope.recursion,
                                parent_folder_id: $rootScope.parentFolderId
                            }
                        }
                    })
                        .then(function successCallback(response) {
                            $scope.isResetTrackNumberInProgress = false;
                            $rootScope.showGlobalAlert(response.data.value, response.data.type);
                            listFolder($rootScope.parentFolderId, true, response.data);
                        }, function errorCallback(response) {
                            $scope.isResetTrackNumberInProgress = false;
                            $rootScope.showGlobalAlert(response.statusText, 'error');
                        });
                };
                // As soon as batchTagEditorController loads, execute:
                $scope.getTrackNameOrderFromServer();
            }
        );
})();
