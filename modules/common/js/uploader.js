(function(){

    angular.module('AudioCMSApp')

        .controller('UploadController', function($rootScope, $scope, $http, FileUploader){
            $scope.isCompletedAll = false;
            $scope.isUploaderError = false;
            $scope.error = [];

            $scope.uploader = new FileUploader({
                url: APPLICATION_URL + 'modules/library/ajax/upload.php'
            });

            $scope.uploader.onAfterAddingFile = function (item) {
                var name = item.file.name,
                    parent = $rootScope.parentFolderId;

                $http({
                    method: 'GET',
                    url: APPLICATION_URL + 'modules/library/ajax/checkExistence.php?parent=' + parent + '&name=' + name
                }).then(function successCallback(response) {
                    if ('undefined' !== typeof response.data["error"]) {
                        item.isSuccess = false;
                        item.isReady = false;
                        item.isError = true;
                        item['error'] = {
                            text: response.data['error'],
                            isExists: true
                        };
                    }
                }, function errorCallback(response) {
                    item['error'] = { text: "Can't check if file exists." };
                });
            };

            $scope.uploader.onBeforeUploadItem = function (item) {
                item.formData.push({ parent:$scope.parentFolderId});
            };

            $scope.uploader.onWhenAddingFileFailed = function(item) {
                item['error'] = { text: "Can't add file to queue." };
            };

            $scope.uploader.onErrorItem = function(item) {
                item['error'] = { text: "Can't upload file." };
            };

            $scope.uploader.onError = function(response, status) {
                $scope.isUploaderError = true;
                $scope.error.text = "Can't upload file(s).";
                $scope.error.response = response;
                $scope.error.status = status;
            };

            $scope.uploader.onCompleteItem = function(item, response) {
                if ('undefined' !== typeof response["error"]) { // If field `error` came from server
                    item.isSuccess = false;
                    item.isError = true;
                    item['error'] = { text: response['error'] };
                    console.error("Server error: " + response.err); // Output error details to console
                }
            };

            $scope.uploader.onCompleteAll = function() {
                if($scope.uploader.progress == 100) $scope.isCompletedAll = true;
            };

            $scope.removeItem = function(item){
                item.remove();
                if($scope.uploader.queue.length < 1){
                    $scope.isUploaderError = false;
                    $scope.error = [];
                }
            };

            $scope.clearQueue = function(){
                $scope.uploader.clearQueue();
                $scope.isUploaderError = false;
                $scope.error = [];
            };
        })
})();
