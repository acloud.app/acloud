<?php
/*
 * This script will be called through AJAX to render database table from database. This script will accept below value:
 * 
 */
//include config file
session_start();
require_once '../../../config/config.php';

if(empty($_SESSION['admin']['adminId'])) {
	header ( 'location:' . APPLICATION_URL.'admin/profile/login' );
	die();
}




//prepare values to be sent to create table
$divId = $_GET ['div'];
$noEdit=!empty($_GET['noEdit'])?true:false;
$noStatus=!empty($_GET['noStatus'])?true:false;
$controller = $_GET ['fetch'];
$page = ! empty ( $_GET ['page'] ) ? $_GET ['page'] : 1;
$searchKey = ! empty ( $_GET ['searchKey'] ) ? $_GET ['searchKey'] : '';
$recordsPerPage = ! empty ( $_GET ['records'] ) ? $_GET ['records'] : RECORDS_PER_PAGE;
$orderBy = ! empty ( $_GET ['order'] ) ? $_GET ['order'] : '';
$recordId = ! empty ( $_GET ['recordId'] ) ? $_GET ['recordId'] : '';
if (strpos ( $_SERVER ['HTTP_REFERER'], '/edit' )) {
	$referer = substr ( $_SERVER ['HTTP_REFERER'], 0, strpos ( $_SERVER ['HTTP_REFERER'], '/edit' ) );
} else if (strpos ( $_SERVER ['HTTP_REFERER'], '/add' )) {
	$referer = substr ( $_SERVER ['HTTP_REFERER'], 0, strpos ( $_SERVER ['HTTP_REFERER'], '/add' ) );
} else if (strpos ( $_SERVER ['HTTP_REFERER'], '/update' )) {
	$referer = substr ( $_SERVER ['HTTP_REFERER'], 0, strpos ( $_SERVER ['HTTP_REFERER'], '/update' ) );
} else {
	$referer = $_SERVER ['HTTP_REFERER'];
}
$action = ! empty ( $_GET ['action'] ) ? $_GET ['action'] : '';
$filter = ! empty ( $_GET ['filter'] ) ? $_GET ['filter'] : '';
$customButton = ! empty ( $_GET ['customButton'] ) ? $_GET ['customButton'] : '';
$customButtonFilter = ! empty ( $_GET ['customButtonFilter'] ) ? $_GET ['customButtonFilter'] : '';
$customButtonAction = ! empty ( $_GET ['customButtonAction'] ) ? $_GET ['customButtonAction'] : '';

$recordTable = new recordTable ( $controller, $referer );
if (empty ( $action )) {
	$recordTable->renderTable ( $divId, $page, $orderBy, $searchKey, $recordsPerPage , $noEdit, $filter, $customButton,$customButtonFilter,$customButtonAction,$noStatus);
} else {
	$recordTable->{$action} ( $recordId, $divId, $page, $orderBy, $searchKey, $recordsPerPage,$noEdit,$filter, $customButton, $customButtonFilter,$customButtonAction,$noStatus);
}
echo $recordTable->result;