<?php
/*
 * This script is not used any where in the project, but just for testing chart creation 
 */

//including config file 
require_once '../../../config/config.php';
//create the object of chart class
$chart= new chart("#b7b7b7");
//create a array having title & values 
//stats
$lib = new library();
$folder_11 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-11 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-11 months'))."'");
$folder_10 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-10 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-10 months'))."'");
$folder_9 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-9 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-9 months'))."'");
$folder_8 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-8 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-8 months'))."'");
$folder_7 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-7 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-7 months'))."'");
$folder_6 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-6 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-6 months'))."'");
$folder_5 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-5 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-5 months'))."'");
$folder_4 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-4 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-4 months'))."'");
$folder_3 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-3 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-3 months'))."'");
$folder_2 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-2 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-2 months'))."'");
$folder_1 = $lib->totalFolders('',"MONTH(addDate) = '".date('m',strtotime('-1 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-1 months'))."'");
$folder_0 = $lib->totalFolders('',"MONTH(addDate) = '".date('m')."' AND YEAR(addDate) = '".date('Y')."'");

$tracks_11 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-11 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-11 months'))."'");
$tracks_10 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-10 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-10 months'))."'");
$tracks_9 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-9 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-9 months'))."'");
$tracks_8 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-8 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-8 months'))."'");
$tracks_7 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-7 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-7 months'))."'");
$tracks_6 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-6 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-6 months'))."'");
$tracks_5 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-5 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-5 months'))."'");
$tracks_4 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-4 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-4 months'))."'");
$tracks_3 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-3 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-3 months'))."'");
$tracks_2 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-2 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-2 months'))."'");
$tracks_1 = $lib->totalTracks('',"MONTH(addDate) = '".date('m',strtotime('-1 months'))."' AND YEAR(addDate) = '".date('Y',strtotime('-1 months'))."'");
$tracks_0 = $lib->totalTracks('',"MONTH(addDate) = '".date('m')."' AND YEAR(addDate) = '".date('Y')."'");
$data = array (
				0=>	array('title'=>date("M Y",strtotime("-11 months")), 'values'=>array('folder'=>$folder_11, 'tracks'=>$tracks_11)),
				1=>	array('title'=>date("M Y",strtotime("-10 months")), 'values'=>array('folder'=>$folder_10, 'tracks'=>$tracks_10)),
				2=>	array('title'=>date("M Y",strtotime("-9 months")), 'values'=>array('folder'=>$folder_9, 'tracks'=>$tracks_9)),
				3=>	array('title'=>date("M Y",strtotime("-8 months")), 'values'=>array('folder'=>$folder_8, 'tracks'=>$tracks_8)),
				4=>	array('title'=>date("M Y",strtotime("-7 months")), 'values'=>array('folder'=>$folder_7, 'tracks'=>$tracks_7)),
				5=> array('title'=>date("M Y",strtotime("-6 months")), 'values'=>array('folder'=>$folder_6, 'tracks'=>$tracks_6)),
				6=> array('title'=>date("M Y",strtotime("-5 months")), 'values'=>array('folder'=>$folder_5, 'tracks'=>$tracks_5)),
				7=> array('title'=>date("M Y",strtotime("-4 months")), 'values'=>array('folder'=>$folder_4, 'tracks'=>$tracks_4)),
				8=> array('title'=>date("M Y",strtotime("-3 months")), 'values'=>array('folder'=>$folder_3, 'tracks'=>$tracks_3)),
				9=> array('title'=>date("M Y",strtotime("-2 months")), 'values'=>array('folder'=>$folder_2, 'tracks'=>$tracks_2)),
				10=> array('title'=>date("M Y",strtotime("-1 months")), 'values'=>array('folder'=>$folder_1, 'tracks'=>$tracks_1)),
				11=> array('title'=>date("M Y"), 'values'=>array('folder'=>$folder_0, 'tracks'=>$tracks_0)),
				); 
//echo "<pre>";
//var_dump($data);
$barColorCode=array('tracks'=>'#dc3912');
//set canvas size
$chart->width="700";
$chart->height="300";
$chart->textColor="#ffffff";	//text color (bottom & left text color)

//set canvas color
//if both canvas Color are same then background having one color
//$chart->canvasColor="#5c5a5d";	//used for background color(gradient top)
//$chart->canvasSecColor="#34373c";	//used for background color(gradient bottom)

header('content-type: image/png');
//$chart->renderStackedBarChart($data, $barColorCode);
imagepng ($chart->renderStackedBarChart($data, $barColorCode));