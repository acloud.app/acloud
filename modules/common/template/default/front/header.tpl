<?php
/*
 * $this will be object of html class
*/
$profile = new profile();
?>
<div id="header">
<div id="header-top">
<div class="header-logo">
<?php
if (LOGO_GRAPHIC=="Yes")
    $this->prepareLink(COMMON_IMAGES_URL."logo.png",APPLICATION_URL,'loadContent',true,'content_root',true,APPLICATION_TITLE);
else
    print '<h1 style="color: white;">'.LOGO_TEXT.'</h1>';
    ?>
</div><!-- header-logo closed -->
<div id="header-links">
<?php $this->fetch ( "information_infoPageList", array (
		'status' => ACTIVE_VALUE,
		'main_menu'=>1
) );
if (is_array ( $this->fetchedResult )) {
	echo '<ul>';
	echo '<li>';
			$this->prepareLink ( ucwords ( AUDIO_PLAYER_LINK ), 'library', 'loadContent', false );
			echo '</li>';
	foreach ( $this->fetchedResult as $result ) {
			echo '<li>';
			$this->prepareLink ( ucwords ( $result['cmsPageTitle'] ), 'information_content_page_' . $result['cmsPage'], 'loadContent', false );
			echo '</li>';
	}
	echo '</ul>';
}?>
</div><!-- header-logSign closed -->
<div class="mobile-navigation">
	<div id="mobile-navigation-menu"><span></span></div>
	<div id="mobile-navigation-content"><?php echo ucwords($this->result['data']['cmsPageTitle']);?></div>
	<div id="mobile-navigation-playlist"><span></span></div>
</div>
</div><!-- header-top closed -->
<div id="player">
<div id="music-player">
<?php $this->addPage("music-player","player");?>
</div>
</div><!-- header-bottom closed -->
</div>