<?php
/*
 * $this will be object of html class
 */
if (empty ( $_GET ['ajax'] )) {
	// add Session Favorite track
	if (! empty ( $_SESSION ['user'] ['favTrackId'] )) {
		$lib = new library ();
		$lib->toggleTrackToFav ( $_SESSION ['user'] ['favTrackId'], $_SESSION ['user'] ['userId'] );
		unset ( $_SESSION ['user'] ['favTrackId'] );
	}
	
	// add general css
	$this->addCommonCSS ( "style.css" );
	$this->addCommonCSSIE ( "styleIE.css" );
	$this->addCommonCSSIE ( "jquery-ui-bitcube.css" );
	// add general JS
	$this->addCommonJS ( "jquery-1.8.3.min.js" );
	$this->addCommonJS ( "jquery-ui-1.8.9.bitcube.js" );
	$this->addCommonJS ( "jQueryForm.js" );
	$this->addCommonJS ( "jquery.cookie.js" );
	$this->addCommonJS ( "site.js" );
	//$this->addCommonJS ( "jsConfig.php" );
	
	// add header
	
	$this->addHeader ();
	
	// add content
	echo '<div id="dialog"></div>';
	echo '<div id="content_root">';
}
$this->addPage ( $this->page );
if (empty ( $_GET ['ajax'] )) {
	echo '</div>';
	// add footer
	//$this->addFooter ();
}