<?php
/*
 * $this will be object of html class
 */
$page_info = explode("/", HOME_PAGE);
$page = (!empty($page_info[1]))?$page_info[1]:'default';
$module = (!empty($page_info[0]))?$page_info[0]:'library';
if($page_info[2]){
$this->controller = new $module ();
$fun = $page_info[2];
if ($page_info[3]) {
    $this->result = $this->controller->{$fun} ( $page_info[3] );
} else {
    $this->result = $this->controller->{$fun} ();
}
}
/*
if($module!="common"){
$this->addCSS("style.css",false,$module);
}
*/
$this->addPage($page,$module);