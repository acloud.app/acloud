<?php
/*
 * $this will be object of template class
*/
//add general css
$this->addCommonCSS("admin.css");
$this->addCommonCSS("dbTable.css");
$this->addCommonCSS("jquery-ui-bitcube.css");
$this->addCommonCSSIE ( "adminIE.css" );
$this->addCommonCSSIE("dbTable.css");
$this->addCommonCSSIE ( "jquery-ui-bitcube.css" );

//add general JS
$this->addCommonJS("jquery-1.8.3.min.js");
$this->addCommonJS("jquery-ui-1.8.9.bitcube.js");
//$this->addCommonJS("jsConfig.php");
$this->addCommonJS("admin.js");
$this->addCommonJS("angular-file-upload.min.js");
$this->addCommonJS("angular-drag-and-drop-lists.min.js");
$this->addCommonJS("angular-app.js");
$this->addCommonJS("folderList.js");
$this->addCommonJS("batchTagEditor.js");
$this->addCommonJS("uploader.js");


$this->addHeader();
//add header
echo '<div id="mask"></div>';
echo '<div id="dialog"></div>';
echo '<div id="popup"></div>';
//add left pane
$this->addCommonContent("left-pane");

//add content
$this->addPage($this->page);
//add footer
$this->addFooter();

