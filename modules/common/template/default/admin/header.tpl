<?php
/*
 * $this will be object of html class
 */
if (! empty ( $_SESSION ['admin'] ['adminId'] )) {
	?>
<div id="header">
	<div id="admin-login-info">
		<div id="login-info-text">
			<img src="<?php echo COMMON_IMAGES_URL; ?>business_user.png"
				align="right" valign="bottom" height="35" /><span class="whiteText"><b><?php echo $this->lang['__WELCOME__'];?> </b><?php echo $_SESSION['admin']['name'];?>   [<a
				href="<?php echo APPLICATION_URL.'admin/profile/session/adminLogout';?>"><?php echo $this->lang['__LOGOUT__'];?></a>]</span><br>
			<!--  <span class="whiteText"><?php echo $_SESSION['admin']['lastLogin'];?></span> -->
			<div class="float-right topAlign">[<a
				href="<?php echo APPLICATION_URL.'admin/profile/password';?>"><?php echo $this->lang['__CHANGE_PASSWORD__'];?></a>]</div>
		</div>
	</div>
	<!-- close admin-login-info-->
</div>
<!-- close header-->
<?php }?>