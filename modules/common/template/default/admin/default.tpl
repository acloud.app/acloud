<?php
/*
 * $this will be object of html class
 */
session_start();
if (empty ( $_SESSION ['admin'] ['adminId'] )) {
	$this->redirect ( APPLICATION_URL . 'admin/profile/login' );
	die ();
}
$lib= new library();
$general = new general();

$totalTracks  = false;
$totalFiles   = false;
$totalFolders = false;
$totalSize    = false;
if (defined ('MEMCACHE_HOST')) {
	$memcache_obj = memcache_connect(MEMCACHE_HOST, MEMCACHE_PORT);
	$totalTracks  = memcache_get($memcache_obj, APPLICATION_PATH.'totalTracks');
	$totalFiles   = memcache_get($memcache_obj, APPLICATION_PATH.'totalFiles');
	$totalFolders = memcache_get($memcache_obj, APPLICATION_PATH.'totalFolders');
	$totalSize    = memcache_get($memcache_obj, APPLICATION_PATH.'totalSize');
}

if (!$totalFiles) {
	$totalFiles = $lib->totalFiles();
	if (defined('MEMCACHE_HOST'))
		memcache_set ($memcache_obj, APPLICATION_PATH.'totalFiles', $totalFiles, 0, MEMCACHE_TTL);
}
if (!$totalTracks) {
	$totalTracks = $lib->totalTracks();
	if (defined('MEMCACHE_HOST'))
		memcache_set ($memcache_obj, APPLICATION_PATH.'totalTracks', $totalTracks, 0, MEMCACHE_TTL);
}
if (!$totalFolders) {
	$totalFolders = $lib->totalFolders();
	if (defined('MEMCACHE_HOST'))
		memcache_set ($memcache_obj, APPLICATION_PATH.'totalFolders', $totalFolders, 0, MEMCACHE_TTL);
}
if (!$totalSize) {
	$totalSizeArray = $general->getDirectorySize(MEDIA_PATH . '/mp3/');
	$totalSize = (int)($totalSizeArray['size'] / (1024 * 1024));
	if (defined('MEMCACHE_HOST'))
		memcache_set ($memcache_obj, APPLICATION_PATH.'totalSize', $totalSize, 0, MEMCACHE_TTL);
}
//$monthCon= "addDate >='".date("Y-m-d H;i:s",strtotime('-7 days'))."'";
//$newTracks = $lib->totalTracks($monthCon);
//$newFolders= $lib->totalFolders($monthCon);
?>

<div id="main-content">
	<div id="title-div">
		<div id="title-div-left">
			<h1><?php echo $this->lang['__DASHBOARD_ADMIN_PAGE_TITLE__']?></h1>
		</div>
		<div id="title-div-right">
			<span class="header2"><?php echo date("M");?></span><span
				class="header1"><?php echo date("d");?></span>
		</div>
	</div>

<div class="education-stats">
	<div class="education-stats-div border-right"><div  class="dashboard-right-number"><span class="whiteText font-size38 bold">
				<?php echo $totalFiles?></span></div>
		<div class="dashboard-right-text"><span class="whiteText font-size16">
				<?php echo $this->lang['__LIBRARY_STAT_TITLE_1__']?></span></div></div>

	<div class="education-stats-div border-right"><div  class="dashboard-right-number"><span class="whiteText font-size38 bold">
				<?php echo $totalTracks?></span></div>
		<div class="dashboard-right-text"><span class="whiteText font-size16">
				<?php echo $this->lang['__LIBRARY_STAT_TITLE_2__']?></span></div></div>

	<div class="education-stats-div border-right"><div  class="dashboard-right-number"><span class="whiteText font-size38 bold">
				<?php echo $totalFolders?></span></div>
		<div class="dashboard-right-text"><span class="whiteText font-size16">
				<?php echo $this->lang['__LIBRARY_STAT_TITLE_3__']?></span></div></div>

	<div class="education-stats-div" ><div  class="dashboard-right-number"><span class="whiteText font-size38 bold">
				<?php echo $totalSize?></span></div>
		<div class="dashboard-right-text"><span class="whiteText font-size16">
				<?php echo $this->lang['__LIBRARY_STAT_TITLE_4__']?></span></div></div>
</div>

	<div id="user-index">
		<div class="top-record">
			<div class="top-record-header">
				<span class="header2">Quick Links</span>
			</div>
			<div id="top-user-name" class="border-dot">
				<a href="<?php echo APPLICATION_URL.'admin/library/tracks'?>" class="top-user-text">Manage Library</a>
			</div>
			<?php if ( $_SESSION[ 'admin' ][ 'groupId' ] == "admin" ) { ?>
				<div id="top-user-name" class="border-dot">
					<a href="<?php echo APPLICATION_URL.'admin/profile/users'?>" class="top-user-text">Manage Users</a>
				</div>
				<div id="top-user-name" class="border-dot">
					<a href="<?php echo APPLICATION_URL.'admin/profile/password'?>" class="top-user-text">Change Password</a>
				</div>
				<div id="top-user-name" class="border-dot">
					<a href="<?php echo APPLICATION_URL.'admin/settings/web-settings'?>" class="top-user-text">Update Settings</a>
				</div>
			<?php } ?>
		</div>
	</div>

</div>
<!-- Main content closed -->
