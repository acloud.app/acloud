<?php
/*
 * This Pags will be used to display left side links in administration area of
 * the website.
 */
if (! empty ( $_SESSION ['admin'] ['adminId'] )) {
	// echo $this->page;
	switch ($this->page) {
		case 'tracks' :
			$active_tracks = "active-tab";
			$over2 = '_over';
			$name2 = "active";
			break;
		case 'users' :
			$active_user = "active-tab";
			$over6 = '_over';
			$name6 = "active";
			break;
		case 'content' :
			$active_content = "active-tab";
			$over8 = '_over';
			$name8 = "active";
			break;
		case 'column' :
				$active_content = "active-tab";
				$over8 = '_over';
				$name8 = "active";
				break;
		case 'web-settings' :
			$active_settings = "active-tab";
			$over7 = '_over';
			$name7 = "active";
			break;
		default :
			$active_dashboard = "active-tab";
			$over1 = '_over';
			$name1 = "active";
			break;
	}
	?>
<div id="side-bar">
	<div id="sideMenu">
		<ul id="adminSideMenu">
			<li class="sideTab <?php echo $active_dashboard;?>"><a
				href="<?php echo APPLICATION_URL.'admin/';?>"><img
					src="<?php echo COMMON_IMAGES_URL.'dashboard'.$over1.'.png';?>"
					class="sideMenuImage" alt="Dashboard" title="Dashboard"
					name="<?php echo $name1;?>" /></a></li>
			<li class="sideTab <?php echo $active_tracks;?>"><a
				href="<?php echo APPLICATION_URL.'admin/library/tracks';?>"><img
					src="<?php echo COMMON_IMAGES_URL.'audio_library'.$over2.'.png';?>"
					class="sideMenuImage" alt="Library" title="Library"
					name="<?php echo $name2;?>" /></a></li>
			<?php if ( $_SESSION[ 'admin' ][ 'groupId' ] == "admin" ) { ?>
					<li class="sideTab <?php echo $active_user;?>"><a
						href="<?php echo APPLICATION_URL.'admin/profile/users';?>"><img
							src="<?php echo COMMON_IMAGES_URL.'users'.$over6.'.png';?>"
							class="sideMenuImage" alt="Users" title="Users"
							name="<?php echo $name6;?>" /></a></li>

					<li class="sideTab <?php echo $active_content;?>"><a
						href="<?php echo APPLICATION_URL.'admin/information/content';?>"><img
							src="<?php echo COMMON_IMAGES_URL.'content'.$over8.'.png';?>"
							class="sideMenuImage" alt="Website Content" title="Website Content"
							name="<?php echo $name8;?>" /></a></li>

					<li class="sideTab <?php echo $active_settings;?>"><a
						href="<?php echo APPLICATION_URL.'admin/settings/web-settings';?>"><img
							src="<?php echo COMMON_IMAGES_URL.'settings'.$over7.'.png';?>"
							class="sideMenuImage" alt="Settings" title="Settings"
							name="<?php echo $name7;?>" /></a></li>
			<?php } ?>
					
		</ul>
	</div>
</div>
<!-- close side-bar-->
<?php }?>