-- DROP TABLE IF EXISTS `admin`;
-- DROP TABLE IF EXISTS `settings`;
-- DROP TABLE IF EXISTS `folder`;
-- DROP TABLE IF EXISTS `playlist`;
-- DROP TABLE IF EXISTS `tracks`;
-- DROP TABLE IF EXISTS `playlistData`;
-- DROP TABLE IF EXISTS `cms`;
-- DROP TABLE IF EXISTS `activity`;

CREATE TABLE IF NOT EXISTS `admin` (
  `adminId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `email` varchar(70) NOT NULL,
  `groupId` enum('admin','author') NOT NULL DEFAULT 'author',
  `lastLogin` datetime DEFAULT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `emailVerification` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `token` varchar(10) NOT NULL,
  `tokenExpiryDate` datetime NOT NULL,
  `emailStatus` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `addDate` datetime NOT NULL,
  `updateDate` datetime DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `baseFolder` INT( 9 ) NOT NULL DEFAULT 0,
  PRIMARY KEY (`adminId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;

INSERT IGNORE INTO `admin`
 (`adminId`, `username`, `password`, `name`, `photo`, `email`, `groupId`, `lastLogin`, `logins`,
  `emailVerification`, `token`, `tokenExpiryDate`, `emailStatus`, `addDate`, `updateDate`, `status`)
VALUES
 (1, 'admin@davidka.ru', '5f4dcc3b5aa765d61d8327deb882cf99', 'Slavik Fursov', 'player.jpg', 'admin@davidka.ru',  'admin', '2013-06-06 21:11:42', 69,
     1, '2jh273n4h', '2011-03-02 00:00:00', 0, '2011-02-01 00:00:00', '2012-07-10 08:09:03', 1);
-- The default password is 'password'

CREATE TABLE IF NOT EXISTS `folder` (
  `folderId` int(9) NOT NULL AUTO_INCREMENT,
  `folderName` varchar(512) NOT NULL,
  `parentId` int(9) NOT NULL DEFAULT '0',
  `privacy` enum('private','public') NOT NULL DEFAULT 'private',
  `downloadable` tinyint(1) NOT NULL DEFAULT '1',
  `zip_file` varchar(255) DEFAULT NULL,
  `hasMP3` BOOLEAN NOT NULL DEFAULT FALSE,
  `addDate` datetime NOT NULL,
  `updateDate` datetime DEFAULT NULL,
  `isCategory` tinyint(1) DEFAULT 0 NOT NULL,
  PRIMARY KEY (`folderId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `playlist` (
  `playlist_id` int(9) NOT NULL AUTO_INCREMENT,
  `friendly_name` varchar(255) NOT NULL,
  `last_accessed` datetime NOT NULL,
  PRIMARY KEY (`playlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `playlistData` (
  `playlist_id` int(9) NOT NULL,
  `track_id` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `tracks` (
  `trackId` int(9) NOT NULL AUTO_INCREMENT,
  `trackTitle` text NOT NULL,
  `folderId` int(9) NOT NULL DEFAULT '0',
  `artist` text,
  `album` text,
  `track_year` varchar(100) DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `track_number` int(4) DEFAULT NULL,
  `trackFile` varchar(255) NOT NULL,
  `track_display_option` varchar(255) DEFAULT NULL,
  `downloadable` tinyint(1) NOT NULL DEFAULT '1',
  `music_file` tinyint(1) NOT NULL DEFAULT '0',
  `width`  int NULL DEFAULT NULL,
  `height` int NULL DEFAULT NULL,
  `privacy` enum('private','public') NOT NULL DEFAULT 'private',
  `file_size` varchar(20) NOT NULL DEFAULT '0',
  `duration` varchar(50) NOT NULL DEFAULT '0',
  `addDate` datetime NOT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`trackId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `cms` (
  `cmsId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cmsPage` varchar(45) NOT NULL,
  `cmsPageTitle` varchar(255) NOT NULL,
  `column_title` int(5) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` enum('page','content block') DEFAULT NULL,
  `main_menu` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(2) DEFAULT '0',
  `meta_title` text,
  `meta_keywords` text,
  `meta_description` text,
  `addDate` datetime NOT NULL,
  `updateDate` datetime DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`cmsId`),
  UNIQUE KEY `cmsPage` (`cmsPage`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT IGNORE INTO `cms`
 (`cmsId`, `cmsPage`, `cmsPageTitle`, `column_title`, `content`, `image`, `type`, `main_menu`, `sort_order`, `meta_title`, `meta_keywords`, `meta_description`, `addDate`, `updateDate`, `status`)
VALUES
 (1, 'about', 'About Us', 0, '<p>\r\n	Testing content for About US</p>\r\n', '', 'page', 1, 1, '', '', '', '2013-06-25 00:00:00', '2013-07-06 00:00:00', 1),
 (2, 'privacy', 'Privacy Policy', 0, '<p>\r\n	Test Content</p>\r\n', '', 'page', 1, 2, '', '', '', '2013-06-25 00:00:00', '2013-06-25 00:00:00', 1);


CREATE TABLE IF NOT EXISTS `activity`
(
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL,
  user_id INT NOT NULL,
  object ENUM ('SESSION', 'FILE', 'MP3TAG', 'PAGE', 'USER') NOT NULL,
  operation ENUM ('NEW', 'EDIT', 'DELETE', 'ERROR') NOT NULL,
  details VARCHAR(1024)
);

CREATE TABLE IF NOT EXISTS `settings` (
  `id`    int NOT NULL AUTO_INCREMENT,
  `name`  varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `help`  varchar(255) NOT NULL,
  `type`  enum('string','checkbox','custom') NOT NULL DEFAULT 'string',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT IGNORE INTO `settings`
 (`id`, `name`, `value`, `help`, `type`)
VALUES
 (1, 'APPLICATION_TITLE',       'Propovednik.com: Проповеди, песнопения, рассказы','HTML title for main page',                                'string'),
 (2, 'ADMIN_TITLE',             'Propovednik: Панель администратора',              'HTML title for admin pages',                              'string'),
 (3, 'COMPANY_NAME',            'Propovednik.com',                                 'Used in Page Title',	                                    'string'),
 (4, 'HOME_PAGE',               'information/content/page/Новости',                'Default Home Page',                                       'custom'),
 (6, 'RECORDS_PER_PAGE',        '50',                                              'Number of records per page, such as search results',      'string'),
 (7, 'AVAILABLE_MUSIC_FORMAT',  'mp3',                                             'Supported music formats (extensions), separated by comma','string'),
 (8, 'DEFAULT_TRACK_NAME',      'album-title',                                     'Default display pattern ',                                'custom'),
 (9, 'AUDIO_PLAYER_LINK',       'Медиатека',                                       'Default Audioplayer Link',                                'string'),
 (10,'AUDIO_LIBRARY',           'Медиатека',                                       'Default Library Text',                                    'string'),
 (11,'GOOGLE_ANALYTICS_ENABLE', 'Yes',                                             'Shall Google Analytics be enabled?',                      'checkbox'),
 (12,'GOOGLE_ANALYTICS_CODE',   'UA-6496015-1',                                    'Google Analytics code',                                   'string'),
 (13,'GOOGLE_ANALYTICS_DOMAIN', 'propovednik.com',                                 'Google Analytics Domain',                                 'string'),
 (15,'COMMENTS_PUBLICATIONS',   'Yes',                                             'Shall HyperComments.com integration be enabled?',         'checkbox'),
 (16,'COMMENTS_PUBLICATIONS_ID','66781',                                           'Identificator',                                           'string'),
 (17,'RSS_ENABLED',             'Yes',                                             'Shall RSS link be added to the page?',                    'checkbox'),
 (18,'RSS_LINK',                'http://page2rss.com/rss/faddda5ca1d348e303c0b44621dbce00', 'RSS Link',                                       'string'),
 (19,'RSS_TITLE',               'Propovednik.com news',                            'RSS Title',                                               'string'),
 (20,'LOGO_GRAPHIC',            'false',                                           'if checked, than show modules/common/images/logo.png, else - LOGO_TEXT','checkbox'),
 (21,'LOGO_TEXT',               'Propovednik.com',                                 'Text for logo',                                           'string'),
 (22,'THUMBNAIL_WIDTH',         '200',                                             'Width of thumbnails for uploaded images',                 'string'),
 (23,'THUMBNAIL_HEIGHT',        '200',                                             'Height of thumbnails for uploaded images',                'string'),
 (24,'NEW_WINDOW_EXT',          'txt, html, htm, pdf, mp4, mpg, vob, webm',        'Extensions to be opened in new window,separated by comma','string'),
 (25,'SCAN_IGNORE',             '@eaDir, #recycle',                                'folders to ignore while scanning',                        'string'),
 (26,'DISPLAY_WIDTH',           '1920',                                            'Max Width to downsize images, to use in photo gallery',   'string'),
 (27,'DISPLAY_HEIGHT',          '1080',                                            'Max Height to downsize images, to use in photo gallery',  'string');
