<?php
session_start ();

//class to manage image files
class image {
	private $fileField, $dbImageField, $result, $imgObj;
	public $imageName, $error, $origHeight, $origWidth, $imageHeight, $imageWidth, $imagePath, $debugMessage;
	//constructer
	function __construct($fileField) {
		$this->fileField = $fileField;
		//check if file field is not empty
		$validate = new validations ( );
		if ($this->result = $validate->validateFile ( $this->fileField )) {
			return true;
		} else {
			echo $this->result = $validate->result;
			return false;
		}
	} //end Consturct function 
	

	//function to create image to file systems to
	function imageToFile($fileName, $path, $imgWidth = "", $imgHeight = "", $extn = "") {
		//check folder
		if(file_exists($path)==false){
			mkdir($path,0777,true);
		}
		
		if(empty($extn)){
			$extn=substr($_FILES[$this->fileField]['name'], strpos($_FILES[$this->fileField]['name'],".")+1);
		}
		
		//create new object
		$this->imgObj = new Imagick ( $_FILES [$this->fileField] ['tmp_name'] );
		
		//get the size of Image
		$this->origHeight = $this->imgObj->getImageHeight ();
		$this->origWidth = $this->imgObj->getImageWidth ();
		
		// compute new image Size
		if (! empty ( $imgWidth ) && empty ( $imgHeight )) { //only width is provided
			$this->imageWidth = ( int ) $imgWidth;
			$this->imageHeight = floor ( ($this->origHeight / $this->origWidth) * $this->imageWidth );
		} else if (empty ( $imgWidth ) && ! empty ( $imgHeight )) { //only Height is given
			$this->imageHeight = ( int ) $imgHeight;
			$this->imageWidth = floor ( ($this->origWidth / $this->origHeight) * $this->imageHeight );
		} else if (! empty ( $imgWidth ) && ! empty ( $imgHeight )) { // both height and width is given
			$this->imageHeight = ( int ) $imgHeight;
			$this->imageWidth = ( int ) $imgWidth;
		} else if (empty ( $imgWidth ) && empty ( $imgHeight )) { //neither height nor width is given
			$this->imageHeight = $this->origHeight;
			$this->imageWidth = $this->origWidth;
		}
		//end New Image Size Computation
		

		//resize Image
		$this->imgObj->resizeImage ( $this->imageWidth, $this->imageHeight, Imagick::FILTER_LANCZOS, 1 );
		//set Image path
		$this->imagePath = $path . "/" . $fileName . "." . $extn;
		$this->imageName=$fileName . "." . $extn;
		//Write Image to File System
		$result = $this->imgObj->writeImage ( $this->imagePath );
		
		// clear and Destroy Object
		$this->imgObj->clear ();
		$this->imgObj->destroy ();
		
		//return result
		if ($result === true) {
			return true;
		} else {
			$this->debugMessage = "Image Can't be saved";
			return false;
		}
	
	} //end imageToFile
	

	//method to store image to databse
	function imageToDB($tbl, $fld = "image", $id = "id", $imgWidth = "", $createThumb = false, $thumbFld = "", $thumbWidth = "70") {
	
	} //end imageToDB
} //end Class
?>