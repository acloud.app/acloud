<?php

class recordTable
{
    //TODO - Insert your code here
    public $controller, $result, $div, $recordsPerPage, $page, $searchKey, $order, $recordId, $referer, $filter;
    private $controllerObj, $fetchMethod, $recordSet, $method, $general, $rawOrder;


    function __construct($controller, $referer)
    {
        //TODO - Insert your code here
        $this->controller = substr($controller, 0, strpos($controller, "_"));
        $this->method = substr(strstr($controller, "_"), 1);
        $this->fetchMethod = "render" . $this->method . "Table";
        $this->controllerObj = new $this->controller();
        $this->referer = $referer;
        $this->general = new general();
    }

    //method to renerate table
    public function renderTable($divId, $page, $orderBy, $searchKey, $recordsPerPage, $noEdit = false, $filter = "", $customButton = "", $customButtonFilter = "", $customButtonAction = "", $noStatus = false)
    {
        //var_dump( $noStatus);
        //set properties
        $this->div = $divId;
        $this->page = $page;
        $this->rawOrder = $orderBy;
        $this->order = strpos($orderBy, "/") === 0 ? '' : str_replace("/", " ", $orderBy);
        $this->searchKey = $searchKey;
        $this->recordsPerPage = $recordsPerPage;
        $this->filter = $filter;
        $this->customButton = $customButton;
        $this->customButtonFilter = $customButtonFilter;
        $this->customButtonAction = $customButtonAction;
        if (!empty($this->filter)) {
            $filterArray = explode("_", $this->filter);
            $filterObj = $filterArray[0];
            $filterMethod = $filterArray[3];
            $tmpObj = new $filterObj();
            $filterValue = $filterArray[4];
            $result = $tmpObj->{$filterMethod}();
            if (!empty($filterValue)) {
                $filters = array($filterArray[1] => $filterValue);
            } else {
                $filters = "";
            }
            $this->filter = $filterArray[0] . '_' . $filterArray[1] . '_' . $filterArray[2] . '_' . $filterArray[3];
        }
        //set call method to generate recordset
        $this->recordSet = $this->controllerObj->{$this->fetchMethod}($this->searchKey, $this->page, $this->recordsPerPage, $this->order, $filters);
        //outer div
        $raw .= '<div id="db-root">';
        //for header
        $raw .= '<div id="db-header">';
        //for number of records drop down
        $raw .= '<div id="db-records-drop-down">';
        $raw .= 'Show <select id="records-per-page" name="records-per-page" class="db-selectbox" onChange ="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&fetch=' . $this->controller . '_' . $this->method . '&page=1&searchKey=' . $this->searchKey . '&customButton=' . $customButton . '&customButtonFilter=' . $customButtonFilter . '&customButtonAction=' . $customButtonAction . '&order=' . $this->order . '&noEdit=' . $noEdit . '&filter=' . $this->filter . '&records=\'+this.value);">';
        $a = 10;
        while ($a <= 50) {
            if ($a == $this->recordsPerPage) {
                $raw .= '<option value="' . $a . '" selected ="selected">' . $a . '</option>';
            } else {
                $raw .= '<option value="' . $a . '">' . $a . '</option>';
            }

            $a += 10;
        }
        $raw .= "</select> records";
        $raw .= '</div>'; // end db-records-drop-down

        //for filter
        if (!empty($this->filter)) {
            $raw .= '<div id="db-records-filter-drop-down">';
            $raw .= 'Filter: <select name="' . $filterArray[1] . '" id="' . $filterArray[1] . '" class="db-filter-selectbox" onChange ="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&fetch=' . $this->controller . '_' . $this->method . '&page=1&searchKey=' . $this->searchKey . '&order=' . $this->order . '&noEdit=' . $noEdit . '&filter=' . $this->filter . '_\'+this.value);">';
            $raw .= '<option value="0">-- None --</option>';
            if (is_array($result)) {
                foreach ($result as $rec) {
                    if ($rec[$filterArray[1]] == $filterValue) {
                        $raw .= '<option value="' . $rec[$filterArray[1]] . '" selected="selected">' . $rec[$filterArray[2]] . '</option>';
                    } else {
                        $raw .= '<option value="' . $rec[$filterArray[1]] . '">' . $rec[$filterArray[2]] . '</option>';
                    }

                }
            }


            $raw .= '</select>';
            $raw .= '</div>';
        }
        //for search field
        $raw .= '<div id="db-search-div">';
        $raw .= 'Search <input type="text" id="searchKey" value="' . $this->searchKey . '" name="searchKey" class="db-search" onChange="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&fetch=' . $this->controller . '_' . $this->method . '&page=' . $this->page . '&records=' . $this->recordsPerPage . '&noEdit=' . $noEdit . '&filter=' . $this->filter . '&order=' . $this->order . '&searchKey=\'+this.value);" />';
        $raw .= "</div>"; //end db-search-div
        $raw .= "</div>";// end db-header

        //for recordset header
        $raw .= '<div id="db-record-div">';
        if (is_array($this->recordSet)) {
            $raw .= '<table id="db-record-table" cellpadding="0" cellspacing="0">';
            // create header
            $headingArray = array_keys($this->recordSet[0]);
            $raw .= '<tr>';
            $raw .= '<td class="db-heading centerAlign"><input type="checkbox" id="selectAll" class="db-checkbox" name="selectAll" onClick = "selectDeselect()"/></td>';
            foreach ($headingArray as $heading) {
                if (strpos($heading, "id:") !== false) {
                    $recordIdHeading = $heading;
                }
                if (strpos($heading, ":") !== false && $heading != "id:adminId") {
                    if ($this->rawOrder == substr(strstr($heading, ":"), 1) . "/asc") {
                        $raw .= '<td class="db-heading"><a href="javascript:;" onClick="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&fetch=' . $this->controller . '_' . $this->method . '&noEdit=' . $noEdit . '&filter=' . $this->filter . '&page=' . $this->page . '&records=' . $this->recordsPerPage . '&order=' . substr(strstr($heading, ":"), 1) . '/desc&searchKey=' . $this->searchKey . '\');">' . ucwords(substr($heading, 0, strpos($heading, ":"))) . ' <img src="' . COMMON_IMAGES_URL . 'up_arrow.png" align="bottom" /></a></td>';
                    } else {
                        $raw .= '<td class="db-heading"><a href="javascript:;" onClick="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&fetch=' . $this->controller . '_' . $this->method . '&noEdit=' . $noEdit . '&filter=' . $this->filter . '&page=' . $this->page . '&records=' . $this->recordsPerPage . '&order=' . substr(strstr($heading, ":"), 1) . '/asc&searchKey=' . $this->searchKey . '\');">' . ucwords(substr($heading, 0, strpos($heading, ":"))) . ' <img src="' . COMMON_IMAGES_URL . 'down_arrow.png" align="bottom"  /></a></td>';
                    }
                }
            }
            $raw .= '<td class="db-heading db-action">Action</td>';
            $raw .= '</tr>';//end db-heading-bar
            $class = "db-grey-row";
            foreach ($this->recordSet as $record) {
                $recordId = $record[$recordIdHeading];
                //$raw.=$recordId;
                //set class
                if ($class == "db-white-row") {
                    $class = "db-grey-row";
                } else {
                    $class = "db-white-row";
                }
                $raw .= '<tr>';
                $raw .= '<td class="db-record ' . $class . ' centerAlign" style="text-align: center;"><input type="checkbox" name="check_id[]" class="check_id" onClick = "uncheck()" value="' . $recordId . '"/></td>';
                foreach ($headingArray as $heading) {
                    //$raw.=var_dump($heading);

                    if (strpos($heading, "status") !== false) {
                        $status = $record[$heading];
                    }

                    if (ucfirst($status) == "Active") {
                        $action = 'deactivateRecord';
                        $img = 'inactive-icon.png';
                        $tooltip = "Deactivate Record";
                    } else {
                        $action = 'activateRecord';
                        $img = 'active-icon.png';
                        $tooltip = "Activate Record";
                    }
                    if (strpos($heading, ":") !== false && $heading != "id:adminId") {
                        $raw .= '<td class="db-record ' . $class . '">' . $this->general->prepareOutput($record[$heading], 200) . '</td>';
                    }
                }
                //	$raw.=var_dump($recordId);
                if ($noEdit == false) {
                    $edit = '<a href="' . $this->referer . '/edit' . $this->method . '/' . $recordId . '"><img src="' . COMMON_IMAGES_URL . 'edit-icon.png" title="Edit Record" alt="Edit Record" class="db-icon" hspace="3" /></a>';
                } else {
                    $edit = '';
                }

                if ($noStatus == false) {
                    $status = ' <a href="javascript: ;" onClick="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&noEdit=' . $noEdit . '&noStatus=' . $noStatus . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&action=' . $action . '&recordId=' . $recordId . '&page=' . $this->page . '&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $customButton . '&customButtonFilter=' . $customButtonFilter . '&customButtonAction=' . $customButtonAction . '\');"><img src="' . COMMON_IMAGES_URL . $img . '" class="db-icon" alt="' . $tooltip . '" title="' . $tooltip . '" hspace="3"  /></a>';
                } else {
                    $status = '';
                }


                if (!empty($customButton)) {
                    if (!empty($customButtonFilter)) {
                        $rawFilter = explode(",", $customButtonFilter);
                        if ($record[$rawFilter[0]] == $rawFilter[1]) {
                            $customButtonCode = '<a href="javascript:;" onClick="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&noEdit=' . $noEdit . '&noStatus=' . $noStatus . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&action=customAction&recordId=' . $recordId . '&page=' . $this->page . '&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $customButton . '&customButtonFilter=' . $customButtonFilter . '&customButtonAction=' . $customButtonAction . '\');"><img src="' . $customButton . '" width="16" /></a>';
                        } else {
                            $customButtonCode = "";
                        }
                    } else {
                        $customButtonCode = '<a href="javascript:;" onClick="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&noEdit=' . $noEdit . '&noStatus=' . $noStatus . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&action=customAction&recordId=' . $recordId . '&page=' . $this->page . '&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $customButton . '&customButtonFilter=' . $customButtonFilter . '&customButtonAction=' . $customButtonAction . '\');"><img src="' . $customButton . '" width="16" /></a>';
                    }
                }
                $raw .= '<td class="db-record db-action ' . $class . '"> ' . $customButtonCode . $edit . $status . ' <a href="javascript:;" onClick="confirmDelete(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&fetch=' . $this->controller . '_' . $this->method . '&action=deleteRecord&recordId=' . $recordId . '&page=' . $this->page . '&noEdit=' . $noEdit . '&noStatus=' . $noStatus . '&records=' . $this->recordsPerPage . '&order=' . $this->parent . '&searchKey=' . $this->searchKey . '\');"><img src="' . COMMON_IMAGES_URL . 'trash_can.png" class="db-icon" title="Send to Trash" alt="Send to Trash" hspace="3"  /></a></td>';
                $raw .= '</tr>';
            }
            $raw .= '</table>';
        } else {
            $raw .= "No Records!!!";
        }
        $recordCount = !empty($this->controllerObj->totalRecords) ? $this->controllerObj->totalRecords : 0;
        $raw .= '<div id="db-record-count"> ' . $recordCount . ' records found</div>';
        $raw .= "</div>"; // end db-record-div
        //for bottom footer
        $raw .= '<div id="db-footer">';
        //for multi-record action
        $raw .= '<div id="db-multi-record-action-div">With Selected: ';
        if ($noStatus == false) {
            $raw .= '<a href="javascript:;" onClick="multiActionDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&fetch=' . $this->controller . '_' . $this->method . '&action=deactivateRecord&noEdit=' . $noEdit . '&noStatus=' . $noStatus . '&page=' . $this->page . '&records=' . $this->recordsPerPage . '&order=' . substr(strstr($heading, ":"), 1) . '/desc&searchKey=' . $this->searchKey . '\');"><img src="' . COMMON_IMAGES_URL . 'inactive-icon.png" class="db-icon" hspace="3" alt="Deactivate Selected Records" title="Deactivate Selected Records"  /></a> <a href="javascript:;" onClick="multiActionDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&fetch=' . $this->controller . '_' . $this->method . '&noEdit=' . $noEdit . '&action=activateRecord&page=' . $this->page . '&records=' . $this->recordsPerPage . '&order=' . substr(strstr($heading, ":"), 1) . '/desc&searchKey=' . $this->searchKey . '\');"><img src="' . COMMON_IMAGES_URL . 'active-icon.png" class="db-icon" hspace="3" alt="Activate Selected Records" title="Activate Selected Records"  /></a>';
        }
        $raw .= ' <a href="javascript:;" onClick="multiActionConfirmDelete(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&fetch=' . $this->controller . '_' . $this->method . '&noEdit=' . $noEdit . '&noStatus=' . $noStatus . '&action=deleteRecord&page=' . $this->page . '&records=' . $this->recordsPerPage . '&order=' . substr(strstr($heading, ":"), 1) . '/desc&searchKey=' . $this->searchKey . '\');"><img src="' . COMMON_IMAGES_URL . 'trash_can.png" class="db-icon" title="Send to Trash" alt="Send to Trash" hspace="3" alt="Delete Selected Records" title="Delete Selected Records"  /></a>';
        $raw .= '</div>'; //end db-multi-record-action-div
        $raw .= '<div id="db-pagination-div">';
        if ($this->controllerObj->totalPages > 1) {
            $raw .= $this->createPagination($this->controllerObj->totalPages, $this->controllerObj->page);
        }
        $raw .= '</div>'; //end db-pagination-div

        $raw .= '</div>';//end db-footer
        $raw .= "</div>";// end db-root

        //set result
        $this->result = $raw;
        return $this->result;
    }

    public function deleteRecord($recordId, $divId, $page, $orderBy, $searchKey, $recordsPerPage, $noEdit = false, $filter = "", $customButton = "", $customButtonFilter = "", $customButtonAction = "", $noStatus = "")
    {
        $this->controllerObj->{"delete" . $this->method . "Record"}(rtrim($recordId, ","));
        $this->renderTable($divId, $page, $orderBy, $searchKey, $recordsPerPage, $noEdit, $filter, $customButton, $customButtonFilter, $customButtonAction, $noStatus);
    }

    public function customAction($recordId, $divId, $page, $orderBy, $searchKey, $recordsPerPage, $noEdit = false, $filter = "", $customButton = "", $customButtonFilter = "", $customButtonAction = "")
    {
        $this->customButton = $customButton;
        $this->customButtonFilter = $customButtonFilter;
        $this->customButtonAction = $customButtonAction;
        $this->controllerObj->{$this->customButtonAction}(rtrim($recordId, ","));
        $this->renderTable($divId, $page, $orderBy, $searchKey, $recordsPerPage, $noEdit, $filter, $customButton, $customButtonFilter, $customButtonAction);
    }

    public function activateRecord($recordId, $divId, $page, $orderBy, $searchKey, $recordsPerPage, $noEdit = false, $filter = "", $customButton = "", $customButtonFilter = "", $customButtonAction = "")
    {
        $this->controllerObj->{"activate" . $this->method . "Record"}(rtrim($recordId, ","));
        $this->renderTable($divId, $page, $orderBy, $searchKey, $recordsPerPage, $noEdit);
    }

    public function deactivateRecord($recordId, $divId, $page, $orderBy, $searchKey, $recordsPerPage, $noEdit = false, $filter = "", $customButton = "", $customButtonFilter = "", $customButtonAction = "")
    {
        $this->controllerObj->{"deactivate" . $this->method . "Record"}(rtrim($recordId, ","));
        //var_dump($this->controllerObj->debugMessage);
        $this->renderTable($divId, $page, $orderBy, $searchKey, $recordsPerPage, $noEdit);
    }


    private function createPagination($totalPages, $page)
    {
        //first page
        if ($page != 1) {
            $pagination .= '<button class="db-page db-pagination-first" onClick="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&page=1&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $this->customButton . '&customButtonFilter=' . $this->customButtonFilter . '&customButtonAction=' . $this->customButtonAction . '\');"><img src="' . COMMON_IMAGES_URL . 'db_pagination_first.png" /></button>';
        } else {
            $pagination .= '<button class="db-pagination-first db-inactive-page"><img src="' . COMMON_IMAGES_URL . 'db_pagination_first.png" /></button>';
        }

        //previous page
        if ($page != 1) {
            $pagination .= '<button class=" db-page db-pagination-last" onClick="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&page=' . ($page - 1) . '&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $this->customButton . '&customButtonFilter=' . $this->customButtonFilter . '&customButtonAction=' . $this->customButtonAction . '\');"><img src="' . COMMON_IMAGES_URL . 'db_pagination_prev.png" /></button>';
        } else {
            $pagination .= '<button class="db-pagination-last db-inactive-page"><img src="' . COMMON_IMAGES_URL . 'db_pagination_prev.png" /></button>';
        }

        //pages
        $pStart = ($page - 3) > 0 ? $page - 3 : 1;
        $pEnd = ($page + 3) < $totalPages ? $page + 3 : $totalPages;

        for ($i = $pStart; $i <= $pEnd; $i++) {
            if ($i == $pStart && $i == $page) {
                $pagination .= '<button class="db-active-page db-pagination-first">' . $i . '</button>';
            } else if ($i == $pStart) {
                $pagination .= '<button class="db-page db-pagination-first" onClick ="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&page=' . $i . '&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $this->customButton . '&customButtonFilter=' . $this->customButtonFilter . '&customButtonAction=' . $this->customButtonAction . '\');">' . $i . '</button>';
            } else if ($i == $pEnd && $i == $page) {
                $pagination .= '<button class="db-active-page db-pagination-last">' . $i . '</button>';
            } else if ($i == $pEnd) {
                $pagination .= '<button class="db-page db-pagination-last" onClick ="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&page=' . $i . '&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $this->customButton . '&customButtonFilter=' . $this->customButtonFilter . '&customButtonAction=' . $this->customButtonAction . '\');">' . $i . '</button>';
            } else if ($i == $page) {
                $pagination .= '<button class="db-active-page">' . $i . '</button>';
            } else {
                $pagination .= '<button class="db-middle-page" onClick ="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&page=' . $i . '&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $this->customButton . '&customButtonFilter=' . $this->customButtonFilter . '&customButtonAction=' . $this->customButtonAction . '\');">' . $i . '</button>';
            }

        }


        //next page
        if ($page != $totalPages) {
            $pagination .= '<button class="db-pagination-first" onClick="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&page=' . ($page + 1) . '&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $this->customButton . '&customButtonFilter=' . $this->customButtonFilter . '&customButtonAction=' . $this->customButtonAction . '\');"><img src="' . COMMON_IMAGES_URL . 'db_pagination_next.png" /></button>';
        } else {
            $pagination .= '<button class="db-pagination-first db-pagination-inactive"><img src="' . COMMON_IMAGES_URL . 'db_pagination_next.png" /></button>';
        }

        //last page
        if ($page != $totalPages) {
            $pagination .= '<button class="db-pagination-last" onClick="loadDBTable(\'' . $this->div . '\',\'' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $this->div . '&filter=' . $this->filter . '&fetch=' . $this->controller . '_' . $this->method . '&page=' . $totalPages . '&records=' . $this->recordsPerPage . '&order=' . $this->order . '&searchKey=' . $this->searchKey . '&customButton=' . $this->customButton . '&customButtonFilter=' . $this->customButtonFilter . '&customButtonAction=' . $this->customButtonAction . '\');"><img src="' . COMMON_IMAGES_URL . 'db_pagination_last.png" /></div>';
        } else {
            $pagination .= '<button class="db-pagination-last db-pagination-inactive"><img src="' . COMMON_IMAGES_URL . 'db_pagination_last.png" /></button>';
        }

        return $pagination;
    }
}

?>