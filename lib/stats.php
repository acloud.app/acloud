<?php

/** 
 * @author Aryan
 * This Class will render images with starts
 * 
 */
class stats {
	//TODO - Insert your code here
	private $image;
	public $width, $height, $canvasFirstColor,$canvasSecColor;

	function __construct($width, $height, $canvasFirstColor ="",$canvasSecColor="") {
		$this->width=$width;
		$this->height= $height;
		$this->image = imagecreatetruecolor ( $this->width, $this->height );
		imageantialias ( $this->image, true );
		$this->canvasFirstColor = $canvasFirstColor;
		$this->canvasSecColor = $canvasSecColor;
		if(!empty($this->canvasFirstColor)){
			//fill background color
			$secColor=!empty($this->canvasSecColor)?$this->canvasSecColor: $this->canvasColor;
			$this->image = $this->gd_gradient_fill ( $this->width, $this->height, "vertical", $this->canvasColor, $secColor );
			imagealphablending ( $this->image, true );
		} else {
			$black = imagecolorallocate($this->image, 0, 0, 0);
			imagecolortransparent($this->image, $black);
		}
		//TODO - Insert your code here
	}
	
	public function createProgressBar($score, $colors){
		$keys = array_keys($colors);
		for($i=0;$i<count($keys); $i++){
			if($score<$keys[$i]) {
				$color=$colors[$keys[$i]];
				break;
			} else if($score>$keys[$i] && $score<=$keys[$i+1]) {
				$color=$colors[$keys[$i+1]];
				break;
			} 
		}
		$filledWidth= ceil(($score/100)* ($this->width-2));
		$lineColor = imagecolorallocate ( $this->image, 204,204,204 );
		imagerectangle ( $this->image,0, 0, $this->width-1, $this->height-1, $lineColor );
				$indexColorRed =hexdec(substr($color,1,2));
				$indexColorGreen =hexdec(substr($color,3,2));
				$indexColorBlue =hexdec(substr($color,5,2));
				$fillColor = imagecolorallocate ( $this->image, $indexColorRed,$indexColorGreen,$indexColorBlue );
				imagefilledrectangle ( $this->image,1, 1, $filledWidth, $this->height-2, $fillColor );
				
				return $this->image;
	}
}// end class

?>