<?php

/** 
 * @author Aryan
 * for BitCube Technologies
 */
class chart {
	//TODO - Insert your code here
	public $height = 100, $width = 100, $imageFormat = 'png', $canvasColor,$textColor="#222222",$textRGBColor,$graphColor, $graphLineColorRed,$graphLineColorGreen,$graphLineColorBlue, $canvasSecColor;	
	private $image, $draw;
	

	function __construct($graphColor="#eaeaea") {
		$this->graphColor=$graphColor;
		$this->graphLineColorRed=hexdec(substr($graphColor,1,2));
		$this->graphLineColorGreen=hexdec(substr($graphColor,3,2));
		$this->graphLineColorBlue=hexdec(substr($graphColor,5,2));
		
		//TODO - Insert your code here
	}
	//method to create bar chart
	public function renderBarChart ($data, $barColorCode) {
		
	}
	
	//method to create stacked bar chart
	public function renderStackedBarChart($data, $barColorCode) {
		// create image
		$this->image = imagecreatetruecolor ( $this->width, $this->height );
		imageantialias ( $this->image, true );
		if(!empty($this->canvasColor)){
			//fill background color
			$secColor=!empty($this->canvasSecColor)?$this->canvasSecColor: $this->canvasColor;
			$this->image = $this->gd_gradient_fill ( $this->width, $this->height, "vertical", $this->canvasColor, $secColor );
			imagealphablending ( $this->image, true );
		} else {
			$black = imagecolorallocate($this->image, 0, 0, 0);
			imagecolortransparent($this->image, $black);
		}
		
		//set text color
				$indexColorRed =hexdec(substr($this->textColor,1,2));
				$indexColorGreen =hexdec(substr($this->textColor,3,2));
				$indexColorBlue =hexdec(substr($this->textColor,5,2));
				$this->textRGBColor = imagecolorallocate ( $this->image, $indexColorRed,$indexColorGreen,$indexColorBlue );
		//draw graph
		$lineColor = imagecolorallocate ( $this->image, $this->graphLineColorRed,$this->graphLineColorGreen,$this->graphLineColorBlue );
		//calculate max value
		foreach ($data as $subData){
			$maxVals[]= $this->calculateMaxValue($subData['values']);
		}
		$maxValue = $this->calculateMaxValue($maxVals);
		
		//calcuate graph parts
		if($maxValue<=50){
			$roundPoint = 5;
		} else if ($maxValue>50 && $maxValue<=100) {
			$roundPoint = 10;
		} else if ($maxValue>100) {
			$roundPoint = 50;
		} 
		$roundPoint;
		$remain = (ceil($maxValue/10)%$roundPoint);
		$pointDifference = (ceil($maxValue/10)+($roundPoint-$remain)); //150
		
		//draw graph parts
		$physicalPart=ceil(($this->height-90)/10); //23
		$value=$pointDifference*10;
		for ($i=0; $i<=10; $i++) {
			$partYPos=($i*$physicalPart)+25;
			imagettftext ( $this->image, 9, 0, 0, $partYPos+5, $this->textRGBColor, MEDIA_PATH . "fonts/arial.ttf", $value . " " );
			imageline ( $this->image, 30, $partYPos, $this->width-110, $partYPos, $lineColor );
			$value -= $pointDifference;
		}
		imageline ( $this->image, 30, 25, 30,$partYPos, $lineColor );
		imageline ( $this->image, 30, $partYPos, $this->width-110,$partYPos, $lineColor );
		
		//draw graph
		$barWidth=floor(($this->width-50)/count($data));
		$barMaxWidth = 30;
		$barMinWidth = 10;
		if($barWidth>30) {
			$barWidth = 30;
		}
		
		if($barWidth<10) {
			$barWidth = 10;
		}
		$barDifference = 5;
		$barStartX= 40;
		foreach ($data as $barRecord) {
			$rec=0;
			$otherTotal=0;
			$currentBarHeight=0;
			foreach ($barRecord['values'] as $key=>$value){
				if($rec==0){
					foreach($barRecord['values'] as $k=>$subVal){
						if(strtolower($k) != "total"){
							$otherTotal+=$subVal;
						}
					}
					$value=$value-$otherTotal;
					//echo "<br>";
				} 
					$barStartY=$partYPos-$currentBarHeight;
				//echo "<b>*****************<br>";
				$barHeight= floor(($physicalPart/$pointDifference)*$value);
				$rawHeight=$barHeight;
				$barHeight = ($barHeight<0)?0:$barHeight;			
				//echo "<b>*****************<br>";
				$currentBarHeight+=$barHeight;
				//echo "<b>*****************<br>";
				$barColorRed =hexdec(substr($barColorCode[$key],1,2));
				$barColorGreen =hexdec(substr($barColorCode[$key],3,2));
				$barColorBlue =hexdec(substr($barColorCode[$key],5,2));
				$barColor = imagecolorallocate ( $this->image, $barColorRed,$barColorGreen,$barColorBlue );
				imagefilledrectangle ( $this->image, $barStartX, $barStartY, $barStartX+$barWidth, $barStartY-$barHeight, $barColor );
				$rec++;
			}
			//echo "<br>";
			imagettftext ( $this->image, 9, 90, $barStartX+($barWidth/2), $partYPos+60, $this->textRGBColor, MEDIA_PATH . "fonts/arial.ttf", ucfirst($barRecord['title']). " " );
			$barStartX=$barStartX+$barWidth+$barDifference;
		}
		
		// show index
		$indexY=40;
		foreach ($barColorCode as $key=>$color){
				$indexColorRed =hexdec(substr($color,1,2));
				$indexColorGreen =hexdec(substr($color,3,2));
				$indexColorBlue =hexdec(substr($color,5,2));
				$indexColor = imagecolorallocate ( $this->image, $indexColorRed,$indexColorGreen,$indexColorBlue );
				imagefilledrectangle ( $this->image, $this->width-80, $indexY, $this->width-60, $indexY+20, $indexColor );
				imagettftext ( $this->image, 10, 0, $this->width-55, $indexY+13, $this->textRGBColor, MEDIA_PATH . "fonts/arial.ttf", ucfirst($key). " " );
				$indexY+=25;
		}		
		
		return $this->image;
	}
	
	//method to create pie chart
	public function renderPIEChart ($data){
		// create image
		$this->image = imagecreatetruecolor ( $this->width, $this->height );
		imageantialias ( $this->image, true );
		if(!empty($this->canvasColor)){
			//fill background color
			$secColor=!empty($this->canvasSecColor)?$this->canvasSecColor: $this->canvasColor;
			$this->image = $this->gd_gradient_fill ( $this->width, $this->height, "vertical", $this->canvasColor, $secColor );
			imagealphablending ( $this->image, true );
		} else {
			$black = imagecolorallocate($this->image, 0, 0, 0);
			imagecolortransparent($this->image, $black);
		}
		//calculate total value
		$total=0;
		foreach($data as $val) {
			$total +=$val['value'];
		}
		
		//calculate Points
		$chartWidth=$this->width-120;
		$midX=floor($chartWidth/2);
		$midY=floor(($this->height)/2);
		//echo $total;
		$perValueAngle = (360/$total);
		//prepare data and draw chart
	
		if(is_array($data)) {
			$startAngle=0;
			$endAngle=0;
			foreach ($data as $key=>$rec) {
				//calculate %age
				$percent=($rec['value']/$total)*100;
				$data[$key]['percent']=$percent;
				$this->getColor($rec['shadowColor']);
				//echo "<p>";
				$startAngle=$endAngle;
				//echo ", ";
				$endAngle= $startAngle +($perValueAngle*$rec['value']);
				//echo "</p>";
				if($rec['value']>0){
				for($i = ($midX+10); $i > $midX; $i--){
					imagefilledarc($this->image, $midX, $i, $chartWidth, floor($this->height-20), $startAngle, $endAngle, $this->color, IMG_ARC_PIE);
				}
				}
				
				
			}
		}
		
		if(is_array($data)) {
			$startAngle=0;
			$endAngle=0;
			foreach ($data as $rec) {
				$this->getColor($rec['color']);
				//echo "<p>";
				$startAngle=$endAngle;
				//echo ", ";
				$endAngle= $startAngle +($perValueAngle*$rec['value']);
				//echo "</p>";
				if($rec['value']>0){
					imagefilledarc($this->image, $midX, $midY, $chartWidth, floor($this->height-20), $startAngle, $endAngle, $this->color, IMG_ARC_PIE);
				}
				
				
			}
		}
		
		$indexY=10;
		foreach ($data as $key=>$rec){
				$this->getColor($rec['color']);
				imagefilledrectangle ( $this->image, $this->width-110, $indexY, $this->width-90, $indexY+20, $this->color );
				imagettftext ( $this->image, 9, 0, $this->width-85, $indexY+13, $this->textRGBColor, MEDIA_PATH . "fonts/arial.ttf", ucfirst($key). " (".round($rec['percent'],2)."%) " );
				$indexY+=25;
		}	
		return $this->image;
	}
	
private function gd_gradient_fill($w, $h, $d, $s, $e, $step = 0) {
		$this->width = $w;
		$this->height = $h;
		$this->direction = $d;
		$this->startcolor = $s;
		$this->endcolor = $e;
		$this->step = intval ( abs ( $step ) );
		
		// Attempt to create a blank image in true colors, or a new palette based image if this fails
		if (empty ( $this->image )) {
			if (function_exists ( 'imagecreatetruecolor' )) {
				$this->image = imagecreatetruecolor ( $this->width, $this->height );
			} elseif (function_exists ( 'imagecreate' )) {
				$this->image = imagecreate ( $this->width, $this->height );
			} else {
				die ( 'Unable to create an image' );
			}
		}
		
		// Fill it
		$this->fill ( $this->image, $this->direction, $this->startcolor, $this->endcolor );
		
		// Show it        
		//  $this->display($this->image);
		

		// Return it
		return $this->image;
	}
	
	private function display($im) {
		if (function_exists ( "imagepng" )) {
			header ( "Content-type: image/png" );
			imagepng ( $im );
		} elseif (function_exists ( "imagegif" )) {
			header ( "Content-type: image/gif" );
			imagegif ( $im );
		} elseif (function_exists ( "imagejpeg" )) {
			header ( "Content-type: image/jpeg" );
			imagejpeg ( $im, "", 0.5 );
		} elseif (function_exists ( "imagewbmp" )) {
			header ( "Content-type: image/vnd.wap.wbmp" );
			imagewbmp ( $im );
		} else {
			die ( "Doh ! No graphical functions on this server ?" );
		}
		return true;
	}
	
	// The main function that draws the gradient
	private function fill($im, $direction, $start, $end) {
		
		switch ($direction) {
			case 'horizontal' :
				$line_numbers = imagesx ( $im );
				$line_width = imagesy ( $im );
				list ( $r1, $g1, $b1 ) = $this->hex2rgb ( $start );
				list ( $r2, $g2, $b2 ) = $this->hex2rgb ( $end );
				break;
			case 'vertical' :
				$line_numbers = imagesy ( $im );
				$line_width = imagesx ( $im );
				list ( $r1, $g1, $b1 ) = $this->hex2rgb ( $start );
				list ( $r2, $g2, $b2 ) = $this->hex2rgb ( $end );
				break;
			case 'ellipse' :
				$width = imagesx ( $im );
				$height = imagesy ( $im );
				$rh = $height > $width ? 1 : $width / $height;
				$rw = $width > $height ? 1 : $height / $width;
				$line_numbers = min ( $width, $height );
				$center_x = $width / 2;
				$center_y = $height / 2;
				list ( $r1, $g1, $b1 ) = $this->hex2rgb ( $end );
				list ( $r2, $g2, $b2 ) = $this->hex2rgb ( $start );
				imagefill ( $im, 0, 0, imagecolorallocate ( $im, $r1, $g1, $b1 ) );
				break;
			case 'ellipse2' :
				$width = imagesx ( $im );
				$height = imagesy ( $im );
				$rh = $height > $width ? 1 : $width / $height;
				$rw = $width > $height ? 1 : $height / $width;
				$line_numbers = sqrt ( pow ( $width, 2 ) + pow ( $height, 2 ) );
				$center_x = $width / 2;
				$center_y = $height / 2;
				list ( $r1, $g1, $b1 ) = $this->hex2rgb ( $end );
				list ( $r2, $g2, $b2 ) = $this->hex2rgb ( $start );
				break;
			case 'circle' :
				$width = imagesx ( $im );
				$height = imagesy ( $im );
				$line_numbers = sqrt ( pow ( $width, 2 ) + pow ( $height, 2 ) );
				$center_x = $width / 2;
				$center_y = $height / 2;
				$rh = $rw = 1;
				list ( $r1, $g1, $b1 ) = $this->hex2rgb ( $end );
				list ( $r2, $g2, $b2 ) = $this->hex2rgb ( $start );
				break;
			case 'circle2' :
				$width = imagesx ( $im );
				$height = imagesy ( $im );
				$line_numbers = min ( $width, $height );
				$center_x = $width / 2;
				$center_y = $height / 2;
				$rh = $rw = 1;
				list ( $r1, $g1, $b1 ) = $this->hex2rgb ( $end );
				list ( $r2, $g2, $b2 ) = $this->hex2rgb ( $start );
				imagefill ( $im, 0, 0, imagecolorallocate ( $im, $r1, $g1, $b1 ) );
				break;
			case 'square' :
			case 'rectangle' :
				$width = imagesx ( $im );
				$height = imagesy ( $im );
				$line_numbers = max ( $width, $height ) / 2;
				list ( $r1, $g1, $b1 ) = $this->hex2rgb ( $end );
				list ( $r2, $g2, $b2 ) = $this->hex2rgb ( $start );
				break;
			case 'diamond' :
				list ( $r1, $g1, $b1 ) = $this->hex2rgb ( $end );
				list ( $r2, $g2, $b2 ) = $this->hex2rgb ( $start );
				$width = imagesx ( $im );
				$height = imagesy ( $im );
				$rh = $height > $width ? 1 : $width / $height;
				$rw = $width > $height ? 1 : $height / $width;
				$line_numbers = min ( $width, $height );
				break;
			default :
		}
		
		for($i = 0; $i < $line_numbers; $i = $i + 1 + $this->step) {
			// old values :
			$old_r = $r;
			$old_g = $g;
			$old_b = $b;
			// new values :
			$r = ($r2 - $r1 != 0) ? intval ( $r1 + ($r2 - $r1) * ($i / $line_numbers) ) : $r1;
			$g = ($g2 - $g1 != 0) ? intval ( $g1 + ($g2 - $g1) * ($i / $line_numbers) ) : $g1;
			$b = ($b2 - $b1 != 0) ? intval ( $b1 + ($b2 - $b1) * ($i / $line_numbers) ) : $b1;
			if ("$old_r,$old_g,$old_b" != "$r,$g,$b")
				$fill = imagecolorallocate ( $im, $r, $g, $b );
			switch ($direction) {
				case 'vertical' :
					imagefilledrectangle ( $im, 0, $i, $line_width, $i + $this->step, $fill );
					break;
				case 'horizontal' :
					imagefilledrectangle ( $im, $i, 0, $i + $this->step, $line_width, $fill );
					break;
				case 'ellipse' :
				case 'ellipse2' :
				case 'circle' :
				case 'circle2' :
					imagefilledellipse ( $im, $center_x, $center_y, ($line_numbers - $i) * $rh, ($line_numbers - $i) * $rw, $fill );
					break;
				case 'square' :
				case 'rectangle' :
					imagefilledrectangle ( $im, $i * $width / $height, $i * $height / $width, $width - ($i * $width / $height), $height - ($i * $height / $width), $fill );
					break;
				case 'diamond' :
					imagefilledpolygon ( $im, array ($width / 2, $i * $rw - 0.5 * $height, $i * $rh - 0.5 * $width, $height / 2, $width / 2, 1.5 * $height - $i * $rw, 1.5 * $width - $i * $rh, $height / 2 ), 4, $fill );
					break;
				default :
			}
		}
	}
	
	// #ff00ff -> array(255,0,255) or #f0f -> array(255,0,255)
	private function hex2rgb($color) {
		$color = str_replace ( '#', '', $color );
		$s = strlen ( $color ) / 3;
		$rgb [] = hexdec ( str_repeat ( substr ( $color, 0, $s ), 2 / $s ) );
		$rgb [] = hexdec ( str_repeat ( substr ( $color, $s, $s ), 2 / $s ) );
		$rgb [] = hexdec ( str_repeat ( substr ( $color, 2 * $s, $s ), 2 / $s ) );
		return $rgb;
	}
	
	private function imageSmoothArcDrawSegment(&$img, $cx, $cy, $a, $b, $aaAngleX, $aaAngleY, $color, $start, $stop, $seg) {
		
		$fillColor = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], $color [3] );
		
		$xStart = abs ( $a * cos ( $start ) );
		$yStart = abs ( $b * sin ( $start ) );
		$xStop = abs ( $a * cos ( $stop ) );
		$yStop = abs ( $b * sin ( $stop ) );
		$dxStart = 0;
		$dyStart = 0;
		$dxStop = 0;
		$dyStop = 0;
		if ($xStart != 0)
			$dyStart = $yStart / $xStart;
		if ($xStop != 0)
			$dyStop = $yStop / $xStop;
		if ($yStart != 0)
			$dxStart = $xStart / $yStart;
		if ($yStop != 0)
			$dxStop = $xStop / $yStop;
		if (abs ( $xStart ) >= abs ( $yStart )) {
			$aaStartX = true;
		} else {
			$aaStartX = false;
		}
		if ($xStop >= $yStop) {
			$aaStopX = true;
		} else {
			$aaStopX = false;
		}
		//$xp = +1; $yp = -1; $xa = +1; $ya = 0;
		for($x = 0; $x < $a; $x += 1) {
			/*$y = $b * sqrt( 1 - ($x*$x)/($a*$a) );
        
        $error = $y - (int)($y);
        $y = (int)($y);
        
        $diffColor = imageColorExactAlpha( $img, $color[0], $color[1], $color[2], 127-(127-$color[3])*$error );*/
			
			$_y1 = $dyStop * $x;
			$_y2 = $dyStart * $x;
			if ($xStart > $xStop) {
				$error1 = $_y1 - ( int ) ($_y1);
				$error2 = 1 - $_y2 + ( int ) $_y2;
				$_y1 = $_y1 - $error1;
				$_y2 = $_y2 + $error2;
			} else {
				$error1 = 1 - $_y1 + ( int ) $_y1;
				$error2 = $_y2 - ( int ) ($_y2);
				$_y1 = $_y1 + $error1;
				$_y2 = $_y2 - $error2;
			}
			/*
        if ($aaStopX)
            $diffColor1 = imageColorExactAlpha( $img, $color[0], $color[1], $color[2], 127-(127-$color[3])*$error1 );
        if ($aaStartX)
            $diffColor2 = imageColorExactAlpha( $img, $color[0], $color[1], $color[2], 127-(127-$color[3])*$error2 );
        */
			
			if ($seg == 0 || $seg == 2) {
				$i = $seg;
				if (! ($start > $i * M_PI / 2 && $x > $xStart)) {
					if ($i == 0) {
						$xp = + 1;
						$yp = - 1;
						$xa = + 1;
						$ya = 0;
					} else {
						$xp = - 1;
						$yp = + 1;
						$xa = 0;
						$ya = + 1;
					}
					if ($stop < ($i + 1) * (M_PI / 2) && $x <= $xStop) {
						$diffColor1 = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error1 );
						$y1 = $_y1;
						if ($aaStopX)
							imageSetPixel ( $img, $cx + $xp * ($x) + $xa, $cy + $yp * ($y1 + 1) + $ya, $diffColor1 );
					
					} else {
						$y = $b * sqrt ( 1 - ($x * $x) / ($a * $a) );
						$error = $y - ( int ) ($y);
						$y = ( int ) ($y);
						$diffColor = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error );
						$y1 = $y;
						if ($x < $aaAngleX)
							imageSetPixel ( $img, $cx + $xp * $x + $xa, $cy + $yp * ($y1 + 1) + $ya, $diffColor );
					}
					if ($start > $i * M_PI / 2 && $x <= $xStart) {
						$diffColor2 = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error2 );
						$y2 = $_y2;
						if ($aaStartX)
							imageSetPixel ( $img, $cx + $xp * $x + $xa, $cy + $yp * ($y2 - 1) + $ya, $diffColor2 );
					} else {
						$y2 = 0;
					}
					if ($y2 <= $y1)
						imageLine ( $img, $cx + $xp * $x + $xa, $cy + $yp * $y1 + $ya, $cx + $xp * $x + $xa, $cy + $yp * $y2 + $ya, $fillColor );
				}
			}
			
			if ($seg == 1 || $seg == 3) {
				$i = $seg;
				if (! ($stop < ($i + 1) * M_PI / 2 && $x > $xStop)) {
					if ($i == 1) {
						$xp = - 1;
						$yp = - 1;
						$xa = 0;
						$ya = 0;
					} else {
						$xp = + 1;
						$yp = + 1;
						$xa = 1;
						$ya = 1;
					}
					if ($start > $i * M_PI / 2 && $x < $xStart) {
						$diffColor2 = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error2 );
						$y1 = $_y2;
						if ($aaStartX)
							imageSetPixel ( $img, $cx + $xp * $x + $xa, $cy + $yp * ($y1 + 1) + $ya, $diffColor2 );
					
					} else {
						$y = $b * sqrt ( 1 - ($x * $x) / ($a * $a) );
						$error = $y - ( int ) ($y);
						$y = ( int ) $y;
						$diffColor = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error );
						$y1 = $y;
						if ($x < $aaAngleX)
							imageSetPixel ( $img, $cx + $xp * $x + $xa, $cy + $yp * ($y1 + 1) + $ya, $diffColor );
					}
					if ($stop < ($i + 1) * M_PI / 2 && $x <= $xStop) {
						$diffColor1 = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error1 );
						$y2 = $_y1;
						if ($aaStopX)
							imageSetPixel ( $img, $cx + $xp * $x + $xa, $cy + $yp * ($y2 - 1) + $ya, $diffColor1 );
					} else {
						$y2 = 0;
					}
					if ($y2 <= $y1)
						imageLine ( $img, $cx + $xp * $x + $xa, $cy + $yp * $y1 + $ya, $cx + $xp * $x + $xa, $cy + $yp * $y2 + $ya, $fillColor );
				}
			}
		}
		
		///YYYYY
		

		for($y = 0; $y < $b; $y += 1) {
			/*$x = $a * sqrt( 1 - ($y*$y)/($b*$b) );
        
        $error = $x - (int)($x);
        $x = (int)($x);
        
        $diffColor = imageColorExactAlpha( $img, $color[0], $color[1], $color[2], 127-(127-$color[3])*$error );
        */
			$_x1 = $dxStop * $y;
			$_x2 = $dxStart * $y;
			if ($yStart > $yStop) {
				$error1 = $_x1 - ( int ) ($_x1);
				$error2 = 1 - $_x2 + ( int ) $_x2;
				$_x1 = $_x1 - $error1;
				$_x2 = $_x2 + $error2;
			} else {
				$error1 = 1 - $_x1 + ( int ) $_x1;
				$error2 = $_x2 - ( int ) ($_x2);
				$_x1 = $_x1 + $error1;
				$_x2 = $_x2 - $error2;
			}
			/*
        if (!$aaStopX)
            $diffColor1 = imageColorExactAlpha( $img, $color[0], $color[1], $color[2], 127-(127-$color[3])*$error1 );
        if (!$aaStartX)
            $diffColor2 = imageColorExactAlpha( $img, $color[0], $color[1], $color[2], 127-(127-$color[3])*$error2 );
*/
			
			if ($seg == 0 || $seg == 2) {
				$i = $seg;
				if (! ($start > $i * M_PI / 2 && $y > $yStop)) {
					if ($i == 0) {
						$xp = + 1;
						$yp = - 1;
						$xa = 1;
						$ya = 0;
					} else {
						$xp = - 1;
						$yp = + 1;
						$xa = 0;
						$ya = 1;
					}
					if ($stop < ($i + 1) * (M_PI / 2) && $y <= $yStop) {
						$diffColor1 = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error1 );
						$x1 = $_x1;
						if (! $aaStopX)
							imageSetPixel ( $img, $cx + $xp * ($x1 - 1) + $xa, $cy + $yp * ($y) + $ya, $diffColor1 );
					}
					if ($start > $i * M_PI / 2 && $y < $yStart) {
						$diffColor2 = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error2 );
						$x2 = $_x2;
						if (! $aaStartX)
							imageSetPixel ( $img, $cx + $xp * ($x2 + 1) + $xa, $cy + $yp * ($y) + $ya, $diffColor2 );
					} else {
						$x = $a * sqrt ( 1 - ($y * $y) / ($b * $b) );
						$error = $x - ( int ) ($x);
						$x = ( int ) ($x);
						$diffColor = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error );
						$x1 = $x;
						if ($y < $aaAngleY && $y <= $yStop)
							imageSetPixel ( $img, $cx + $xp * ($x1 + 1) + $xa, $cy + $yp * $y + $ya, $diffColor );
					}
				}
			}
			
			if ($seg == 1 || $seg == 3) {
				$i = $seg;
				if (! ($stop < ($i + 1) * M_PI / 2 && $y > $yStart)) {
					if ($i == 1) {
						$xp = - 1;
						$yp = - 1;
						$xa = 0;
						$ya = 0;
					} else {
						$xp = + 1;
						$yp = + 1;
						$xa = 1;
						$ya = 1;
					}
					if ($start > $i * M_PI / 2 && $y < $yStart) {
						$diffColor2 = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error2 );
						$x1 = $_x2;
						if (! $aaStartX)
							imageSetPixel ( $img, $cx + $xp * ($x1 - 1) + $xa, $cy + $yp * $y + $ya, $diffColor2 );
					}
					if ($stop < ($i + 1) * M_PI / 2 && $y <= $yStop) {
						$diffColor1 = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error1 );
						$x2 = $_x1;
						if (! $aaStopX)
							imageSetPixel ( $img, $cx + $xp * ($x2 + 1) + $xa, $cy + $yp * $y + $ya, $diffColor1 );
					} else {
						$x = $a * sqrt ( 1 - ($y * $y) / ($b * $b) );
						$error = $x - ( int ) ($x);
						$x = ( int ) ($x);
						$diffColor = imageColorExactAlpha ( $img, $color [0], $color [1], $color [2], 127 - (127 - $color [3]) * $error );
						$x1 = $x;
						if ($y < $aaAngleY && $y < $yStart)
							imageSetPixel ( $img, $cx + $xp * ($x1 + 1) + $xa, $cy + $yp * $y + $ya, $diffColor );
					}
				}
			}
		}
	}
	
	private function imageSmoothArc(&$img, $cx, $cy, $w, $h, $color, $start, $stop) {
		
		while ( $start < 0 )
			$start += 2 * M_PI;
		while ( $stop < 0 )
			$stop += 2 * M_PI;
		
		while ( $start > 2 * M_PI )
			$start -= 2 * M_PI;
		
		while ( $stop > 2 * M_PI )
			$stop -= 2 * M_PI;
		
		if ($start > $stop) {
			$this->imageSmoothArc ( $img, $cx, $cy, $w, $h, $color, $start, 2 * M_PI );
			$this->imageSmoothArc ( $img, $cx, $cy, $w, $h, $color, 0, $stop );
			return;
		}
		
		$a = 1.0 * round ( $w / 2 );
		$b = 1.0 * round ( $h / 2 );
		$cx = 1.0 * round ( $cx );
		$cy = 1.0 * round ( $cy );
		
		$aaAngle = atan ( ($b * $b) / ($a * $a) * tan ( 0.25 * M_PI ) );
		$aaAngleX = $a * cos ( $aaAngle );
		$aaAngleY = $b * sin ( $aaAngle );
		
		$a -= 0.5; // looks better...
		$b -= 0.5;
		
		for($i = 0; $i < 4; $i ++) {
			if ($start < ($i + 1) * M_PI / 2) {
				if ($start > $i * M_PI / 2) {
					if ($stop > ($i + 1) * M_PI / 2) {
						$this->imageSmoothArcDrawSegment ( $img, $cx, $cy, $a, $b, $aaAngleX, $aaAngleY, $color, $start, ($i + 1) * M_PI / 2, $i );
					} else {
						$this->imageSmoothArcDrawSegment ( $img, $cx, $cy, $a, $b, $aaAngleX, $aaAngleY, $color, $start, $stop, $i );
						break;
					}
				} else {
					if ($stop > ($i + 1) * M_PI / 2) {
						$this->imageSmoothArcDrawSegment ( $img, $cx, $cy, $a, $b, $aaAngleX, $aaAngleY, $color, $i * M_PI / 2, ($i + 1) * M_PI / 2, $i );
					} else {
						$this->imageSmoothArcDrawSegment ( $img, $cx, $cy, $a, $b, $aaAngleX, $aaAngleY, $color, $i * M_PI / 2, $stop, $i );
						break;
					}
				}
			}
		}
	}
	//method to calculate max value
	private function calculateMaxValue($array) {
		return max($array);
	}
	
	//method to retun color
	private function getColor($hexColor){
		$colorRed =hexdec(substr($hexColor,1,2));
		$colorGreen =hexdec(substr($hexColor,3,2));
		$colorBlue =hexdec(substr($hexColor,5,2));
		$this->color = imagecolorallocate ( $this->image, $colorRed,$colorGreen,$colorBlue );
	}
}

?>