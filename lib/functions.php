<?php
// functions to autoload controllers
function renderProfileImage($session_id, $width = "", $height = "", $user_id = "", $user_name = "") {
	if (session_id () !== $session_id && ! empty ( $session_id )) {
		//return 'incorrect Session parameter';
	}
	
	$thumbs=($width<=80)?"thumbs":"";
	// set parameters
	$width = ! empty ( $width ) ? $width : DEFAULT_AVATAR_WIDTH;
	$height = ! empty ( $height ) ? $height : DEFAULT_AVATAR_HEIGHT;

	// prepare condition
	$con ['u.status'] = ACTIVE_VALUE;
	if (! empty ( $user_id )) {
		$con ['u.userId'] = $user_id;
	} else if (! empty ( $user_name )) {
		$con ['u.username'] = $user_name;
	} else {
		$con ['u.userId'] = $_SESSION ['user'] ['userId'];
	}

	// set object
	$user = new profile ();
	$userImageRaw = $user->userProfileDetail (  $con, "p.avatarImage, u.gender");
	if (! empty ( $userImageRaw [0] ['gender'] )) {
		if (! empty ( $userImageRaw [0] ['avatarImage']) && file_exists(MEDIA_PATH . "avatar/thumbs/" . $userImageRaw [0] ['avatarImage'])!=false) {
			$size = getimagesize(MEDIA_PATH . "avatar/" .$thumbs.'/'. $userImageRaw [0] ['avatarImage']);
			$userImage = MEDIA_URL . "avatar/".$thumbs."/" . $userImageRaw [0] ['avatarImage'];
			$divHeight=($height>$size[1])?$size[1]:$height;
			$newHeight = ceil(($size[1]/$size[0])*$width);
			return '<div style="background: url(' . $userImage . ') no-repeat top center; background-size: '.$width.'px '.$newHeight.'px;width:' . $width . 'px; height:' . $divHeight . 'px; overflow:hidden"></div>';
		} else if ($userImageRaw [0] ['gender'] == MALE) {
			$userImage = MEDIA_URL . "avatar/".$thumbs."/default-avatar-male.png";
		} else {
			$userImage = MEDIA_URL . "avatar/".$thumbs."/default-avatar-female.png";
		}
		return '<div class="user-image" style="margin-left: 0px;background: url(' . $userImage . ') no-repeat top center;background-size:' . $width . 'px ' . $height . 'px; width:' . $width . 'px; height:' . $height . 'px; overflow:hidden"></div>';
	}
}

/*
 * This function will display any user's image PARAMETERS session_id: must be
* provided to check active session user_id: [optional] profile ID to fetch
* avatar image user_name: [optinal] profile name of the user to fetch avatar
* image if user_id and user_name are not provided then this script will render
* logged in user's dispaly name
*/
function displayName($session_id, $user_id = "", $user_name = "", $link = "", $noDiv="",$length=0) {
	// check for session
	if (session_id () !== $session_id) {
		//return 'incorrect Session parameter';
	}
	
	// prepare condition
	$con ['p.status'] = ACTIVE_VALUE;
	if (! empty ( $user_id )) {
		$con ['p.userId'] = $user_id;
	} else if (! empty ( $user_name )) {
		$con ['p.username'] = $user_name;
	} else {
		$con ['p.userId'] = $_SESSION ['user'] ['userId'];
	}

	// set object
	$user = new profile ();
	$userRaw = $user->userProfileDetail ( $con ,"p.displayName,u.username,u.firstName, u.lastName" );
	if (! empty ( $userRaw [0] ['displayName'] )) {
		$displayName = $userRaw [0] ['displayName'];
	} else {
		$displayName = $userRaw [0] ['firstName'] . ' ' . $userRaw [0] ['lastName'];
	}
	if (! empty ( $userRaw [0] ['username'] )) {
		$userStamp = $userRaw [0] ['username'];
	} else {
		$userStamp = $userRaw [0] ['userId'];
	}
	if (empty ( $link ) && $link !== false) {
		$link = APPLICATION_URL .  $userStamp ;
	}
	
	if($length>0){
		$general = new general();
		$displayName = $general->prepareOutput($displayName,$length);
	}
	if ($link != false) {
		//return '<div class="user-name"><a href="' . $link . '">' . ucwords($displayName) . '</a></div>';
		return '<div class="user-name"><a href="javascript:;" onClick="loadContent(\'' . $link . '\',\'content_root\')">' . ucwords($displayName) . '</a></div>';
	} else {
		return '<div class="user-name">' . ucwords($displayName) . '</div>';
	}
}

/*
 * This function will return time based on raw time
*/
function showTime($time) {
	$now = date ( "Y-m-d H:i:s" );
	$general = new general ();
	$html = new html();
	$diff = $general->get_time_difference ( $time, $now );
	if ($diff ['days'] > 1) {
		return $general->convertDate ( $time, "date" ) ;
	} else if ($diff ['days'] == 1) {
		return $html->lang ['__YESTERDAY__'] . ' ' . $html->lang ['__AT__'] . ' ' . date ( "H:i",  strtotime($time));
	} else if ($diff ['hours'] > 1) {
		return $html->lang ['__ABOUT__'] . ' ' . $diff ['hours'] . ' ' . $html->lang ['__HOURS__'] . ' ' . $html->lang ['__AGO__'];
	} else if ($diff ['hours'] == 1) {
		return $html->lang ['__ABOUT__'] . ' ' . $diff ['hours'] . ' ' . $html->lang ['__HOUR__'] . ' ' . $html->lang ['__AGO__'];
	} else if ($diff ['minutes'] > 1) {
		return $html->lang ['__ABOUT__'] . ' ' . $diff ['minutes'] . ' ' . $html->lang ['__MINUTES__'] . ' ' . $html->lang ['__AGO__'];
	} else {
		return $html->lang ['__FEW__'] . ' ' . $html->lang ['__SECONDS__'] . ' ' . $html->lang ['__AGO__'];
	}
}

function renderSystemImage($imageName, $width = "", $height = "") {
	$width = ! empty ( $width ) ? $width : DEFAULT_MEDIA_WIDTH;
	$height = ! empty ( $height ) ? $height : DEFAULT_MEDIA_HEIGHT;
	$template = new template ();
	return '<div style="background: url(' . $template->templateInfo ['imageURL'] . $imageName . ');background-size:' . $width . 'px ' . $height . 'px; width:' . $width . 'px; height:' . $height . 'px; overflow:hidden"></div>';
}

function showPost($post, $showLike = true, $userId = "", $profileId = "") {
	global $html;
	if (empty ( $userId )) {
		global $userId;
	}
	if (empty ( $profileId )) {
		global $profileId;
	}
	if ($showLike == false) {
		$picWidth = 300;
	} else {
		$picWidth = 400;
	}
	echo '<div id="' . $post ['shareId'] . '" class="post">';
	echo '<div class="share-user-pic">' . renderProfileImage ( session_id (), 50, 50, $post ['actUserId'] ) . '</div>';
	echo '<div class="share-content">';
	echo '<div class="post-title">' . displayName ( session_id (), $post ['actUserId'] );
    //Process activities here

	if ($post ['likes'] [0] == $userId) {
		$likeText = $html->lang ['__UNLIKE__'];
	} else {
		$likeText = $html->lang ['__LIKE__'];
	}
	if ($showLike == true) {
		if ($post ['actUserId'] !== $userId) {
			$shareLink = '<a href="javascript:void();" onClick="resharePost(' . $post ['activityId'] . ');">' . $html->lang ['__SHARE__'] . '</a>';
		} else {
			$shareLink = "";
		}
		echo '<div class="activity-action"><a href="javascript:void();" id="like_' . $post ['activityId'] . '" onClick="likePost(' . $post ['activityId'] . ')">' . $likeText . '</a> <a href="javascript:void();" onClick="showCommentFrm(' . $post ['activityId'] . ')">' . $html->lang ['__COMMENT__'] . '</a> ' . $shareLink . ' <div class="activity-time">' . showTime ( $post ['activityDate'] ) . '</div></div>';
		if (! empty ( $post ['likes'] ) || ! empty ( $post ['comments'] )) {
			$class = "commentDiv";
		} else {
			$class = "commentDivHidden";
		}

		echo '<div class="' . $class . '" id="commentDiv_' . $post ['activityId'] . '">';
		echo '<div class="likeCountDiv" id="likeCountDiv_' . $post ['activityId'] . '">';
		if (count ( $post ['likes'] ) > 0) {
			echo renderSystemImage ( "icon-like.png", 18, 18 );
			echo ' <span class="likeLink">' . count ( $post ['likes'] ) . '</span> ' . $html->lang ['__LIKE_BOX_TEXT__'] . ' ';
			if (count ( $post ['likes'] ) > 3) {
				$count = 3;
			} else {
				$count = count ( $post ['likes'] );
			}
			$likeList = "";
			for($i = 0; $i < $count; $i ++) {
				if ($post ['likes'] [$i] == $_SESSION ['user'] ['userId']) {
					$likeList .= '<span class="likeLink">' . ucwords ( $html->lang ['__YOU__'] ) . '</span>, ';
				} else {
					$likeList .= displayName ( session_id (), $post ['likes'] [$i] ) . ', ';
				}
			}
			echo ' <div>' . rtrim ( $likeList, ", " ) . '</div>';
		}
		echo '</div>';
		// comment list

		echo '<div id="activityComment_' . $post ['activityId'] . '">';
		if (is_array ( $post ['comments'] )) {
			foreach ( $post ['comments'] as $comment ) {
				echo '<div class="comment-post">';
				echo renderProfileImage ( session_id (), 25, 25, $comment ['userId'] );
				echo '<div class="comment-content">' . displayName ( session_id (), $comment ['userId'] ) . ': ' . $comment ['comment'] . '<br>';
				echo '<div class="comment-time">' . showTime ( $comment ['addDate'] ) . '</div></div>';
				echo '</div>'; // ended post comment div
			}
		}
		echo '</div>';

		// new comment form
		echo '<div class="commentFormDiv">';
		echo renderProfileImage ( session_id (), 30, 30 );
		echo '<div class="commentForm"><form action="" method="post" id="" onSubmit="postComment(' . $post ['activityId'] . ')" class="commentFrm" ><input type="text" name="comment" id="commentInput_' . $post ['activityId'] . '" placeholder="' . $html->lang ['__POST_COMMENT_PLACEHOLDER__'] . '"></form></div>';
		echo '</div>'; // closd commentForm
		echo '</div>'; // closed comment Div
	}
	echo '</div>';
	echo '</div>';
	if ($showLike == true) {
		echo '</div>';
	}
	echo '<script>var options = {
	        resetForm: true
	    }; $(".commentFrm").ajaxForm(options);</script>';
}
