<?php
class validations
{
private $fileField;
	// this function checks for all the required values 
	// will return an array of error if any value not found and true if all the values are right
	function cleanInput($field)
	{
		//$output=$_POST[$field];
		$output=htmlspecialchars(strip_tags(trim($field)));
		$output=escapeshellcmd($output);
		//$output=escapeshellarg(escapeshellcmd($output));
		return $output;	
	}
	
	
	//  this function checks email format is correct or not
	function isEmail($email)
	{
		if(!preg_match("/^[A-Za-z0-9\._\-+]+@[A-Za-z0-9_\-+]+(\.[A-Za-z0-9_\-+]+)+$/",$email))
			return false;
		return true;
	}
	// End of is_email function
	
	//  this function checks the given number is signed/unsigned number
	function isNumber($number)
	{
		if(is_numeric($number))
			return true;
		return false;
	}
	// End of is_number Function
	
	//  this function checks the given string is alphanumeric word or not
	function isAlpha_numeric($str)
	{
		if(!preg_match("/[A-Za-z0-9 ]/i",trim($str)))
			return false;
		return true;
	}
	
function isPhone($phoneNumber)
	{
		$regex="[0-9 ():.ext,+-]";
		if(preg_match("/^[0-9]{10}$/",$phoneNumber)){
			return true;
		}
			
		return false;
	}
	// End of is_alpha_numeric Function
	
	//  this function checks the given date is valid or not.
	function isDate($d)
	{
		if(!preg_match("/^(\d){1,2}[-\/](\d){1,2}[-\/]\d{4}$/",$d,$matches))
			return -1;//Bad Date Format
			
		return true;
		//$T = split("[-/\\]",$d);
		//$MON=array(0,31,28,31,30,31,30,31,31,30,31,30,31);
		////$M = $T[0];
		//$D = $T[1];
		//$Y = $T[2];
		//return $D>0 && ($D<=$MON[$M] ||	$D==29 && $Y%4==0 && ($Y%100!=0 || $Y%400==0)); 
	}
	//End of is_data function
	 
	 //fuction to validate uploaded file
	function validateFile($fileField){
		if($_FILES[$fileField]['name']!=""){	
			if($_FILES[$fileField]['size']>0){  //check if file size if larger then zero
				if(is_uploaded_file($_FILES[$fileField]['tmp_name'])){//check if file is uploaded and not provided by any other source
				return true;				
				} else { //if File is provided by Any other Source
				$this->result="Provided file in not uploaded using our system. Stopping process to stop any attack to the Script.";
				return false;
				}
			} else { //if file size is 0 or empty file
				$this->result="Empty File Uploaded.";
				return false;
			} // END if empty file
	
		}else { //if file Field is Empty
		$this->result="File Field is Empty. Please select any File to upload.".$this->fileField;
		return false;
		}
	
	}


}
?>