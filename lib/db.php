<?php
class db {
	private $pdoObj, $fieldList, $condition, $tbl, $sort, $limit, $value, $statement, $start, $limitRecords,$general;
	public $result, $resource, $query,$error,$errorCode;
	
	function __construct() {
		$this->pdoObj = new PDO ( "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',PDO::ATTR_PERSISTENT => true));
		return $this->pdoObj;
	}
	
	private function setGeneral(){
		$this->general=new general();
		return true;
	}
	
	private function cleanOutput(array $inputArray){
			if(!$this->general){
				$this->setGeneral();
			}
		foreach ($inputArray as $inputKey=>$subInputs){
			if(is_array($subInputs)){
				foreach ($subInputs as $subInputKey=>$subInput){
					if(is_array($subInput)){
						foreach($subInput as $subKey=>$subValue){
							$outputArray[$inputKey][$subInputKey][$subKey]=$this->general->prepareOutput($subValue);
						}
					}else{
						$outputArray[$inputKey][$subInputKey]=$this->general->prepareOutput($subInput);
					}
				}				
			} else {
				$outputArray[$inputKey]=$this->general->prepareOutput($subInputs);	
			}
		}
		return $outputArray;
	}//end cleanOutput

    public function errorAndQuery () {
        return "DB Error: " . $this->error . "\nQuery: " . $this->query;
    }

	function addRecord($tbl, $data) {
		if(!$this->general)
				$this->setGeneral();
		$this->tbl = $tbl;
		$this->fieldList = "";
		$this->value = "";
		//fetch data 
        if (is_array($data))
            foreach ($data as $field => $value) {
                $this->fieldList .= "`" . $field . "`,";
                if ($value === null)            // If value is null (and type is null, otherwise it consider 0 as null)...
                    $this->value .= "null,";    // ...we don't need quotes around it and it should be hard coded like this
                                                // because otherwise php will add NOTHING to SQL statement...
                else
                    $this->value .= $this->pdoObj->quote($this->general->cleanInput($value)) . ","; // ...else wrap it with quotes
            }

		$this->query = "insert into `" . $this->tbl . "` (" . rtrim ( $this->fieldList, "," ) . ") values (" . rtrim ( $this->value, "," ) . ")";
		$this->statement = $this->pdoObj->prepare ( $this->query );
		
		if ($this->statement->execute ()) {
			$this->result = $this->pdoObj->lastInsertId ();
			$this->statement->closeCursor ();
			return true;
		} else {
			$this->errorCode= $this->statement->errorCode ();
			$errorArray = $this->statement->errorInfo ();
			$this->error = $errorArray[2];
			$this->statement->closeCursor ();
            $this->general->errorTrace("97f9048d: Error while adding record" . $this->errorAndQuery());
			return false;
		}
	}
	
    function updateRecord($tbl, $data, $condition, $conJoin = " AND", $specialCondition = "") {
		if(!$this->general)
		    $this->setGeneral();
        if(strpos($tbl," as "))
            $this->tbl = "`".substr($tbl,0,strpos($tbl," as "))."` ".strstr($tbl," as ");
        else
			$this->tbl="`".$tbl."`";
		$this->value = "";
		$this->condition = "";
		
		if ($condition != "" || $specialCondition != "") {
			$this->condition = " where ";
			// fetch all the conditions
			if(is_array($condition))
    			foreach ( $condition as $field => $value )
	    			$this->condition .= "`".$field . "`= " . $this->pdoObj->quote ( $this->general->cleanInput($value) ) . " " . $conJoin . " ";
			$this->condition = rtrim ( $this->condition, $conJoin );
		} else
			$this->condition = "";
		
		//fetch data
        if (is_array($data))
            foreach ($data as $field => $value) {
                $this->value .= "`".$field . "`=";
                if ($value === null)            // If value is null (and type is null, otherwise it consider 0 as null)...
                    $this->value .= "null,";    // ...we don't need quotes around it and it should be hard coded like this
                                                // because otherwise php will add NOTHING to SQL statement...
                else
                    $this->value .= $this->pdoObj->quote($value) . ","; // ...else wrap it with quotes
            }

		if(strlen($this->condition)>7 && !empty($specialCondition))
			$specialCondition =" AND " .$specialCondition ;

		//prepare query
		$this->query = "update " . $this->tbl . " set " . rtrim ( $this->value, "," ) . " " . $this->condition . " " . $specialCondition;
		$this->result = $this->pdoObj->exec ( $this->query );
		
		if (($this->pdoObj->errorCode ())==="00000") {
			return true;
		} else {
			$this->errorCode= $this->pdoObj->errorCode();
			$errorArray = $this->pdoObj->errorInfo ();
			$this->error = $errorArray[2];
            $this->general->errorTrace("edad3641: Error while updating record" . $this->errorAndQuery());
			return false;
		}
	}
	
    function deleteRecord($tbl, $condition, $conJoin = " AND", $specialCondition = "") {
		if(!$this->general){
				$this->setGeneral();
			}
		$this->tbl = $tbl;
		if ($condition != "" || $specialCondition != "") {
			$this->condition = " where ";
			// fetch all the conditions
			if(is_array($condition)){
			foreach ( $condition as $field => $value ) {
				$this->condition .= $field . "= " . $this->pdoObj->quote ( $value ) . " " . $conJoin . " ";
			}
			}
			$this->condition = rtrim ( $this->condition, $conJoin );
		} else {
			$this->condition = "";
		}
		if(strlen($this->condition)>7 && !empty($specialCondition)){
			$specialCondition =" AND " .$specialCondition ;
		}

		$this->query = "delete from `" . $this->tbl . "` " . $this->condition . " " . $specialCondition;
		$this->result = $this->pdoObj->exec ( $this->query );
		
		if ($this->result > 0) {
			return true;
		} else {
			$this->errorCode= $this->pdoObj->errorCode ();
			$errorArray = $this->pdoObj->errorInfo ();
			$this->error = $errorArray[2];
            $this->general->errorTrace("ca7c6352: Error while deleting record" . $this->errorAndQuery());
			return false;
		}
	}

    public function getRecordOrDie ($tbl, $fields = "", $condition = "", $message='error while querying DB') {
        if (!$this->fetchRecord($tbl, $fields, $condition)) {
            $this->general->page404();
        } else {
            if (sizeOf($this->result) == 0)
                $this->general->page404($message, "Query: " . $this->query);
            else
                return true;
        }
    }

    public function getRecordOrError ($tbl, $fields = "", $condition = "", $conJoin = " AND", $specialCondition = "") {
        if (!$this->fetchRecord($tbl, $fields, $condition, $conJoin, $specialCondition)) {
            return false;
        } else {
            if (sizeOf($this->result) == 0) {
                $this->general->errorTrace('27b004ee: No data, while querying DB', "Query: " . $this->query);
                return false;
            } else
                return true;
        }
    }

	function fetchRecord($tbl, $fields = "", $condition = "", $conJoin = " AND", $specialCondition = "", $sort = "", $start = "", $limit = "", $clean=1,$having=0) {
        if (!$this->general)
            $this->setGeneral();

        if (strpos($tbl, " as ")) {
            $this->tbl = "`" . substr($tbl, 0, strpos($tbl, " as ")) . "` " . strstr($tbl, " as ");
        } else {
            $this->tbl = "`" . $tbl . "`";
        }

        $this->condition = "";
        if ($having == 1) {
            $conState = "HAVING";
        } else {
            $conState = "WHERE";
        }
        //get conditions
        if ($condition != "" || $specialCondition != "") {
            $this->condition = " " . $conState . " ";
        }
        if (is_array($condition) == false) {
            if (stristr($condition, $conState)) {
                $this->condition = $condition;
            }
        } else {
            // fetch all the conditions
            if (is_array($condition)) {
                foreach ($condition as $field => $value) {
                    $this->condition .= "`" . $field . "`= " . $this->pdoObj->quote($value) . " " . $conJoin . " ";
                    $tmpCon = $conJoin;
                }
            } else {
                $tmpCon = "";
            }
            $this->condition = rtrim($this->condition, $conJoin);

        }
        if ((!empty($this->condition) || !empty($specialCondition)) && (stristr($this->condition, $conState)) == false) {
            $this->condition = " " . $conState . " " . $this->condition;
        }
        //check real time where condition end
        $this->condition = rtrim($this->condition, " " . $conJoin);

        if ($fields != "")
            $this->fieldList = $fields;
        else
            $this->fieldList = "*";

        if ($sort != "")
            $this->sort = ' ORDER BY ' . $sort;
        else
            $this->sort = " ";

        //get Limit condition
        if ($start != "" || $limit != "") {
            if ($start != " " || $start == 0) {
                $this->start = $start . ",";
                $this->limit = " Limit ";
                $this->limitRecords = $limit;
            } else {
                $this->limitRecords = $limit;
                $this->start = " ";
                $this->limit = " Limit ";
                $this->limitRecords = $limit;
            }
        } else {
            $this->limitRecords = " ";
            $this->start = " ";
            $this->limit = " ";
        }
        if (strlen($this->condition) > 7 && !empty($specialCondition)) {
            $specialCondition = $tmpCon . " " . $specialCondition;
        }
        //	echo "this is limit: ".$this->limitRecords;
        $this->query = "select " . $this->fieldList . " from " . $this->tbl . " " . $this->condition . " " . $specialCondition . " " . $this->sort . " " . $this->limit . " " . $this->start . " " . $this->limitRecords;
        $this->statement = $this->pdoObj->prepare($this->query);
        if (!$this->statement) {
            $this->general->log("ERROR", "716d861d: prepare(this->query) failed. Query: " . $this->query);
            $this->error = 'prepare(this->query) failed';
            return false;
        }

        if ($this->statement->execute()) {
            $resultArray = $this->statement->fetchAll();
            if ($clean == 1) {
                $this->result = $this->cleanOutput($resultArray);
            } else {
                $this->result = $resultArray;
            }
            $this->statement->closeCursor();
            return true;
        } else {
            $this->errorCode = $this->statement->errorCode();
            $errorArray = $this->statement->errorInfo();
            $this->error = $errorArray[2];
            $this->statement->closeCursor();
            $this->general->errorTrace("6ae1eef5: Error while fetching record" . $this->errorAndQuery());
            return false;
        }
    }

	//method for getting number of rows in table
	function getRows($tbl, $condition = "", $conJoin = " AND ", $specialCondition = "",$fields=" * ",$having=0) {
			if(!$this->general){
				$this->setGeneral();
			}
			if(strpos($tbl," as ")) {
				$this->tbl = "`".substr($tbl,0,strpos($tbl," as "))."` ".strstr($tbl," as ");
			} else {
				$this->tbl="`".$tbl."`";
			}
		
		if($having==1){
			$conState="HAVING";
		} else {
			$conState="WHERE";
		}
		//var_dump($condition);
		if (is_array($condition)==false && stristr ( $condition, $conState )) {
			$this->condition = $condition;
			$tmpCon=$conJoin;
		} else {
			if ($condition != "") {
				$this->condition = " ".$conState." ";
				
				// fetch all the conditions
				if(is_array($condition)){
				foreach ( $condition as $field => $value ) {
					$this->condition .= $field . "= " . $this->pdoObj->quote ( $value ) . " " . $conJoin . " ";
					$tmpCon=$conJoin;
				}
				}
				$this->condition = rtrim ( $this->condition, $conJoin );
			} else {
				$this->condition = "";
			}
		} //check real time where condition end
		if(empty($this->condition)&& !empty($specialCondition)){
			$this->condition = " ".$conState." ";
		} else if(!empty($this->condition)&& !empty($specialCondition)) {
			$specialCondition = $tmpCon." " .$specialCondition ;
		} else {
			$specialCondition="";
		}

		$this->query = "select ".$fields." from " . $this->tbl . " " . $this->condition . " " . $specialCondition;
		$this->statement = $this->pdoObj->prepare ( $this->query );
			
		if ($this->statement->execute ()) {
            $this->result = $this->statement->rowCount();
			$this->statement->closeCursor ();
			return $this->result;
		} else {
			$this->errorCode= $this->statement->errorCode ();
			$errorArray = $this->statement->errorInfo ();
			$this->error = $errorArray[2];
			$this->statement->closeCursor ();
			return false;
		}	
	}	
	
	//method for executing query and return resource and number of rows
	function runQuery($query) {
		//$query=$this->pdoObj->quote($query);
		$this->query=$query;
		$this->statement = $this->pdoObj->prepare ( $this->query );
		//$this->result = $this->pdoObj->exec ( $this->query );
		if ($this->statement->execute ()) {
			$resultArray = $this->statement->fetchAll ();
				$this->result=$resultArray;
			$this->statement->closeCursor ();
			return true;
		} else {
			$this->errorCode= $this->pdoObj->errorCode ();
			$errorArray = $this->pdoObj->errorInfo ();
			$this->error = $errorArray[2];
			return false;
		}
	}
	//end of runQuery Method
	

	//method for returning result from resource
	function fetchResource($res) {
		$this->result = $this->statement->fetchAll ();
		return $this->result;
	} //end of fetchResource Method
	

	//method for Build query
	function buildQuery($tbl, $fields = "", $condition = "", $conJoin = " AND", $specialCondition = "", $sort = "", $start = "", $limit = "") {
		$this->tbl = $tbl;
		//get Conditions
		if (stristr ( $condition, "where" )) {
			$this->condition = $condition;
		} else {
			if ($condition != "" || $specialCondition != "") {
				$this->condition = " where ";
				
				// fetch all the conditions
				if(is_array($condition))
		    		foreach ( $condition as $field => $value )
			    		$this->condition .= $field . "= " . $this->pdoObj->quote ( $value ) . " " . $conJoin . " ";
				$this->condition = rtrim ( $this->condition, $conJoin );
			} else {
				$this->condition = "";
			}
		} //check real time where condition end
		

		//get Fields
		if ($fields != "") {
			// fetch all Fields
			$this->fieldList = $fields;
		} else {
			$this->fieldList = "*";
		}
		
		//get sort condition
		if ($sort != "") {
			$this->sort = $sort;
		} else {
			$this->sort = " ";
		}
		
		//get Limit condition
		if ($start != "" || $limit != "") {
			if ($start != " " || $start == 0) {
				$this->start = $start . ",";
				$this->limit = " Limit ";
				$this->limitRecords = $limit;
			} else {
				$this->limitRecords = $limit;
				$this->start = " ";
				$this->limit = " Limit ";
				$this->limitRecords = $limit;
			}
		} else {
			$this->limitRecords = " ";
			$this->start = " ";
			$this->limit = " ";
		}
		
		$this->query = "select " . $this->fieldList . " from `" . $this->tbl . "` " . $this->condition . " " . $this->sort . " " . $this->limit . " " . $this->start . " " . $this->limitRecords;
		return $this->query;
	}
	//end of buildQuery Method
	

	//method to get results of joined query
	function fetchJoined($primaryTbl, $secondryTbl, $joinType = "INNER", $joinCondition = "", $fields = "", $condition = "", $conJoin = " AND", $specialCondition = "", $sort = "", $start = "", $limit = "", $clean=1,$having=0) {
			if(!$this->general){
				$this->setGeneral();
			}
			//to set table name
			if(strpos($primaryTbl," as ")) {
				$primaryTbl = "`".substr($primaryTbl,0,strpos($primaryTbl," as "))."` ".strstr($primaryTbl," as ");
			} else {
				$primaryTbl="`".$primaryTbl."`";
			}
			
	if($having==1){
			$conState="HAVING";
		} else {
			$conState="WHERE";
		}
		//get Conditons
		if ($condition != "" || $specialCondition != "") {
				$this->condition = " ".$conState." ";
		}
		if(is_array($condition)==false){
		if (stristr ( $condition, $conState )) {
			$this->condition = $condition;
		}
		} else {
			
				
				// fetch all the conditions
				if(is_array($condition)){
				foreach ( $condition as $field => $value ) {
					$this->condition .= $field . "= " . $this->pdoObj->quote ( $value ) . " " . $conJoin . " ";
					$tmpCon=$conJoin;
				}
				} else {
					$tmpCon="";
				}
				$this->condition = rtrim ( $this->condition, $conJoin );

		} 
		if ((!empty($this->condition) || !empty($specialCondition)) && (stristr ( $this->condition, $conState ))==false) {
			$this->condition =" ".$conState." ".$this->condition;
		} //check real time where condtion end
		//get Fields
		if ($fields != "") {
			// fetch all Fields
			$this->fieldList = $fields;
		} else {
			$this->fieldList = "*";
		}
		//get sort condition
		if (!empty($sort)) {
			$this->sort = " ORDER BY ".$sort;
		} else {
			$this->sort = " ";
		}
		
		//get Limit condition
		if ($start != "" || $limit != "") {
			if ($start != " " || $start == 0) {
				$this->start = $start . ",";
				$this->limit = " Limit ";
				$this->limitRecords = $limit;
			} else {
				$this->limitRecords = $limit;
				$this->start = " ";
				$this->limit = " Limit ";
				$this->limitRecords = $limit;
			}
		} else {
			$this->limitRecords = " ";
			$this->start = " ";
			$this->limit = " ";
		}
		//get the type of join

        $join = "";
		if ($joinType == "INNER") { //if Join Type inner
			$join = "INNER JOIN ON " . $joinCondition;
		} else { //if other Join Type
			if(is_array($secondryTbl)){
			foreach ( $secondryTbl as $key => $value ) {
				//to set table name
				if(strpos($value," as ")) {
					$value = "`".substr($value,0,strpos($value," as "))."` ".strstr($value," as ");
				} else {
					$value="`".$value."`";
				}
				$join .= " " . $joinType . " JOIN " . $value . " ON " . $joinCondition [$key];
			}
			}
		}
		if(strlen($this->condition)>8 && !empty($specialCondition)){
			$specialCondition =" AND " .$specialCondition ;
		}
		//prepare query
		$this->query = "select " . $this->fieldList . " from " . $primaryTbl . " " . $join . " " . $this->condition . " " . $specialCondition . " " . $this->sort . " " . $this->limit . " " . $this->start . " " . $this->limitRecords;
		$this->statement = $this->pdoObj->prepare ( $this->query );
		
		//check if query executed
		if ($this->statement->execute ()) {
			$resultArray = $this->statement->fetchAll ();
			if($clean==1){
				$this->result=$this->cleanOutput($resultArray);
			} else {
				$this->result=$resultArray;
			}
			$this->statement->closeCursor ();
			return true;
		} else {
			$this->errorCode= $this->statement->errorCode ();
			$errorArray = $this->statement->errorInfo ();
			$this->error = $errorArray[2];
			$this->statement->closeCursor ();
			return false;
		}
	}

}
