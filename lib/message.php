<?php
class message  {
//define class properties
	public $debugMessage, $message;
	private $dbObj, $general,$table=TBL_JOB,$tblMsgCenter=TBL_MESSAGE_CENTER,$tblUser=TBL_USER,$tblAttachment=TBL_ATTACHMENT;

	//method to initiate database object
	private function setDBObj() {
		if ($this->dbObj = new db ( )) {
			return true;
		} else {
			$this->debugMessage = "can't initiate database Object";
			return false;
		}
	} //end setDBObj
	
	//method to initiate general class object
	private function setGeneral() {
		if ($this->general = new general ( )) {
			return true;
		} else {
			$this->debugMessage = "can't initiate general class";
			return false;
		}
	} //end setGeneral
	
	//send SMS 
	public function sendText($message, $number, $carrierSufix){
		return $this->sendMail($number.$carrierSufix, "Lead form IDEAS", $message,ADMIN_MAIL,COMPANY_TITLE);
	}// end sendText
	
	public function sendMail ($to, $subject, $message,$by,$name=COMPANY_NAME) {
	$headers = 'From: "'.$name.'" <'.$by.'>' . PHP_EOL .
           'X-Mailer: PHP-' . phpversion() . PHP_EOL.
			'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=UTF-8';

			if(@mail($to, $subject, $message, $headers)) {
				return true;
			} else {
				$general= new general();
				$general->log("ERROR","MAIL not sent");
				return false;
			}
	}
	


	//method to add attachment
	public function addAttachment($data) {
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		if(is_object($this->general)==false){
			$this->setGeneral();
		}
		if($this->dbObj->addRecord($this->tblAttachment,$data)) {
			$this->attachmentId=$this->dbObj->result;
			$this->message="File Uploaded Successfully";
			return $this->attachmentId;
		} else {
			$this->message="Upload Attachment Failed Due to Technical Reasons. Please Contact Application Admin.";
			$this->debugMessage="Error Adding Record [".$this->dbObj->errorCode.": ".$this->dbObj->error."]: Query: ".$this->dbObj->query;
		}
	}//end add()
	

	public function getMessageDetail($id) {
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		
		$condition['msgId']=$id;
		$this->dbObj->fetchRecord(TBL_MESSAGE_CENTER,"*", $condition);
		return $this->dbObj->result[0];
	}
	
	public function removeMessage($msgId,$userId) {
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		if(is_object($this->general)==false){
			$this->setGeneral();
		}
		
		$messageDetails=$this->getMessageDetail($msgId);
		if($messageDetails['from']==$userId) {
			$data['initializerStatus']=INACTIVE_VALUE;
		} else {
			$data['recieverStatus']=INACTIVE_VALUE;
		}
		
		
		$condition['msgId']=$msgId;
		if ($this->dbObj->updateRecord ( $this->tblMsgCenter, $data, $condition)) {
			$this->message="New message sent Sucessfully!!!";
			return true;
		} else {
			$this->message="Enable to sent new message.";
			$this->debugMessage="Error Updating Record [".$this->dbObj->errorCode.": ".$this->dbObj->error."]: Query: ".$this->dbObj->query;
		}
	
	}
	
	public function deleteMessage($msgId,$userId) {
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		if(is_object($this->general)==false){
			$this->setGeneral();
		}
	
		$messageDetails=$this->getMessageDetail($msgId);
		if($messageDetails['from']==$userId) {
			$data['initializerStatus']=TRASHED;
		} else {
			$data['recieverStatus']=TRASHED;
		}
	
	
		$condition['msgId']=$msgId;
		if ($this->dbObj->updateRecord ( $this->tblMsgCenter, $data, $condition)) {
			$this->message="New message sent Sucessfully!!!";
			return true;
		} else {
			$this->message="Enable to sent new message.";
			$this->debugMessage="Error Updating Record [".$this->dbObj->errorCode.": ".$this->dbObj->error."]: Query: ".$this->dbObj->query;
		}
	
	}
	
	public function restoreMessage($msgId,$userId) {
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		if(is_object($this->general)==false){
			$this->setGeneral();
		}
	
		$messageDetails=$this->getMessageDetail($msgId);
		if($messageDetails['from']==$userId) {
			$data['initializerStatus']=ACTIVE_VALUE;
		} else {
			$data['recieverStatus']=ACTIVE_VALUE;
		}
	
	
		$condition['msgId']=$msgId;
		if ($this->dbObj->updateRecord ( $this->tblMsgCenter, $data, $condition)) {
			$this->message="New message sent Sucessfully!!!";
			return true;
		} else {
			$this->message="Enable to sent new message.";
			$this->debugMessage="Error Updating Record [".$this->dbObj->errorCode.": ".$this->dbObj->error."]: Query: ".$this->dbObj->query;
		}
	
	}
	
	
//method to get message details
	public function getPrimaryUserMsg($to="",$from="",$condition="",$order = "date desc") {
		if(is_object($this->dbObj)===false) {
			$this->setDBObj();
			$this->setGeneral();
		}
		$start=0;
		$limit=1;
		$specCon = " status !=". TRASHED;
		$specCon.=" AND (`from`=$to AND `sendTo`=$from ) OR";
		$specCon.="(`from`=$from AND `sendTo`=$to )";	
		$fields = "DISTINCT case when `from`=$to AND `sendTo`=$from then `sendTo` else `from` end as msgId,`from`,`sendTo`";
		if ($this->dbObj->fetchRecord($this->tblMsgCenter." as m",$fields,$condition," AND ",$specCon,$order,$start, $limit, 1)) {
			echo $this->dbObj->query;
			 $this->debugMessage="[".$this->dbObj->errorCode."]- ".$this->dbObj->error.". Query: ". $this->dbObj->query;                   
			return $this->dbObj->result;
		} else {
			$this->message="Unable to get message Details";
			$this->debugMessage="[".$this->dbObj->errorCode."]- ".$this->dbObj->error.". Query: ". $this->dbObj->query;
			return false;
		}
	}
	//getting the 'sendto' and 'from' id from above method
	//change status in message center table if session id of user is equal to 'sendto' then set status (initializerStatus=5)
	// if session id of user is equal to 'from' then set status (recieverStatus=5)

	
} //end class
?>