<?php
/*
 * This script will work as controller and created to handle registration, email verification and login
 */
class auth{
	//declare class property
	public $result, $xmlResult, $debugMessage,$message,$loginId;
	private $table,$dbObj,$general,$returnMailId,$tableId, $obj;
	
	//class construct
	public function __construct($table){
		$this->table=$table;
		$this->tableId=$this->table."Id";
	}//end function construct
	
	//method to set dbObj
	private function setDBObj(){
		if(is_object($this->dbObj)===false){
			$this->dbObj= new db();
		}
	}// end setDBObj
	
	//method to set general class object
	private function setGeneral(){
		if(is_object($this->general)===false){
			$this->general=new general();
		}
	}//end setGeneral
	
	//method to register new user
	public function register($data){
		//set dbObj
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		
		//set general
		if(is_object($this->general)===false){
			$this->setGeneral();
		}
		$rows=$this->dbObj->getRows($this->table,array('username'=>trim($data['username'])));
		$rowsEmail=$this->dbObj->getRows($this->table,array('email'=>$data['email']));
		$flag=0;
		$blackListUserName=array(BLACKLIST_WORDS);
		foreach($blackListUserName as $blackList){
			if(!empty($data['username']) && $data['username']==$blackList){
				echo $flag=1;
			}
		}

		if($rowsEmail>0 || $flag==1){
			$xmlArray['error']="username or email is already registered";
			$xmlArray['errno']="1020";
			$this->result=$this->response('error',$xmlArray);
		if(!empty($this->result)){
				return $this->result;
			}else {
				$this->debugMessage="error in XML Result";
				return false;
			}
		}
		
		//set additional fields
		$data['lastLogin']=date("Y-m-d H:i:s");
		$data['logins']=0;
		$data['updateDate']=date("Y-m-d H:i:s");
		$data['addDate']=date("Y-m-d H:i:s");
		$data['token']=$this->general->randomCode(10);
		$data['tokenExpiryDate']=date("Y-m-d H:i:s",strtotime('30 days'));
		$password=$data['password'];
		$data['password']=md5($data['password']);
		
		
		//insert data in table
		if($this->dbObj->addRecord($this->table,$data)){
			$this->dbObj->query;
			$this->tableId=$this->dbObj->result;
			$sucRes['userId']=$this->tableId;
			$this->result=$this->response('success',$sucRes);
			//send mail
			$mailData[$this->table."Id"]=$this->tableId;
			$mailData['email']=$data['email'];
			if(!empty($data['username'])){
				$mailData['username']=$data['username'];
			} else {
				$mailData['username']=$data['email'];
			}
			$mailData['name']=$data['firstName']." ".$data['lastName'];
			$mailData['password']=$password;
			$mailData['varificationToken']=$data['token'];
			$mailData['tokenExpiry']=$data['tokenExpiry'];
			$mailData['subject']=COMPANY_NAME.":: New Account Registration ";
			foreach ($mailData as $k=>$tmprec) {
				$log=$k."=>".$tmprec;
				//$this->general->log('test', $log);
			}
			
				$this->obj = new message();
				$message=$this->general->readFile(MODULE_PATH."profile/txt/userRegister.txt");
				$search = array('[USER_NAME]','[LOGO_PATH]', '[VERIFY_LINK]', '[LOGIN]','[PASSWORD]','[COMPANY_NAME]' );
				$replace = array($data ['firstName'], COMMON_IMAGES_URL.'logo.png',APPLICATION_URL.'profile/verify/verifyAccount/'.$data['token'] ,$data['username'],$_POST['password'], COMPANY);
				$message = str_replace($search, $replace, $message);
				
				if($this->obj->sendMail($data['email'], "Email Verification at ".COMPANY, $message, ADMIN_MAIL)){
					$condition[$this->table."Id"]=$this->tableId;
					$newData['emailStatus']=ACTIVE_VALUE;
					$this->dbObj->updateRecord($this->table, $newData, $condition);
				}
			
			//return result
			if(!empty($this->result)){
				return $this->result;
			}else {
				$this->debugMessage="error in XML Result";
				return true;
			}
		} else {
			$xmlArray['errorCode']=$this->dbObj->errorCode;
			$xmlArray['error']=$this->dbObj->error;
			$xmlArray['query']=$this->dbObj->query;
			$this->result=$this->response('error',$xmlArray);
		if(!empty($this->result)){
				return $this->result;
			}else {
				$this->debugMessage="error in XML Result";
				return false;
			}
		}
		
	}//end method register
	
	public function login(array $data, $admin=0)
	{
		if (is_object($this->dbObj) == false)
			$this->setDBObj();

		if (is_object($this->general) == false)
			$this->setGeneral();

		foreach ($data as $key => $value)
			if ($key != 'password')
				$rowData[$key] = $value;

		$seperator = " AND ";
		$rowData = array();

		if ($admin == 0) {
			$specCon = "(email = '" . $data['username'] . "' OR username = '" . $data['username'] . "' OR phone = '" . $data['username'] . "') AND (status=" . ACTIVE_VALUE . " OR status = " . INACTIVE_VALUE . ")";
			unset($rowData['username']);
		} else {
			$rowData['username'] = $data['username'];
			$rowData['emailVerification'] = 1;
			$rowData['status'] = 1;
		}

		//check if username exists
		$rows = $this->dbObj->getRows($this->table, $rowData, $seperator, $specCon);//removed ,'emailVerification'=>1 for letting userd login without verification for some time

		//echo $this->dbObj->query;
		$this->general->log('admin_Login_Failure', "[" . $this->dbObj->query);

		if ($admin == 1)
			$fields = ',username,name,logins,lastLogin,groupId,baseFolder';
		else
			$fields = ',firstName,lastName,email, logins,lastLogin,username, status,groupId,baseFolder';

		if ($rows == 1) {
			if (EMAIL_VERIFICATION == true)
				$rowData['emailVerification'] = 1;

			//check if username and password matches
			if ($this->dbObj->fetchRecord($this->table, $this->tableId . ",password" . $fields, $rowData, $seperator, $specCon)) { //removed ,'emailVerification'=>1
				if (count($this->dbObj->result) > 0) {
					if (md5($data['password']) === $this->dbObj->result[0]['password']) {
						$this->loginId = $this->dbObj->result[0][$this->tableId];

						//fetch Results
						$userDetails = $this->dbObj->result[0];
						$this->updateLoginStats($this->loginId, $this->dbObj->result[0]['logins']);
						if ($this->result = $this->response('success', $userDetails)) {
							return $this->result;
						} else {
							$this->debugMessage = "error in XML Result";
							return true;
						}
					} else {
						$xmlArray['errorCode'] = "1010";
						$xmlArray['error'] = "Password is incorrect";
						$xmlArray['query'] = $this->dbObj->query;
						if ($this->result = $this->response('failure', $xmlArray)) {
							return $this->result;
						} else {
							$this->debugMessage = "error in XML Result";
							return false;
						}
					}
				} else {
					$xmlArray['errorCode'] = "1010";
					$xmlArray['error'] = "Your email verification is pending. Please Verify your email address to login ";
					$xmlArray['query'] = $this->dbObj->query;
					if ($this->result = $this->response('failure', $xmlArray)) {
						return $this->result;
					} else {
						$this->debugMessage = "error in XML Result";
						return false;
					}
				}
			}
		} else {
			$xmlArray['error'] = "The username/email or password you entered is incorrect.";
			$xmlArray['errno'] = "1021";
			$xmlArray['query'] = $this->dbObj->query;
			if ($this->result = $this->response('failure', $xmlArray)) {
				return $this->result;
			} else {
				$this->debugMessage = "error in XML Result";
				return false;
			}
		}
	}
	
	//method to change password
	public function changeLogin($newValue,$oldValue,$id,$field='password'){
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		if($field=='password'){
			$oldValue=md5($oldValue);
			$newValue=md5($newValue);
		}
		//check if the old value is correct
		$rows=$this->dbObj->getRows($this->table,array($field=>$oldValue,$this->tableId=>$id));
		
		if($rows > 0){//if old values are correct
			//change the values
			if($this->dbObj->updateRecord($this->table,array($field=>$newValue,'updateDate'=>date("Y-m-d H:i:s")),array($this->tableId=>$id))){
				$xmlArray['error']="Successfully changed ".$field;
				if($this->result=$this->response('success',$xmlArray)){
					return $this->result;
				}else{
					$this->debugMessage="error in XML Result";
					return false;
				}
			}else{
				$xmlArray['error']="Unable to change ".$field;
				$xmlArray['errno']="1124";
				$xmlArray['query']=$this->dbObj->query;
				if($this->result=$this->response('Error',$xmlArray)){
					return $this->result;
				}else{
					$this->debugMessage="error in XML Result";
					return false;
				}
			}
			
		} else { //if old value doesn't match
				$xmlArray['error']="Incorrect current ".$field;
				$xmlArray['errno']="1123";
				$xmlArray['query']=$this->dbObj->query;
				if($this->result=$this->response('failure',$xmlArray)){
					return $this->result;
				}else{
					$this->debugMessage="error in XML Result";
					return false;
				}
		}	
		
	}//end changeLogin
	
	
	//method to recover Login
	public function recoverLogin($email, $admin=0){
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		
		if(is_object($this->general)==false){
			$this->setGeneral();
		}
		
		//check if email exist
		$emailRows=$this->dbObj->getRows($this->table,array('email'=>$email));
		$this->general->log("recover_login","Email Rows: ".$this->dbObj->query);
		//if email is found
		if($emailRows>0){
			//fetch data from table
			
		if($admin==1){
				$fields=$this->tableId.",username";
			} else {
				$fields=$this->tableId.",email";
			}
			
			$condition['email']=$email;
			if(EMAIL_VERIFICATION==1){
				$fields.=",emailVerification";
				$condition['emailVerification']=1;
			}
			if($this->dbObj->fetchRecord($this->table,$fields,$condition)){
				$result=$this->dbObj->result[0];
				$id=$this->dbObj->result[0][$this->tableId];
				//check if mail is verified
					
					//create new password
					$password=$this->general->randomCode(12);
					$this->general->log("recover_login","\n\r Password: ".$password);
					$this->general->log("recover_login","\n\r ID: ".$id);
					if($this->dbObj->updateRecord($this->table,array('password'=>md5($password)),array($this->tableId=>$id))){
						//send mail
						$mailData[$this->tableId]=$this->tableId;
						$mailData['email']=$email;
						$mailData['username']=$result['username'];
						$mailData['password']=$password;
						$mailData['subject']="Login Recovery Mail";
						if($this->sendMail('loginRecovery',$mailData)){
							$xmlArray['message']="email with username and password sent to your email.";
							$this->result=$this->response('success',$xmlArray);
							if(!empty($this->result)){
								return $this->result;
							}else {
								$this->debugMessage="error in XML Result";
								return false;
							}
						} else {
							$xmlArray['error']="email can't be sent. Please try again.";
							$this->result=$this->response('failure',$xmlArray);
							if(!empty($this->result)){
								return $this->result;
							}else {
								$this->debugMessage="error in XML Result";
								return false;
							}
						}
					
					}else {//if unable to set password
						$xmlArray['error']="error in reseting password";
						$xmlArray['errno']="1028";
						$xmlArray['query']=$this->dbObj->query;
						$this->result=$this->response('error',$xmlArray);
						if(!empty($this->result)){
							return $this->result;
						}else {
							$this->debugMessage="error in XML Result";
							return false;
						}
					}
			}else { //if error in fetching data from db
				$xmlArray['error']="error in fetching data from db.";
				$xmlArray['errno']="1026";
				$xmlArray['query']=$this->dbObj->query;
				$this->result=$this->response('error',$xmlArray);
				if(!empty($this->result)){
					return $this->result;
				}else {
					$this->debugMessage="error in XML Result";
					return false;
				}
			} //end fetching data from db
			

		} else {//if email is not found in system
			$xmlArray['error']="Incorrect email or email not registered.";
			$xmlArray['errno']="1025";
			$this->result=$this->response('error',$xmlArray);
			if(!empty($this->result)){
				return $this->result;
			}else {
				$this->debugMessage="error in XML Result";
				return false;
			}
		}//end if email is not found in system
		
	}//end recover Login
	
	private function updateLoginStats($ownerId,$logins){
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		$loginStat['logins']=$logins+1;
		$loginStat['status']=ACTIVE_VALUE;
		$loginStat['lastLogin']=date("Y-m-d H:i:s");
		$this->dbObj->updateRecord($this->table,$loginStat,array($this->tableId=>$ownerId));
	}
	
	public function verifyMail($action="verify",$verificationCode){
		if(is_object($this->dbObj)==false){
			$this->setDBObj();
		}
		if($action=="verify"){
			$code=1;
		} else{
			$code=2;
		}
		if($this->dbObj->updateRecord($this->table,array('emailVerification'=>$code),array('token'=>$verificationCode))){
					if($action=="verify"){
						$xmlArray['message']="Your email has been verified successfully! Welcome to Our website. Please login using user login details";
					} else{
						$xmlArray['message']="Your Email has been successfully removed form our system.";
					}		
			
					if($this->result=$this->response('success',$xmlArray)){
						return $this->result;
					}else{
						$this->debugMessage="error in XML Result";
						return false;
					}
		} else {
					$xmlArray['errorCode']=$this->dbObj->errorCode;
					$xmlArray['error']=$this->dbObj->error;
					if($this->result=$this->response('failure',$xmlArray)){
						return $this->result;
					}else{
						$this->debugMessage="error in XML Result";
						return false;
					}
		}
		
	}//end verifyMail
	
	//method to send confirmation mail
	private function sendMail($type,$data){
		//get file content
		$fh=fopen(TEXT_FILE_PATH.$type.".txt","r");
		$fileContent=fread($fh,filesize(TEXT_FILE_PATH.$type.".txt"));
		fclose($fh);
		//replace content
		$find=array("[IP_ADDRESS]","[VERIFY_LINK]","[USER_NAME]","[LOGIN]","[PASSWORD]","[COMPANY_NAME]","[LOGO_PATH]",);
		$replace=array($_SERVER['REMOTE_ADDR'],APPLICATION_URL."verify.php?verifyCode=".$data['varificationToken'],$data['name'],$data['email'],$data['password'],COMPANY,COMMON_IMAGES_URL."/logo.png");
		
		//prepare content
		$content=nl2br(str_replace($find,$replace,$fileContent));
		$mail=new mail();
		//$body = $mail->getFile('contents.html');
		$body = $content;//eregi_replace("[\]",'',$body);
		
		$mail->IsSMTP();
		$mail->SMTPAuth   = SMTP_AUTH;                  // enable SMTP authentication
		$mail->SMTPSecure = SMTP_SECURE;                 // sets the prefix to the servier
		$mail->Host       = SMTP_HOST;      // sets GMAIL as the SMTP server
		$mail->Port       = SMTP_PORT;                   // set the SMTP port for the GMAIL server
		
		$mail->Username   = SMTP_AUTH_USERNAME;  // GMAIL username
		$mail->Password   = SMTP_AUTH_PASSWORD;            // GMAIL password
		
		$mail->AddReplyTo(ADMIN_REPLY_MAIL,ADMIN_MAIL_NAME);
		
		$mail->From       = ADMIN_MAIL;
		$mail->FromName   = ADMIN_MAIL_NAME;
		
		$mail->Subject    = $data['subject'];
		
		$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$mail->WordWrap   = 50; // set word wrap
		
		$mail->MsgHTML($body);
		
		$mail->AddAddress($data['email'], "John Doe");
			
		$mail->IsHTML(true); // send as HTML
	

		// Additional headers
		$headers = "To: ".$data['email']."\r\n";
		$headers .= "From: ".ADMIN_EMAIL." \r\n";
		
		if(@$mail->Send()){
			if(is_object($this->dbObj)==false){
				$this->setDBObj();
			} 
			$updateData['emailStatus']=1;
			if($this->dbObj->updateRecord($this->table,$updateData,array("userId"=>$data['userId']))) {
				
			} else {
				$this->debugMessage="[".date("Y-m-d H:i:s")."] ERROR: ".$mail->ErrorInfo. " \n \n";
				$this->general->log('Mail_Send_Failure',$this->dbObj->query);
			}
		} else {
			$headers = 'From: '.ADMIN_MAIL . PHP_EOL .
           'X-Mailer: PHP-' . phpversion() . PHP_EOL.
			'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=UTF-8';
				$to=$data['email'];
				$subject=$data['subject'];
				$message=$body;
			if(mail($to, $subject, $message, $headers)) {
				$updateData['emailStatus']=1;
				$this->dbObj->updateRecord($this->table,$updateData,array("userId"=>$data['userId']));
				return true;
			} else {
				$this->debugMessage="[".date("Y-m-d H:i:s")."] ERROR: ".$mail->ErrorInfo. " \n \n";
				$this->general->log('Mail_Send_Failure',$mail->debugMessage);
				return false;
			}
			
			
		}
		
		
	}//end sendMail
	
	//method to generate XML
	private function xmlResponse($xmlResult,$data){
		$response="<auth>";
		$response.="<result>".$xmlResult."</result>";
		if(is_array($data)){
			foreach($data as $key=>$value){
				if(is_numeric($key)===false){
				$response.="<".$key.">".$value."</".$key.">";
				}
			}
		}
		$response.="</auth>"; 
		return $response;
	}//end xmlResult
	
	
	private function response ($res,$data) {
		$array=array();	
		$array['result']=$res;
	if(is_array($data)){
			foreach($data as $key=>$value){
				if(is_numeric($key)===false){
				$array['data'][$key]=$value;
				}
			}
		}
		return $array;
	}
	//
}//end class auth