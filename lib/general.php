<?php
class general {
	public $message;
	
	function cleanInput($field) {
		//$output=$_POST[$field];
		$output = htmlspecialchars ( strip_tags ( trim ( $field ) ) );
		//$output = escapeshellcmd ( $output );
		//$output=escapeshellarg(escapeshellcmd($output));
		return $output;
	}
	function getFileExtension($str) {
		$i = strrpos ( $str, "." );
		
		if ($i == "") {
			return "";
		}
		
		$l = strlen ( $str ) - $i;
		$ext = substr ( $str, $i + 1, $l );
		return $ext;
	}
	function compare_values($val, $arr) {
		$val = trim ( $val );
		$cmpflag = false;
		foreach ( $arr as $item ) {
			$item1 = trim ( $item );
			if (strcasecmp ( $item1, $val ) == 0) {
				$cmpflag = true;
				break;
			}
		}
		return $cmpflag;
	}
	function getCurrentDate() {
		$currdate = strftime ( "%Y-%m-%d" );
		return $currdate;
	
	}
	
	public function getOS(){
		$os_string = php_uname('s');
		if (strpos(strtoupper($os_string), 'WIN')!==false){	
			return 'WIN';
		} else {
			return 'LINUX';
		}
	}
	
	//function to upload file
	function uploadFile($fld, $name, $path) {
		$file = new validations ( );
		//var_dump($_FILES[$fld]);
		switch ($_FILES [$fld] ['error']) {
			case 0 :
				if ($file->validateFile ( $fld )) { //if valid file
					if (move_uploaded_file ( $_FILES [$fld] ['tmp_name'], $path . "/" . $name ))
						$this->message = "File Uploaded!!!";
						return true;
				} else { //if not valid file
					$this->message = "Uploaded file in not Valid!!!" . $file->result;
				}
				break;
			case 1 :
				$this->message = "Upload File exceeded Maximum file size allowed by the server.";
				break;
			case 2 :
				$this->message = "Upload File exceeded Maximum file size allowed.";
				break;
			case 3 :
				$this->message = "Uploaded file was only partially uploaded.";
				break;
			case 4 :
				$this->message = "No File Uploaded.";
				break;
			case 6 :
				$this->message = "Temporary file folder missing.";
				break;
			case 7 :
				$this->message = "Failed to write file to disk.";
				break;
			case 8 :
				$this->message = "File upload stopped due to extension.";
				break;
		}
		return false;
	
	}
	function get_time_difference($start, $end) {
		$uts ['start'] = strtotime ( $start );
		$uts ['end'] = strtotime ( $end );
		if ($uts ['start'] !== - 1 && $uts ['end'] !== - 1) {
			if ($uts ['end'] >= $uts ['start']) {
				$diff = $uts ['end'] - $uts ['start'];
				if ($days = intval ( (floor ( $diff / 86400 )) ))
					$diff = $diff % 86400;
				if ($hours = intval ( (floor ( $diff / 3600 )) ))
					$diff = $diff % 3600;
				if ($minutes = intval ( (floor ( $diff / 60 )) ))
					$diff = $diff % 60;
				$diff = intval ( $diff );
				return (array ('days' => $days, 'hours' => $hours, 'minutes' => $minutes, 'seconds' => $diff ));
			} else {
				trigger_error ( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
			}
		} else {
			trigger_error ( "Invalid date/time data detected", E_USER_WARNING );
			return (false);
		}
	
	}
	public function prepareOutput($inputData, $length = "") {
		$output = stripslashes ( $inputData );
		$output = stripslashes ( $output );
		//$output=htmlentities($inputData);
		$output = trim ( $output );
		$output = str_replace ( "%20", " ", $output );
		if (empty ( $length )) {
			$length = strlen ( $output );
		}
		if (! empty ( $length ) && is_numeric ( $length )) {
			if (strlen ( $output ) > $length) {
				$output = substr ( $output, 0, $length );
				$output.= "... ";
			} 
			return $output;
		}
	}
	
	//method to redirect to given location
	public function redirect($location) {
		header ( "Location:" . $location );
		return true;
	}

	//method to prepare message
	public function prepareMessage($msg, $debug = "") {
		if (SHOW_DEBUG === true) {
			$msg .= "<br>" . $debug;
			return $msg;
		} else {
			return $msg;
		}
		
	}

	private static $DBlink;

	public function logToDB ($object, $operation, $message) {
		if (!isset($this->DBlink)) {
			$this->DBlink = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("DB Error " . mysqli_error($this->DBlink));
			mysqli_query($this->DBlink,"SET NAMES utf8");
		}
		$query  = "INSERT INTO `".TBL_ACTIVITY."` (`user_id`,   `object`, `operation`,     `details`)";
		$query .= " VALUES (" .$_SESSION['admin']['adminId'].", '".$object."', '".$operation."','".$message."')";
		mysqli_query($this->DBlink, $query);
	}
	
	public function log($type, $string, $printToOutput = false) {
		if ($printToOutput)
			echo $string . "\n";
		if (LOGGING) {
			if (! file_exists ( LOG_PATH . $type . "/" ))
				mkdir ( LOG_PATH . $type . "/", 0777, true );
			$fp = fopen ( LOG_PATH . $type . "/" . $type . "-" . date ( 'Y-m-d' ) . ".txt", "a+" );
			fwrite ( $fp, "[" . date ( 'Y-m-d H:i:s' ) . "] " . $string." \n" );
			fclose ( $fp );		
		}
	}

    public function errorTrace ($message, $trace="") {
        $this->log ("ERROR", $message);
        $fp = fopen ( LOG_PATH . "ERROR/DEBUG-" . date ( 'Y-m-d' ) . ".txt", "a+" );
        fwrite ( $fp, "[" . date ( 'Y-m-d H:i:s' ) . "] " . $message." \n" .
            $trace . "\n" .
            "SERVER" .   print_r($_SERVER, true) .
            "COOKIE: " . print_r($_COOKIE, true) .
            "GET" .      print_r($_GET, true) .
            "SESSION" .  print_r($_SESSION, true) . "\n");
        fclose ( $fp );
    }

    public function page404($error = null, $trace="") {
        if ($error != null)
            $this->errorTrace ($error, $trace);
        header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
        print '<html>
<head>
    <meta charset="utf-8">
    <title>Page not found</title>
<body>
<h2>
    Page not found - error 404.
    </h2>
Try to start at <big><b><a href="/">home page</a></b></big>.

</body>
</html>';
        die();
    }

	public function randomCode($length, $type = "ALL") {
		$chars = "abcdefghijklmnopqrstuvwxyz";
		$nums = "1234567890";
		
		if ($type == "CHARS") {
			$probs = $chars;
		} else if ($type == "NUMS") {
			$probs = $nums;
		} else {
			$probs = $chars . $nums;
		}

        $randomCode = "";
		for($i = 0; $i < $length; $i ++) {
			$randomNumber = rand ( 0, 35 );
			$randomCode .= substr ( $probs, $randomNumber, 1 );
		}
		return $randomCode;
	}

	public function convertDate($date,$type='all') {
		if($type=='date'){
			$newDate=date(DATE_FORMAT,strtotime($date));
		} else if($type=='time'){
			$newDate=date(TIME_FORMAT,strtotime($date));
		} else {
			$newDate=date(DATE_TIMES_FORMAT,strtotime($date));
		}
		return $newDate;
	}

	public function readFile($file) {
		if (file_exists($file)) {
			if (is_file($file)) {
				if (is_readable($file)) {
					$fh=fopen($file,"rb");
					$content=@fread($fh,filesize($file));
					fclose($fh);
					return $content;
				} else {
					$this->message = "File is not readable.";
					return false;
				} 
			} else {
				$this->message = "Specified file is not regular file.";
				return false;
			}
		} else {
			$this->message = "File $file does not exist.";
			return false;
		}
	}
	
	function scanFolder ($folder_path) {
		if($files = opendir($folder_path)) {
            //WIN:  opendir(iconv('UTF-8','Windows-1251',$folder_path))) {
			while ($file =readdir($files)) {
				if($file !="." && $file != "..")
					$array[] = $file;
                        //WIN: iconv('Windows-1251', 'UTF-8', $file);
			}
			closedir($files);
		} else {
			$this->errorMessage ="Sorry! folder Not found.";
		}
		
		return $array;
	}

	function formatSizeUnits($bytes)
	{
		if ($bytes >= 1073741824)
		{
			$bytes = number_format($bytes / 1073741824, 2) . ' GB';
		}
		elseif ($bytes >= 1048576)
		{
			$bytes = number_format($bytes / 1048576, 2) . ' MB';
		}
		elseif ($bytes >= 1024)
		{
			$bytes = number_format($bytes / 1024, 2) . ' KB';
		}
		elseif ($bytes > 1)
		{
			$bytes = $bytes . ' bytes';
		}
		elseif ($bytes == 1)
		{
			$bytes = $bytes . ' byte';
		}
		else
		{
			$bytes = '0 bytes';
		}

		return $bytes;
	}

	function scanDir($dir, &$ar){
	$dh = opendir($dir);
				$path_curent = '';
				while (($file = readdir($dh)) !== false) {
					if ($file != "." and $file != "..") {
						$path = $dir . $file;
						//print $path."<br>";
						if (is_dir($path)) {
							$path_curent.= $this->scanDir($path."/", $ar);
							//* можно и файлы
						}else{
							if(is_file($path)) {
							$ar[]=$path;
							}

						}
					}
				}
				closedir($dh);

			return $ar;

	}
	
	
	function removedir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir."/".$object) == "dir")
						$this->removedir($dir."/".$object);
					else unlink   ($dir."/".$object);
				}
			}
			reset($objects);
			rmdir($dir);
		}
	}

	function getDirectorySize($path) {
		$totalsize = 0;
		$totalcount = 0;
		$dircount = 0;
		if($handle = opendir($path))
		{
			while (false !== ($file = readdir($handle)))
			{
				$nextpath = $path . '/' . $file;
				if($file != '.' && $file != '..' && !is_link ($nextpath))
				{
					if(is_dir($nextpath))
					{
						$dircount++;
						$result = $this->getDirectorySize($nextpath);
						$totalsize += $result['size'];
						$totalcount += $result['count'];
						$dircount += $result['dircount'];
					}
					else if(is_file ($nextpath))
					{
						$totalsize += filesize ($nextpath);
						$totalcount++;
					}
				}
			}
            closedir($handle);
        }
		$total['size'] = $totalsize;
		$total['count'] = $totalcount;
		$total['dircount'] = $dircount;
		return $total;
	}
	
}
