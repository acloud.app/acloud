<?php
class html {
	// class properties
	public $error, $result, $fetchedResult, $general, $blockContent, $jqLoadScript, $script, $scriptURL, $head, $templateName, $module, $lang, $langSymbol, $controller, $query;
	private $css, $recordTable, $js, $cssIE, $headData, $header, $page, $content, $genPage, $genPageContent, $pageLayout, $childArray, $showError, $errorMsg, $msg;
	public function __construct($template = CURRENT_TEMPLATE) {
		// add common lang array
		$this->general = new general ();
		$this->templateName = $template;
		$this->query = explode ( "/", str_replace ( "index.php/", "", $_GET ['query'] ) );
		$this->head = ! empty ( $_GET ['head'] ) ? $_GET ['head'] : DEFAULT_HEAD;
		$this->module = ! empty ( $this->query [0] ) ? $this->checkModule () : DEFAULT_MODULE;
		
		// for non-existing URLs
		if (!file_exists ( MODULE_PATH .  $this->query [0] ) && $this->head !="admin") {
                header("HTTP/1.0 404 Not Found");
                die("Error 404<br>Page not found.");
		}
		// echo DEFAULT_LANGUAGE;
		$this->langSymbol = ! empty ( $_SESSION ['language'] ['symbol'] ) ? $_SESSION ['language'] ['symbol'] : strtolower ( DEFAULT_LANGUAGE );
		if (! empty ( $_SESSION ['language'] ['symbol'] )) {
			$this->langSymbol = strtolower ( $_SESSION ['language'] ['symbol'] );
			// $filePath = MODULE_PATH . 'common/' . LANG_PATH . '/' . $_SESSION
			// ['language'] ['symbol'] . '.txt';
		} else {
			$this->langSymbol = strtolower ( DEFAULT_LANGUAGE );
			// $filePath = MODULE_PATH . 'common/' . LANG_PATH . '/' . $tmp .
			// '.txt';
		}
		
		// load lang files
		$modules = scandir ( MODULE_PATH );
		if (is_array ( $modules )) {
			foreach ( $modules as $mod ) {
				if (strpos ( $mod, "." ) === false) {
					$this->addLang ( MODULE_PATH . $mod . LANG_PATH . '/' . $this->langSymbol . '.txt' );
				}
			}
		}
		
		// add common language
		
		$module = $this->module;
		if ($module != "common") {
			$this->controller = new $module ();
		} else {
			$this->controller = new general ();
		}
		$this->query = explode ( "/", str_replace ( "index.php/", "", $_GET ['query'] ) );
		$this->page = $this->query [1];
		if (! empty ( $this->query [2] )) {
			$fun = $this->query [2];
			if (method_exists ( $this->controller, $fun )) {
				if ($this->query [3]) {
					$this->result = $this->controller->{$fun} ( $this->query [3] );
				} else {
					$this->result = $this->controller->{$fun} ();
				}
			}
		}
		$this->page = ! empty ( $this->query [1] ) ? $this->query [1] : DEFAULT_PAGE;
	}

	public function fetch($param, $con = "") {
		$mod = substr ( $param, 0, strpos ( $param, "_" ) );
		$method = substr ( $param, strpos ( $param, "_" ) + 1 );
		$obj = new $mod ();
		$this->fetchedResult = $obj->{$method} ( $con );
	}
	
	// private method to set module
	private function checkModule() {
		$moduleList = scandir ( MODULE_PATH );
		if (in_array ( $this->query [0], $moduleList )) {
			return $this->query [0];
		} else {
			if ($this->head != "admin") {
				return PROFILE_MODULE;
			} else {
				return DEFAULT_MODULE;
			}
		}
	}
	
	// method to load template
	public function load() {
		require_once MODULE_PATH . 'common/template/' . $this->templateName . '/' . $this->head . '/page.tpl';
	} // end load

	public function __destruct() {
		if (empty ( $_GET ['ajax'] )) {
			echo "\n\r</body>\n\r";
			echo "</html>";
		}
	}

	private function addLang($file) {
		$langFile = $file;
		if (file_exists ( $langFile )) {
			$content = $this->general->readFile ( $langFile );
			$rawArray = explode ( ",", $content );
			if (is_array ( $rawArray )) {
				foreach ( $rawArray as $rawElement ) {
					$rawVal = explode ( "=>", $rawElement );
					$key = trim ( $rawVal [0] );
					$this->lang [$key] = trim ( $rawVal [1], "'" );
				}
			}
		}
	}

	public function addHeader() {
		echo '<!DOCTYPE html>
<html><head>
<meta charset="' . $_ENV ['meta'] ['charset'] . '">';

		foreach ( $_ENV ['meta'] ['http-equiv'] as $id => $value )
			echo "\n\r<meta http-equiv=\"" . $id . "\" content=\"" . $value . "\">";

		echo "\n\r".'<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">'."\n\r";

		//for custom pages
		if ($this->module=="information") {
		    $info = new information();
		    $pageDetails = $info->page($this->query[3]);
		    echo '<title>'.$pageDetails['data']['meta_title'].'</title>';
		} else if ($this->head == "admin")
			echo "<title>" . ADMIN_TITLE . "</title>\n\r";
		else {// all other pages
			$title='';
			//Тут выводим титлы альбомов и треков
			if(!empty($_GET['album'])){
				$lib = new library ();
				$folderDetail = $lib->getFolderDetails ( intval($_GET['album']));
				$title= "<title>".COMPANY_NAME.": " . $folderDetail['folderName'] . "</title>\n\r";
			}

			if(!empty($_GET['track'])){
				$lib = new library ();
				$arTrack = $lib->getTrackDetails( intval($_GET['track']));

				if(strlen($arTrack['trackTitle'])<3){
					@$formatFile = end(explode(".", $arTrack['trackFile'])); // File extension
					$length = strlen($formatFile) + 1; // number of chars in extension
					$file_title = mb_substr($arTrack['trackFile'], 0, -$length);
				} else
					$file_title=$arTrack['trackTitle'];

				$title= "<title>".COMPANY_NAME.": " .$arTrack['album'] ." ".  $arTrack['artist'] ." ". $file_title. "</title>\n\r";
			}
			if($title=='')
				$title= "<title>" . APPLICATION_TITLE . "</title>\n\r";
			print $title;

		}
		print "<script type='text/javascript'>
		var  comment= '".COMMENTS_PUBLICATIONS."';
		var  comment_id= '".COMMENTS_PUBLICATIONS_ID."';
</script>\n\r";
		
		// add IE related CSS
		if (is_array ( $this->cssIE )) {
			foreach ( $this->cssIE as $cssItem ) {
				if (! empty ( $cssItem )) {
					echo "<link href=\"" . MODULE_URL . 'common/css/' . $cssItem . "\" type=\"text/css\"  rel=\"stylesheet\"/> \n\r";
				}
			}
		}
		// include non-IE style sheets
		echo "<![if !IE]> \n\r";
		if (is_array ( $this->css )) {
			foreach ( $this->css as $cssItem ) {
				if (! empty ( $cssItem )) {
					echo "<link href=\"" . MODULE_URL . 'common/css/' . $cssItem . "\" type=\"text/css\"  rel=\"stylesheet\"/> \n\r";
				}
			}
		}
		
		echo "<![endif]> \n\r";

		if($this->head == "admin"){
			echo "\n";
			echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">';
			echo "\n";
			echo '<link rel="stylesheet" href="' . MODULE_URL . 'common/css/AudioCMSTools.css">';
			echo "\n";
			echo '<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>';
			echo "\n\n";
		}

		if (! empty ( $this->js )) {
			echo '<script>var globalApplicationTitle = "'.APPLICATION_TITLE.'", globalLibraryTitle = "'.AUDIO_LIBRARY.'", APPLICATION_URL = "'.APPLICATION_URL.'", COMPANY_NAME = "'.COMPANY_NAME.'"';
			if ( $this->head == "admin" )
				echo ', baseFolder = "' . ( $_SESSION[ "admin" ][ "groupId" ] == "admin" ? "0" : $_SESSION[ "admin" ][ "baseFolder" ] ) . '"';
			echo ';</script>' . "\n\r";
			foreach ( $this->js as $jsItem ) {
				if (! empty ( $jsItem )) {
					echo "<script src=\"" . MODULE_URL . 'common/js/' . $jsItem . "\" type=\"text/javascript\"></script>\n\r";
				}
			}
		}
if(GOOGLE_ANALYTICS_ENABLE=='Yes'){
		echo '<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push([\'_setAccount\', \'' . GOOGLE_ANALYTICS_CODE . '\']);
	_gaq.push([\'_setDomainName\', \'' . GOOGLE_ANALYTICS_DOMAIN . '\']);
	_gaq.push([\'_trackPageview\']);

	(function() {
	var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
	ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';
	var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>';
}
echo '
<!-- Files for Gallery -->
<link rel="stylesheet" href="' . MODULE_URL . 'gallery/photoswipe.css">
<link rel="stylesheet" href="' . MODULE_URL . 'gallery/default-skin/default-skin.css">
<script src="' . MODULE_URL . 'gallery/photoswipe.min.js"></script>
<script src="' . MODULE_URL . 'gallery/photoswipe-ui-default.min.js"></script>';

		echo "
<!-- Script for Gallery -->
<script>
var openPhotoSwipe = function(indexID) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
    items = [],
	indexID_Int = parseInt(indexID);

	$(\"a[link-type='galleyItem']\").each(function(){
		var photo = {
                src:            $(this).attr('img-url'),
                w:              $(this).attr('img-width'),
                h:              $(this).attr('img-height'),
                original_src:   $(this).attr('img-url-original')
	    };
		items.push(photo);
	});

    var options = {
        index: indexID_Int,
        focus: false,
        history: false,
        showAnimationDuration: 500,
        hideAnimationDuration: 500,
        shareEl: false
    };

    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.listen('afterChange', function() {
        $('#download_photo').attr('href', gallery.currItem.original_src);
    });
	gallery.init();

	$('#header').hide();

	gallery.listen('close', function() {
		$('#header').show();
	});
};
</script>

<!-- Script for Menu -->
<script>
$(document).ready(function(){
	$('#mobile-navigation-content').click(function(){
		toggleMobile('#header-links', menuShown, 'off');
		toggleMobile('#playList', playListShown, 'off');
	});
	$('#mobile-navigation-menu').click(function(){
		toggleMobile('#header-links', menuShown);
	});
	// If any of menu links is clicked, hide menu
	$('#header-links > ul > li > a').click(function(){
		if($('#header-links').css('display') == 'block'){
			toggleMobile('#header-links', menuShown, 'off');
			toggleMobile('#playList', playListShown, 'off');
		}
	});
	$('#mobile-navigation-playlist').click(function(){
		toggleMobile('#playList', playListShown);
	});
});
</script>
";
        if (mb_strtolower(RSS_ENABLED)=="yes") {
			echo '<link href="' . RSS_LINK . '" rel="alternate" type="application/rss+xml" title="' . RSS_TITLE . '" />' . "\r\n";
		}
echo "</head>
<body>
   <div id='mask'></div>
   <div id='popUpDiv'></div>
   <div id='popUpDiv1'></div>
   <div id='root'>
";
		include MODULE_PATH . 'common/' . TEMPLATE_PATH . '/' . $this->templateName . '/' . $this->head . '/header.tpl';
	}

	public function addPage($page, $module = "") {
		// add common language
		$module = ! empty ( $module ) ? $module : $this->module;
		if (file_exists ( MODULE_PATH . $module . '/' . TEMPLATE_PATH . '/' . $this->templateName . '/' . $this->head . '/' . $page . '.tpl' )) {
			include MODULE_PATH . $module . '/' . TEMPLATE_PATH . '/' . $this->templateName . '/' . $this->head . '/' . $page . '.tpl';
		} else
			header("HTTP/1.0 404 Not Found");
	}

	public function addCommonContent($contentPage) {
		include MODULE_PATH . 'common/' . TEMPLATE_PATH . '/' . $this->templateName . '/' . $this->head . '/' . $contentPage . '.tpl';
	}

	public function addFooter() {
		include MODULE_PATH . 'common/' . TEMPLATE_PATH . '/' . $this->templateName . '/' . $this->head . '/footer.tpl';
		echo "</div>\n\r"; // closed div #root
	}

	public function addCommonCSS($css) {
		$this->css [] = $css;
	}

	public function addCommonCSSIE($css) {
		$this->cssIE [] = $css;
	}

	public function addCommonJS($js) {
		$this->js [] = $js;
	}

	public function addCSS($css, $common = false, $module = "") {
		if ($common == true) {
			echo "<link href=\"" . MODULE_URL . 'common/css/' . $css . "\" type=\"text/css\"  rel=\"stylesheet\"/> \n\r";
		} else if (! empty ( $module )) {
			$link = "<link href=\"" . MODULE_URL . $module . '/css/' . $css . "\" type=\"text/css\"  rel=\"stylesheet\" >";
			echo $link;
		} else {
			$link = "<link href=\"" . MODULE_URL . $this->module . '/css/' . $css . "\" type=\"text/css\"  rel=\"stylesheet\" >";
			echo $link;
		}
	}

	public function addCSSIE($css, $common = false, $module = "") {
		if ($common == true) {
			echo "<link href=\"" . MODULE_URL . 'common/css/' . $css . "\" type=\"text/css\"  rel=\"stylesheet\"/> \n\r";
		} else if (! empty ( $module )) {
			$link = "<link href=\"" . MODULE_URL . $module . '/css/' . $css . "\" type=\"text/css\"  rel=\"stylesheet\" >";
			echo $link;
		} else {
			echo "<link href=\"" . MODULE_URL . $this->module . '/css/' . $css . "\" type=\"text/css\"  rel=\"stylesheet\"/> \n\r";
		}
	}

	public function addJS($js, $common = false, $module = "") {
		if ($common == true) {
			echo "<script src=\"" . MODULE_URL . 'common/js/' . $js . "\" type=\"text/javascript\"></script>\n\r";
		} else if (! empty ( $module )) {
			echo "<script src=\"" . MODULE_URL . $module . '/js/' . $js . "\" type=\"text/javascript\"></script>\n\r";
		} else {
			echo "<script src=\"" . MODULE_URL . $this->module . '/js/' . $js . "\" type=\"text/javascript\"></script>\n\r";
		}
	}
	
	// method to redirect page
	public function redirect($url) {
		header ( 'location:' . $url );
	}

	// method to render basic html
	public function loadDatabaseTable($controller, $page = 1, $searchKey = "", $recordsPerPage = RECORDS_PER_PAGE, $orderBy = "", $recordId = "", $action = "", $divId = "dbTableDiv") {
		if (strpos ( $_SERVER ['HTTP_REFERER'], '/edit' )) {
			$referer = substr ( $_SERVER ['HTTP_REFERER'], 0, strpos ( $_SERVER ['HTTP_REFERER'], '/edit' ) );
		} else {
			$referer = $_SERVER ['HTTP_REFERER'];
		}
		
		$this->recordTable = new recordTable ( $controller, $referer );
		if (empty ( $action )) {
			$this->recordTable->renderTable ( $divId, $page, $orderBy, $searchKey, $recordsPerPage );
		} else {
			$this->recordTable->{$action} ( $recordId, $divId, $page, $orderBy, $searchKey, $recordsPerPage );
		}
		echo '<div id="' . $divId . '">';
		echo $this->recordTable->result;
		echo "</div>\n\r";
	}

	public function addLoadDatabaseTableScript($fetch, $divId = "dbTableDiv", $editRecords = false, $filter = "", $customButton = "",$customButtonFilter = "",$customButtonAction = "", $no_status=false) {
		echo '<div id="' . $divId . '"></div>';
		if ($editRecords !== false) {
			$noEdit = "&noEdit=true";
		} else {
			$noEdit = "";
		}
		
		if ($no_status !== false) {
			$noStatus = "&noStatus=true";
		} else {
			$noStatus = "";
		}
		echo '<script type="text/javascript">
$(document).ready(function(){ 
loadDBTable("dbTableDiv","' . MODULE_URL . 'common/ajax/databaseTable.php?div=' . $divId . '&fetch=' . $fetch . '&page=1' . $noEdit . $noStatus . '&filter=' . $filter . '&customButton=' . $customButton . '&customButtonFilter=' . $customButtonFilter . '&customButtonAction=' . $customButtonAction . '");
});
</script>';
	}

	public function prepareLink($content, $link, $action = "", $ajax = false, $container = 'content_root', $image = false, $title = "", $target = "", $class = "",$id="") {
		if ($ajax == true) {
			$href = "javascript:;";
		} else {
			if (strpos ( $link, "http" )===0) {
				$href = $link;
			} else {
				$href = APPLICATION_URL . str_replace ( "_", "/", $link );
			}
		}
		
		if (! empty ( $action ) && strpos ( $action, "(" ) == false) {
			$onClick = 'onClick="' . $action . '(\'' . APPLICATION_URL . str_replace ( "_", "/", $link ) . '\',\'' . $container . '\');  return false;"';
		} else if (! empty ( $action ) && strpos ( $action, "(" ) != false) {
			$onClick = 'onClick="' . $action . '"';
		}
		
		if (! empty ( $target )) {
			$linkTarget = 'target="' . $target . '"';
		}
		
		$id=!empty($id)?"id='".$id."'":'';
		
		if ($image == true) {
			echo '<a href="' . $href . '" ' . $onClick . ' ' . $linkTarget . ' '.$id.' class="' . $class . '"><img src="' . $content . '" title="' . $title . '" alt="' . $title . '" /></a>';
		} else {
			echo '<a href="' . $href . '" ' . $onClick . ' ' . $linkTarget . ' class="' . $class . '">' . $content . '</a>';
		}
	}

	public function renderDBSelectBox($module, $action, $name, $valueField = "", $displayField = "", $id = "", $defaultValue = "") {
		$obj = new $module ();
		$result = $obj->{$action} ();
		if (is_array ( $result )) {
			echo '<select name="' . $name . '" id="' . $id . '">';
			echo '<option value="0">-- Select --</option>';
			foreach ( $result as $record ) {
				if ($record [$valueField] == $defaultValue) {
					echo '<option value="' . $record [$valueField] . '" selected="selected">' . $record [$displayField] . '</option>';
				} else {
					echo '<option value="' . $record [$valueField] . '">' . $record [$displayField] . '</option>';
				}
			}
			echo '</select>';
		}
	}

	public function ajaxPagination($totalPages = 1, $currentPage = 1, $link = "", $action = "", $container = "") {
		if ($totalPages < 1) {
			return;
		}
		// first page
		$pagination = '';
		if ($currentPage != 1) {
			$pagination .= '<a href="#" class="pagination-button" onClick="' . $action . '(\'' . $link . '1' . '\',\'' . $container . '\')"><img src="' . COMMON_IMAGES_URL . 'pagination_first.png" /></a>';
		} else {
			$pagination .= '<a href="#" class="pagination-button"><img src="' . COMMON_IMAGES_URL . 'pagination_first.png" /></a>';
		}
		
		// previous page
		if ($currentPage != 1) {
			$pagination .= '<a href="#" class="pagination-button" onClick="' . $action . '(\'' . $link . ($currentPage - 1) . '\',\'' . $container . '\')"><img src="' . COMMON_IMAGES_URL . 'pagination_prev.png" /></a>';
		} else {
			$pagination .= '<a href="#" class="pagination-button"><img src="' . COMMON_IMAGES_URL . 'pagination_prev.png" /></a>';
		}
		
		// pages
		$pStart = ($currentPage - 3) > 0 ? $currentPage - 3 : 1;
		$pEnd = ($currentPage + 3) < $totalPages ? $currentPage + 3 : $totalPages;
		
		for($i = $pStart; $i <= $pEnd; $i ++) {
			if ($i == $pStart && $i == $currentPage) {
				$pagination .= '<div class="pagination-active-page">' . $i . '</div>';
			} else if ($i == $pStart) {
				$pagination .= '<a href="#" class="pagination-inactive-page" onClick ="' . $action . '(\'' . $link . ($i) . '\',\'' . $container . '\')">' . $i . '</a>';
			} else if ($i == $pEnd && $i == $currentPage) {
				$pagination .= '<div class="pagination-active-page">' . $i . '</div>';
			} else if ($i == $pEnd) {
				$pagination .= '<a href="#" class="pagination-inactive-page" onClick ="' . $action . '(\'' . $link . ($i) . '\',\'' . $container . '\')">' . $i . '</a>';
			} else if ($i == $currentPage) {
				$pagination .= '<div class="pagination-active-page">' . $i . '</div>';
			} else {
				$pagination .= '<a href="#" class="pagination-inactive-page" onClick ="' . $action . '(\'' . $link . ($i) . '\',\'' . $container . '\')">' . $i . '</a>';
			}
		}
		
		// next page
		if ($currentPage != $totalPages) {
			$pagination .= '<a href="#" class="pagination-button" onClick="' . $action . '(\'' . $link . ($currentPage + 1) . '\',\'' . $container . '\')"><img src="' . COMMON_IMAGES_URL . 'pagination_next.png" /></a>';
		} else {
			$pagination .= '<a href="#"class="pagination-button"><img src="' . COMMON_IMAGES_URL . 'pagination_next.png" /></a>';
		}
		
		// last page
		if ($currentPage != $totalPages) {
			$pagination .= '<a href="#" class="pagination-button" onClick="' . $action . '(\'' . $link . ($totalPages) . '\',\'' . $container . '\')"><img src="' . COMMON_IMAGES_URL . 'pagination_last.png" /></a>';
		} else {
			$pagination .= '<a href="#" class="pagination-button"><img src="' . COMMON_IMAGES_URL . 'pagination_last.png" /></a>';
		}
		return $pagination;
	}

	public function renderMessage($message, $refresh) {
		echo '<div id="msgDiv" class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>' . $message . '</div></div>';
		echo '<script>
			var refresh = '.($refresh)?true:false.';
			setTimeout(function(){
				$("#msgDiv").fadeOut("slow", function () {
					$("#msgDiv").attr("display","None");
					if(refresh == true){
						window.location="'.ADMIN_URL.$this->module.'/'.$this->page.'";
					}
				});
			}, 2000);
		</script>';
	}

	public function renderErrorMessage($message,$refresh=false ) {
		echo '<div id="msgDiv" class="ui-widget" style="margin: 0 auto;float:none;"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>' . $message . '</div></div>';
		echo '<script>setTimeout(function(){
  $("#msgDiv").fadeOut("slow", function () {
  $("#msgDiv").attr("display","None");
				';
		if($refresh==true){
			echo 'window.location="'.ADMIN_URL.$this->module.'/'.$this->page.'"';
						}
	echo '
      });
	
}, 2000);</script>
		';
	}
}