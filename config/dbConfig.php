<?php
define ("DB_HOST",      "localhost" );
define ("DB_NAME",      "devprop" );
define ("DB_USERNAME",  "root" );
define ("DB_PASSWORD",  "secret" );

//tables
define ("TBL_ADMIN",            "admin" );
define ("TBL_ACTIVITY",         "activity" );
define ("TBL_TRACKS",           "tracks");
define ("TBL_FOLDERS",          "folder");
define ("TBL_PLAYLIST",         "playlist");
define ("TBL_PLAYLIST_DATA",    "playlistData");
define ("TBL_CMS",              "cms");
define ("TBL_SETTINGS",         "settings");