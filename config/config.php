<?php
/*
 * Script for General configuration, autoload and to load other config files
 */

define ( 'APPLICATION_PATH', dirname(__FILE__)."/../" );
define ( 'CONFIG_PATH',      APPLICATION_PATH . "config/" );
define ( 'GEN_CONFIG_PATH',  CONFIG_PATH . "config.php" );
define ( 'LOG_PATH',         APPLICATION_PATH . "logs/" );
define ( 'MODEL_PATH',       APPLICATION_PATH . "lib/" );
define ( 'GETID3_PATH',      APPLICATION_PATH . "vendor/james-heinrich/getid3/getid3/" );
define ( 'MODULE_PATH',      APPLICATION_PATH . "modules/" );
define ( 'TEXT_FILE_PATH',   APPLICATION_PATH . "txt/" );
define ( 'CONTROLLER_PATH',  "/controllers/" );
define ( 'LANG_PATH',        "/language/" );
define ( 'SCRIPT_PATH',      "/scripts/" );
define ( 'MEDIA_PATH',       APPLICATION_PATH . "media/" );

// URLS
define ( 'APPLICATION_URL',   "/" );
define ( 'LIB_URL',           APPLICATION_URL . "lib/" );
define ( 'MODULE_URL',        APPLICATION_URL . "modules/" );
define ( 'COMMON_JS_URL',     MODULE_URL . "common/js/" );
define ( 'COMMON_CSS_URL',    MODULE_URL . "common/css/" );
define ( 'COMMON_IMAGES_URL', MODULE_URL . "common/images/" );
define ( 'SCRIPT_URL',        LIB_URL . "scripts/" );
define ( 'ADMIN_URL',         APPLICATION_URL . "admin/" );
define ( 'ADMIN_INDEX',       ADMIN_URL );
define ( 'ADMIN_LOGIN',       ADMIN_URL . "login/" );
define ( 'MEDIA_URL',         APPLICATION_URL . "media/" );

// TEMPLATE
define ( 'TEMPLATE_PATH',  "template/" );
define ( 'DEFAULT_TEMPLATE', "default" );
define ( 'DEFAULT_HEAD',  "front" );
define ( 'DEFAULT_MODULE',  "common" );
define ( 'PROFILE_MODULE',  "profile" );
define ( 'DEFAULT_PAGE',  "default" );
define ( 'DEFAULT_TEMPLATE_PATH', TEMPLATE_PATH . "front/default/" );
define ( "CURRENT_TEMPLATE",'default');

define ( "BLACKLIST_WORDS","" ); //words which will be omitted as username
define ( "LOGGING",true ); //Keep true if every action performed needs to be recorded otherwise can be kept true;;
define ( "EMAIL_VERIFICATION",true); //Keep this value true is email verification if needed;;
define ( "LOCALE","ru_RU.utf8" );//Locale sets number format, currency handling etc. of the Application;;
define ( "DATE_TIMES_FORMAT","D, M d, Y H:i" );//Format of the Date D=>day, M=>full month name,m=>month in short form, d=>date, Y=>year in 4 digit, y=>year in 2 digits, H=> hour in 24 hour pattern, h=>hour in 12 hour pattern, i=>minute, s=>second;;
define ( "TIME_FORMAT","H:i" );//Format of the Time H=> hour in 24 hour pattern, h=>hour in 12 hour pattern, i=>minute, s=>second;;
define ( "DATE_FORMAT","M d, Y" );//Format of the Date D=>day, M=>full month name,m=>month in short form, d=>date, Y=>year in 4 digit, y=>year in 2 digits;;
define ( "DEFAULT_LANGUAGE","en" );//Sets time and date of application;;

define ('SCAN_UPD', false);   // log every scanned file and folder?
define ('SCAN_TAG', false);  // log every scanned tag?

define ('PROHIBITED_EXTENSIONS', "php, cgi, htaccess, ini, js");

define ('MEMCACHE_HOST', 'localhost');  //comment this line, if you do not want to use MEMCACHE
define ('MEMCACHE_PORT', 11211);
define ('MEMCACHE_TTL', 60*30);  //TTL in seconds. Currently only used for Dashboard page

define ('ZIP_LIMIT',4294967296);
define ('ZIP_LIMIT_TEXT','4GB');

//constants
define('ACTIVE_VALUE',  1);
define('INACTIVE_VALUE',2);
define('APPROVED',      3);
define('TRASHED',       5);

// THUMBNAILS
define ('THUMBNAIL_DIR', 'thumbs/');
define ('THUMBNAIL_SUFFIX', 'thumb_');
define ('DISPLAY_SUFFIX',   'display_');

//HTML CONFIG
$_ENV['meta']['charset']="utf-8";
$_ENV['meta']['http-equiv']['Expires']="never";

require_once 'dbConfig.php';

$link = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("DB Error " . mysqli_error($link));
mysqli_query($link,"SET NAMES utf8");
$query ="SELECT * FROM ".TBL_SETTINGS;
$result = mysqli_query($link, $query) or die("Error " . mysqli_error($link));
if($result){
    while($a = mysqli_fetch_array($result)){
        define( $a['name'],$a['value']);
    }
}
mysqli_close($link);

ob_start ();

if (! empty ( $_SESSION ['locale'] ))
    setlocale ( LC_ALL, $_SESSION ['locale'] );
else
    setlocale ( LC_ALL, LOCALE );

$locale_info = localeconv ();
ini_set('default_charset',           'UTF-8' );
ini_set('mbstring.internal_encoding','UTF-8');
ini_set('mbstring.func_overload',     7);
header('Content-Type: text/html; charset=UTF-8');

// error settings
error_reporting ( E_ALL & ~ E_WARNING & ~ E_NOTICE );
ini_set ( 'display_errors', 0 );
define ( "SHOW_DEBUG", false );

// Autoload
function loadModel($class) {
    $path = MODEL_PATH . $class . '.php';
    if ($class == 'getid3')
        $path = GETID3_PATH . 'getid3.php';
    if ($class == 'getid3_writetags')
        $path = GETID3_PATH . 'write.php';

    if (file_exists ( $path ))
        @require_once $path;
    else
        loadController ( $class );
}

function loadController($class) {
    $path = MODULE_PATH . $class. CONTROLLER_PATH . $class . '.php';
    if (file_exists ( $path )) {
        @include_once $path;
    } else {
        $general = new general ();
        $general->log("ERROR", "Class $class not found!");
    }
}

spl_autoload_register ( 'loadModel' );
$general = new general ();