to run via Docker:
-------
```
cd docker
docker build -t slavik-dock .
docker-compose up
```

then log in to phpMyAdmin:  
http://localhost:8765/

default creds: root / secret

create DB 'devprop'

create tables
