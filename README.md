# Audio Content Management System

See [docs/newDB.sql](docs/newDB.sql) for SQL to create new DB 

See [docs/nginx_vhost.conf](docs/nginx_vhost.conf) for example of nginx config

See [docs/.htaccess](docs/.htaccess) for example of Apache config

### CI system

`master` branch is protected. You can't push to it. Only via MR (Merge Request).

Whenever MR to `master` merged - it automatically deployed to production (Propovednik)

Usage of **forks** is not recommended.

Use **branches**:
- push branch
- it will be published to `/var/www/BRANCH.propovednik.com/web`
- it will be accessible via `http://BRANCH.propovednik.com` **if** there is vhost created for that name.
You can add / update vhosts [here](https://dev.slavikf.com/system/deb96-ovh)
- create MR to `master`. When it merged - it will be published to `Propovednik.com`

#### Composer dependency: getID3 lib

`getID3` managed by composer.  
To install Composer on Debian / Ubuntu:
```
curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer
```

For first use:
```
composer install
```

To update:
```
composer update
```
