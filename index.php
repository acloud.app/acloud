<?php
// start session
session_start ();
if(!empty($_COOKIE ['user'] ['userId'])) {
	if(!empty($_POST['keep_logged_in'])){
		$_SESSION ['user'] ['sessionId'] = $_COOKIE ['user'] ['sessionId'];
		$_SESSION ['user'] ['userId'] = $_COOKIE ['user'] ['userId'];
		$_SESSION ['user'] ['name'] = $_COOKIE ['user'] ['name'];
		$_SESSION ['user'] ['email'] =  $_COOKIE ['user'] ['email'];
		$_SESSION ['user'] ['groupId'] =  $_COOKIE ['user'] ['groupId'];
		$_SESSION ['user'] ['lastLogin'] =  $_COOKIE ['user'] ['lastLogin'];
		$_SESSION ['user'] ['lastLoginRaw'] =  $_COOKIE ['user'] ['lastLoginRaw'];
		$_SESSION ['user'] ['logins'] = $_COOKIE ['user'] ['logins'];
		$_SESSION ['user'] ['baseFolder'] = $_COOKIE ['user'] ['baseFolder'];
	}
}
// include config
require_once 'config/config.php';

// create template object
$html = new html (CURRENT_TEMPLATE);

// render view
$html->load();